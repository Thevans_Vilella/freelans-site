<div class="boxfull">
	<div class="titfxtop">Conheça nossos: <span class="titfxtop-sub">Profissionais em destaque</span></div>

	
	
		<div class="boxfull cinza">
			<div class="blg">

					<?php require_once("banner.php"); ?>
					<?php require_once("busca-avancada.php"); ?>




					<div class="bl-dest-bloco">
						<?php 
						$sql		=	"select * from perfil
										INNER JOIN planos		  	on		planos.pla_codigo			=	perfil.pla_codigo
										INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
										INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
										WHERE 
										per_tip_codigo = 2
										AND 
										per_sta_codigo = 1
										AND 
										planos.pla_codigo != 4
										ORDER BY 
										pla_ordem, ava_media_estrelas DESC, perf_nome";
						$res		=	mysqli_query($cn, $sql);
						// ============================ TRABALHANDO PARA INCREMENTAR PAGINACAO ========================
						if (isset($_GET['CodigAbriR'])) {

							$fl= $_GET['CodigAbriR']; 

						} else {

							$fl = 1; 

						}  


						$numtotal = mysqli_num_rows($res);  
						$total_registro = 24;  // QUANTOS REGISTROS VEM POR VEZ...

						if ($numtotal <= $total_registro){ 
						$total_paginas = 1;  
						}  
						else { 
						$total_paginas = ceil($numtotal/$total_registro); 
						} 

						$linha_inicial = ($fl - 1) * $total_registro;  
						$linha_final = $linha_inicial + $total_registro - 1; 

						$ponteiro = 0;  
						$i = "1";  
						// ===================================================================================================	
						while($lin	=	mysqli_fetch_array($res))  {
						// ==================== Pagina&ccedil;&atilde;o =======================================================
						if ($ponteiro >= $linha_inicial and $ponteiro <= $linha_final) { 
						// ====================================================================================================

							$perfavacod	=	$lin['perf_codigo'];


							$sqlavaluser	=	"select * from avaliacoes_usuario WHERE perf_cod_ref = '$perfavacod'";
							$resavaluser	=	mysqli_query($cn, $sqlavaluser);
							$cntavaluser	=	mysqli_num_rows($resavaluser);
							$linavaluser	=	mysqli_fetch_array($resavaluser);	


							$sqlaval	=	"select * from avaliacoes WHERE perf_codigo = '$perfavacod'";
							$resaval	=	mysqli_query($cn, $sqlaval);
							$linaval	=	mysqli_fetch_array($resaval);
						?>
						<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
							<div class="boxit">
								<a href="perfil/<?php echo $lin['perf_codigo']; ?>/<?php echo CorrigirNome($lin['perf_nome']); ?>" target="_blank">
									<div class="boxit-tipo"><?php echo $lin['pla_nome']; ?></div>
									<div class="boxit-fot">
										<img src="painel/<?php echo $lin['perf_fotop']; ?>" alt="Favoritos"/>
									</div>
									<div class="boxit-tit"><?php echo $lin['perf_nome']; ?></div>
									<div class="boxit-esp"><?php echo $lin['cat_nome']; ?></div>
									<div class="boxit-estrela">
										<?php 
											for($contador = 1; $contador <= 5; $contador++) {

													if($contador <= $linaval['ava_media_estrelas']){

														echo "<i class='fas fa-star estyellow'></i>";

													} else {

														echo "<i class='fas fa-star'></i>";

													}

											}

											if($cntavaluser > 0){ 
												$cntavaluser	=	"(".$cntavaluser.")"; 
											} else {
												$cntavaluser	=	""; 
											}

										?>
									</div>
									<div class="boxit-nota"><?php echo $cntavaluser; ?></div>
								</a>	
							</div>
						</div>
						<?php 
							// Pagina&ccedil;&atilde;o
							$i = $i + 1; 
							} // fecha IF 
							$ponteiro = $ponteiro + 1; 

						} // Fecha Whille 

						?>

						<div class="paginacao">
						<?php                 
						//Pagina&ccedil;&atilde;o: Nesse bloco montamos a pagina&ccedil;&atilde;o 
						// se estivermos na primeira p&aacute;gina, o link "anterior" n&atilde;o precisa ter link  
						if ($fl == 1){  
						?>
						<?php 
						}  
						// Caso contr&aacute;rio, ter&aacute; um link paraa p&aacute;gina anterior  
						else{  
						?>

						<span class="nextprev">
							<a href='destaque/<?= $fl - 1; ?>'>
								<i class="fas fa-angle-left"></i>
							</a>
						</span>


						<?php 
						}  

						// gerando os n&uacute;meros com os respectivos links  
						$i = 1;  
						while ($i <= $total_paginas) {  


							// Aqui limitamos o número de apenas 5 quadrados para exibir
							// Ou seja, o quadrado atual mais 5

							$contpag	=	5; // Quantos quadrados de páginas quer

							if($i >= $fl and $i <= $fl + $contpag) {  

									// A p&aacute;gina atual n&atilde;o precisa de link 
									if ($i == $fl) {  
									?>
									<span class="numeracao"><?=$i;?></span>
									<?php 
									}  
									// As outras p&aacute;ginas deve ter os links  
									else {  
									?>
									<span class="numeracao_link">
										<a href='destaque/<?=$i;?>'>
											<?=$i;?>
										</a>
									</span>

							<?php } // Termina aqui IF que limita número de quadrados =====================?>

						<?php 
							} // Aqui termina while da geração de quadrados referente a número de páginas
						?>


						<?php

						// Incrementa o n&uacute;mero da p&aacute;gina 
						$i = $i + 1;  
						}  
						// Quando estiver na &uacute;ltima p&aacute;gina, removemos o link do texto "pr&oacute;ximo"               
						if ($fl == $total_paginas) {  
						?>
						<?php
						}  
						// Caso contr&aacute;rio, devemos linkar a palavra pr&oacute;xima para p&aacute;gina seguinte $pg + 1 
						else {  
						?>

						<span class="nextprev">
							<a href='destaque/<?= $fl + 1; ?>'>
								<i class="fas fa-angle-right"></i>
							</a>
						</span>

						<?php 
						}             
						// Aqui fecha a P&aacute;gina&ccedil;&atilde;o ============================================================================================================
						?>
						</div>				
					</div>

			
			
			</div>
			<div class="vazio"></div>
		</div>
</div>