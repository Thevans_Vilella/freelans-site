<?php
if(!isset($_GET['CodigAbriR'])) { 
	
	require_once("erro.php"); 
	exit(); 
}

$type	=	(int)$_GET['CodigAbriR'];
?>
<main id="register-customer">
    <div class="d-flex flex-wrap container-fluid no-padding-left-right">
        <div id="register-background" style="background-image: linear-gradient(rgba(1,70,116,0.2), rgba(1,70,116,0.2)), url(assets/_img/helpers/img-customer-profile.png);" 
             class="col-md-6 d-none d-md-block no-padding-left-right">
            <a class="link-logo" href=""><img class="logo register-logo" src="assets/_img/icons/freelans-logo.png" alt="Freelans"></a>
        </div>
        <div class="col-md-6 col-12 no-padding-left-right">

            <div class="return-container col-12">
                <a href="<?= $lin11['tag_url'] ?>" class="return-button">
                    <img class="return-image" src="assets/_img/icons/chevron-left.svg" alt="Voltar"> Voltar
                </a>
            </div>
            <div class="half-container">
                <h1 class="register-title">Criar minha conta</h1>
                <form name="cadastro" method="post" action="perfil-inserir.php" onSubmit="return validarSenha()">
                    <div class="form-group">

						<label class="register-label" for="name"><span style="color: #F79F0E; text-shadow: 0 0 1px #F79F0E;">Selecione seu Perfil:</span></label><br/><br/>
                        <label class="radio-container radio-active">Perfil Cliente
               <input id="radio-profile-customer" class="radio-profile" type="radio" name="tipo" value="1" <?php if(isset($type) and ($type == 0)) {echo "checked";} ?>>
                            <span class="checkmark"></span>
                        </label>

                        <label class="radio-container">Perfil Profissional
            <input id="radio-profile-professional" class="radio-profile" type="radio" name="tipo" value="2" <?php if(isset($type) and ($type > 0)) {echo "checked";} ?>>
				
                            <span class="checkmark"></span>
                        </label>

                    </div>
					
                    <div class="form-group">
                        <label class="register-label" for="name">Nome</label>
                        <input type="text" class="form-control register-input" id="name" name="name" placeholder="Nome pessoal ou da empresa" required>
                    </div>
                    <div class="form-group professional-input">
                        <label class="register-label" for="profession">Cidade de Atuação</label>
						<select name="city" class="select" id="city" required>
							<!--
							<option value="">Selecione uma Cidade</option>
							<option value="">----------------------------</option>
							-->
							<?php
							$sql	=	"select * from cidades
										INNER JOIN estado	  	on		estado.est_codigo		=	cidades.est_codigo
										order by est_nome, cid_nome";
							$res	=	mysqli_query($cn, $sql);
							while($lin	=	mysqli_fetch_array($res))  {
							?>
							<option value="<?php echo $lin['cid_codigo']; ?>"><?php echo $lin['cid_nome']; ?>/ <?php echo $lin['est_sigla']; ?></option>
							<?php } ?>
						</select>
                    </div>
                    <div class="form-group professional-input">
                        <label class="register-label" for="profession">Categoria da Profissão</label>
						<select name="category" class="select" id="category" required>
							<!--
							<option value="">Selecione uma Categoria</option>
							<option value="">----------------------------</option>
							-->
							<?php
							$sql01	=	"select * from categorias order by cat_ordem";
							$res01	=	mysqli_query($cn, $sql01);
							while($lin01	=	mysqli_fetch_array($res01))  {
							?>
							<option value="<?php echo $lin01['cat_codigo']; ?>"><?php echo $lin01['cat_nome']; ?></option>
							<?php } ?>
						</select>
                    </div>
                    <div class="form-group professional-input">
                        <label class="register-label" for="profession">Plano</label>
						<select name="plans" class="select" id="plans" required>
							<!--
							<option value="">Selecione um Plano</option>
							<option value="">----------------------------</option>
							-->
							<?php
							$sql02	=	"select * from planos order by pla_ordem";
							$res02	=	mysqli_query($cn, $sql02);
							while($lin02	=	mysqli_fetch_array($res02))  {
							?>
							<option value="<?php echo $lin02['pla_codigo']; ?>">
								<?php echo $lin02['pla_nome']; ?> - R$<?php echo CorrigirDinheiro($lin02['pla_valor']); ?>/<?php echo $lin02['pla_titulo']; ?>
							</option>
							<?php } ?>
						</select>
                    </div>
					
                    <div class="form-group">
                        <label class="register-label" for="email">Telefone Fixo</label>
						<input type="tel" class="form-control register-input" id="fone" name="fone" placeholder="(11)1111-1111" onkeyup="maskIt(this,event,'(##) ####-#####')" maxlength="14">
                    </div>
                    <div class="form-group">
                        <label class="register-label" for="email">Celular</label>
						<input type="tel" class="form-control register-input" id="celular" name="celular" placeholder="(99)99999-9999" onkeyup="maskIt(this,event,'(##) ####-#####')" maxlength="15" required>
                    </div>
                    <div class="form-group">
                        <label class="register-label" for="email">E-mail</label>
						<input type="email" class="form-control register-input" id="email" name="email" placeholder="Ex: nome@email.com.br">
                    </div>
					<!--
                    <div class="form-group professional-input">
                        <div id="expertise">
                            <p data-toggle="modal" data-target="#modal-occupations" data-backdrop="static" data-keyboard="false">Adicionar áreas de atuação <span class="plus-circle"></span></p>
                            <div class="chip-list"></div> 
								<textarea class="msgcont" name="occupations" id="occupations" cols="1" rows="5"></textarea>
                        </div>
                    </div>
					-->
                    <div class="form-group">
                        <label class="register-label" for="password">Senha</label>
                        <input type="password" class="form-control register-input" id="password" name="password" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" required>
                    </div>
                    <div class="form-group">
                        <label class="register-label" for="confirm-password">Confirme sua senha</label>
                        <input type="password" class="form-control register-input" id="passwordconf" name="passwordconf" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" required>
                    </div>
                    <div class="form-group">
                        <label class="accept-policy-label checkbox-container">Declaro que li e aceitei os <a data-toggle="modal" data-target="#modal-termo" data-backdrop="static" data-keyboard="false">termos de uso</a> e a <a data-toggle="modal" data-target="#modal-politica" data-backdrop="static" data-keyboard="false">política de privacidade</a>.
                            <input type="checkbox" name="politica" checked="checked" value="1" required>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="form-group button-register-container">
						<!--<input type="submit" class="facebook-register" value="Cadastrar com Facebook" />-->
						<input type="submit" class="register-button" value="Criar Conta" />
                    </div>
                </form>

            </div>

            <?php require_once("contact-shortcut.php"); ?>
        
        </div>
    </div>
</main>

<?php
require_once("termo.php");
require_once("politica.php");
?>
<div class="modal fade" id="modal-occupations" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Adicione até 5 atividades</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="chip-list"></div>
				<textarea class="msgcont" name="occupations" id="occupations" cols="1" rows="5"></textarea>
            </div>
            <div class="modal-body">
                <input type="text" name="input_occupations" id="input_occupations" placeholder="Digite uma atividade por vez">
                <ul id="occupation-list"></ul>
            </div>
            <div class="modal-footer">
                <button id="add-occupation" data-dismiss="modal" type="button" class="btn btn-primary">Adicionar</button>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript">
	
	
			
			var getvalor = "<?php print $type; ?>";
			
            if(getvalor >= 1) {
                $('.professional-input').css("display", "block");
			} else {
                $('.professional-input').css("display", "none");
            }
	
	
	
		window.onload = function(){
		var divconteudo = document.getElementsByClassName("chip-list")[0].textContent;
		document.getElementById("occupations").value = divconteudo;
    };

	
	
	$(function(){
		var valorDaDiv = $(".chip-list").text();    
		$("#occupations").val(valorDaDiv);

	});
	
	
	

	
    var background = ["img-customer-profile.png", "img-professional-profile.png"];

    $('.radio-profile').each(function(index) {
            
        $(this).on('click', function() {
                
            $('.radio-profile').each(function(index) {
                if(!$(this).prop("checked")) {
                    
                    $(this).parent().removeClass("radio-active");
                
                } else {

                    $('#register-background').css('background-image', 'linear-gradient(rgba(1,70,116,0.2), rgba(1,70,116,0.2)), url(assets/_img/helpers/' + background[index] + ')');
                    $(this).parent().addClass("radio-active");
                }
            });

            if($(this).attr('id') == 'radio-profile-professional')
                $('.professional-input').css("display", "block");
            else {
                $('.professional-input').css("display", "none");
            }
        });

    });

    var search_occ = $("input#input_occupations");
    var timeout = null;
    
    search_occ.on("keyup", function() {
       
        let valor = $(this).val(); 
        var list = $('#occupation-list');
        clearTimeout(timeout);
        
        timeout = setTimeout(function() {
            if(valor.length > 0) {
                $.ajax({
                    type: 'POST',
                    url: 'ajax_occupations.php',
                    dataType: 'json',
                    data: {termo: valor}
                }).done(function(data) {
                    list.css("display", "flex");
                    list.html("");

                    if(data.length > 0) {
                        
                        data.forEach(function(value, key) {
                            list.append(`<li class="col-12 list-category">
                                             <a id="${value.id}" class="occupation-item" href="javascript:void(0);">${value.ocupacao}</a>
                                         </li>`);    
                        });

                    } else {
                        
                        list.append("<p>Nenhum termo encontrado</p>");

                    }
                    
                }).fail(function() {
                    console.log("Falha na requisição");
                });

            } else {
                list.css("display", "none");
            }
        }, 700);
        
    });

    $(document).ready(function() {
        var chipObject = {};
        
        $(document).on("click", "a.occupation-item" , function() {
            if(Object.keys(chipObject).length < 5) {
                let index = $(this).attr("id");
                    
                if(Object.keys(chipObject).indexOf(index) != -1) {
                    alert("Você já selecionou essa ocupação");
                
                } else {

                    let chip = $(this).html();
                    chipObject[parseInt(index)] = chip;
                    
                    $('.chip-list').html("");
                    for(const key in chipObject) {
                        $('.chip-list').append(`<span data-id="${key}" class="chip">${chipObject[key]}</span>`);
                    }

                }
                
            } else {
                alert("São permitidas até 5 ocupações");
            }
        });

        $(document).on("click", "span.chip" , function() {
            let resposta = confirm(`Deseja remover a ocupação "${$(this).html()}"`);

            if(resposta) {
                let chip = $(this).attr("data-id");
                delete chipObject[parseInt(chip)];
                $("[data-id=" + chip + "]").remove();
            }
            
        });

    });

</script>
