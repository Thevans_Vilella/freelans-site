<?php
$sql11		=	"SELECT * from textos WHERE txt_codigo = '11'";
$res11		=	mysqli_query($cn, $sql11);
$lin11		=	mysqli_fetch_array($res11);
?>
<section id="professional_profile">
    <div class="container-fluid no-padding-left-right d-flex flex-wrap">
        <div class="col-md-6 col-12 no-padding-left-right">
            <div class="professional_profile_container float-right half-container">
                <h1 class="profile_title">Perfil Profissional</h1>
                <h2 class="profile_subtitle underscore_effect">Divulgue melhor o seu trabalho</h2>
                <p class="profile_text"><?php echo $lin11['txt_descricao']; ?></p>
                <a class="register_button" href="register-account/1">Fazer meu cadastro</a>
            </div>
        </div>
        <div class="col-md-6 d-md-block d-none no-padding-left-right">
            <img class="profile_background" src="assets/_img/helpers/img-professional-profile.png" alt="">
        </div>
    </div>
</section>