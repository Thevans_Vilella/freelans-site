<nav id="side-menu">
	
	<?php if(isset($_SESSION["login_ses"]) and ($_SESSION["status_ses"] == "1")) { ?>
		<?php 

			$user	=	$_SESSION["login_ses"];
			$codpf	=	$_SESSION["cod_ses"];

			$sql	=	"select * from perfil where perf_login = '$user'";
			$res	=	mysqli_query($cn, $sql);
			$lin	=	mysqli_fetch_array($res);	
	
				if ($lin['perf_fotop'] == "") {
					
					$fot	=	"assets/_img/fotperfil.png";
					
				} else {
					
					$fot	=	"painel/".$lin['perf_fotop'];
					
				}
	
	
				if ($_SESSION["tipo_ses"] == "1") {
					
					$perf	=	"perfil-edit-cliente";
					$past	=	"perfil-edit-pass";
					
				} else {
					
					$perf		=	"perfil-edit-profissional";
					$past		=	"perfil-edit-pass-profissional";
					$visaperf	=	"perfil/".$lin['perf_codigo']."/".CorrigirNome($lin['perf_nome']);

				}
	
		?>
		<div class="profile-container">
			<?php if ($lin['perf_fotop'] == "") { ?>
			<div class="photo-container"><i class="fas fa-user user-nofoto"></i></div>
			<?php } else { ?>
			<div class="photo-container" style="background-image: url(<?php echo $fot; ?>)"></div>
			<?php } ?>
			<div class="name-container">
				<h1 class="profile-name"><?php echo $lin['perf_nome']; ?></h1>
				<div class="profile-logout"><a href="sair.php">Sair</a></div>
			</div>
		</div>
	<?php } ?>
	
	
	<!--
	<div class="filter-container">
		<a class="filter-button" href="javascript:void(0)" data-toggle="modal" data-target="#modal-search">filtrar <img src="assets/_img/icons/filter-variant.svg" alt=""></a>
	</div>
	-->



	<nav>
		<div class="menu">
			<ul> 
				<?php  if(!isset($_SESSION["login_ses"])) { ?>
				<li><a href="login"><i class="fas fa-user"></i>Login</a></li>
				<?php } ?>
				
				<?php  if(isset($_SESSION["login_ses"])) { ?>
				<li><a href="ofertas"><i class="fas fa-money-check-alt"></i>Oportunidades de Serviços</a></li>	
				<?php } ?>
					
				<li><a href="profissionais"><i class="fas fa-home"></i>Início</a></li>
				<li><a href="promocoes"><i class="fas fa-piggy-bank"></i>Promoções</a></li>
				
				<?php  if(isset($_SESSION["login_ses"])) { ?>
				<li><a href="favoritos"><i class="fas fa-heart estred"></i>Meus Favoritos</a></li>				
				<li><a href="<?php echo $perf; ?>"><i class="fas fa-user-edit"></i>Editar Meu Perfil</a></li>	
				<li><a href="<?php echo $past; ?>"><i class="fas fa-user-lock"></i>Editar Minha Senha</a></li>	
				<li><a href="perfil-excluir-confirma"><i class="fas fa-user-slash estred"></i>Excluir Meu Perfil</a></li>
				
				<?php  if(isset($_SESSION["login_ses"]) and ($_SESSION["tipo_ses"] == "2")) { ?>
				<li><a href="perfil-promocao-edit-profissional"><i class="fas fa-money-check-alt"></i>Minhas Promoções</a></li>	
				<li><a href="<?php echo $visaperf; ?>"><i class="fas fa-user"></i>Visualizar Meu Perfil</a></li>
				<?php } ?>
					
				<?php } ?>	

                    
                <li><a href="blog/"><i class="fab fa-wordpress iconcinza margicon"></i>Blog</a></li>
				<!--
				<li><a href="#"><i class="fas fa-envelope"></i>Mensagens</a></li>
				<li><a href="#"><i class="fas fa-money-check-alt"></i>Orçamentos</a></li>
				-->

				<!--
				<li><a href="#">Sub-menu</a>
					<ul>
						<li><a href="#">Sub Item 1</a>
							<ul class="sub">
								<li><a href="#">Sub Sub Item 1</a></li>
								<li><a href="#">Sub Sub Item 2</a></li>
								<li><a href="#">Sub Sub Item 3</a></li>
								<li><a href="#">Sub Sub Item 4</a></li>
							</ul>
						</li>
						<li><a href="#">Sub Item 2</a></li>
						<li><a href="#">Sub Item 3</a></li>
						<li><a href="#">Sub Item 4</a></li>
					</ul>
				</li>
				-->

				<!-- 
				<li>
					<div class="socialmedia">
						<ul>
							<li><a href="#" class="fa fa-facebook"></a></li>
							<li><a href="#" class="fa fa-twitter"></a></li>
							<li><a href="#" class="fa fa-linkedin"></a></li>
							<li><a href="#" class="fa fa fa-youtube-play"></a></li>
						</ul>
					</div>/*socialmedia*/
				</li>
				-->
			</ul>
		</div><!--/menu-->
	</nav>

	<!-- 
	<div id="profile-switch">
		<label id="switch">
			<input id="switch-button" type="checkbox" checked="checked">
			<span class="slider round"></span>
		</label>
		<label id="switch-label">perfil profissional</label>
	</div>
	-->
</nav>
