<?php 
session_start();

require_once("painel/conexao.php");
require_once("painel/classes.php");


// ======================================================================================================================================
// Verifica se está logado e se veio valor no $_GET.
// Se não tiver volta para a página inicial.
if(!isset($_SESSION["login_ses"]) or (!isset($_GET['CodigAbriR'])) or ($_GET['CodigAbriR'] <= "0")) { 
    
    require_once "alerta.php"; 
    exit(); 

}
// ======================================================================================================================================



$sql11 = "SELECT * FROM metas_google";
$res11 = mysqli_query($cn, $sql11);
$lin11 = mysqli_fetch_array($res11);

$urlproj = $lin11['tag_url'];

$vgoft	    =	$_GET['CodigAbriR'];

$sqluser	=	"select * from ofertas_trabalho
                INNER JOIN categorias	  	            on		categorias.cat_codigo		                    =	ofertas_trabalho.cat_codigo
                INNER JOIN cidades	  	                on		cidades.cid_codigo		                        =	ofertas_trabalho.cid_codigo
                INNER JOIN estado	  	                on		estado.est_codigo		                        =	cidades.est_codigo
                INNER JOIN ofertas_trabalho_status	  	on		ofertas_trabalho_status.ofe_tra_stat_codigo		=	ofertas_trabalho.ofe_tra_stat_codigo
                WHERE
                ofe_tra_codigo = $vgoft
                AND
                ofertas_trabalho_status.ofe_tra_stat_codigo = 1
                ";
$resuser	=	mysqli_query($cn, $sqluser);
$linuser	=	mysqli_fetch_array($resuser);

$staoft     =   $linuser['ofe_tra_stat_codigo'];
$catoft     =   $linuser['cat_codigo'];



$user	=	$_SESSION["login_ses"];

$sql	=	"select * from perfil where perf_login = '$user'";
$res	=	mysqli_query($cn, $sql);
$lin	=	mysqli_fetch_array($res);	

$perfcod	=	$lin['perf_codigo'];
$perfcat	=	$lin['cat_codigo'];



$retorno    =   $urlproj."/profissionais";
?>

<?php if(
            isset($_SESSION["login_ses"])
             and ($_SESSION["status_ses"] == "1")
             and ($_SESSION["tipo_ses"] == "2")
             and ($staoft == "1")
             //and ($catoft == $perfcat) 
             and ($_SESSION["cod_ses"] == $perfcod)
        ) 
    {
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<base href="<?= $lin11['tag_url']; ?>"/>
<title>Freelans</title>
    
<link rel="stylesheet" type="text/css" href="assets/_css/estilo-oferta.css" media="screen" /> 
<link rel="stylesheet" type="text/css" media="screen" href="assets/_css/estilo-fontes.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css">
    
<script src="https://kit.fontawesome.com/967572049d.js"></script>

</head>
<body>
    
    <div class="container-oferta">
        <div class="tit-oferta"><?php echo $linuser['ofe_tra_nome']; ?></div>
        <div class="cid-oferta"><?php echo $linuser['cid_nome']; ?>/<?php echo $linuser['est_sigla']; ?></div>
        <div class="desc-oferta"><?php echo $linuser['ofe_tra_descricao']; ?></div>
        
        <form name="cadastro" class="ok-oferta" method="post" action="oferta-profissional.php">
            <button><i class="fas fa-check okoft"></i> Me Candidatar</button>
            
            <input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
            <input type="hidden" name="ofet" size="5" value="<?php echo $vgoft; ?>" />
        </form>	
        
        <button class="btn btn-primary" onclick="javascript:parent.jQuery.fancybox.close();">Dismiss</button>
    </div>
    
    <!--<div class="fechar"><a onclick="javascript:parent.jQuery.fancybox.close();"><i class="fas fa-times close"></i></a></div>-->
    
</body>
</html>
<?php } else { header("Location: $retorno"); } ?>