<?php
$sqlcat_verif_perf		=	"SELECT * FROM promocao 
							INNER JOIN perfil	  		on		perfil.perf_codigo			=	promocao.perf_codigo
							INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
							INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	promocao.atua_codigo
							INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
							WHERE 
							per_tip_codigo = 2
							AND 
							per_sta_codigo = 1
							";
$rescat_verif_perf		=	mysqli_query($cn, $sqlcat_verif_perf);
$contacat_verif_perf	=	mysqli_num_rows($rescat_verif_perf);
$lincat_verif_perf		=	mysqli_fetch_array($rescat_verif_perf);

	if($contacat_verif_perf == 0) {
		
		$class	=	"";
		
	} else {
		
		$class	=	"black";

	}

?>

<div class="boxfull">
	<div class="titfxtop">Confira: <span class="titfxtop-sub">Promoções</span></div>

		<div class="boxfull <?php echo $class; ?>">
			<div class="blg">
				
				
				<?php if($contacat_verif_perf == 0) { // Se não tiver promoção faz isso ?>
				<div class="bl ">
					<div class="titfxdest-meio">Não existem promoções cadastradas neste momento.</div>
					<div class="subtititem-meio"></div>
				</div>
				
				<?php }  else { // Se tiver promoção faz abaixo. ?>
				
				
				<div class="blpromo">
					<div id="itemContainer">


						<?php
						$sql01			=	"select * from promocao 
											INNER JOIN perfil	  		on		perfil.perf_codigo			=	promocao.perf_codigo
											INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
											INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	promocao.atua_codigo
											INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
											WHERE 
											per_tip_codigo = 2
											AND 
											per_sta_codigo = 1
											ORDER BY 
											pro_desconto DESC, ava_media_estrelas DESC";
						$res01			=	mysqli_query($cn, $sql01);
						while($lin01	=	mysqli_fetch_array($res01))  {
							
							$perfcod	=	$lin01['perf_codigo'];
							
							
								$sqlfav	=	"select * from favoritos where perf_codigo = '$perfcod'";
								$resfav	=	mysqli_query($cn, $sqlfav);
								$cntfav	=	mysqli_num_rows($resfav);
								$linfav	=	mysqli_fetch_array($resfav);	

								if($cntfav >0){

									$favorito = " estred";

								} else {

									$favorito = " iconbrc";	
								}
					
					
									$sqlavaluser	=	"select * from avaliacoes_usuario WHERE perf_cod_ref = '$perfcod'";
									$resavaluser	=	mysqli_query($cn, $sqlavaluser);
									$cntavaluser	=	mysqli_num_rows($resavaluser);
									$linavaluser	=	mysqli_fetch_array($resavaluser);	


									$sqlaval	=	"select * from avaliacoes WHERE perf_codigo = '$perfcod'";
									$resaval	=	mysqli_query($cn, $sqlaval);
									$linaval	=	mysqli_fetch_array($resaval);	
						?>
						<div class="grdGRL-4 grdDSK-6 grdSMPr-12">
							<div class="cx-promocao">
								<div class="promo-fot" style="background-image: url(painel/<?php echo $lin01['pro_fotog']; ?>)">
									<a href="perfil-promocoes/<?php echo $lin01['perf_codigo']; ?>/<?php echo CorrigirNome($lin01['perf_nome']); ?>">
										<div class="promo-mask"></div>
										<!--<div class="boxit-tipo">PREMIUM</div>-->
										<div class="promo-name"><?php echo $lin01['perf_nome']; ?></div>
										<div class="promo-name-serv"><?php echo $lin01['cat_nome']; ?></div>
										<div class="boxit-estrela-promo">
											<?php 
												for($contador = 1; $contador <= 5; $contador++) {

														if($contador <= $linaval['ava_media_estrelas']){

															echo "<i class='fas fa-star estyellow'></i>";

														} else {

															echo "<i class='fas fa-star'></i>";

														}

												}
											?>
										</div>
										<div class="boxit-promo-nota">(<?php echo $cntavaluser; ?>)</div>
										<div class="boxit-promo-love"><i class="fas fa-heart <?php echo $favorito; ?>"></i></div>
									</a>
								</div>
								<div class="promo-desc"><?php echo $lin01['pro_desconto']; ?>% <span class="promo-desconto">DESCONTO</span></div>
								<div class="bx-dad-tit-promo">
									<div class="promo-tit-serv"><?php echo $lin01['atua_nome']; ?></div>
									<div class="promo-desc-serv"><?php echo $lin01['pro_descricao']; ?></div>
								</div>
							</div>
						</div>
						<?php } ?>


						
					</div><!-- paginação -->
					<div class="holder"></div>
				</div><!-- bl -->
				
			<?php } // Termina IF que verifica se tem promoção  ?>
			</div>
		</div>
</div>