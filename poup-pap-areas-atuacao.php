<div id="ex2" class="modal">
	<div class="modform">
		
		<div class="closefor"><a href="#" rel="modal:close"><i class="fas fa-times-circle closeforx"></i></a></div>
		
			<div class="bl-atua-tit">ÁREA DE ATUAÇÃO</div>
			<div class="bl-atua-mais-palavras"><a>Você pode Cadastrar até <?php echo $linuser['pla_qtd_atuacao']; ?> palavras</a></div>
			<div class="bl-atua-linha"></div>

			<?php
			$sql02			=	"select * from atuacao_perfil 
								INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	atuacao_perfil.atua_codigo
								WHERE 
								perf_codigo = $perfcod 
								ORDER BY
								atua_nome";
			$res02			=	mysqli_query($cn, $sql02);
			while($lin02	=	mysqli_fetch_array($res02))  {

			$atuacat	=	$lin02['atua_nome'];


			$sql01		=	"select * from categorias WHERE cat_nome = '$atuacat'";
			$res01		=	mysqli_query($cn, $sql01);
			$conta01	=	mysqli_num_rows($res01);
			$lin01		=	mysqli_fetch_array($res01);
			?>
				<div class="bl-atua-area">&#8226; <?php echo $lin02['atua_nome']; ?></div>
				<?php if ( $atuacat != $perfcat ) { ?>
				<form method="post" action="perfil-edit-profissional-atua.php">
					<input type="submit" class="closex" value="x" />
					<input type="hidden" name="area" size="5" value="<?php echo $lin02['atua_codigo']; ?>" />
					<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
				</form>
				<?php } ?>
			<?php } ?>


			<?php if($contacat_verif_perf < $qtdarea) { ?>
			<form name="cadastro"  class="cadastro" method="post" action="perfil-edit-profissional-area.php">
				<ul>
					<li>
						<div class="dad">
							<br>
							<label>Selecione uma área de atuação existente:</label>
							<!--<select name="name" class="select validate[required]" id="category">-->
							<select name="name" class="bus-area-select validate[required]" placeholder="Digite uma área de atuação. Exemplo: Pintor" id="country-selector" autofocus="autofocus" autocorrect="off" autocomplete="off">
								<option value="">Selecione uma área de atuação</option>
								<option value="">----------------------------</option>
								<?php
								$sql11	=	"select * from atuacao order by atua_nome";
								$res11	=	mysqli_query($cn, $sql11);
								while($lin11	=	mysqli_fetch_array($res11))  {
								?>
									<option value="<?php echo $lin11['atua_nome']; ?>"><?php echo $lin11['atua_nome']; ?></option>
								<?php } ?>
							</select>
						</div>
					</li>
				</ul>

				<input type="submit" class="regdados" value="Cadastrar Área de Atuação" />
				<input type="hidden" name="catuser" size="5" value="<?php echo $catcod; ?>" />
				<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
			</form>
		
            <div class="dad">
                <br>
                <label>Ou cadastre uma nova área de atuação:</label>
            </div>

            <div class="altpoup"><a href="https://api.whatsapp.com/send?phone=5544984090941&text=Ol%C3%A1!%20Gostaria%20de%20cadastrar%20uma%20nova%20%C3%A1rea%20de%20atua%C3%A7%C3%A3o%2C%20poderia%20me%20ajudar%3F" target="_blank">Cadastrar Nova Área</a></div>
			<?php } ?>
		
	</div>
</div>