<?php
session_start();
require_once("painel/conexao.php");
require_once("painel/classes.php");

$pg = isset($_GET['IniSeSsiSit']) ? $_GET['IniSeSsiSit'] : "home";

$pagina = "$pg.php";
if(!file_exists($pagina)){
	$pagina = "erro.php";
}

require_once("padrao.php"); 
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<meta name="keywords" content="<?= $name ?> - <?= Corrigirkeywords($busca_chave) ?>" />
<meta name="description" content="<?= $name ?> - <?= CorrigirDescription($description) ?>" />
<meta name="geo.placename" content="<?= $name ?> - <?= $lin10['log_sigla'] ?> <?= $lin10['cont_endereco'] ?>, <?= $lin10['cont_bairro'] ?>, <?= $lin10['cont_cidade'] ?>/<?= $lin10['est_sigla'] ?>, CEP: <?= $lin10['cont_cep'] ?>"/>
<meta name="geo.region" content="BR-<?= $lin10['est_nome'] ?>"/>
<meta name="author" content="<?= $lin11['tag_autor'] ?>" />
<meta property="og:image" content="<?= $lin11['tag_url'] ?>/assets/_img/logomarca.png"/>
<meta property="og:title" content="<?= $name ?>" />
<meta property="og:phone_number" content="<?= $lin10['cont_telefone'] ?>" />
<meta property="og:street-address" content="<?= $lin10['log_sigla'] ?> <?= $lin10['cont_endereco'] ?>, <?= $lin10['cont_numero'] ?> - <?= $lin10['cont_bairro'] ?>" />
<meta property="og:locality" content="<?= $lin10['cont_cidade'] ?>" />
<meta property="og:region" content="<?= $lin10['est_sigla'] ?>" />
<meta property="og:email" content="<?= $lin10['cont_email'] ?>" />
<meta property="og:type" content="website"/>
<meta property="og:url" content="<?= $lin11['tag_url'] ?>" />
<meta property="og:site_name" content="<?= $name ?>"/>
<meta property="og:description" content="<?= CorrigirDescription($lin11['tag_descricao']) ?>" />
<meta property="busca:title" content="<?= $name ?> - <?php if(isset($pagname)){	echo ($pagname); } ?>" />
<meta property="busca:species" content="<?php if(isset($pagname)){	echo ($pagname); } ?>" />
<meta name="format-detection" content="address=no; email=no; telephone=no" />

<base href="<?= $lin11['tag_url'] ?>"/>

<title><?= $namestop ?> | <?= $titulo ?></title>

<link rel="canonical" href="<?= $lin11['tag_url'] ?>" />
<link rel="icon" href="assets/_img/favicon.png" type="image/x-icon"/>
<link rel="stylesheet" href="assets/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="assets/font-awesome/all.css">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen" href="assets/_css/styles.css" />
<link rel="stylesheet" type="text/css" media="screen" href="assets/_css/nav.css" />
<link href="https://fonts.googleapis.com/css?family=Roboto|Work+Sans:400,600" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen" href="assets/_css/estilo.css" />
<link rel="stylesheet" type="text/css" media="screen" href="assets/_css/estilo-fontes.css" />
<link rel="stylesheet" type="text/css" media="screen" href="assets/select/jquery-ui.css">
<link rel="stylesheet" href="assets/fancybox/jquery.fancybox.min.css">
<script type="application/javascript" src="assets/jquery/jquery-3.4.0.min.js"></script>
<script src="https://kit.fontawesome.com/967572049d.js"></script>
<script src="https://www.google.com/recaptcha/api.js?hl=pt-BR"></script>

<!-- google adsense  -->
<script data-ad-client="ca-pub-4303453127500008" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	
<!-- Global site tag (gtag.js) - Google Analytics -->
<script type="application/javascript" async src="https://www.googletagmanager.com/gtag/js?id=UA-147164363-1"></script>
<script type="text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-147164363-1');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 701640832 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-701640832"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-701640832');
</script>    
    
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K9LPBG2');</script>
<!-- End Google Tag Manager -->
    
    
<script type="text/javascript">
	function fechaLayer(){
	document.getElementById('fecha').style.display = "none";
	}
</script>
    
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K9LPBG2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    
<?php
$sqltut	    =	"select * from tutoriais where tut_codigo = '1'";
$restut	    =	mysqli_query($cn, $sqltut);
$lintut	    =	mysqli_fetch_array($restut);    
?>
<?php
if (isset($lintut['tut_codigo']) == "1" and $lintut['tut_link'] != "") { // Verifica se existe o tutorial de código 1 faz abaixo
?>
        <?php
        $home1 = isset($_GET['IniSeSsiSit']) ? $_GET['IniSeSsiSit'] : "";

        if ($home1 == "" or $home1 == "index" or $home1 == "home") { 
        ?>
        <div id="fecha">
            <div id="caixa_campanha">
                <div id="close"><a href="#" onClick="javascript:fechaLayer()"><img src="assets/_img/fechar.png" alt="<?php echo $lintut['tut_nome']; ?>"/></a></div>
                <div id="arte">
                    <div class="mapa">
                        <iframe class="maps" src="https://www.youtube.com/embed/<?php echo $lintut['tut_link']; ?>?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <?php  } ?>
<?php  } // Termina IF de verificação?>
    
<?php 
require_once($pagina);  
require_once("footer.php");    
require_once("painel/destruidor.php"); 
?>

<script type="application/javascript" src="assets/facebook/facebook.js"></script>
<script type="application/javascript" src="assets/popper/popper.min.js"></script>
<script type="application/javascript" src="assets/bootstrap/bootstrap.min.js"></script>
<script type="application/javascript" src="assets/_js/scripts.js"></script>
<script type="application/javascript" src="assets/fancybox/jquery.fancybox.min.js"></script>
	
<script type="text/javascript" src="assets/select/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="assets/select/jquery-ui.min.js"></script>
<script type="text/javascript" src="assets/select/jquery.select-to-autocomplete.js"></script>
<script>
  (function($){
	$(function(){
	  $('.bus-area-select').selectToAutocomplete();
//	  $('.search-professional-home').selectToAutocomplete();
//	  $('form').submit(function(){
//		alert( $(this).serialize() );
//		return false;
//	  });
	});
  })(jQuery);
</script>

<script>
	$('div.anchors a.anchor-link').each(function(index) {
		var link = $(this).attr('href');
		$(this).on("click", function() {

			$('html, body').animate({
				scrollTop: $(link).offset().top
			}, 1000);
			return false;
		});

	});


//	$(document).mouseup(function(e) {
//		var container = $("ul#area-list");
//
//		if(!container.is(e.target) && container.has(e.target).length === 0){
//			container.hide();
//		}
//	});
//
//	var search_pro = $("input#search-professional"); 
//
//	search_pro.on("click", function() {
//		$("ul#area-list").css("display", "flex");
//	});
//
//	search_pro.on("keyup", function() {
//		var value = $(this).val().toLowerCase();
//		$("ul#area-list li.list-category a").filter(function() {
//			$(this).parent().toggle($(this).text().toLowerCase().indexOf(value) > -1);
//		});
//	});

</script>

    
<!-- Facebook Pixel Code -->
<script type="text/javascript">
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
     fbq('init', '742815162918006'); 
    fbq('track', 'PageView');
</script>
    
<noscript>
 <img height="1" width="1" src="https://www.facebook.com/tr?id=742815162918006&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
    
    
</body>
</html>