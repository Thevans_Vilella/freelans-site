<?php
$sql12		=	"SELECT * from textos WHERE txt_codigo = '12'";
$res12		=	mysqli_query($cn, $sql12);
$lin12		=	mysqli_fetch_array($res12);
?>
<section id="customer_profile">
    <div class="container-fluid no-padding-left-right d-flex flex-wrap">
        <div class="col-md-6 d-md-block d-none no-padding-left-right">
            <img class="profile_background" src="assets/_img/helpers/img-customer-profile.png" alt="">
        </div>
        <div class="col-md-6 col-12 no-padding-left-right">
            <div class="customer_profile_container half-container">
                <h1 class="profile_title">Perfil Cliente</h1>
                <h2 class="profile_subtitle underscore_effect">Encontre os melhores Profissionais</h2>
                <p class="profile_text"><?php echo $lin12['txt_descricao']; ?></p>
                <a class="register_button" href="register-account/0">Fazer meu cadastro</a>
            </div>
        </div>
    </div>
</section>