<?php
session_start();
if(isset($_SESSION["login_ses"]) and ($_SESSION["status_ses"] == "1") and ($_SESSION["tipo_ses"] == "2")) { 
	
require_once("painel/conexao.php");
require_once("painel/classes.php");

$perfcod	=	(int)$_POST['user'];
	
//===================================================================================================================================================
$sqluser	=	"select * from perfil where perf_codigo = '$perfcod'";
$resuser	=	mysqli_query($cn, $sqluser);
$linuser	=	mysqli_fetch_array($resuser);	

$username	=	$linuser['perf_nome'];
//===================================================================================================================================================
// Corrigindo o nome da Imagem
date_default_timezone_set('America/Sao_Paulo');
$tempo_image = md5(date("Ymd-His"));
$time_image = substr($tempo_image, 0, 5);

$namefoto 	= 	trim(CorrigirNome($username)."-".$time_image);
/*==================================================================================================================================================*/ 
/*==================================================================================================================================================*/ 

if ( $perfcod == "" or $username == "" )  { 

	echo "<script type='text/javascript'>alert('Por Favor, Preencha todos os Campos!', 'perfil-edit-profissional');</script>";
	include "painel/destruidor.php";
	exit();

} 
//=========================================================================================================================================================
//================ Daqui pra baixo INSERE FOTOS NO FTP (Arquivo) ==========================================================================================

if ($_FILES['arquivo']['size'] > 0) {


				include "verificar_extensao.php";
				

				// AGORA DEVO INSERIR FOTOS NOVAS NO FTP ---------------------
				$arq   = $_FILES['arquivo'];
				
				
				$arq2    = $arq['tmp_name'];
				$nome    = $arq['name'];
				$tipo    = $arq['type'];
				$tamanho = $arq['size'];
				
				
				$ptr_arq = fopen($arq2, "r");
				$lido    = fread($ptr_arq, filesize($arq2));
				$foto    = addslashes($lido);
				fclose($ptr_arq);
				
				/********SCRIPT PARA REDIMENSIONAMENTO DA IMAGEM*********/
				/********CRIANDO A IMAGEM GRANDE REDIMENSIONADA**********/
				
				// dados sobre a imagem fonte
				$img_fonte    = imagecreatefromjpeg("$arq2"); //abre a imagem (do disco para a memória))
				$largura      = imagesx($img_fonte);            //pega a largura
				$altura       = imagesy($img_fonte);            //pega a altura
				
				// dados para a nova imagem - FOTO GRANDE
				$nova_largura = 600;                            //define nova largura
				$nova_altura  = $altura*$nova_largura/$largura; //define nova altura
				$img_origem   = $img_fonte;
				$img_destino  = imagecreatetruecolor($nova_largura,$nova_altura) or die("Cannot Initialize new GD image stream");
				
				// redimensiona
				imagecopyresampled($img_destino,$img_origem,0,0,0,0,$nova_largura,$nova_altura,$largura,$altura); 
				
				// mostra a imagem 
				//header("content-type: image/jpeg");
				//imagejpeg($img_fonte);
				
				$nomeimagem = $namefoto; //gera nome do arquivo com data e hora
				
				//Verifica se o diretório existe
				if(!file_exists("painel/fotos/perfiladd/grandes")){
				 mkdir("painel/fotos/perfiladd/grandes");
				}
				
				$end_foto = "painel/fotos/perfiladd/grandes/".$nomeimagem.".jpg";
				imagejpeg($img_destino,$end_foto,80);
				
				chmod("$end_foto", 0755);
				
				// libera a memória 
				imagedestroy($img_fonte);
				imagedestroy($img_destino);
				
				/********CRIANDO A IMAGEM THUMBS REDIMENSIONADA**********/
				$img_fonte    = imagecreatefromjpeg("$arq2"); //abre a imagem (do disco para a memória))
				$largura      = imagesx($img_fonte);            //pega a largura
				$altura       = imagesy($img_fonte);            //pega a altura
				
				// dados para a nova imagem - FOTO PEQUENA
				$nova_largura = 90;                            //define nova largura
				$nova_altura  = $altura*$nova_largura/$largura; //define nova altura
				$img_origem   = $img_fonte;
				$img_destino  = imagecreatetruecolor($nova_largura,$nova_altura) or die("Cannot Initialize new GD image stream");
				
				// redimensiona
				imagecopyresampled($img_destino,$img_origem,0,0,0,0,$nova_largura,$nova_altura,$largura,$altura); 
				
				// mostra a imagem 
				//header("content-type: image/jpeg");
				
				//imagejpeg($img_fonte);
				
				if(!file_exists("painel/fotos/perfiladd/pequenas")){
				 mkdir("painel/fotos/perfiladd/pequenas");
				}
				
				$end_thumbs = "painel/fotos/perfiladd/pequenas/".$nomeimagem.".jpg";
				imagejpeg($img_destino,$end_thumbs,80);
				
				chmod("$end_thumbs", 0755);
				
				// libera a memória 
				imagedestroy($img_fonte);
				imagedestroy($img_destino);


	
				$end_thumbs		=	CorrigirCaminhoPerfil($end_thumbs);
				$end_foto		=	CorrigirCaminhoPerfil($end_foto);




} else { // Não trouxe foto nenhuma

$end_thumbs		=	"";
$end_foto		=	"";

} 
//=========================================================================================================================================================
//=========================================================================================================================================================
	
$sql	=	"INSERT INTO perfil_fotos 
(perf_codigo, perf_fot_fotop, perf_fot_fotog) 

VALUES 
('$perfcod', '$end_thumbs', '$end_foto')";

mysqli_query($cn, $sql);


	
//=========================================================================================================================================================
//=========================================================================================================================================================

echo "<script>window.location.href='perfil-edit-profissional';</script>";
include "painel/destruidor.php";

?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>