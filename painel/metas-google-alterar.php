﻿<?php  if(isset($_SESSION["login_ses"])) {  ?>

<?php
$sql	=	"SELECT * from metas_google";
$res	=	mysqli_query($cn, $sql);
$lin	=	mysqli_fetch_array($res);	
?>

<div class="tit">Dados Cadastrados das Metas do Google</div>
<div class="sombra-tit"></div>
<div class="sub-tit"></div>

<form id="cadastro" name="form" method="post" enctype="multipart/form-data" action="metas-google-inserir.php">
	<ul>
	<li>
    <div class="dad">
    <label>URL:</label>
    <div class="dado"><input type="text" name="url" id="url" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" class="validate[required]" value="<?php echo $lin['tag_url']; ?>"/></div>
    </div>
    </li>
    
	<li>
    <div class="dad">
    <label>Autor:</label>
    <div class="dado"><input type="text" name="autor" id="autor" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" class="validate[required]" value="<?php echo $lin['tag_autor']; ?>"/></div>
    </div>
    </li>
    
	<li>
    <div class="dad">
    <label>Descrição:</label>
    <div class="dado"><input type="text" name="descricao" id="descricao" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" class="validate[required]" value="<?php echo $lin['tag_descricao']; ?>"/></div>
    </div>
    </li>
    
	<li>
    <div class="dad">
    <label>Tag Alt:</label>
    <div class="dado"><input type="text" name="alt" id="alt" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" class="validate[required]" value="<?php echo $lin['tag_alt']; ?>"/></div>
    </div>
    </li>
    
	<li>
    <div class="desc-cadastro">
    <label>Palavras Chaves:</label>
    <div class="dado"><textarea name="palavras" cols="1" rows="5" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)"><?php echo $lin['tag_palavras_chaves']; ?></textarea></div>
    </div>
    </li>
    
	<li>
    <div class="desc-cadastro">
    <label>Longdesc:</label>
    <div class="dado"><textarea name="longdesc" cols="1" rows="5" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)"><?php echo $lin['tag_londesc']; ?></textarea></div>
    </div>
    </li>
	</ul>
    
	<input type="image" src="imagens/alterar.png" class="cadastrar" />
	<input name="codigo" type="hidden" id="codigo" size="5" value="<?php echo $lin['tag_codigo']; ?>" />
</form>
</div>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
