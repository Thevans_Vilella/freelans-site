﻿<?php  if(isset($_SESSION["login_ses"])) { ?>

<div class="tit">Cadastro de Administrador</div>
<div class="sombra-tit"></div>
<div class="sub-tit"></div>

<form id="cadastro" method="post" action="administrador-inserir.php">
    <ul>
    <li><div class="dad"><label>Nome:</label><div class="dado"><input type="text" name="nome" id="nome" onKeyup="CaixaBaixa(this)" onBlur="CaixaBaixa(this),vazio(this)" class="validate[required]" autofocus/></div></div></li>
    <li><div class="dad"><label>Email:</label><div class="dado"><input type="text" name="usuario" id="usuario" onKeyup="mail(this)" onBlur="mail(this),vazio(this)" class="validate[required,custom[email]]"/></div></div></li>
    <li><div class="dad"><label>Senha:</label><div class="dado"><input type="password" name="senha" id="senha" onKeyup="vazio(this)" onBlur="vazio(this)" class="validate[required]"/></div></div></li>
    </ul>
    
    <input type="image" src="imagens/cadastrar.png" class="cadastrar" />
</form>



<div class="sec-cadastrados">
<div class="tit-item-cadastrados">Administradores Cadastrados</div>
</div>

<?php
$sql	=	"select * from administrador order by adm_nome";
$res	=	mysqli_query($cn, $sql);
while($lin	=	mysqli_fetch_array($res))  {
?>
<div class="box-cad">
<div class="cliente-nome"><?php echo $lin['adm_nome']; ?></div>
<form id="form1" class="adm-alterar-adm" method="post" action="administrador-alterar">
    <input type="image" src="imagens/altera.png"/>
    <input type="hidden" name="adm" value="<?php echo $lin['adm_codigo']; ?>"/>
</form>
<form id="form1" class="adm-alterar-adm" method="post" action="administrador-excluir-update.php" onClick="return confirmation()">
    <input type="image" src="imagens/exclui.png"/>
    <input type="hidden" name="adm" value="<?php echo $lin['adm_codigo']; ?>"/>
</form>
</div>
<?php } ?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
