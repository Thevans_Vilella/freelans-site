﻿<?php  if(isset($_SESSION["login_ses"])) {  ?>

<div class="tit">Dados Cadastrados da Empresa</div>
<div class="sombra-tit"></div>
<div class="sub-tit"></div>

<form id="cadastro" name="form" method="post" enctype="multipart/form-data" action="empresa-dados-alterar-update.php">
	<ul>
	<li>
    <div class="dad">
    <label>Nome:</label>
    <div class="dado"><input type="text" name="cliente" id="cliente" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" class="validate[required]" value="<?php echo $lin10['cont_nome']; ?>"/></div>
    </div>
    </li>
    
	<li>
    <div class="dad">
    <label>Cep:</label>
    <div class="dado"><input type="text" name="cep" id="cep" class="validate[required]" onkeyup="maskIt(this,event,'#####-###')" onBlur="findCEP(this)" maxlength="9" value="<?php echo $lin10['cont_cep']; ?>"/></div>
    </div>
    </li>

    <div id="teste">
    <div class="dad">
    <label>Avenida/Rua:</label>
    <div class="dado">
    <select name="lograd" class="list-menu" id="lograd">
    <option value="<?php echo $lin10['log_nome']; ?>"><?php echo $lin10['log_nome']; ?></option>
    <option value="<?php echo $lin10['log_nome']; ?>">-------------------------------</option>
    <?php
    $sql	=	"select * from logradouro order by log_nome";
    $res	=	mysqli_query($cn, $sql);
    while ($lin	=	mysqli_fetch_array($res))  {
    ?>
    <option value="<?php echo $lin['log_nome']; ?>"><?php echo $lin['log_nome']; ?></option>
    <?php } ?>
    </select>
    </div>
    </div>
    </div>

	<li>
    <div class="dad">
    <label>Endereço:</label>
    <div class="dado"><input type="text" name="endereco" id="rua" class="validate[required]" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" value="<?php echo $lin10['cont_endereco']; ?>"/></div>
    </div>
    </li>
    
	<li>
    <div class="dad">
    <label>Número:</label>
    <div class="dado"><input type="text" name="numero" id="numero" class="validate[required]" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" value="<?php echo $lin10['cont_numero']; ?>"/></div>
    </div>
    </li>

	<li>
    <div class="dad">
    <label>Bairro:</label>
    <div class="dado"><input type="text" name="bairro" id="bairro" class="validate[required]" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" value="<?php echo $lin10['cont_bairro']; ?>"/></div>
    </div>
    </li>
    
	<li>
    <div class="dad">
    <label>Telefone:</label>
    <div class="dado"><input type="text" name="telefone" id="telefone" class="validate[required]" onkeyup="maskIt(this,event,'(##) ####-#####')" maxlength="15" value="<?php echo $lin10['cont_telefone']; ?>"/></div>
    </div>
    </li>
    
	<li>
    <div class="dad">
    <label>Email:</label>
    <div class="dado"><input type="text" name="email" id="email" class="validate[required,custom[email]]" onKeyup="mail(this)" onBlur="mail(this)" value="<?php echo $lin10['cont_email']; ?>"/></div>
    </div>
    </li>
    
	<li>
    <div class="dad">
    <label>Email Formulário:</label>
    <div class="dado"><input type="text" name="emailform" id="emailform" class="validate[required]" onKeyup="mail(this)" onBlur="mail(this)" value="<?php echo $lin10['cont_email_formulario']; ?>"/></div>
    </div>
    </li>

	<li>
	<div class="dad"><label>Link Vídeo:</label><div class="dado"><input type="text" name="link" id="link" onKeyup="vazio(this)" onBlur="vazio(this)" value="<?php echo $lin10['cont_video']; ?>"/></div></div>
	</li>


	<li>
    <div class="dad">
    <label>Google Map:</label>
    <div class="dado"><input type="text" name="link_mapa" id="link_mapa" onKeyup="CaixaBaixa(this),vazio(this)" onBlur="CaixaBaixa(this),vazio(this)" value="<?php echo $lin10['cont_mapa']; ?>"/></div>
    </div>
    </li>
    
	<li>
    <div class="dad">
    <label>Cidade:</label>
    <div class="dado"><input type="text" name="cidade" id="cidade" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" class="validate[required]" value="<?php echo $lin10['cont_cidade']; ?>"/></div>
    </div>
    </li>
	</ul>
	
	<div class="dad"><label>Estado:</label>
	<div class="dado">
	<select name="estado" class="list-menu" id="estado">
	<option value="<?php echo $lin10['est_sigla']; ?>"><?php echo $lin10['est_nome']; ?></option>
	<option value="<?php echo $lin10['est_sigla']; ?>">----------------------------</option>
	<?php
	$sql01	=	"select * from estado order by est_nome";
	$res01	=	mysqli_query($cn, $sql01);
	while ($lin01	=	mysqli_fetch_array($res01))  {
	?>
	<option value="<?php echo $lin01['est_sigla']; ?>"><?php echo $lin01['est_nome']; ?></option>
	<?php } ?>
	</select>
	</div>
    </div>

    <ul>
    <div class="sub-item">A Imagem deve ter 170 x 100 pixel no formato jpg.</div>
    <li><div class="dad-fot"><label>Logomarca:</label><div class="dado"><input name="arquivo" type="file" id="arquivo" onChange="validaimagem();"/></div></div></li>
	</ul>
    
	<input type="image" src="imagens/alterar.png" class="cadastrar" />
	<input type="hidden" name="emp" id="emp" size="5" value="<?php echo $lin10['cont_codigo']; ?>" />
</form>
</div>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
