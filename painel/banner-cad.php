﻿<?php  if(isset($_SESSION["login_ses"])) {  ?>

<div class="tit">Cadastro de Banner Principal</div>
<div class="sombra-tit"></div>

<form id="cadastro" name="form" method="post" enctype="multipart/form-data" action="banner-inserir.php">
    <ul>
    <li>
    <div class="dad">
    <label>Link:</label>
    <div class="dado"><input type="text" name="site" id="site" onKeyup="vazio(this),excesso(this)" onBlur="vazio(this),excesso(this)"/></div>
    </div>
    </li>

    <div class="sub-item">A Imagem deve ter 1425 x 420 pixel no formato jpg.</div>
    <li><div class="dad-fot"><label>Foto Desktop:</label><div class="dado"><input name="arquivo" type="file" class="validate[required]" id="arquivo" onChange="validaimagemdesk();"/></div></div></li>

    <div class="sub-item">A Imagem deve ter 770 x 250 pixel no formato jpg.</div>
    <li><div class="dad-fot"><label>Tablet:</label><div class="dado"><input name="arquivotab" type="file" class="validate[required]" id="arquivotab" onChange="validaimagemtablet();"/></div></div></li>

    <div class="sub-item">A Imagem deve ter 400 x 150 pixel no formato jpg.</div>
    <li><div class="dad-fot"><label>Mobile:</label><div class="dado"><input name="arquivomob" type="file" class="validate[required]" id="arquivomob" onChange="validaimagemmobile();"/></div></div></li>
    </ul>
    
    <input type="image" src="imagens/cadastrar.png" class="cadastrar" />
</form>



<div class="tit">Banners Cadastradas</div>
<div class="sombra-tit"></div>
<div class="avisa">Arraste as Imagens para Alterar a Ordem das Fotos.</div>


    <form name="form1" method="post" action="banner-alterar-ordem.php">
    <div class="qd-fotos">
    <ul id="sortable">
		
		<form></form>
		
		<?php
        $sql01	=	"select * from banner order by ban_ordem";
        $res01	=	mysqli_query($cn, $sql01);
        while	($lin01	=	mysqli_fetch_array($res01)) {
        ?>
        <li>
            <div class="cx-foto">
            <div class="foto"><a rel="shadowbox[Mixed];" style="background-image: url(<?php echo $lin01['ban_fotom']; ?>);" href="<?php echo $lin01['ban_fotog']; ?>"></a></div>
            <form name="alterafoto" id="alterafoto" class="fot-altera" method="post" action="banner-alterar">
                <input type="image" src="imagens/altera.png"/>
                <input type="hidden" name="banner" value="<?php echo $lin01['ban_codigo']; ?>"/>
            </form>
            <form id="form2" class="fot-alterar" method="post" action="banner-excluir-update.php" onClick="return confirmation()">
                <input type="image" src="imagens/exclui.png"/>
                <input type="hidden" name="banner" value="<?php echo $lin01['ban_codigo']; ?>"/>
            </form>
            </div>

        <input type="hidden" name="id[]" value="<?php echo $lin01['ban_codigo']; ?>">
        </li>
        <?php } ?>
    </ul>    
    </div>


<div class="sombra-tit"></div>
<div class="avisa">
<input type="image" src="imagens/confirmar-alteracao.png" name="Atualizar" class="confirma-alteracao" id="Atualizar" value="Atualizar" alt="Confirmar" onclick="document.form1.submit();"/>
</div>
</form>


<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
