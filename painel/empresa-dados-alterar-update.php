<?php
session_start();
if(isset($_SESSION["login_ses"])) {
?>

<?php include "alerta/scripts.php"; ?>

<?php
include "conexao.php";
include "classes.php";

$emp			=	(int) $_POST['emp'];

//===============================================================================================================================================

$log			=	anti_injection($_POST['lograd']);
$est			=	anti_injection($_POST['estado']);

$sql777			=	"select * from logradouro where log_nome = '$log'";
$res777			=	mysqli_query($cn, $sql777);
$lin777			=	mysqli_fetch_array($res777);

$sql888			=	"select * from estado where est_sigla = '$est'";
$res888			=	mysqli_query($cn, $sql888);
$lin888			=	mysqli_fetch_array($res888);

$lograd			=	(int) $lin777['log_codigo'];
$estado			=	(int) $lin888['est_codigo'];

//===============================================================================================================================================

$cliente		=	ConverteItem(anti_injection($_POST['cliente']));
$cep			=	CorrigirCEP(anti_injection($_POST['cep']));
$endereco		=	ConverteItem(anti_injection($_POST['endereco']));
$numero			=	CorrigirNumero(anti_injection($_POST['numero']));
$bairro			=	ConverteItem(anti_injection($_POST['bairro']));
$telefone		=	CorrigirTelefone(anti_injection($_POST['telefone']));
$email			=	ConverteEmail(anti_injection($_POST['email']));
$emailform		=	ConverteEmail(anti_injection($_POST['emailform']));
$cidade			=	ConverteItem(anti_injection($_POST['cidade']));
$link			=	$_POST['link'];
$link_mapa		=	CorrigirLink(anti_injection($_POST['link_mapa']));


//============================================================================================================
if ( $log == "" or $est == "" or $cliente == "" or $cep == "" or $endereco == "" or $numero == "" or $bairro == "" or $telefone == "" or $email == "" or $emailform == "" or $cidade == "" or $link == ""  or $link_mapa == "" )  { 

	echo "<script type='text/javascript'>alert('Por Favor, Preencha os Campos Obrigatórios!', 'empresa-dados-alterar');</script>";
	include "destruidor.php";
	exit() ;

} 
//========================================================================================================================
// Corrigindo o nome da Imagem
$namefoto 	= 	CorrigirNome($cliente)."-"."logomarca"; //gera nome do arquivo com data e hora
/*======================================================================================================================*/ 

if ($_FILES['arquivo']['size'] > 0) {   // Verificando Se Foi Colocado Foto para Download ==============================

	include "verificar_extensao.php"; 

	// Seleciono no banco o caminho que devo excluir as fotos do FTP.
	$sql_verif	=	"select * from contato where cont_codigo = '$emp'";
	$res_verif	=	mysqli_query($cn, $sql_verif);
	$lin_verif	=	mysqli_fetch_array($res_verif);

	unlink($lin_verif['cont_fotop']); // Entrar na Pasta do FTP e Excluir Foto Antiga
	unlink($lin_verif['cont_fotog']); // Entrar na Pasta do FTP e Excluir Foto Antiga



				// AGORA DEVO INSERIR FOTOS NOVAS NO FTP ---------------------
				$arq   = $_FILES['arquivo'];
				
				
				
				$arq2    = $arq['tmp_name'];
				$nome    = $arq['name'];
				$tipo    = $arq['type'];
				$tamanho = $arq['size'];
				
				
				$ptr_arq = fopen($arq2, "r");
				$lido    = fread($ptr_arq, filesize($arq2));
				$foto    = addslashes($lido);
				fclose($ptr_arq);
				
				/********SCRIPT PARA REDIMENSIONAMENTO DA IMAGEM*********/
				/********CRIANDO A IMAGEM GRANDE REDIMENSIONADA**********/
				
				// dados sobre a imagem fonte
				$img_fonte    = imagecreatefromjpeg("$arq2"); //abre a imagem (do disco para a memória))
				$largura      = imagesx($img_fonte);            //pega a largura
				$altura       = imagesy($img_fonte);            //pega a altura
				
				// dados para a nova imagem - FOTO GRANDE
				$nova_largura = 170;                            //define nova largura
				$nova_altura  = $altura*$nova_largura/$largura; //define nova altura
				$img_origem   = $img_fonte;
				$img_destino  = imagecreatetruecolor($nova_largura,$nova_altura) or die("Cannot Initialize new GD image stream");
				
				// redimensiona
				imagecopyresampled($img_destino,$img_origem,0,0,0,0,$nova_largura,$nova_altura,$largura,$altura); 
				
				// mostra a imagem 
				//header("content-type: image/jpeg");
				//imagejpeg($img_fonte);
				
				$nomeimagem = $namefoto; //gera nome do arquivo com data e hora
				
				//Verifica se o diretório existe
				if(!file_exists("fotos/grandes")){
				 mkdir("fotos/grandes");
				}
				
				$end_foto = "fotos/grandes/".$nomeimagem.".jpg";
				imagejpeg($img_destino,$end_foto,100);
				
				chmod("$end_foto", 0755);
				
				// libera a memória 
				imagedestroy($img_fonte);
				imagedestroy($img_destino);
				
				/********CRIANDO A IMAGEM THUMBS REDIMENSIONADA**********/
				$img_fonte    = imagecreatefromjpeg("$arq2"); //abre a imagem (do disco para a memória))
				$largura      = imagesx($img_fonte);            //pega a largura
				$altura       = imagesy($img_fonte);            //pega a altura
				
				// dados para a nova imagem - FOTO PEQUENA
				$nova_largura = 120;                            //define nova largura
				$nova_altura  = $altura*$nova_largura/$largura; //define nova altura
				$img_origem   = $img_fonte;
				$img_destino  = imagecreatetruecolor($nova_largura,$nova_altura) or die("Cannot Initialize new GD image stream");
				
				// redimensiona
				imagecopyresampled($img_destino,$img_origem,0,0,0,0,$nova_largura,$nova_altura,$largura,$altura); 
				
				// mostra a imagem 
				//header("content-type: image/jpeg");
				
				//imagejpeg($img_fonte);
				
				if(!file_exists("fotos/pequenas")){
				 mkdir("fotos/pequenas");
				}
				
				$end_thumbs = "fotos/pequenas/".$nomeimagem.".jpg";
				imagejpeg($img_destino,$end_thumbs,100);
				
				chmod("$end_thumbs", 0755);
				
				// libera a memória 
				imagedestroy($img_fonte);
				imagedestroy($img_destino);





} else { // Não trouxe foto nenhuma

	// Seleciono no banco o caminho que devo excluir as fotos do FTP.
	$sql_verif	=	"select * from contato where cont_codigo = '$emp'";
	$res_verif	=	mysqli_query($cn, $sql_verif);
	$lin_verif	=	mysqli_fetch_array($res_verif);
	
	
$end_foto		=	$lin_verif['cont_fotog'];
$end_thumbs		=	$lin_verif['cont_fotop'];

} 
//======================== Daqui pra baixo INSERE DADOS NO BANCO ========================================

$sql	=	"UPDATE contato SET 

cont_nome						= '$cliente',
cont_cep						= '$cep',
log_codigo						= '$lograd',
cont_endereco					= '$endereco',
cont_numero						= '$numero',
cont_bairro						= '$bairro',
cont_telefone					= '$telefone',
cont_email						= '$email',
cont_email_formulario			= '$emailform',
cont_cidade						= '$cidade',
est_codigo						= '$estado',
cont_video						= '$link',
cont_mapa						= '$link_mapa',
cont_fotop						= '$end_thumbs',
cont_fotog						= '$end_foto'

WHERE cont_codigo ='$emp'";

mysqli_query($cn, $sql);

?>

<?php
echo "<script type='text/javascript'>alert('Dados da Empresa Alterado com Sucesso!', 'empresa-dados-alterar');</script>";
include "destruidor.php";
?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>