function ValidarUsuario() {
	var useres = document.usuario;

	if (useres.user.value.length == 0) {
		alert('O campo [ Usuário ] não foi preenchido.');
		useres.user.focus();
		return false;
	}else{
	    if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(useres.user.value))){
		   alert("É necessário o preenchimento de um endereço de e-mail válido.");
		   useres.user.focus();
		   return false;
	    }
	}
}
function validarAcesso() {
	var login = document.login;

	if (login.usuario.value.length == 0) {
		alert('O campo [ Email ] não foi preenchido.');
		login.usuario.focus();
		return false;
	}
	if (login.senha.value.length == 0) {
		alert('O campo [ Senha ] não foi preenchido.');
		login.senha.focus();
		return false;
	}
}


$(document).ready(function(){
	$("a[class=modal]").click( function(ev){
		ev.preventDefault();

		var id = $(this).attr("href");

		var alturaTela = $(document).height();
		var larguraTela = $(window).width();

		$('#cobre').css({'width':larguraTela,'height':alturaTela});
		$('#cobre').fadeIn(800);	
		$('#cobre').fadeTo("slow",0.8);

		var left = ($(window).width() /2) - ( $(id).width() / 2 );
		var top = ($(window).height() / 2) - ( $(id).height() / 2 );
		
		$(id).css({'top':top,'left':left});
		$(id).show();	
	});

	$("#cobre").click( function(){
		$(this).hide();
		$(".window").hide();
	});

	$('.find').click(function(ev){
		ev.preventDefault();
		$("#cobre").hide();
		$(".window").hide();
	});
});