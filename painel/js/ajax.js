//===================================================================
$(document).ready(function(){
   $("form").attr('autocomplete','off');
});
//===================================================================

function confirmation(){
   msg = "Tem Certeza que Deseja Excluir este Ítem?";
   return confirm(msg);
}


function validarAcesso() {
	var login = document.login;

	if (login.usuario.value.length == 0) {
		alert('O campo [ Usuário ] não foi preenchido.');
		login.usuario.focus();
		return false;
	}
	if (login.senha.value.length == 0) {
		alert('O campo [ Senha ] não foi preenchido.');
		login.senha.focus();
		return false;
	}
}


//=====================================================================================================================
function formataData(campo){
	campo.value = Limpar(campo.value,"0123456789");
	vr = campo.value;
	tam = vr.length;

	if ( tam > 2 && tam < 5 )
		campo.value = vr.substr( 0, 2  ) + '/' + vr.substr( 2, tam );
		//campo.value = vr.substr( 0, tam - 2  ) + '/' + vr.substr( tam - 2, tam );
	if ( tam >= 5 && tam <= 10 )
		campo.value = vr.substr( 0, 2 ) + '/' + vr.substr( 2, 2 ) + '/' + vr.substr( 4, 4 ); 
}

function Limpar(valor, validos) {
	// retira caracteres invalidos da string
	var result = "";
	var aux;
	for (var i=0; i < valor.length; i++) {
		aux = validos.indexOf(valor.substring(i, i+1));
		if (aux>=0) {
			result += aux;
		}
	}
	return result;
}

function ValidarData(campo){
	if (campo.value!='') {
		var expReg = /^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$/;
		var msgErro = 'Data inválida! ';
		if ((campo.value.match(expReg)) && (campo.value!='')){
			var dia = campo.value.substring(0,2);
			var mes = campo.value.substring(3,5);
			var ano = campo.value.substring(6,10);
			if((mes==4 || mes==6 || mes==9 || mes==11) && dia>30){
				ct_campo = eval("campo.value = ''");
				campo.focus();
				alert("Data incorreta!! O mês especificado contém no máximo 30 dias.");
				return false;
			} else{
				if(ano%4!=0 && mes==2 && dia>28){
					ct_campo = eval("campo.value = ''");
					campo.focus();
					alert("Data incorreta!! O mês especificado contém no máximo 28 dias.");
					return false;
				} else{
					if(ano%4==0 && mes==2 && dia>29){
						ct_campo = eval("campo.value = ''");
						campo.focus();
						alert("Data incorreta!! O mês especificado contém no máximo 29 dias.");
						return false;
					} else{
						//alert ("Data correta!");
						return true;
					}
				}
			}
		} else {
			ct_campo = eval("campo.value = ''");
			alert(msgErro);
			campo.focus();
			return false;
		}
	}
}

//=====================================================================================================================
	function set(obj, v){
		$('#' + obj).val(v.substr(-5).replace(/[^\d]+/g,''));
		return (v);
	}
//=====================================================================================================================

//=====================================================================================================================
	function alimentarCampo() {
		var status = document.getElementById("status");
		document.getElementById("pagamento").value = status.options[status.selectedIndex].value;
	}
//=====================================================================================================================

//=====================================================================================================================
	function up(lstr){
		var str=lstr.value;
		lstr.value=str.replace(/^\s+/,"").toUpperCase();
	}
	
	function down(lstr){
		var str=lstr.value;
		lstr.value=str.replace(/^\s+/,"").toLowerCase();
	}
//=====================================================================================================================

//=====================================================================================================================
	function email(lstr){
		var str    =  lstr.value;
		lstr.value =  lstr.value.replace(/\s{1,}/g, '').toLowerCase();
		lstr.value =  lstr.value.replace(/\.+$/gi, "");

		retorno 	   =  lstr.value.substr(0, 1);
		if (retorno == ".") {
			lstr.value 	   =  lstr.value.substr(1);	
	
		} 
	}
//=====================================================================================================================

//=====================================================================================================================
	function Usuario(lstr){
		var str    =  lstr.value;
		lstr.value =  lstr.value.replace(/[^a-z0-9]/gi, "").toLowerCase();
		lstr.value =  lstr.value.replace(/\s{1,}/g, '');
	}
//=====================================================================================================================

//=====================================================================================================================
	function UsuarioAcess(lstr){
		var str    =  lstr.value;
		lstr.value =  lstr.value.replace(/[^a-z0-9.]/gi, "").toLowerCase();
		lstr.value =  lstr.value.replace(/\s{1,}/g, '');
	}
//=====================================================================================================================

//=====================================================================================================================
	function mail(lstr){
		var str    =  lstr.value;
		lstr.value = lstr.value.replace(/[^a-z0-9.@-_]/gi, "").toLowerCase();
		lstr.value =  lstr.value.replace(/\s{1,}/g, '');
		lstr.value =  lstr.value.replace(/\@{2,}/g, '@');
		lstr.value =  lstr.value.replace(/\.{2,}/g, '.');

		retorno 	   =  lstr.value.substr(0, 1);
		if (retorno == ".") {
			lstr.value 	   =  lstr.value.substr(1);	
	
		} 
	}
//=====================================================================================================================

//=====================================================================================================================
	function User(lstr){
		var str    =  lstr.value;
		lstr.value =  lstr.value.replace(/[^a-z0-9]/gi, "").toLowerCase();
		lstr.value =  lstr.value.replace(/\s{1,}/g, '');
	}
//=====================================================================================================================

//=====================================================================================================================
	function Users(lstr){
		var str    =  lstr.value;
		lstr.value =  lstr.value.replace(/[^a-z0-9]/gi, "").toLowerCase();
		lstr.value =  lstr.value.replace(/\s{1,}/g, '');

		if (lstr.value.length < 7) {
			alert('O Campo [ Usuário ] Precisa ter Pelo Menos [ 7 ] Digítos!');
			$("#usuario").focus();
			return false;
		}
	}
//=====================================================================================================================

//=====================================================================================================================
	function Senha(lstr){
		var str    =  lstr.value;
		lstr.value =  str.replace(/\s{1,}/g,""); 

		retorno 	   =  lstr.value.substr(0, 1);
		if (retorno == ".") {
			lstr.value 	   =  lstr.value.substr(1);	
	
		} 
	}
//=====================================================================================================================

//=====================================================================================================================
	function vazio(lstr){
		var str    =  lstr.value;
		lstr.value =  str.replace(/^\s+|\s+$/g,"");
	}
//=====================================================================================================================

//=====================================================================================================================
	function excesso(lstr){
		var str    =  lstr.value;
		lstr.value =  str.replace(/\s{2,}/g, ' ');
	}
//=====================================================================================================================

//=====================================================================================================================
	function CaixaBaixa(lstr){
		var str    =  lstr.value;
		lstr.value =  lstr.value.replace(/\s{2,}/g, ' ').toLowerCase();

		retorno 	   =  lstr.value.substr(0, 1);
		if (retorno == ".") {
			lstr.value 	   =  lstr.value.substr(1);	
	
		} 
	}
//=====================================================================================================================

//=====================================================================================================================
	function KeyChaves(lstr){
		var str    =  lstr.value;
		lstr.value =  lstr.value.replace(/\s{2,}/g, ' ').toLowerCase();;
		lstr.value =  lstr.value.replace(/\,{2,}/g, ',');
		lstr.value =  lstr.value.replace(/-/g, ',');
		lstr.value =  lstr.value.replace(/ ,/g, ',');

		retorno 	   =  lstr.value.substr(0, 1);
		if (retorno == ".") {
			lstr.value 	   =  lstr.value.substr(1);	
	
		} 
	}
//=====================================================================================================================

//=====================================================================================================================
	function CaixaBaixaDesc(lstr){
		var str    =  lstr.value;
		lstr.value =  lstr.value.toLowerCase();
		lstr.value =  lstr.value.replace(/^\W+/gi, "");
		lstr.value =  lstr.value.replace(/\_{2,}/gi, "_");
		lstr.value =  lstr.value.replace(/\.{2,}/g, '.');
		lstr.value =  lstr.value.replace(/\?{2,}/g, '?');
		lstr.value =  lstr.value.replace(/\;{2,}/g, ';');
		lstr.value =  lstr.value.replace(/\!{2,}/g, '!');


		retorno 	   =  lstr.value.substr(0, 1);
		
		if (retorno == "_") {
			lstr.value 	   =  lstr.value.substr(1);	
		} 
	}
//=====================================================================================================================

//=====================================================================================================================
	function CaixaBaixaDesc2(lstr){
		var str    =  lstr.value;
		lstr.value =  lstr.value.toLowerCase();
		lstr.value =  lstr.value.replace(/^\W+/gi, "");
		lstr.value =  lstr.value.replace(/^\s+|\s+$/g,"");
		lstr.value =  lstr.value.replace(/\_{2,}/gi, "_");
		lstr.value =  lstr.value.replace(/\.{2,}/g, '.');
		lstr.value =  lstr.value.replace(/\?{2,}/g, '?');
		lstr.value =  lstr.value.replace(/\;{2,}/g, ';');
		lstr.value =  lstr.value.replace(/\!{2,}/g, '!');


		retorno 	   =  lstr.value.substr(-1);
		
		if (retorno == "." || retorno == "?" || retorno == "!" || retorno == ";") {
			} else { 
			  lstr.value 	   =  lstr.value + ".";
		    } 
	}
//=====================================================================================================================

//=====================================================================================================================
	function CaixaBaixaDesc3(lstr){
		var str    =  lstr.value;
		
		lstr.value =  lstr.value.replace(/\n/g, '')
		lstr.value =  lstr.value.replace(/\r/g, '')
		lstr.value =  lstr.value.replace(/\t/g, '')

		lstr.value =  lstr.value.replace(/\  ./g, '.');
		lstr.value =  lstr.value.replace(/\. /g, '.');
		
		lstr.value =  lstr.value.replace(/\_{2,}/gi, "_");
		lstr.value =  lstr.value.replace(/\.{2,}/g, '.');
		lstr.value =  lstr.value.replace(/\?{2,}/g, '?');
		lstr.value =  lstr.value.replace(/\;{2,}/g, ';');
		lstr.value =  lstr.value.replace(/\!{2,}/g, '!');
		
		lstr.value =  lstr.value.replace(/\.!/g, '.');
		lstr.value =  lstr.value.replace(/\.+?/g, '.');
		lstr.value =  lstr.value.replace(/\.;/g, '.');

		lstr.value =  lstr.value.replace(/\!./g, '!');
		lstr.value =  lstr.value.replace(/\!+?/g, '!');
		lstr.value =  lstr.value.replace(/\!;/g, '!');

		lstr.value =  lstr.value.replace(/\?./g, '?');
		lstr.value =  lstr.value.replace(/\?!/g, '?');
		lstr.value =  lstr.value.replace(/\?;/g, '?!');

		lstr.value =  lstr.value.replace(/\;./g, ';');
		lstr.value =  lstr.value.replace(/\;!/g, ';');
		lstr.value =  lstr.value.replace(/\;+?/g, ';');

		if (lstr.value.length == 1) {
			  lstr.value 	   =  "";
		    
			alert('O campo [ Descrição ] não foi preenchido.');
			$("#pointdesc").focus();
			return false;
			
			} 
	}
//=====================================================================================================================

//=====================================================================================================================
	function dominios(lstr){
		var str 	   =  lstr.value;
		lstr.value 	   =  lstr.value.replace(/[^a-z0-9.]/gi, "").toLowerCase();
		lstr.value     =  lstr.value.replace(/\.{2,}/gi, ".");
		lstr.value     =  lstr.value.replace(/.w./g, "");

		retorno 	   =  lstr.value.substr(0, 1);
		if (retorno == ".") {
			lstr.value 	   =  lstr.value.substr(1);	
		} 
	}
	
	function dominios2(lstr){ 
		var str        =  lstr.value;
		lstr.value 	   =  lstr.value.replace(/\.+$/gi, "");
		lstr.value     =  lstr.value.replace(/w./g, "www");
		lstr.value     =  lstr.value.replace(/ww./g, "www.");
	
		retorno 	   =  lstr.value.split(".");
		parte1		   =  retorno[0];
		
		if (parte1 != "www"){ 
			lstr.value 	   =  "www." + lstr.value;	
		} 
	}
//=====================================================================================================================

//=====================================================================================================================
	function Ano(lstr){
		var str    =  lstr.value;
		lstr.value =  lstr.value.replace(/[^0-9]/gi, "").toLowerCase();
		lstr.value =  lstr.value.replace(/\s{1,}/g, '');

		conta 	   =  lstr.value.lenght;
		 if (conta == "4") {
		    } else {
				alert('Obrigatório Colocar [ 4 ] Dígitos no Ano!!!');
				$("#nome").focus();
		    }
	}
//=====================================================================================================================

//=====================================================================================================================
	function ValidaUrl(lstr){
		var str 	   =  lstr.value;
		lstr.value 	   =  lstr.value.replace(/[^a-z0-9.:/]/gi, "").toLowerCase();
		lstr.value     =  lstr.value.replace(/\.{2,}/gi, ".");
		lstr.value     =  lstr.value.replace(/[0-9$]/gi, "");
		lstr.value     =  lstr.value.replace(/\s{2,}/g, '');
		lstr.value 	   =  lstr.value.replace(/^[\.\/\:]/gi, "")
		lstr.value 	   =  lstr.value.replace(/[\.\/\:]+$/gi, "")
	}
	
	function ValidaUrl1(lstr){
		var str 	   =  lstr.value;
		
		if(lstr.value.match(/^((http|https):\/\/)+www+\.([a-z0-9\-]+\.)?[a-z0-9\-]+\.[a-z0-9]{2,4}(\.[a-z0-9]{2,4})?(\/.*)?$/i)) {
		} else { 
				alert('A URL é Inválida!!!');
				$("#url").focus();
				}
	}
//=====================================================================================================================

//=====================================================================================================================
	function Domini(obj, v){
		$('#' + obj).val(v.substr(7).replace(/[\/]/g, ""));
			return (v);
	}
//=====================================================================================================================


//=====================================================================================================================
	function Desconto(lstr){
		var str 	   =  lstr.value;
		lstr.value 	   =  lstr.value.replace(/[^0-9]/g,'');
		lstr.value 	   =  lstr.value.replace(/\s{1,}/g, '');
		lstr.value     =  lstr.value.replace(/^(0+)(\d)/g,"$2");

		if (lstr.value.length == "3" && lstr.value >= "101") {
			alert('O Valor do Desconto não Pode ser Maior que [ 100% ] !!!');
			
			lstr.value     = 	"";
			$("#desconto").focus();
		}
	}

	
	function Desconto2(lstr){
		var str 	   =  lstr.value;
		lstr.value 	   =  lstr.value.replace(/[^0-9]/g,'');
		lstr.value 	   =  lstr.value.replace(/\s{1,}/g, '');
		lstr.value     =  lstr.value.replace(/^(0+)(\d)/g,"$2");
		

		if (lstr.value == "") {
			lstr.value     = 	"0";
		}

		if (lstr.value.length == "3" && lstr.value >= "101") {
			alert('O Valor do Desconto não Pode ser Maior que [ 100% ] !!!');
			
			lstr.value     = 	"";
			$("#desconto").focus();
		}
	}
//=====================================================================================================================

//=====================================================================================================================
	function QtdePessoa(lstr){
		var str 	   =  lstr.value;
		lstr.value 	   =  lstr.value.replace(/[^0-9]/g,'');
		lstr.value 	   =  lstr.value.replace(/\s{1,}/g, '');
		lstr.value     =  lstr.value.replace(/^(0+)(\d)/g,"$2");

		if (lstr.value == "0") {
			lstr.value     = 	"";
		}

	}



	function QtdePessoa2(lstr){
		var str 	   =  lstr.value;
		lstr.value 	   =  lstr.value.replace(/[^0-9]/g,'');
		lstr.value 	   =  lstr.value.replace(/\s{1,}/g, '');
		lstr.value     =  lstr.value.replace(/^(0+)(\d)/g,"$2");

		if (lstr.value == "" || lstr.value == "0") {
			lstr.value 	   =  "";
			alert('Valor Mínimo [ 1 ] !!!');
			$("#qtdade").focus();
		} 

	}
//=====================================================================================================================

//=====================================================================================================================
	function Numeros(lstr){
		var str    =  lstr.value;
		lstr.value =  lstr.value.replace(/[^0-9]/gi, "");
		lstr.value =  lstr.value.replace(/\s{1,}/g, '');
	}
//=====================================================================================================================

//=====================================================================================================================
   function findCEP() {
	if($.trim($("#cep").val()) != ""){
		$("#ajax-loading").css('display','inline');
		$.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+$("#cep").val().replace("-", ""), function(){
			if(resultadoCEP["resultado"] == 1){
				$("#lograd").val(unescape(resultadoCEP["tipo_logradouro"]));
				$("#rua").val(unescape(resultadoCEP["logradouro"]));
				$("#bairro").val(unescape(resultadoCEP["bairro"]));
				$("#cidade").val(unescape(resultadoCEP["cidade"]));
				$("#estado").val(unescape(resultadoCEP["uf"]));
				$("#numero").focus();
			}else{
				alert("Endereço não encontrado para o cep ");
			}
			$("#ajax-loading").hide();
		});
	   }
	 }
	   $(document).ready(function(){
		$("#cep").mask("99999-999")
   });
//=====================================================================================================================

//=====================================================================================================================
function Qtda(z){  
		v = z.value;
		v=v.replace(/\D/g,"") 
		v=v.replace(/[0-9]{12}/,"inválido")  
		v=v.replace(/(\d{1})(\d{3})$/,"$1.$2")  
		z.value = v;
	}
//=====================================================================================================================

//=====================================================================================================================
function DimDim(z){  
		v = z.value;
		v=v.replace(/\D/g,"")                      
		v=v.replace(/[0-9]{12}/,"inválido")        
		v=v.replace(/(\d{1})(\d{8})$/,"$1.$2")      
		v=v.replace(/(\d{1})(\d{5})$/,"$1.$2")     
		v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2")	
		z.value = v;
	}
//=====================================================================================================================

//=====================================================================================================================
function Mascara_Hora(Hora, campo){  
   var hora01 = '';  
   hora01 = hora01 + Hora;  
   if (hora01.length == 2){  
      hora01 = hora01 + ':';  
      campo.value = hora01;  
   }  
   if (hora01.length == 5){  
      Verifica_Hora(campo);  
   }  
}  
  
function Verifica_Hora(campo){  
   hrs = (campo.value.substring(0,2));  
   min = (campo.value.substring(3,5));  
   estado = "";  
   if ((hrs < 00 ) || (hrs > 23) || ( min < 00) ||( min > 59)){  
      estado = "errada";  
   }  
  
   if (campo.value == "") {  
      estado = "errada";  
   }  
   if (estado == "errada") {  
      alert("Hora Inválida!");  
      campo.focus();  
   }  
}
//=====================================================================================================================

/*============= Função Para Validar Extensão de Foto ===================================================================================================================*/
function validaimagem() {
	var extensoesOk = ",.jpg,.jpeg,";
	var extensao	= "," + document.form.arquivo.value.substr( document.form.arquivo.value.length - 4 ).toLowerCase() + ",";
	var tipoimg	    = "A Imagem Selecionada tem o Formato " + "[" + document.form.arquivo.value.substr( document.form.arquivo.value.length - 4 ).toUpperCase() + "]";
		
		if (document.form.arquivo.value == "") {
			alert("Favor Inserir Uma Imagem!!!")
		  }
		  
		  
		 else if( extensoesOk.indexOf( extensao ) == -1 ) {
		    alert( "O Formato da Imagem Não é Compatível!!!\n" + tipoimg + "\n\nFavor Inserir uma Imagem no Formato JPG!!!" );
			document.getElementById("arquivo").value = "";
		   }

   }

/*============= Função para Formulário que tiver Fotos Adicionais ======================================================================================================*/
function validaimagemAdd() {
	var extensoesOk = ",.jpg,.jpeg,";
	var extensao	= "," + document.formulario.arquivonovo.value.substr( document.formulario.arquivonovo.value.length - 4 ).toLowerCase() + ",";
	var tipoimg	    = "A Imagem Selecionada tem o Formato " + "[" + document.formulario.arquivonovo.value.substr( document.formulario.arquivonovo.value.length - 4 ).toUpperCase() + "]";
		
		if (document.formulario.arquivonovo.value == "") {
			alert("Favor Inserir Uma Imagem!!!")
		  }
		  
		  
		 else if( extensoesOk.indexOf( extensao ) == -1 ) {
		    alert( "O Formato da Imagem Não é Compatível!!!\n" + tipoimg + "\n\nFavor Inserir uma Imagem no Formato JPG!!!" );
			document.getElementById("arquivonovo").value = "";
		   }

   }
/*=======================================================================================================================================================================*/
/*============= Função para Formulário que tiver Fotos PNG ======================================================================================================*/
function validaimagemPNG() {
	var extensoesOk = ",.png,";
	var extensao	= "," + document.form.arquivo.value.substr( document.form.arquivo.value.length - 4 ).toLowerCase() + ",";
	var tipoimg	    = "A Imagem Selecionada tem o Formato " + "[" + document.form.arquivo.value.substr( document.form.arquivo.value.length - 4 ).toUpperCase() + "]";
		
		if (document.form.arquivo.value == "") {
			alert("Favor Inserir Uma Imagem!!!")
		  }
		  
		  
		 else if( extensoesOk.indexOf( extensao ) == -1 ) {
		    alert( "O Formato da Imagem Não é Compatível!!!\n" + tipoimg + "\n\nFavor Inserir uma Imagem no Formato PNG!!!" );
			document.getElementById("arquivo").value = "";
		   }

   }
/*=======================================================================================================================================================================*/
/*============= Função para Formulário que tiver ARquivos ======================================================================================================*/
function validaarquivo() {
	var extensoesOk = ",.pdf,.zip,.rar,";
	var extensao	= "," + document.form.arquivo.value.substr( document.form.arquivo.value.length - 4 ).toLowerCase() + ",";
	var tipoimg	    = "O Arquivo Selecionado tem o Formato " + "[" + document.form.arquivo.value.substr( document.form.arquivo.value.length - 4 ).toUpperCase() + "]";
		
		if (document.form.arquivo.value == "") {
			alert("Favor Inserir Um Arquivo!!!")
		  }
		  
		  
		 else if( extensoesOk.indexOf( extensao ) == -1 ) {
		    alert( "O Formato do Arquivo Não é Compatível!!!\n" + tipoimg + "\n\nFavor Inserir um Arquivo no Formato PDF ou ZIP ou RAR!!!" );
			document.getElementById("arquivo").value = "";
		   }

   }
/*=======================================================================================================================================================================*/
/*============= Função para Verificar Dimensões da Imagem ===============================================================================================================*/

	function DimensaoImgJPG(){
	var _URL = window.URL || window.webkitURL;
	
	$("#arquivo").change(function(e) {
		var file, img;
	
		if ((file = this.files[0])) {
			img = new Image();
			
			
				img.onload = function() {
					
					if( this.width > 3000 ){
					mensagem1 = 'Imagem Fora do Tamanho Especificado.\n\n';
					mensagem2 = 'A Imagem Tem [ ' + this.width + " px ] de Largura" + '\n\n';
					mensagem = mensagem1 + mensagem2 + 'A Imagem Deve Ter no Máximo [ 3000 px ] de Largura';
					alert(mensagem);
					return false;
					}
	
					if( this.height > 3000 ){
					mensagem1 = 'Imagem Fora do Tamanho Especificado.\n\n';
					mensagem2 = 'A Imagem Tem [ ' + this.height + " px ] de Altura" + '\n\n';
					mensagem = mensagem1 + mensagem2 + 'A Imagem Deve Ter no Máximo [ 3000 px ] de Altura';
					alert(mensagem);
					return false;
					}
				
			   }

				img.src = _URL.createObjectURL(file);
			}
	
		});
	}


	function DimensaoImgPNG(){
	var _URL = window.URL || window.webkitURL;
	
	$("#UploadFile").change(function(e) {
		var file, img;
	
		if ((file = this.files[0])) {
			img = new Image();
			
			
				img.onload = function() {
					
					if( this.width > 106 ){
					mensagem1 = 'Imagem Fora do Tamanho Especificado.\n\n';
					mensagem2 = 'A Imagem Tem [ ' + this.width + " px ] de Largura" + '\n\n';
					mensagem = mensagem1 + mensagem2 + 'A Imagem Deve Ter no Máximo [ 105 px ] de Largura';
					alert(mensagem);
					return false;
					}
	
					if( this.height > 76 ){
					mensagem1 = 'Imagem Fora do Tamanho Especificado.\n\n';
					mensagem2 = 'A Imagem Tem [ ' + this.height + " px ] de Altura" + '\n\n';
					mensagem = mensagem1 + mensagem2 + 'A Imagem Deve Ter no Máximo [ 75 px ] de Altura';
					alert(mensagem);
					return false;
					}
				
			   }

				img.src = _URL.createObjectURL(file);
			}
	
		});
	}
/*=======================================================================================================================================================================*/
/**  
 * Função para aplicar máscara em campos de texto
  */
function maskIt(w,e,m,r,a){
        
        // Cancela se o evento for Backspace
        if (!e) var e = window.event
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;
        
        // Variáveis da função
        var txt  = (!r) ? w.value.replace(/[^\d]+/gi,'') : w.value.replace(/[^\d]+/gi,'').reverse();
        var mask = (!r) ? m : m.reverse();
        var pre  = (a ) ? a.pre : "";
        var pos  = (a ) ? a.pos : "";
        var ret  = "";

        if(code == 9 || code == 8 || txt.length == mask.replace(/[^#]+/g,'').length) return false;

        // Loop na máscara para aplicar os caracteres
        for(var x=0,y=0, z=mask.length;x<z && y<txt.length;){
                if(mask.charAt(x)!='#'){
                        ret += mask.charAt(x); x++;
                } else{
                        ret += txt.charAt(y); y++; x++;
                }
        }
        
        // Retorno da função
        ret = (!r) ? ret : ret.reverse()        
        w.value = pre+ret+pos;
}

// Novo método para o objeto 'String'
String.prototype.reverse = function(){
        return this.split('').reverse().join('');
};




//Consulta de CEP =============================================================================
function consultacep(cep){
  cep = cep.replace(/\D/g,"")
  url="http://cep.correiocontrol.com.br/"+cep+".js"
  s=document.createElement('script')
  s.setAttribute('charset','utf-8')
  s.src=url
  document.querySelector('head').appendChild(s)
}

function correiocontrolcep(valor){
  if (valor.erro) {
	document.getElementById('logradouro').value="";
	document.getElementById('bairro').value="";
	document.getElementById('localidade').value="";
	document.getElementById('uf').value="";
	alert('Cep não encontrado');        
	return;
  };
  document.getElementById('logradouro').value=valor.logradouro
  document.getElementById('bairro').value=valor.bairro
  document.getElementById('localidade').value=valor.localidade
  document.getElementById('uf').value=valor.uf
}
//===========================================================================================

//Validações ================================================================================
//adiciona mascara de cnpj
function MascaraCNPJ(cnpj){
        if(mascaraInteiro(cnpj)==false){
                event.returnValue = false;
        }       
        return formataCampo(cnpj, '00.000.000/0000-00', event);
}

//adiciona mascara de cep
function MascaraCep(cep){
                if(mascaraInteiro(cep)==false){
                event.returnValue = false;
        }       
        return formataCampo(cep, '00.000-000', event);
}

//adiciona mascara ao telefone
function MascaraTelefone(telefone){  
        if(mascaraInteiro(telefone)==false){
                event.returnValue = false;
        }       
        return formataCampo(telefone, '(00) 0000-0000', event);
}

//adiciona mascara ao nosso número
function MascaraNosso(numero){  
        if(mascaraInteiro(numero)==false){
                event.returnValue = false;
        }       
        return formataCampo(numero, '000-0', event);
}

//adiciona mascara ao CPF
function MascaraCPF(cpf){
        if(mascaraInteiro(cpf)==false){
                event.returnValue = false;
        }       
        return formataCampo(cpf, '000.000.000-00', event);
}

//valida telefone
function ValidaTelefone(telefone){
        exp = /\(\d{2}\)\ \d{4}\-\d{4}/
        if(!exp.test(telefone.value))
                alert('Número de Telefone Inválido!');
}

//valida CEP
function ValidaCep(cep){
        exp = /\d{2}\.\d{3}\-\d{3}/
        if(!exp.test(cep.value))
                alert('Número de Cep Inválido!');               
}

//valida o CPF digitado
function ValidarCPF(Objcpf){
        var cpf = Objcpf.value;
        exp = /\.|\-/g
        cpf = cpf.toString().replace( exp, "" ); 
        var digitoDigitado = eval(cpf.charAt(9)+cpf.charAt(10));
        var soma1=0, soma2=0;
        var vlr =11;

        for(i=0;i<9;i++){
                soma1+=eval(cpf.charAt(i)*(vlr-1));
                soma2+=eval(cpf.charAt(i)*vlr);
                vlr--;
        }       
        soma1 = (((soma1*10)%11)==10 ? 0:((soma1*10)%11));
        soma2=(((soma2+(2*soma1))*10)%11);

        var digitoGerado=(soma1*10)+soma2;
        if(digitoGerado!=digitoDigitado)        
                alert('CPF Inválido!');         
}

//valida numero inteiro com mascara
function mascaraInteiro(){
        if (event.keyCode < 48 || event.keyCode > 57){
                event.returnValue = false;
                return false;
        }
        return true;
}

//valida o CNPJ digitado
function ValidarCNPJ(ObjCnpj){
        var cnpj = ObjCnpj.value;
        var valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2);
        var dig1= new Number;
        var dig2= new Number;

        exp = /\.|\-|\//g
        cnpj = cnpj.toString().replace( exp, "" ); 
        var digito = new Number(eval(cnpj.charAt(12)+cnpj.charAt(13)));

        for(i = 0; i<valida.length; i++){
                dig1 += (i>0? (cnpj.charAt(i-1)*valida[i]):0);  
                dig2 += cnpj.charAt(i)*valida[i];       
        }
        dig1 = (((dig1%11)<2)? 0:(11-(dig1%11)));
        dig2 = (((dig2%11)<2)? 0:(11-(dig2%11)));

        if(((dig1*10)+dig2) != digito)  
                alert('CNPJ Inválido!');

}

//formata de forma generica os campos
function formataCampo(campo, Mascara, evento) { 
        var boleanoMascara; 

        var Digitato = evento.keyCode;
        exp = /\-|\.|\/|\(|\)| /g
        campoSoNumeros = campo.value.toString().replace( exp, "" ); 

        var posicaoCampo = 0;    
        var NovoValorCampo="";
        var TamanhoMascara = campoSoNumeros.length;; 

        if (Digitato != 8) { // backspace 
                for(i=0; i<= TamanhoMascara; i++) { 
                        boleanoMascara  = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
                                                                || (Mascara.charAt(i) == "/")) 
                        boleanoMascara  = boleanoMascara || ((Mascara.charAt(i) == "(") 
                                                                || (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " ")) 
                        if (boleanoMascara) { 
                                NovoValorCampo += Mascara.charAt(i); 
                                  TamanhoMascara++;
                        }else { 
                                NovoValorCampo += campoSoNumeros.charAt(posicaoCampo); 
                                posicaoCampo++; 
                          }              
                  }      
                campo.value = NovoValorCampo;
                  return true; 
        }else { 
                return true; 
        }
}

//=====================================================================================================================
function AparecerDiv() {
var select = document.getElementById("pessoa");

	if(select.options[select.selectedIndex].value == "1"){
	   document.getElementById("fis").style.display = "block";
	   document.getElementById("jur").style.display = "none";
	  }
	
	if(select.options[select.selectedIndex].value == "2"){
	   document.getElementById("jur").style.display = "block";
	   document.getElementById("fis").style.display = "none";
	  }
}
//=====================================================================================================================

//=====================================================================================================================

/* AJAX */
function sendRequest(url,id,postData){
	document.getElementById(id).innerHTML = "<center><img src='../imagens/carregando.gif' alt='Aguarde...' /></center>";
	var req = createXMLHTTPObject();
	if (!req) return;
	var method = (postData) ? "POST" : "GET";
	req.open(method,url,true);
	req.setRequestHeader('User-Agent','XMLHTTP/1.0');
	if (req.overrideMimeType) req.overrideMimeType('text/html');
	if (postData) req.setRequestHeader('Content-type','application/x-www-form-urlencoded');

	req.onreadystatechange = function () {
		if (req.readyState != 4) return;
		if (req.status != 200 && req.status != 304) {
			alert('HTTP error ' + req.status);
			return;
		}
		document.getElementById(id).innerHTML = req.responseText; 
	}
	if (req.readyState == 4) return;
	req.send(postData);
}

var XMLHttpFactories = [
	function () {return new XMLHttpRequest()},
	function () {return new ActiveXObject("Msxml2.XMLHTTP")},
	function () {return new ActiveXObject("Msxml3.XMLHTTP")},
	function () {return new ActiveXObject("Microsoft.XMLHTTP")}
];

function createXMLHTTPObject() {
	var xmlhttp = false;
	for (var i=0;i<XMLHttpFactories.length;i++) {
		try { xmlhttp = XMLHttpFactories[i](); }
		catch (e) { continue; }
		break;
	}
	return xmlhttp;
}


var req;

function loadXMLDoc(url){
 req = null;

if (window.XMLHttpRequest) {
 req = new XMLHttpRequest();
 req.onreadystatechange = processReqChange;
 req.open("GET", url, true); 
 req.send(null);

			} else if (window.ActiveXObject) {
			try {
			req = new ActiveXObject("Msxml2.XMLHTTP.4.0");
			} catch(e) {
			try {
			req = new ActiveXObject("Msxml2.XMLHTTP.3.0");
			} catch(e) {
			try {
			req = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
			try {
			req = new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e) {
			req = false;
			}
			}
			}
			}
if (req) {
		 req.onreadystatechange = processReqChange;
		 req.open("GET", url, true);
		 req.send();
		}
	}
}


function processReqChange(){

if (req.readyState == 4) {
   if (req.status == 200) {

      document.getElementById("atualiza").innerHTML = req.responseText;
	  } else {
     alert("Houve um problema ao obter os dados:\n" + req.statusText);
   }
}  else
	{
      document.getElementById("atualiza").innerHTML = "Carregando...";
	} 
}

function atualiza(valor){
loadXMLDoc("sub-categorias.php?ID="+valor);
}