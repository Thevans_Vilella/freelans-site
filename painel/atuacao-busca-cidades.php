﻿<?php  if (isset($_SESSION["login_ses"])) { ?>

<?php

$inicial    =   CorrigirDate($_POST['inicial']);
$final      =   CorrigirDate($_POST['final']);

$total		=	(int) $_POST['total'];
$atuacao	=	(int) $_POST['atuacao'];
	
$sql2		=	"select * from atuacao where atua_codigo = $atuacao";
$res2		=	mysqli_query($cn, $sql2);
$lin2		=	mysqli_fetch_array($res2);
	
//=============================================================================
// Aqui excluímos os dados existentes no banco de dados
$sql360	=	"DELETE FROM busca_atua_cidade_individual";
mysqli_query($cn, $sql360);
//=============================================================================
	
$sql		=	"select * from busca_atua_cidade 
				INNER JOIN atuacao	     on      atuacao.atua_codigo       =       busca_atua_cidade.atua_codigo
				INNER JOIN cidades	     on      cidades.cid_codigo        =       busca_atua_cidade.cid_codigo
				INNER JOIN estado	     on      estado.est_codigo         =       cidades.est_codigo
				WHERE
				bus_atu_cid_data BETWEEN ('$inicial') AND ('$final') 
				AND 
				busca_atua_cidade.atua_codigo = $atuacao
				GROUP BY cidades.cid_codigo
				ORDER BY cid_nome";
$res		=	mysqli_query($cn, $sql);
while($lin	=	mysqli_fetch_array($res))  { // Aqui abrimos o while para gerar os dados
	
$cidcod		=	$lin['cid_codigo'];	

	
			$sqlatu		=	"select * from busca_atua_cidade 
							WHERE
							bus_atu_cid_data BETWEEN ('$inicial') AND ('$final') 
							AND
							atua_codigo = $atuacao
							AND
							cid_codigo = $cidcod
							";
			$resatu		=	mysqli_query($cn, $sqlatu);
			$cntatu		=	mysqli_num_rows($resatu);
			$linatu		=	mysqli_fetch_array($resatu);

			$qtdeatu	=	$cntatu;
	
	
	
	
					$sqlrel	=	"insert into busca_atua_cidade_individual 
					(atua_codigo, cid_codigo, bus_atu_cid_ind_qtde, bus_atu_cid_ind_data_inicio, bus_atu_cid_ind_data_fim)

					VALUES 
					('$atuacao', '$cidcod', '$qtdeatu', '$inicial', '$final')";

					mysqli_query($cn, $sqlrel);
	
	
	
} // Aqui fechamos o while para geração de dados
?>

<div class="tit">Relatório de Busca de Áreas de Atuação / <span class="observacao-codigo"><?php echo $lin2['atua_nome']; ?></span></div>
<div class="sombra-tit"></div>
<div class="sub-tit">Selecione a data inicial e a data final da busca.</div>

<form id="cadastro" method="post" action="atuacao-busca-dados">
    <ul>
    <li><div class="dad"><label>Data Inicial:</label><div class="dado"><input type="text" name="inicial" id="datepicker" class="validate[required]" maxlength="10"/></div></div></li>
    <li><div class="dad"><label>Data Final:</label><div class="dado"><input type="text" name="final" id="datesicker" class="validate[required]" maxlength="10"/></div></div></li>
  	</ul>
  
    <input type="image" src="imagens/buscar.png" class="cadastrar" />
</form>




<div class="sec-cadastrados">
<div class="tit-item-cadastrados">Cidades</div>
<div class="tit-item-cadastrados">Quantidade de Busca no Período - <?php echo $total; ?></div>
</div>




<?php
$sqlbsc			=	"select * from busca_atua_cidade_individual
					INNER JOIN atuacao	     on      atuacao.atua_codigo       =       busca_atua_cidade_individual.atua_codigo
					INNER JOIN cidades	     on      cidades.cid_codigo        =       busca_atua_cidade_individual.cid_codigo
					INNER JOIN estado	     on      estado.est_codigo         =       cidades.est_codigo
					ORDER BY
					bus_atu_cid_ind_qtde DESC, cid_nome";
$resbsc			=	mysqli_query($cn, $sqlbsc);
while($linbsc	=	mysqli_fetch_array($resbsc))  {
?>
<div class="box-cad">
<div class="cliente-nome"><?php echo $linbsc['cid_nome']; ?>/ <?php echo $linbsc['est_sigla']; ?></div>

<div class="cliente-razao"><?php echo $linbsc['bus_atu_cid_ind_qtde']; ?></div>
</div>
<?php } ?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
