﻿<?php  if(isset($_SESSION["login_ses"])) {  ?>

<?php
$banner		=	(int) $_POST['banner'];

$sql	=	"select * from banner where ban_codigo = '$banner'";
$res	=	mysqli_query($cn, $sql);
$lin	=	mysqli_fetch_array($res);
?>

<div class="tit">Alterar Cadastro de Banner Principal</div>
<div class="sombra-tit"></div>

<form id="cadastro" name="form" method="post" enctype="multipart/form-data" action="banner-alterar-update.php">
    <ul>
    <li>
    <div class="dad">
    <label>Link:</label>
    <div class="dado"><input type="text" name="site" id="site" onKeyup="vazio(this),excesso(this)" onBlur="vazio(this),excesso(this)" value="<?php echo $lin['ban_site']; ?>"/></div>
    </div>
    </li>

    <div class="sub-item">A Imagem deve ter 1425 x 420 pixel no formato jpg.</div>
    <li><div class="dad-fot"><label>Foto Desktop:</label><div class="dado"><input name="arquivo" type="file" id="arquivo" onChange="validaimagemdesk();"/></div></div></li>

    <div class="sub-item">A Imagem deve ter 770 x 250 pixel no formato jpg.</div>
    <li><div class="dad-fot"><label>Tablet:</label><div class="dado"><input name="arquivotab" type="file" id="arquivotab" onChange="validaimagemtablet();"/></div></div></li>

    <div class="sub-item">A Imagem deve ter 400 x 150 pixel no formato jpg.</div>
    <li><div class="dad-fot"><label>Mobile:</label><div class="dado"><input name="arquivomob" type="file" id="arquivomob" onChange="validaimagemmobile();"/></div></div></li>
    </ul>
    
    <input type="image" src="imagens/alterar.png" class="cadastrar" />
    <input type="hidden" name="banner" size="5" value="<?php echo $banner; ?>" />
</form>






<div class="tit">Cadastro de Cidades e Categorias para Banner</div>
<div class="sombra-tit"></div>



<form id="cadastrando" name="form1" method="post" action="banner-categoria.php">
    <div class="dad-check">
    <label>Cidade para exibir Banner:</label>
    <div class="dado">
            <div class="dado">
                <?php
                $sql456			=	"SELECT * from cidades 
									INNER JOIN estado	  		on		estado.est_codigo			=	cidades.est_codigo
									ORDER BY cid_nome";
                $res456			=	mysqli_query($cn, $sql456);
                while($lin456	=	mysqli_fetch_array($res456))  {
					
				$cidcod			=	$lin456['cid_codigo'];
					
					
				$res_tamanho29  = mysqli_query($cn, "SELECT * FROM banner_categorias where ban_codigo = $banner AND cid_codigo = $cidcod");
					$contagem29 = mysqli_num_rows($res_tamanho29);
					
                ?>
                <div class="set-nome">
					<?php if ( $contagem29 <> 0 ) { ?>
					<input type="checkbox" name="cidade[]" id="cidade[]" value="<?php echo $cidcod; ?>" checked="checked" /><?php echo $lin456['cid_nome']; ?>/ <?php echo $lin456['est_sigla']; ?>
					<?php } else { ?>
					<input type="checkbox" name="cidade[]" id="cidade[]" value="<?php echo $cidcod; ?>" /><?php echo $lin456['cid_nome']; ?>/ <?php echo $lin456['est_sigla']; ?>
					<?php } ?>
                </div>
                <?php } ?>
            </div>
    </div>
    </div>
	
	
	
	
	
    <div class="dad-check">
    <label>Categoria para exibir Banner:</label>
    <div class="dado">
            <div class="dado">
                <?php
                $sql01			=	"SELECT * from categorias ORDER BY cat_nome";
                $res01			=	mysqli_query($cn, $sql01);
                while($lin01	=	mysqli_fetch_array($res01))  {
					
				$catcod			=	$lin01['cat_codigo'];
					
					
					$res_tamanho2 = mysqli_query($cn, "SELECT * FROM banner_categorias where ban_codigo = $banner AND cat_codigo = $catcod");
						$contagem = mysqli_num_rows($res_tamanho2);
					
                ?>
                <div class="set-nome">
					<?php if ( $contagem <> 0 ) { ?>
					<input type="checkbox" name="modulo[]" id="modulo[]" value="<?php echo $catcod; ?>" checked="checked" /><?php echo $lin01['cat_nome']; ?>
					<?php } else { ?>
					<input type="checkbox" name="modulo[]" id="modulo[]" value="<?php echo $catcod; ?>" /><?php echo $lin01['cat_nome']; ?>
					<?php } ?>
                </div>
                <?php } ?>
            </div>
    </div>
    </div>
    
    <input type="image" src="imagens/alterar.png" class="cadastrar" />
    <input type="hidden" name="banner" size="5" value="<?php echo $banner; ?>" />
</form>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
