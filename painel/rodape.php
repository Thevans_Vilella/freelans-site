﻿<footer>
<div id="sec-rodape">
<div id="box-rodape">
<div id="rodape">

<div id="box-sistem">
<div class="sistem"><?php echo $lin12['aut_nome']; ?></div>
<div class="sistem-slogan"><?php echo $lin12['aut_slogan']; ?></div>
<div class="sistem-contact">Telefone: <span class="sistem-contact-fone"><?php echo $lin12['aut_fone']; ?></span></div>
<div class="sistem-slogan"><?php echo $lin12['aut_contato']; ?></div>
</div>

<div id="agradecimento">"Tudo Quanto Fizer, Prosperará"</div>

<div id="box-suporte">
<div id="suporte-tecnico">Suporte Técnico</div>
<div id="chat"><?php include "atendimento.php"; ?></div>
<div id="suporte"><?php echo $lin12['aut_suporte']; ?><span class="suporte-mail"><img src="imagens/mail-sistem.jpg" alt="<?php echo $lin12['aut_nome']; ?>"/></span></div>
</div>

<div id="rod-fim">
<div id="direitos">© Todos os Direitos Reservados à <?php echo $lin12['aut_nome']; ?></div>
<div id="validacao"><a href="http://validator.w3.org/check?uri=referer" target="_blank"><img src="imagens/w3c-validacao.png" alt="<?php  echo $lin10['cont_nome']; ?>" /></a></div>
<div id="galanti"><a href="http://www.galantidesign.com.br/" target="_blank"><img src="imagens/galanti-design.png" alt="Galanti Design - Desenvolvimento de Sites" /></a></div>
</div>

</div>
</div>
</div>
</footer>
