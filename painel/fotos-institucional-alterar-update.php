<?php
session_start();
if(isset($_SESSION["login_ses"])) {
?>

<?php include "alerta/scripts.php"; ?>

<?php
include  "conexao.php";
include  "classes.php";

$item			=	(int) $_POST['item'];
$codigo_foto	=	(int) $_POST['codigo_foto'];

//================ Daqui pra baixo INSERE FOTOS NO FTP (Arquivo) =====================================================================================

if ($_FILES['arquivo']['size'] > 0) {   // Verificando Se Foi Colocado Foto para Download ---------------

				include "verificar_extensao2.php"; 

				// Seleciono no banco o caminho que devo excluir as fotos do FTP.
				$sql_verif	=	"select * from institucional_fotos where inst_fot_codigo = '$codigo_foto'";
				$res_verif	=	mysqli_query($cn, $sql_verif);
				$lin_verif	=	mysqli_fetch_array($res_verif);
			
				unlink($lin_verif['inst_fot_fotop']); // Entrar na Pasta do FTP e Excluir Foto Antiga
				unlink($lin_verif['inst_fot_fotog']); // Entrar na Pasta do FTP e Excluir Foto Antiga



				
				// AGORA DEVO INSERIR FOTOS NOVAS NO FTP ---------------------
				$arq   = $_FILES['arquivo'];
				
				
				
				$arq2    = $arq['tmp_name'];
				$nome    = $arq['name'];
				$tipo    = $arq['type'];
				$tamanho = $arq['size'];
				
				
				$ptr_arq = fopen($arq2, "r");
				$lido    = fread($ptr_arq, filesize($arq2));
				$foto    = addslashes($lido);
				fclose($ptr_arq);
				
				/********SCRIPT PARA REDIMENSIONAMENTO DA IMAGEM*********/
				/********CRIANDO A IMAGEM GRANDE REDIMENSIONADA**********/
				
				// dados sobre a imagem fonte
				$img_fonte    = imagecreatefromjpeg("$arq2"); //abre a imagem (do disco para a memória))
				$largura      = imagesx($img_fonte);            //pega a largura
				$altura       = imagesy($img_fonte);            //pega a altura
				
				// dados para a nova imagem - FOTO GRANDE
				$nova_largura = 800;                            //define nova largura
				$nova_altura  = $altura*$nova_largura/$largura; //define nova altura
				$img_origem   = $img_fonte;
				$img_destino  = imagecreatetruecolor($nova_largura,$nova_altura) or die("Cannot Initialize new GD image stream");
				
				// redimensiona
				imagecopyresampled($img_destino,$img_origem,0,0,0,0,$nova_largura,$nova_altura,$largura,$altura); 
				
				// mostra a imagem 
				//header("content-type: image/jpeg");
				//imagejpeg($img_fonte);
				
				$nomeimagem = md5(date("Ymd_His")); //gera nome do arquivo com data e hora
				
				//Verifica se o diretório existe
				if(!file_exists("fotos/institucional/grandes")){
				 mkdir("fotos/institucional/grandes");
				}
				
				$end_foto = "fotos/institucional/grandes/".$nomeimagem.".jpg";
				imagejpeg($img_destino,$end_foto,80);
				
				chmod("$end_foto", 0755);
				
				// libera a memória 
				imagedestroy($img_fonte);
				imagedestroy($img_destino);
				
				/********CRIANDO A IMAGEM THUMBS REDIMENSIONADA**********/
				$img_fonte    = imagecreatefromjpeg("$arq2"); //abre a imagem (do disco para a memória))
				$largura      = imagesx($img_fonte);            //pega a largura
				$altura       = imagesy($img_fonte);            //pega a altura
				
				// dados para a nova imagem - FOTO PEQUENA
				$nova_largura = 420;                            //define nova largura
				$nova_altura  = $altura*$nova_largura/$largura; //define nova altura
				$img_origem   = $img_fonte;
				$img_destino  = imagecreatetruecolor($nova_largura,$nova_altura) or die("Cannot Initialize new GD image stream");
				
				// redimensiona
				imagecopyresampled($img_destino,$img_origem,0,0,0,0,$nova_largura,$nova_altura,$largura,$altura); 
				
				// mostra a imagem 
				//header("content-type: image/jpeg");
				
				//imagejpeg($img_fonte);
				
				if(!file_exists("fotos/institucional/pequenas")){
				 mkdir("fotos/institucional/pequenas");
				}
				
				$end_thumbs = "fotos/institucional/pequenas/".$nomeimagem.".jpg";
				imagejpeg($img_destino,$end_thumbs,80);
				
				chmod("$end_thumbs", 0755);
				
				// libera a memória 
				imagedestroy($img_fonte);
				imagedestroy($img_destino);





} else { // Não trouxe foto nenhuma

	// Seleciono no banco o caminho que devo excluir as fotos do FTP.
	$sql_verif	=	"select * from institucional_fotos where inst_fot_codigo = '$codigo_foto'";
	$res_verif	=	mysqli_query($cn, $sql_verif);
	$lin_verif	=	mysqli_fetch_array($res_verif);
	
	
$end_thumbs		=	$lin_verif['inst_fot_fotop'];
$end_foto		=	$lin_verif['inst_fot_fotog'];

} 
//====================================================================================================================================================

$sql	=	"UPDATE institucional_fotos SET 

inst_fot_fotop			= '$end_thumbs',
inst_fot_fotog			= '$end_foto'


WHERE inst_fot_codigo = '$codigo_foto'";

mysqli_query($cn, $sql);

?>

<?php
echo "<script type='text/javascript'>alert('Foto Alterada com Sucesso!', 'institucional-alterar/$item');</script>";
include "destruidor.php";
?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>