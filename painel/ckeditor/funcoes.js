window.onload = function()  {
  CKEDITOR.replace( 'editor1', {
	   toolbar: [
		 { name: 'clipboard', items : [ 'PasteText' ] },
		 { name: 'editing', items : [ 'Scayt' ] },
		 { name: 'colors', items : [ 'TextColor','BGColor' ] },
		 { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike' ] },
		 { name: 'paragraph', items : [ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
		 { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		 { name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','SpecialChar','Iframe' ] },
   		 ]} 
     );
  CKEDITOR.replace( 'editor2', {
	   toolbar: [
		 { name: 'clipboard', items : [ 'PasteText' ] },
		 { name: 'editing', items : [ 'Scayt' ] },
		 { name: 'colors', items : [ 'TextColor','BGColor' ] },
		 { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike' ] },
		 { name: 'paragraph', items : [ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
		 { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		 { name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','SpecialChar','Iframe' ] },
   		 ]} 
     );
};