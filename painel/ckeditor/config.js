/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.enterMode = CKEDITOR.ENTER_BR;
	//Neste caso, ao digitar ENTER, temos as opções:
	//CKEDITOR.ENTER_P – novo parágrafo P é criado;
	//CKEDITOR.ENTER_BR – quebra de linha com BR;
	//CKEDITOR.ENTER_DIV – novo bloco com DIV é criado.
};