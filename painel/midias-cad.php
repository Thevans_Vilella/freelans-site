﻿<?php  if(isset($_SESSION["login_ses"])) {  ?>

<div class="tit">Cadastro de Mídias</div>
<div class="sombra-tit"></div>
<div class="sub-tit"></div>

<form id="cadastro" name="form" method="post" enctype="multipart/form-data" action="midias-inserir.php">
    
    <ul>
    <li>
    <div class="dad">
    <label>Nome:</label>
    <div class="dado"><input type="text" name="nomeparceiro" id="nomeparceiro" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" class="validate[required]" autofocus/></div>
    </div>
    </li>
    
    <li>
    <div class="dad">
    <label>Link Site:</label>
    <div class="dado"><input type="text" name="site" id="site" onKeyup="vazio(this)" onBlur="vazio(this)" class="validate[required]"/></div>
    </div>
    </li>

    <div class="sub-item">A Imagem deve ter 40 x 40 pixel e ser no formato png.</div>
    <li><div class="dad-fot"><label>Foto:</label><div class="dado"><input name="arquivo" type="file" class="validate[required]" id="arquivo" onChange="validaimagemPNG();DimensaoExtensao();"/></div></div></li>
    </ul>
    
    <input type="image" src="imagens/cadastrar.png" class="cadastrar" />
</form>



<div class="sec-cadastrados">
<div class="tit-cadastrados">Mídias Sociais Cadastradas</div>
</div>

<form name="form1" method="post" action="midias-alterar-ordem.php">
	<ul id="sortable">
		<?php
        $sql		=	"select * from midias order by mid_ordem";
        $res		=	mysqli_query($cn, $sql);
        while($lin	=	mysqli_fetch_array($res))  {
        ?>
        <li class="box-cad">
        <div class="nome-admin"><?php echo $lin['mid_nome']; ?></div>
		<div class="adm-alterar-adm"><a href="midias-alterar/<?php echo $lin['mid_codigo']; ?>"><img src="imagens/altera.png" alt="empresa"></a></div>
		<div class="adm-alterar-adm"><a onClick="return confirmation()" href="midias-excluir-update.php?IteMp=<?php echo $lin['mid_codigo']; ?>"><img src="imagens/exclui.png" alt="empresa"></a></div>
        <input type="hidden" name="id[]" value="<?php echo $lin['mid_codigo']; ?>">
        </li>
        <?php } ?>
    </ul>



<div class="sombra-tit"></div>
<div class="avisa">
<input type="image" src="imagens/confirmar-alteracao.png" name="Atualizar" class="confirma-alteracao" id="Atualizar" value="Atualizar" alt="Confirmar" onclick="document.form1.submit();"/>
</div>
</form>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
