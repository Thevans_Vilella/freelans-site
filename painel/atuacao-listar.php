﻿<?php  if(isset($_SESSION["login_ses"])) {  ?>

<div class="tit">Áreas de Atuação</div>
<div class="sombra-tit"></div>
<div class="sub-tit"></div>

<form id="cadastro" method="post" action="atuacao-inserir.php">
    <div class="dad"><label>Categoria:</label>
    <div class="dado">
    <select name="categoria" class="list-menu validate[required]">
    <option value="">Selecione uma Categoria</option>
    <option value="">----------------------------</option>
    <?php
    $sql01	        =	"select * from categorias order by cat_ordem";
    $res01	        =	mysqli_query($cn, $sql01);
    while($lin01	=	mysqli_fetch_array($res01))  {
    ?>
    <option value="<?php echo $lin01['cat_codigo']; ?>"><?php echo $lin01['cat_nome']; ?></option>
    <?php } ?>
    </select>
    </div>
    </div>
    
    
    <ul>
    <li><div class="dad"><label>Nome:</label><div class="dado"><input type="text" name="nome" id="nome" class="validate[required]" autofocus/></div></div></li>
    </ul>
    
    <input type="image" src="imagens/cadastrar.png" class="cadastrar" />
</form>



<div class="sec-cadastrados">
<div class="tit-cadastrados">Áreas de Atuação Cadastradas</div>
<div class="tit-cadastrados">Categoria</div>
</div>

<?php
$sql		=	"select * from atuacao 
				INNER JOIN categorias	  	on		categorias.cat_codigo		=	atuacao.cat_codigo
				ORDER BY atua_nome";
$res		=	mysqli_query($cn, $sql);
while($lin	=	mysqli_fetch_array($res))  {
?>
<div class="box-cad">
<div class="nome-admin"><?php echo $lin['atua_nome']; ?></div>
	
	
<div class="cliente-razao">
	<div class="dadadd">
	<div class="dado">
		<form 
		  method="post" 
		  name="AtuaCategoria<?php echo $lin['atua_codigo']; ?>" 
		  onchange="document.forms['AtuaCategoria<?php echo $lin['atua_codigo']; ?>'].submit();" 
		  action="atuacao-categoria.php"
		 >
			<select name="categoria" class="list-menu validate[required]">
			<option value="<?php echo $lin['atua_codigo']; ?>/<?php echo $lin['cat_codigo']; ?>"><?php echo $lin['cat_nome']; ?></option>
			<option value="<?php echo $lin['atua_codigo']; ?>/<?php echo $lin['cat_codigo']; ?>" disabled>----------------------------</option>
			<?php
			$sql02			=	"select * from categorias ORDER BY cat_nome";
			$res02			=	mysqli_query($cn, $sql02);
			while($lin02	=	mysqli_fetch_array($res02))  {
			?>
			<option value="<?php echo $lin['atua_codigo']; ?>/<?php echo $lin02['cat_codigo']; ?>"><?php echo $lin02['cat_nome']; ?></option>
			<?php } ?>
			</select>
		</form>
	</div>
    </div>
	
	
</div>
	
	
	<form id="form1" class="adm-alterar-adm" method="post" action="atuacao-alterar">
		<input type="image" src="imagens/altera.png"/>
		<input type="hidden" name="item" value="<?php echo $lin['atua_codigo']; ?>"/>
	</form>

</div>
<?php } ?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
