<?php
session_start();
if(isset($_SESSION["login_ses"])) {
?>

<?php include "alerta/scripts.php"; ?>

<?php
include "conexao.php";
include "classes.php";

$item			=	(int) $_POST['item'];

$status			=	(int) $_POST['status'];
$plano			=	(int) $_POST['plano'];
//===================================================================================================================================================
if ( $item == "" or $status == "" or $plano == "" )  { 

	echo "<script type='text/javascript'>alert('Por Favor, Preencha os Campos Obrigatórios!', 'perfil-listar/$status');</script>";
	include "destruidor.php";
	exit() ;

} 
//===================================================================================================================================================
$sql300		=	"select * from perfil where perf_codigo = '$item'";
$res300		=	mysqli_query($cn, $sql300);
$lin300		=	mysqli_fetch_array($res300);

$pagin300	=	$lin300['perf_nome'];

$perfil		=	$lin300['perf_nome'];

//===================================================================================================================================================
$sqlcat_verif	=	"SELECT * FROM perfil_status WHERE per_sta_codigo = $status";
$rescat_verif	=	mysqli_query($cn, $sqlcat_verif);
$lincat_verif	=	mysqli_fetch_array($rescat_verif);

$stanome		=	CorrigirNomeUser($perfil);

								   
								   
if ( $status == 3 )  { 
	
		$pernome	=	$lincat_verif['per_sta_nome']." - ".$stanome;
	
	} else {
	
		$pernome	=	$stanome;
	
}

// Aqui verificamos se existe
// Outro cadastro com o mesmo nome
$res_verifica = mysqli_query($cn, "SELECT * FROM perfil WHERE perf_nome = '$pernome' and perf_codigo != $item");
echo $conta_verifica = mysqli_num_rows($res_verifica);
if ( $conta_verifica <> 0 ) {

	echo "<script type='text/javascript'>alert('Nome já existente!', 'perfil-listar/3');</script>";
	include "destruidor.php";
	exit();
}
	   
					   
								   
//===================================================================================================================================================
// Corrigindo o nome da Imagem
date_default_timezone_set('America/Sao_Paulo');
$tempo_image = md5(date("Ymd-His"));
$time_image = substr($tempo_image, 0, 5);

$namefoto 	= 	trim(CorrigirNome($pagin300)."-".$time_image);
/*==================================================================================================================================================*/ 
//================ Daqui pra baixo INSERE FOTOS NO FTP (Arquivo) =====================================================================================

if ($_FILES['arquivo']['size'] > 0) {   // Verificando Se Foi Colocado Foto para Download ---------------


				// Seleciono no banco o caminho que devo excluir as fotos do FTP.
				$sql_verif	=	"select * from perfil where perf_codigo = '$item'";
				$res_verif	=	mysqli_query($cn, $sql_verif);
				$lin_verif	=	mysqli_fetch_array($res_verif);
			
				unlink($lin_verif['perf_fotop']); // Entrar na Pasta do FTP e Excluir Foto Antiga
				unlink($lin_verif['perf_fotog']); // Entrar na Pasta do FTP e Excluir Foto Antiga


				include "verificar_extensao.php";
				
				// AGORA DEVO INSERIR FOTOS NOVAS NO FTP ---------------------
				$arq   = $_FILES['arquivo'];
				
				
				
				$arq2    = $arq['tmp_name'];
				$nome    = $arq['name'];
				$tipo    = $arq['type'];
				$tamanho = $arq['size'];
				
				
				$ptr_arq = fopen($arq2, "r");
				$lido    = fread($ptr_arq, filesize($arq2));
				$foto    = addslashes($lido);
				fclose($ptr_arq);
				
				/********SCRIPT PARA REDIMENSIONAMENTO DA IMAGEM*********/
				/********CRIANDO A IMAGEM GRANDE REDIMENSIONADA**********/
				
				// dados sobre a imagem fonte
				$img_fonte    = imagecreatefromjpeg("$arq2"); //abre a imagem (do disco para a memória))
				$largura      = imagesx($img_fonte);            //pega a largura
				$altura       = imagesy($img_fonte);            //pega a altura
				
				// dados para a nova imagem - FOTO GRANDE
				$nova_largura = 460;                            //define nova largura
				$nova_altura  = $altura*$nova_largura/$largura; //define nova altura
				$img_origem   = $img_fonte;
				$img_destino  = imagecreatetruecolor($nova_largura,$nova_altura) or die("Cannot Initialize new GD image stream");
				
				// redimensiona
				imagecopyresampled($img_destino,$img_origem,0,0,0,0,$nova_largura,$nova_altura,$largura,$altura); 
				
				// mostra a imagem 
				//header("content-type: image/jpeg");
				//imagejpeg($img_fonte);
				
				$nomeimagem = $namefoto; //gera nome do arquivo com data e hora
				
				//Verifica se o diretório existe
				if(!file_exists("fotos/perfil/grandes")){
				 mkdir("fotos/perfil/grandes");
				}
				
				$end_foto = "fotos/perfil/grandes/".$nomeimagem.".jpg";
				imagejpeg($img_destino,$end_foto,80);
				
				chmod("$end_foto", 0755);
				
				// libera a memória 
				imagedestroy($img_fonte);
				imagedestroy($img_destino);
				
				/********CRIANDO A IMAGEM THUMBS REDIMENSIONADA**********/
				$img_fonte    = imagecreatefromjpeg("$arq2"); //abre a imagem (do disco para a memória))
				$largura      = imagesx($img_fonte);            //pega a largura
				$altura       = imagesy($img_fonte);            //pega a altura
				
				// dados para a nova imagem - FOTO PEQUENA
				$nova_largura = 170;                            //define nova largura
				$nova_altura  = $altura*$nova_largura/$largura; //define nova altura
				$img_origem   = $img_fonte;
				$img_destino  = imagecreatetruecolor($nova_largura,$nova_altura) or die("Cannot Initialize new GD image stream");
				
				// redimensiona
				imagecopyresampled($img_destino,$img_origem,0,0,0,0,$nova_largura,$nova_altura,$largura,$altura); 
				
				// mostra a imagem 
				//header("content-type: image/jpeg");
				
				//imagejpeg($img_fonte);
				
				if(!file_exists("fotos/perfil/pequenas")){
				 mkdir("fotos/perfil/pequenas");
				}
				
				$end_thumbs = "fotos/perfil/pequenas/".$nomeimagem.".jpg";
				imagejpeg($img_destino,$end_thumbs,80);
				
				chmod("$end_thumbs", 0755);
				
				// libera a memória 
				imagedestroy($img_fonte);
				imagedestroy($img_destino);





} else { // Não trouxe foto nenhuma

	// Seleciono no banco o caminho que devo excluir as fotos do FTP.
	$sql_verif	=	"select * from perfil where perf_codigo = '$item'";
	$res_verif	=	mysqli_query($cn, $sql_verif);
	$lin_verif	=	mysqli_fetch_array($res_verif);
	
	

	

$end_thumbs		=	$lin_verif['perf_fotop'];
$end_foto		=	$lin_verif['perf_fotog'];

} 
//====================================================================================================================================================

$sql	=	"UPDATE perfil SET 

per_sta_codigo	  = '$status',
pla_codigo	  	  = '$plano',
perf_nome		  = '$pernome',
perf_fotop		  = '$end_thumbs',
perf_fotog		  = '$end_foto'


WHERE perf_codigo = '$item'";

mysqli_query($cn, $sql);

?>

<?php
echo "<script type='text/javascript'>alert('Alterado com Sucesso!', 'perfil-listar/$status');</script>";
include "destruidor.php";
?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>