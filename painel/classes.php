<?php
// Aqui Seleciona os Dados do Sistema ==========================================================================================
$sqlsis	   =	"SELECT * from sistema";
$ressis	   =	mysqli_query($cn, $sqlsis);
$linsis	   =	mysqli_fetch_array($ressis);	

$user_mail 	 		= 	$linsis['sis_email'];
$user_mail_res 	 	= 	$linsis['sis_email_resposta'];
$user_crip  		= 	$linsis['sis_conexao_smtp'];
$user_porta 		= 	$linsis['sis_porta_smtp'];
$user_sis  			= 	$linsis['sis_usuario_smtp'];
$pass_sis  			= 	$linsis['sis_senha_smtp'];
$serv_sis  			= 	$linsis['sis_servidor_smtp'];
//===============================================================================================================================
// Função para Evitar Invasão por Injection =====================================================================================
function anti_injection($string){ 
	$string = strtolower($string);
	$string = str_replace(array("from", "select", "insert", "delete", "where", "drop table", "show tables", "update", "join", "inner", "truncate", "create", "delimiter", "like", "applet", "object"), "", $string);
	$string = trim($string);					  //limpa espaços vazios
	//$string = strip_tags($string); 			  //tira tags html e php
	$string = addslashes($string); 				  //adiciona barras invertidas a uma string
	return $string;
	
	include "destruidor.php";
}
// ============================================================================================================================


// Função para Corrigir Meta Tag Keywords no Serviço de SEO ===================================================================
function Corrigirkeywords($string, $qtde_letras = 160){
   $p = explode(' ', $string);
	    $c = 0;
	    $cortada = '';
	 
   foreach($p as $posicao => $p1){
	   
			  if ($c<$qtde_letras && ($c+strlen($p1) <= $qtde_letras)){
				 $cortada .= ' '.$p1;
				 $c += strlen($p1)+1;
			  }else{
				 break;
			  
		}
	  
   }
   
   $string = strlen($cortada) < $qtde_letras ? $cortada.'...' : $cortada;  
   
   return trim($string, ' ');
   
   include "destruidor.php";
}
// ============================================================================================================================


// Função para Corrigir Meta Tag Descrição no Serviço de SEO ==================================================================
function CorrigirDescription($string, $qtde_letras = 160){
   $p = explode(' ', $string);
	    $c = 0;
	    $cortada = '';
	 
   foreach($p as $posicao => $p1){
	   
			  if ($c<$qtde_letras && ($c+strlen($p1) <= $qtde_letras)){
				 $cortada .= ' '.$p1;
				 $c += strlen($p1)+1;
			  }else{
				 break;
			  
		}
	  
   }
   
   $string = strlen($cortada) < $qtde_letras ? $cortada.'...' : $cortada;  
   
   return trim($string, ' ');
   
   include "destruidor.php";
}
// ============================================================================================================================

// Função para Evitar Invasão por Injection =====================================================================================
function anti_injection_Desc($string){ 
	$string = str_replace(array("from", "select", "insert", "delete", "where", "drop table", "show tables", "update", "join", "inner", "truncate", "create", "delimiter", "like", "applet", "object"), "", $string);
	$string = trim($string);					  //limpa espaços vazios
	//$string = strip_tags($string); 			  //tira tags html e php
	$string = addslashes($string); 				  //adiciona barras invertidas a uma string
	return $string;
	
	include "destruidor.php";
}
// ============================================================================================================================






// Aqui Começa Função para Corrigir Usuário Excluído ==========================================================================
function CorrigirNomeUser($string) {
	
	$string = str_replace("Excluído - ", "", $string );

	return $string; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Função para Corrigir URL de Link ===========================================================================================
function CorrigirLinkSite ($link){
	
	$link = strtolower($link);
	$link = str_replace("&", "&amp;", $link );
	$link = str_replace(" ", "", $link );
	
	
	$certf = substr($link, 0, 3);
	
	if(strlen($certf) > 1 and $certf != "htt") {
	
		$link	=	"http://".$link;
		
		}
	
	
	return trim($link);
	
	require_once("destruidor.php");

}
// ============================================================================================================================


// Função para Corrigir Vídeos ================================================================================================
function CorrigirVideo($frame){

	$separar = explode("watch?v=", $frame);
	$cont = count($separar);
	
	if ($cont <= 1) { 
		
		$frame   = $frame;
		
	} else {
	
		$parte1	 =	 $separar[0];
		$parte2	 =	 $separar[1];
		$frame   = 	 $parte2;
		
	}
	
	return trim($frame);
	
	require_once("destruidor.php");

}
// ============================================================================================================================



// Função para Corrigir Valores Finceiros =====================================================================================
function CorrigirValor ($valor){
	
	$valor	=	trim($valor);
	$valor  = 	preg_replace("/[^0-9]/", "", $valor); 
	
	if ($valor > 0) {
		$valor	=	substr($valor, 0, -2).".".substr($valor, -2);
	}

	return $valor; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Função para Corrigir Valores Finceiros =====================================================================================
function CorrigirDinheiro ($valor){
	$valor = number_format($valor, 2, ',', '.');

	return $valor;
	
	include "destruidor.php";
}


function CorrigirDinheiro2 ($valor){
	$valor = str_replace(".", "", $valor); 

	$valor = str_replace(",", ".", $valor); 

	return $valor;

}
// ============================================================================================================================





// Função para Corrigir URL de Link ===========================================================================================
function CorrigirLink ($link){
	
	$link = strtolower($link);
	$link = str_replace("&", "&amp;", $link );
	$link = str_replace(" ", "-", $link );
	
	return trim($link);
	
	include "destruidor.php";
}
// ============================================================================================================================





// Função para Corrigir Vídeos ================================================================================================
function CorrigirVídeo ($frame){
	$frame = str_replace('frameborder="0" ', "", $frame);
	$frame = preg_replace('/\s(?=\s)/', '', $frame);
	
	return trim($frame);
	
	include "destruidor.php";
}
// ============================================================================================================================





// Aqui Começa Função para Corrigir Email =====================================================================================
function ConverteEmail($string) { 

	$string = strtolower($string);
	$string = preg_replace("/[^a-zA-Z0-9-|@|.|_]/", "", $string);
	$string	= str_replace(" ", "", $string);
	$string = preg_replace('/\|{1,}/', '', $string);
	$string = preg_replace('/\.{1,}/', '.', $string);
	$string = preg_replace('/\@{1,}/', '@', $string);
	$string = preg_replace('/\-{1,}/', '-', $string);
	$string = preg_replace('/\_{1,}/', '_', $string);
	$string = preg_replace('/[\n\r\t]/', ' ', $string);
	
	if (substr($string, -1) == "." or substr($string, -1) == "@" or substr($string, -1) == "-" or substr($string, -1) == "_") { 
	
		echo "<script type='text/javascript'>alert('Email Inválido! Verifique o email digitado!!!', 'home');</script>";
		include "destruidor.php";
		exit();
	}

	return $string; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir Email =====================================================================================
function ConverteMail($string) { 

	$string = strtolower($string);
	$string = preg_replace("/[^a-zA-Z0-9-|@|.|_]/", "", $string);
	$string	= str_replace(" ", "", $string);
	$string = preg_replace('/\|{1,}/', '', $string);
	$string = preg_replace('/\.{1,}/', '.', $string);
	$string = preg_replace('/\@{1,}/', '@', $string);
	$string = preg_replace('/\-{1,}/', '-', $string);
	$string = preg_replace('/\_{1,}/', '_', $string);
	$string = preg_replace('/[\n\r\t]/', ' ', $string);
	
	if (substr($string, -1) == "." or substr($string, -1) == "@" or substr($string, -1) == "-" or substr($string, -1) == "_") { 
	?>
		<script>alert("Email Inválido! Verifique o email digitado!!!\n");</script>
		<script>window.location.href='index';</script>
		
        <?php
        include "destruidor.php";
		exit();
	}

	return $string; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir URL =======================================================================================
function CorrigirURL($url) {
	
	$url	=	trim($url); 
	$url	= 	strtolower($url);
	$url 	= 	preg_replace("/[^a-zA-Z0-9-|.|:|_]/", "", $url); 
	$url	= 	str_replace(" ", "", $url);
	$url	= 	str_replace(":", "://", $url);
	$url	= 	preg_replace('/\.{1,}/', '.', $url);
	$url	= 	preg_replace('/\:{1,}/', ':', $url);
	$url	= 	preg_replace('/\-{1,}/', '-', $url);
	$url	= 	preg_replace('/\_{1,}/', '_', $url);

	return $url; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir Domínio ===================================================================================
function CorrigirDominio($url) {
	
	$url	=	trim($url); 
	$url	= 	strtolower($url);
	$url 	= 	preg_replace("/[^a-zA-Z0-9-|.|_]/", "", $url); 
	$url	= 	str_replace(" ", "", $url);
	$url	= 	preg_replace('/\.{1,}/', '.', $url);
	$url	= 	preg_replace('/\-{1,}/', '-', $url);
	$url	= 	preg_replace('/\_{1,}/', '_', $url);

	return $url; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir Domínio ===================================================================================
function CorrigirDominioEmail($url) {
	
	$separar = explode("www.", $url);
	$parte1	 =	 $separar[0];
	$parte2	 =	 $separar[1];

	return $parte2; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir Domínio ===================================================================================
function CorrigirRedesSociais($url) {
	
	$separar = explode("www.", $url);
	$parte1	 =	 $separar[0];
	
		if($parte1 == "https://" or $parte1 == "http://") {
			
			$url	 =	$url; 
			
		} else {
			
			$url = "http://www.".$url;
			
			$url = str_replace("www.www", "www", $url);
			
			
		}

	return $url; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir Senha =====================================================================================
function CorrigirSenha($string) {
	
	$string	=	trim($string); 
	$string	= 	str_replace(" ", "", $string);

	return $string; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir Senha =====================================================================================
function CorrigirSenhaUser($string) {
	
	$string		=	trim($string); 
	$string		=	strtolower($string); 
	$string		= 	preg_replace("/[^a-zA-Z0-9-|@|!|*|#]/", "", $string); 

	return md5($string); 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir Usuário ===================================================================================
function CorrigirUseres($string) {
	
	$string		=	trim($string); 
	$string		=	strtolower($string); 
	$string		= 	preg_replace("/[^a-zA-Z0-9]/", "", $string); 

	return $string; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir Usuário ===================================================================================
function CorrigirUsuario($string) {
	
	$string		=	trim($string); 
	$string		=	strtolower($string); 
	$string		= 	preg_replace("/[^a-zA-Z0-9]/", "", $string); 

	return md5($string); 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir Números ===================================================================================
function CorrigirNumero($string) {
	
	$string	=	trim($string)*1;
	$string = 	preg_replace("/[^0-9]/", "", $string); 

	if ($string == 0 or $string == "") {
		$string		=	"s/n";
	} 
	
	return $string; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir Números ===================================================================================
function CorrigirNumeros($string) {
	
	$string = 	preg_replace("/[^0-9]/", "", $string); 

	return $string; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir Quantidade ================================================================================
function CorrigirQtde($string) {
	
	$string	=	trim($string);
	$string = 	preg_replace("/[^0-9]/", "", $string); 
	$string	=	$string * 1;
	
	if ($string > 999) { 
		$string	=	substr($string, 0, -3).".".substr($string, -3);
		$string = 	number_format($string, 3, '.', '.');
	}
	
	return $string; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir CEP =======================================================================================
function CorrigirCEP($string) {
	
	$string	=	trim($string); 
	$string = 	preg_replace("/[^0-9]/", "", $string); 
	
	$parte1	=	substr($string, 0, 5);
	$parte2	=	substr($string, 5, 3);

	$certo	=	$parte1."-".$parte2;
	
	return $certo; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir CPF =======================================================================================
function CorrigirCPF($string) {
	
	$string	=	trim($string); 
	$string = 	preg_replace("/[^0-9]/", "", $string); 
	
	$parte1	=	substr($string, 0, 3);
	$parte2	=	substr($string, 3, 3);
	$parte3	=	substr($string, 6, 3);
	$parte4	=	substr($string, -2);

	$certo	=	$parte1.".".$parte2.".".$parte3."-".$parte4;
	
	return $certo; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir CEP =======================================================================================
function CorrigirRG($string) {
	
	$string	=	trim($string); 
	$string = 	preg_replace("/[^0-9]/", "", $string); 
	
	$parte1	=	substr($string, 0, -7).".";
	$parte2	=	substr($string, -7, 3).".";
	$parte3	=	substr($string, -4, 3)."-";
	$parte4	=	substr($string, -1);

	$certo	=	$parte1.$parte2.$parte3.$parte4;
	
	return $certo; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir CEP =======================================================================================
function CorrigirCNPJ($string) {
	
	$string	=	trim($string); 
	$string = 	preg_replace("/[^0-9]/", "", $string); 
	
	$parte1	=	substr($string, 0, 2);
	$parte2	=	substr($string, 2, 3);
	$parte3	=	substr($string, 5, 3);
	$parte4	=	substr($string, 8, 4);
	$parte5	=	substr($string, -2);

	$certo	=	$parte1.".".$parte2.".".$parte3."/".$parte4."-".$parte5;
	
	return $certo; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Aqui Começa Função para Corrigir CEP =======================================================================================
function CorrigirIE($string) {
	
	$string	=	trim($string); 
	$string = 	preg_replace("/[^0-9]/", "", $string); 
	
	if($string == "") {
	
		$certo = "Isento";	
	
	} else {
	
	$string	=	trim($string); 
	$string = 	preg_replace("/[^0-9]/", "", $string); 
	
	$parte1	=	substr($string, 0, -8).".";
	$parte2	=	substr($string, -8, 3).".";
	$parte3	=	substr($string, -5, 3)."-";
	$parte4	=	substr($string, -2);

	$certo	=	$parte1.$parte2.$parte3.$parte4;
	
	return $certo; 
	
	}
	
	return $certo; 
	
	include "destruidor.php";
} 
// ============================================================================================================================





// Função para Corrigir Data =================================================================================================
function CorrigirData($data){
	
	$data = preg_replace("/[^0-9]/", "", $data);
	$data = substr($data, 0, 8);

	$dia	=	substr($data, -2);
	$mes	=	substr($data, 4, 2);
	$ano	=	substr($data, 0, 4);
	$data	=	"$dia/$mes/$ano";
	
	return $data;
	
	include "destruidor.php";
}
// ============================================================================================================================





// Função para Corrigir Data Banco de Dados ===================================================================================
function CorrigirDate ($data){
	
	$data = preg_replace("/[^0-9]/", "", $data);
	$data = substr($data, 0, 8);

	$ano	=	substr($data, -4);
	$mes	=	substr($data, 2, 2);
	$dia	=	substr($data, 0, 2);
	$data	=	"$ano-$mes-$dia";
	
	return $data;
	
	include "destruidor.php";
}
// ============================================================================================================================





// Função para Corrigir Telefone ==============================================================================================
function CorrigirTelefone($fone){

	$fone = trim($fone);
	
	$fone = preg_replace("/[^0-9\s]/", "", $fone);
	$fone = preg_replace('/\s(?=\s)/', '', $fone);
	$fone = preg_replace('/[\n\r\t]/', ' ', $fone);
	$fone = str_replace(" ", "", $fone );
	
	$fone1 = substr($fone, 0, 2);
	$fone2 = substr($fone, 2, -4);
	$fone3 = substr($fone, -4);
	
	$fone = "(".$fone1.")"." ".$fone2."-".$fone3;
	return $fone;
	
	include "destruidor.php";
}
// ============================================================================================================================





// Função para Corrigir Telefone ==============================================================================================
function CorrigirHora($string){

	$string = trim($string);
	
	$string = preg_replace("/[^0-9\s]/", "", $string);
	$string = preg_replace('/\s(?=\s)/', '', $string);
	$string = preg_replace('/[\n\r\t]/', ' ', $string);
	$string = str_replace(" ", "", $string );
	
	$time1 = substr($string, 0, 2);
	$time2 = substr($string, 2, 2);
	
	$string = $time1.":".$time2;
	return $string;
	
	include "destruidor.php";
}
// ============================================================================================================================





// Função para Corrigir Telefone ==============================================================================================
function CorrigirHoraInteira ($string){

	$string = trim($string);
	
	$string = preg_replace("/[^0-9\s]/", "", $string);
	$string = preg_replace('/\s(?=\s)/', '', $string);
	$string = preg_replace('/[\n\r\t]/', ' ', $string);
	$string = str_replace(" ", "", $string );
	
	$time1 = substr($string, 0, 2);
	$time2 = substr($string, 2, 2);
	
	$string = $time1.":".$time2.":00";
	return $string;
	
	include "destruidor.php";
}
// ============================================================================================================================





// Aqui Começa Função para Retirar Sinais Especiais da String =================================================================
function CorrigirSinais($string) {
	$string = trim($string);

	$string = preg_replace("/[[:punct:]]/", '', $string);
	$string = preg_replace('/\s(?=\s)/', '', $string);
	$string = preg_replace('/[\n\r\t]/', ' ', $string);
	
	return trim($string);
	
	include "destruidor.php";
}
// ============================================================================================================================





// Função para Corrigir Caminho da Imagem de Upload ===========================================================================
function CorrigirCaminhoPerfil ($caminho){
	
	$separar = explode("painel/", $caminho);
	$parte1	 =	 $separar[0];
	$parte2	 =	 $separar[1];
	
	$caminho	=	"$parte2";
	
	return $caminho;
	
	include "destruidor.php";
}
// ============================================================================================================================





// Função para Corrigir Caminho da Imagem de Upload ===========================================================================
function CorrigirCaminho ($caminho){
	
	$separar = explode("..", $caminho);
	$parte1	 =	 $separar[0];
	$parte2	 =	 $separar[1];
	
	$caminho	=	"$parte2";
	
	return $caminho;
	
	include "destruidor.php";
}
// ============================================================================================================================





// Aqui Começa Função para Corrigir URL =======================================================================================
function CorrigirNome($nome) {
	$result = urlencode($nome);
	$result = @iconv('UTF-8', 'ASCII//TRANSLIT', $nome);
	$result = strtolower($result);
	$result = preg_replace("/[^a-z0-9\s-]/", "", $result);
	$result = trim(preg_replace("/\s+/", " ", $result));
	$result = preg_replace("/\s/", "-", $result);
	$result = preg_replace("/[\/_|+ -]+/", '-', $result);
	return trim($result);
	
	include "destruidor.php";
}
// ============================================================================================================================






// ============================================================================================================================
//Função para calcular o próximo e o último dia útil de uma data
date_default_timezone_set('America/Sao_Paulo');

function DiaUtil($data, $saida = 'd/m/Y') {
// Converte $data em um UNIX TIMESTAMP
$timestamp = strtotime($data);

// Calcula qual o dia da semana de $data
// O resultado será um valor numérico:
// 1 -> Segunda ... 7 -> Domingo
$dia = date('N', $timestamp);


		// Se for sábado (6) ou domingo (7), calcula a próxima segunda-feira
		if ($dia >= 6) {
		$timestamp_final = $timestamp + ((5 - $dia) * 3600 * 24); // Aqui 5 Determina último dia útil antes da data atual ===== Ou 8 para indicar próximo dia útil ((5 - $dia)
		} 
		
		
		
		// Se for Segunda Feira
		if ($dia == 1) {
		$timestamp_final = $timestamp + ((-3) * 3600 * 24); // Aqui ((-3) Determina que a data volta para Sexta Feira
		}
		
		
		
		// Se for de Terça a Sexta Feira
		if ($dia >= 2 and $dia <= 5) {
		$timestamp_final = $timestamp - 1;  // Aqui se colocarmos a $timestamp - 1 ele vai mostrar a data do dia anterior
		}


	return date($saida, $timestamp_final);
	
	include "destruidor.php";
}
// ============================================================================================================================





// Função para Comparar Datas =================================================================================================
function CompararDatas($final, $inicial){ 

    // Calcula a diferença em segundos entre as datas
    $diferenca = strtotime($final) - strtotime($inicial);

    //Calcula a diferença em dias
    $dias = floor($diferenca / (60 * 60 * 24));

	return $dias;
	
	include "destruidor.php";
}
// ============================================================================================================================





// Função para Comparar Datas =================================================================================================
function ComparaData($inicial, $final){ 

list($dia_i, $mes_i, $ano_i) = explode("/", $inicial);    // Data final 
list($dia_f, $mes_f, $ano_f) = explode("/", $final);      // Data inicial 
$mk_i = mktime(0, 0, 0, $mes_i, $dia_i, $ano_i);          // obtem tempo unix no formato timestamp 
$mk_f = mktime(0, 0, 0, $mes_f, $dia_f, $ano_f);          // obtem tempo unix no formato timestamp 

$diferenca = $mk_i - $mk_f; //Acha a diferença entre as datas 

	if($diferenca > 0 ){ 

	echo "<script type='text/javascript'>alert('Data Inicial Maior que a Data Final!!!', 'home');</script>";
	include "destruidor.php";
    exit();
	}
	
	include "destruidor.php";
}
// ============================================================================================================================





// Função para Limitar Quantidade de Caracteres no Listar de Notícia ==========================================================
function CorrigirListar($string, $qtde_letras = 30){
   $p = explode(' ', $string);
	    $c = 0;
	    $cortada = '';

   foreach($p as $posicao => $p1){
	   
		  if ($c<$qtde_letras && ($c+strlen($p1) <= $qtde_letras)){
			 $cortada .= ' '.$p1;
			 $c += strlen($p1)+1;
		  }else{
			 break;
		}
	  
   }
   $string = strlen($cortada) < $qtde_letras ? $cortada/*.'...'*/ : $cortada;  
   return $string;
	
	include "destruidor.php";
}
// ============================================================================================================================





// Função para Limitar Quantidade de Caracteres no Listar de Oferta ==========================================================
function CorrigirListarPrevia($string, $qtde_letras = 60){
   $p = explode(' ', $string);
	    $c = 0;
	    $cortada = '';

   foreach($p as $posicao => $p1){
	   
		  if ($c<$qtde_letras && ($c+strlen($p1) <= $qtde_letras)){
			 $cortada .= ' '.$p1;
			 $c += strlen($p1)+1;
		  }else{
			 break;
		}
	  
   }
   $string = strlen($cortada) < $qtde_letras ? $cortada/*.'...'*/ : $cortada;  
   return $string;
	
	include "destruidor.php";
}
// ============================================================================================================================




// Aqui Começa Função para Corrigir Nome com 1ª Letra Maiúscula ===============================================================
function ConverteTexto($term, $tp) { 
    if ($tp == "1") $palavra = strtr(strtoupper($term),"àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ","ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß"); 
    elseif ($tp == "0") $palavra = strtr(strtolower($term),"ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß","àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ"); 
    
	$palavra = preg_replace('/\s(?=\s)/', '', $palavra);
	$palavra = preg_replace('/[\n\r\t]/', ' ', $palavra);
	return trim($palavra); 
	
	include "destruidor.php";
} 
// ============================================================================================================================










// Aqui Começa Função para Corrigir Nome com 1ª Letra Maiúscula ===============================================================
function ConverteItem($palavra) { 
		
		$palavra = strtolower($palavra);
		
		
		
		// Aqui Retiramos o Excesso de Espaçamento entre as palavras ==========================================================
		$palavra = preg_replace('/\s(?=\s)/', '', $palavra);
		$palavra = preg_replace('/[\n\r\t]/', ' ', $palavra);

    
	
	   // Aqui começa a parte que conta os caracteres da palavra e se tiver mais que 2 letras deixa maíúscula
	   $p = explode(' ', $palavra);
	   $c = 0;
	   $cortada = '';
	   
	   foreach($p as $posicao => $p1){ // Começa foreach
	   
		   if(mb_strlen($p1, 'utf8') <= 2 ){ 
			  $cortada .= ' '.strtolower($p1);
		   } 
		   else { 
			//$cortada .= ' '.ucwords($p1);

				// ============ Aqui Verificamos se a 1 Letra de cada palavra está maisúcula e com ascento se tiver ==============================================
						$inicia = substr($p1, 0, 2);
						$typs = preg_match('/^[a-zA-Z0-9]+/', $inicia); // Resultado 0 é igual letra com ascento === Resultado 1 é igual a letra sem ascento
				
						if ($typs == 0) {
							$inicia    = strtr(strtoupper($inicia),"àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ","ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß");
							$finaliza  = substr($p1, 2);
							$cortada  .= ' '.$inicia.$finaliza; 
							}
				
						else {
							$inicia = ucwords(substr($p1, 0, 1));
							$finaliza  = substr($p1, 1);
							$cortada  .= ' '.$inicia.$finaliza; 
							}
				// ============ Aqui Termina função que Corrigi 1 Letra de cada palavra deixando maisúcula com ou sem ascento se tiver ===========================


		   } // Termina Else ================================================================================
   
	   } // Termina foreach =================================================================================
	   
	  
	  
	   //=====================================================================================================================================================
	   $palavra = trim($cortada);  // Aqui chegou a string padrão...
	   //=====================================================================================================================================================

			
			
		// ============ Aqui Verificamos se a 1 Letra da Frase vem com Ascento para Corrigirmos ela no tamnaho maiúscula =================================
				$inicio = substr($palavra, 0, 2);
				$typ = preg_match('/^[a-zA-Z0-9]+/', $inicio); // Resultado 0 é igual letra com ascento === Resultado 1 é igual a letra sem ascento
		
				if ($typ == 0) {
					//$inicio = substr($palavra, 0, 2);
					$inicio = strtr(strtoupper($inicio),"àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ","ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß");
					$final  = substr($palavra, 2);
					$resultado = $inicio.$final; 
					}
	
				else {
					$inicio = ucwords(substr($palavra, 0, 1));
					$final  = substr($palavra, 1);
					$resultado = $inicio.$final; 
					}
		// ============ Aqui Termina função que Corrigi Maiúcula com Ascento da 1 Letra da Frase =========================================================	
		
	//=====================================================================================================================================================
			$resultado = preg_replace('/\s(?=\s)/', '', $resultado);
			$resultado = preg_replace('/[\n\r\t]/', ' ', $resultado);
			return trim($resultado); // Aqui chegado o resultado final da string tudo corrigido com acentuação...
   //=====================================================================================================================================================
	
	include "destruidor.php";
} 		
// Aqui Termina Função para Corrigir Nome com 1ª Letra Maiúscula ==============================================================










// Aqui Começa Função para Corrigir Nome com 1ª Letra Maiúscula ===============================================================
function ConvertePrevia($palavra) { 
		
		$palavra = strtolower($palavra);
		
		
		
		// Aqui Retiramos o Excesso de Espaçamento entre as palavras ==========================================================
		$palavra = preg_replace('/\s(?=\s)/', '', $palavra);
		$palavra = preg_replace('/[\n\r\t]/', ' ', $palavra);

    
	
	   // Aqui começa a parte que Enconta os Caracteres de Pontuação da String
	   $p = preg_split('/([.?!;])/', $palavra, -1 , PREG_SPLIT_DELIM_CAPTURE);
	   $p = str_replace(".", ".&nbsp;", $p );
	   $p = str_replace("!", "!&nbsp;", $p );
	   $p = str_replace("?", "?&nbsp;", $p );
	   $p = str_replace(";", ";&nbsp;", $p );
	   $c = 0;
	   $cortada = '';
	   
	   	   
	   foreach($p as $posicao => $p1){ // Começa foreach

			// ============ Aqui Verificamos se a 1 Letra de cada palavra está maisúcula e com ascento se tiver ==============================================
					$p1 	= trim($p1);
					$inicia = substr($p1, 0, 2);
					$typs 	= preg_match('/^[a-zA-Z0-9]+/', $inicia); // Resultado 0 é igual letra com ascento === Resultado 1 é igual a letra sem ascento
						
					if ($typs == 0) {
						$inicia    = trim(strtr(strtoupper($inicia),"àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ","ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß"));
						$finaliza  = substr($p1, 2);
						$cortada  .= $inicia.$finaliza;
						}
			
					else {
						$inicia = ucwords(substr($p1, 0, 1));
						$finaliza  = substr($p1, 1);
						$cortada  .= $inicia.$finaliza;
						}
			// ============ Aqui Termina função que Corrigi 1 Letra de cada palavra deixando maisúcula com ou sem ascento se tiver ===========================

   
	   } // Termina foreach =================================================================================
	   
	  
	  
	//=====================================================================================================================================================
	   		$cortada = str_replace("&nbsp;&nbsp;", " ", $cortada );
			$cortada = preg_replace('/\s(?=\s)/', '', $cortada);
			$cortada = preg_replace('/[\n\r\t]/', ' ', $cortada);
			
			if(trim(substr($cortada, -1)) == " ") {
					return trim(substr($cortada, 0, -1)); 
			} else {
					return trim($cortada); // Aqui chegado o resultado final da string tudo corrigido com acentuação... 
			}
   //=====================================================================================================================================================
	
	include "destruidor.php";
} 		
// Aqui Termina Função para Corrigir Nome com 1ª Letra Maiúscula ==============================================================
?>