﻿<?php  if(isset($_SESSION["login_ses"])) {  ?>
<?php
$item	=	(int) $_POST['item'];

$sql	=	"select * from planos where pla_codigo = '$item'";
$res	=	mysqli_query($cn, $sql);
$lin	=	mysqli_fetch_array($res);
?>

<div class="tit">Alterar Cadastro de Planos</div>
<div class="sombra-tit"></div>
<div class="sub-tit"></div>

<form id="cadastro" name="form" method="post" action="planos-alterar-update.php">
    <ul>
    <li>
    <div class="dad">
    <label>Nome:</label>
    <div class="dado"><input type="text" name="nome_servico" id="nome_servico" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" class="validate[required]" value="<?php echo $lin['pla_nome']; ?>"/></div>
    </div>
    </li>
		
    <li>
    <div class="dad">
    <label>Título:</label>
    <div class="dado"><input type="text" name="titulo" id="titulo" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" class="validate[required]" value="<?php echo $lin['pla_titulo']; ?>"/></div>
    </div>
    </li>
		
    <li>
    <div class="dad">
    <label>Valor:</label>
    <div class="dado"><input type="text" name="valor" id="dinheiro" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" value="<?php echo CorrigirDinheiro($lin['pla_valor']); ?>"/></div>
    </div>
    </li>
		
    <li>
    <div class="dad">
    <label>Quantidade de áreas de atuação:</label>
    <div class="dado"><input type="text" name="qtdareas" id="qtdareas" onKeyup="Numeros(this)" onBlur="Numeros(this)" class="validate[required]" maxlength="2"  value="<?php echo $lin['pla_qtd_atuacao']; ?>"/></div>
    </div>
    </li>
		
    <li>
    <div class="dad">
    <label>Quantidade de fotos:</label>
    <div class="dado"><input type="text" name="qtdfotos" id="qtdfotos" onKeyup="Numeros(this)" onBlur="Numeros(this)" class="validate[required]" maxlength="2"  value="<?php echo $lin['pla_qtd_fotos']; ?>"/></div>
    </div>
    </li>
		
    <li>
    <div class="dad">
    <label>Quantidade de vídeos:</label>
    <div class="dado"><input type="text" name="qtdvideos" id="qtdvideos" onKeyup="Numeros(this)" onBlur="Numeros(this)" class="validate[required]" maxlength="2" value="<?php echo $lin['pla_qtd_video']; ?>"/></div>
    </div>
    </li>
		
    <li>
    <div class="dad">
    <label>Quantidade de promoções:</label>
    <div class="dado"><input type="text" name="qtdpromo" id="qtdpromo" onKeyup="Numeros(this)" onBlur="Numeros(this)" class="validate[required]" maxlength="2" value="<?php echo $lin['pla_qtd_promo']; ?>"/></div>
    </div>
    </li>
		
    </ul>
    
    <div class="sub-item">Descrição</div>
    <div class="desc-cadastro">
    <div class="desc-cad"><textarea id="editor1" name="descricao" cols="1" rows="10"><?php echo $lin['pla_desc']; ?></textarea></div>
	</div>

<input type="image" src="imagens/alterar.png" class="cadastrar" />
<input type="hidden" name="item" size="5" value="<?php echo $item; ?>" />
</form>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
