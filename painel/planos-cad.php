﻿<?php  if(isset($_SESSION["login_ses"])) {  ?>

<div class="tit">Cadastro de Planos</div>
<div class="sombra-tit"></div>
<div class="sub-tit"></div>

<form id="cadastro" method="post" action="planos-inserir.php">
    <ul>
    <li>
    <div class="dad">
    <label>Nome:</label>
    <div class="dado"><input type="text" name="nome_servico" id="nome_servico" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" class="validate[required]" autofocus/></div>
    </div>
    </li>
		
    <li>
    <div class="dad">
    <label>Título:</label>
    <div class="dado"><input type="text" name="titulo" id="titulo" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" class="validate[required]"/></div>
    </div>
    </li>
		
    <li>
    <div class="dad">
    <label>Valor:</label>
    <div class="dado"><input type="text" name="valor" id="dinheiro" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" /></div>
    </div>
    </li>
		
    <li>
    <div class="dad">
    <label>Quantidade de áreas de atuação:</label>
    <div class="dado"><input type="text" name="qtdareas" id="qtdareas" onKeyup="Numeros(this)" onBlur="Numeros(this)" class="validate[required]" maxlength="2" /></div>
    </div>
    </li>
		
    <li>
    <div class="dad">
    <label>Quantidade de fotos:</label>
    <div class="dado"><input type="text" name="qtdfotos" id="qtdfotos" onKeyup="Numeros(this)" onBlur="Numeros(this)" class="validate[required]" maxlength="2"/></div>
    </div>
    </li>
		
    <li>
    <div class="dad">
    <label>Quantidade de vídeos:</label>
    <div class="dado"><input type="text" name="qtdvideos" id="qtdvideos" onKeyup="Numeros(this)" onBlur="Numeros(this)" class="validate[required]" maxlength="2"/></div>
    </div>
    </li>
		
    <li>
    <div class="dad">
    <label>Quantidade de promoções:</label>
    <div class="dado"><input type="text" name="qtdpromo" id="qtdpromo" onKeyup="Numeros(this)" onBlur="Numeros(this)" class="validate[required]" maxlength="2"/></div>
    </div>
    </li>
		
    </ul>
    
    <div class="sub-item">Descrição</div>
    <div class="desc-cadastro">
    <div class="desc-cad"><textarea id="editor1" name="descricao" cols="1" rows="10"></textarea></div>
	</div>
    
    <input type="image" src="imagens/cadastrar.png" class="cadastrar" />
</form>


<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
