<?php
session_start();
if(isset($_SESSION["login_ses"])) {
?>

<?php include "alerta/scripts.php"; ?>

<?php
include "conexao.php";
include "classes.php";

$item			=	(int) $_POST['item'];

$nomeparceiro	=	ConverteItem(anti_injection($_POST['nomeparceiro']));
$exep			=	ConverteTexto(anti_injection($_POST['exep']), 0);

//================================================================================================================================================================
if ( $nomeparceiro == "" or $exep == "" )  { 
?>

<?php
echo "<script type='text/javascript'>alert('Por favor, é necessário o prenchimento de todos os campos obrigatórios!', 'midias-contato-cad');</script>";
include "destruidor.php";
exit() ;

} 
//================================================================================================================================================================
if ($_FILES['arquivo']['size'] > 0) {   // Verificando Se Foi Colocado Arquivo para Download ========================================

/*==================================================================================================================================*/ 
		// Corrigindo o nome da Imagem
		date_default_timezone_set('America/Sao_Paulo');
		$tempo		= md5(date("Ymd-His"));
		$time_image = substr($tempo, 0, 3);
		
		$namefoto 	= 	CorrigirNome($nomeparceiro)."-".$time_image;
	/*==================================================================================================================================*/ 
		
		$sql1 = "select * from midias_contato where mid_cont_codigo = $item";
		$res1 = mysqli_query($cn, $sql1);
		$lin1 = mysqli_fetch_array($res1);
		
		unlink($lin1['mid_cont_foto']);
		
	/*==================================================================================================================================*/ 
		$nomedoarquivo 	= 		$namefoto; 
		$manual  	 	= 		$_FILES['arquivo'];
		$extensao    	= 		$manual['type']; 
		
		$desagrupa		=		explode("/",$extensao);
		$parte1			=		$desagrupa[0];
		$parte2			=		$desagrupa[1];
		
		if($parte2 != "png"){
		  
			echo "<script type='text/javascript'>alert('Por favor, é necessário uma imagem no formato [ PNG ]!', 'midias-contato-cad');</script>";
			include "destruidor.php";
			exit() ;
		   
		  }
		
		$destinofinal	=	"fotos/midias-contato/".$nomedoarquivo.".".$parte2;
		
	/*==================================================================================================================================*/ 
		// Movendo o arquivo com seu respectivo nome para a pasta Desejada.
		move_uploaded_file ($_FILES['arquivo'] ['tmp_name'], "$destinofinal");
	/*==================================================================================================================================*/ 
  
		$sql5	=	"UPDATE midias_contato SET 
		
		mid_cont_foto			= 	'$destinofinal'
		
		WHERE mid_cont_codigo 	= 	'$item'";
		
		mysqli_query($cn, $sql5);
	/*==================================================================================================================================*/ 

  }// Fecha IF de Verificação se Veio Foto
//================ Daqui pra baixo INSERE DADOS NO BANCO ===============================================================================

$sql	=	"UPDATE midias_contato SET 

mid_cont_nome			= '$nomeparceiro',
mid_cont_exemplo		= '$exep'

WHERE mid_cont_codigo 	= '$item'";

mysqli_query($cn, $sql);

?>

<?php
echo "<script type='text/javascript'>alert('Ítem Alterada com Sucesso!', 'midias-contato-cad');</script>";
include "destruidor.php";
?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>