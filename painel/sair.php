<?php
session_start();

  // primeiro destruímos os dados associados à sessão
  $_SESSION = array();
  
  // destruímos então o cookie relacionado a esta sessão
  if(isset($_COOKIE[session_name()])){
    setcookie(session_name(), '', time() - 1000, '/');
  }

session_destroy();
unset($_SESSION);
echo "<script>window.location.href='index.php';</script>";
?>