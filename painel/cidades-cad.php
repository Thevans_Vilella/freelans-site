﻿<?php  if(isset($_SESSION["login_ses"])) {  ?>

<div class="tit">Cadastro de Cidades</div>
<div class="sombra-tit"></div>
<div class="sub-tit"></div>

<form id="cadastro" method="post" action="cidades-inserir.php">
	<div class="dad"><label>Estado:</label>
	<div class="dado">
	<select name="estado" class="list-menu validate[required]" id="estado">
	<option value="">Selecione um Estado</option>
	<option value="">----------------------------</option>
	<?php
	$sql01	=	"select * from estado order by est_nome";
	$res01	=	mysqli_query($cn, $sql01);
	while ($lin01	=	mysqli_fetch_array($res01))  {
	?>
	<option value="<?php echo $lin01['est_codigo']; ?>"><?php echo $lin01['est_nome']; ?></option>
	<?php } ?>
	</select>
	</div>
    </div>
	
	<ul>
		<li>
		<div class="dad">
		<label>Cidade:</label>
		<div class="dado"><input type="text" name="cidade" id="cidade" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" class="validate[required]"/></div>
		</div>
		</li>
    </ul>
    
    <input type="image" src="imagens/cadastrar.png" class="cadastrar" />
</form>



<div class="sec-cadastrados">
<div class="tit-item-cadastrados">Cidades Cadastradas</div>
<div class="tit-item-cadastrados">Cidades Relacionadas Cadastradas</div>
</div>

<?php
$sql	=	"select * from cidades
			INNER JOIN estado	  	on		estado.est_codigo		=	cidades.est_codigo
			order by est_nome, cid_nome";
$res	=	mysqli_query($cn, $sql);
while($lin	=	mysqli_fetch_array($res))  {
	
	
$cidref		=	$lin['cid_codigo'];
?>
<div class="box-cad">
<div class="cliente-nome"><?php echo $lin['cid_nome']; ?>/ <?php echo $lin['est_sigla']; ?></div>

<div class="cliente-razao">
	<?php
	$sqlcid			=	"select * from cidades_relacionadas
						INNER JOIN cidades	  	on		cidades.cid_codigo		=	cidades_relacionadas.cid_codigo
						INNER JOIN estado	  	on		estado.est_codigo		=	cidades.est_codigo
						WHERE 
						cidades_relacionadas.cid_referencia = $cidref
						order by 
						cid_nome
						";
	$rescid			=	mysqli_query($cn, $sqlcid);
	while($lincid	=	mysqli_fetch_array($rescid))  {
	?>
	
		<?php echo $lincid['cid_nome']; ?>/ <?php echo $lincid['est_sigla']; ?>&nbsp;&nbsp;&nbsp;
	
	<?php } ?>
</div>


	<form class="adm-alterar-adm">&nbsp;</form>
	<form class="adm-alterar-adm">&nbsp;</form>


	<form id="form1" class="adm-alterar-adm" method="post" action="cidades-alterar">
		<input type="image" src="imagens/altera.png"/>
		<input type="hidden" name="item" value="<?php echo $lin['cid_codigo']; ?>"/>
	</form>

	<!--
	<form id="form1" class="adm-alterar-adm" method="post" action="cidades-excluir-update.php" onClick="return confirmation()">
		<input type="image" src="imagens/exclui.png"/>
		<input type="hidden" name="item" value="<?php echo $lin['cid_codigo']; ?>"/>
	</form>
	-->
</div>
<?php } ?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
