<?php
session_start();
if(isset($_SESSION["login_ses"])) {
?>

<?php include "alerta/scripts.php"; ?>

<?php
include  "conexao.php";
include  "classes.php";

$item			=	(int) $_POST['item'];

//========================= Aqui Pegamos O nome da Página para Gerar o Nome da Foto ==================================================
$sql300		=	"select * from institucional where inst_codigo = '$item'";
$res300		=	mysqli_query($cn, $sql300);
$lin300		=	mysqli_fetch_array($res300);
$pagin300	=	$lin300['inst_nome'];
//====================================================================================================================================
// Corrigindo o nome da Imagem
date_default_timezone_set('America/Sao_Paulo');
$tempo_image = md5(date("Ymd-His"));
$time_image = substr($tempo_image, 10);

$namefoto 	= 	trim(CorrigirNome($pagin300)."-".$time_image);
/*===================================================================================================================================*/ 
//========================= Daqui pra baixo INSERE FOTOS NO FTP (Arquivo) ============================================================


				if ($_FILES['arquivo']['size'] == 0) {   // Verificando Se Foi Colocado Foto para Download ---------------
				?>
				
				<?php
				 echo "<script type='text/javascript'>alert('Favor Inserir uma Imagem!', 'institucional-alterar/$item');</script>"; 
				 include "destruidor.php";
				 exit() ;
				}


				include "verificar_extensao.php";
				
				// AGORA DEVO INSERIR FOTOS NOVAS NO FTP ---------------------
				$arq   = $_FILES['arquivo'];
				
				
				$arq2    = $arq['tmp_name'];
				$nome    = $arq['name'];
				$tipo    = $arq['type'];
				$tamanho = $arq['size'];
				
				
				$ptr_arq = fopen($arq2, "r");
				$lido    = fread($ptr_arq, filesize($arq2));
				$foto    = addslashes($lido);
				fclose($ptr_arq);
				
				/********SCRIPT PARA REDIMENSIONAMENTO DA IMAGEM*********/
				/********CRIANDO A IMAGEM GRANDE REDIMENSIONADA**********/
				
				// dados sobre a imagem fonte
				$img_fonte    = imagecreatefromjpeg("$arq2"); //abre a imagem (do disco para a memória))
				$largura      = imagesx($img_fonte);            //pega a largura
				$altura       = imagesy($img_fonte);            //pega a altura
				
				// dados para a nova imagem - FOTO GRANDE
				$nova_largura = 1920;                            //define nova largura
				$nova_altura  = $altura*$nova_largura/$largura; //define nova altura
				$img_origem   = $img_fonte;
				$img_destino  = imagecreatetruecolor($nova_largura,$nova_altura) or die("Cannot Initialize new GD image stream");
				
				// redimensiona
				imagecopyresampled($img_destino,$img_origem,0,0,0,0,$nova_largura,$nova_altura,$largura,$altura); 
				
				// mostra a imagem 
				//header("content-type: image/jpeg");
				//imagejpeg($img_fonte);
				
				$nomeimagem = $namefoto; //gera nome do arquivo com data e hora
				
				//Verifica se o diretório existe
				if(!file_exists("fotos/institucional/grandes")){
				 mkdir("fotos/institucional/grandes");
				}
				
				$end_foto = "fotos/institucional/grandes/".$nomeimagem.".jpg";
				imagejpeg($img_destino,$end_foto,80);
				
				chmod("$end_foto", 0755);
				
				// libera a memória 
				imagedestroy($img_fonte);
				imagedestroy($img_destino);
				
				/********CRIANDO A IMAGEM THUMBS REDIMENSIONADA**********/
				$img_fonte    = imagecreatefromjpeg("$arq2"); //abre a imagem (do disco para a memória))
				$largura      = imagesx($img_fonte);            //pega a largura
				$altura       = imagesy($img_fonte);            //pega a altura
				
				// dados para a nova imagem - FOTO PEQUENA
				$nova_largura = 1920;                            //define nova largura
				$nova_altura  = $altura*$nova_largura/$largura; //define nova altura
				$img_origem   = $img_fonte;
				$img_destino  = imagecreatetruecolor($nova_largura,$nova_altura) or die("Cannot Initialize new GD image stream");
				
				// redimensiona
				imagecopyresampled($img_destino,$img_origem,0,0,0,0,$nova_largura,$nova_altura,$largura,$altura); 
				
				// mostra a imagem 
				//header("content-type: image/jpeg");
				
				//imagejpeg($img_fonte);
				
				if(!file_exists("fotos/institucional/pequenas")){
				 mkdir("fotos/institucional/pequenas");
				}
				
				$end_thumbs = "fotos/institucional/pequenas/".$nomeimagem.".jpg";
				imagejpeg($img_destino,$end_thumbs,80);
				
				chmod("$end_thumbs", 0755);
				
				// libera a memória 
				imagedestroy($img_fonte);
				imagedestroy($img_destino);

//============================================= Aqui termina a parte de foto =================================================================

$sql	=	"INSERT INTO institucional_fotos 
(inst_codigo, inst_fot_fotop, inst_fot_fotog) 

VALUES 
('$item', '$end_thumbs', '$end_foto')";

mysqli_query($cn, $sql);

?>

<?php
echo "<script type='text/javascript'>alert('Foto Institucional Cadastrada com Sucesso!', 'institucional-alterar/$item');</script>";
include "destruidor.php";
?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>