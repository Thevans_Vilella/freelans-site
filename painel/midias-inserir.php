<?php
session_start();
if(isset($_SESSION["login_ses"])) {
?>

<?php include "alerta/scripts.php"; ?>

<?php
include "conexao.php";
include "classes.php";


$nomeparceiro	=	ConverteItem(anti_injection($_POST['nomeparceiro']));
$site			=	anti_injection($_POST['site']);

//==================================================================================================================================================================================================
if ( $nomeparceiro == ""  or $site == "" )  { 
?>

<?php
echo "<script type='text/javascript'>alert('Por favor, é necessário o prenchimento de todos os campos obrigatórios!', 'midias-cad');</script>";
include "destruidor.php";
exit() ;

} 
//==================================================================================================================================================================================================
// Corrigindo o nome da Imagem
date_default_timezone_set('America/Sao_Paulo');
$tempo		= md5(date("Ymd-His"));
$time_image = substr($tempo, 0, 3);

$namefoto 	= 	CorrigirNome($nomeparceiro)."-".$time_image;
/*==================================================================================================================================*/ 

//====================================================================================================================================
if ($_FILES['arquivo']['size'] == 0) {   // Verificando Se Foi Colocado Arquivo para Download ========================================

	echo "<script type='text/javascript'>alert('Favor Inserir um Arquivo!!!', 'midias-cad');</script>";
	include "destruidor.php";
	exit() ;
  }
//====================================================================================================================================
?>

<?php
$nomedoarquivo 	= 		$namefoto; 
$manual  	 	= 		$_FILES['arquivo'];
$extensao    	= 		$manual['type']; 

$desagrupa		=		explode("/",$extensao);
$parte1			=		$desagrupa[0];
$parte2			=		$desagrupa[1];

if($parte2 != "png"){
  
	echo "<script type='text/javascript'>alert('Por favor, é necessário uma imagem no formato [ PNG ]!', 'midias-cad');</script>";
	include "destruidor.php";
	exit() ;
   
  }

$destinofinal	=	"fotos/midias/".$nomedoarquivo.".".$parte2;

//====================================================================================================================================
// Movendo o arquivo com seu respectivo nome para a pasta Desejada.
move_uploaded_file ($_FILES['arquivo'] ['tmp_name'], "$destinofinal");
//====================================================================================================================================

//============================================= Aqui termina a parte de foto =========================================================

$sql	=	"INSERT INTO midias
(mid_nome, mid_link, mid_foto)

VALUES 
('$nomeparceiro', '$site', '$destinofinal')";

mysqli_query($cn, $sql);

//============================================================================================================================================
?>

<?php
echo "<script type='text/javascript'>alert('Mídia Social Cadastrada Com Sucesso!', 'midias-cad');</script>";
include "destruidor.php";
?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>