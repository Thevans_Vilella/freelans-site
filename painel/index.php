<?php
session_start();
include "conexao.php"; 
?>
<?php
$sql10	=	"SELECT * from contato
INNER JOIN estado 		on 		estado.est_codigo		=	contato.est_codigo
INNER JOIN logradouro 	on 		logradouro.log_codigo	=	contato.log_codigo";
$res10	=	mysqli_query($cn, $sql10);
$lin10	=	mysqli_fetch_array($res10);	

$fone1 = substr($lin10['cont_telefone'], 0, 4);
$fone2 = substr($lin10['cont_telefone'], 5, 9);
?>
<?php
$sql11	=	"SELECT * from metas_google";
$res11	=	mysqli_query($cn, $sql11);
$lin11	=	mysqli_fetch_array($res11);	
?>
<?php
$sql12	=	"SELECT * from autor";
$res12	=	mysqli_query($cn, $sql12);
$lin12	=	mysqli_fetch_array($res12);	
?>
<!doctype html>
<html lang="pt-br">
<head>
<meta charset="UTF-8"/>
<meta name="keywords" content="<?php echo $lin10['cont_nome']; ?>, <?php echo $lin11['tag_palavras_chaves']; ?>" />
<meta name="description" content="<?php echo $lin10['cont_nome']; ?> - <?php echo $lin11['tag_descricao']; ?>" />
<meta name="geo.placename" content="<?php echo $lin10['log_nome']; ?>: <?php echo $lin10['cont_endereco']; ?>, <?php echo $lin10['cont_numero']; ?> - <?php echo $lin10['cont_bairro']; ?> - CEP: <?php echo $lin10['cont_cep']; ?> - <?php echo $lin10['cont_cidade']; ?>-<?php echo $lin10['est_sigla']; ?>"/>
<meta name="geo.region" content="BR-<?php echo $lin10['est_nome']; ?>"/>
<meta name="author" content="<?php echo $lin11['tag_autor']; ?>" />
<meta property="og:title" content="<?php echo $lin10['cont_nome']; ?>" />
<meta property="og:type" content="website"/>
<meta property="og:url" content="<?php echo $lin11['tag_url']; ?>" />
<meta property="og:site_name" content="<?php echo $lin10['cont_nome']; ?>"/>
<meta property="og:description" content="<?php echo $lin11['tag_descricao']; ?>" />
<meta property="busca:title" content="<?php echo $lin10['cont_nome']; ?> - <?php echo $lin11['tag_palavras_chaves']; ?>" />
<meta property="busca:species" content="Home" />
<meta name="viewport" content="width=1000"/>
<title>..:: <?php echo $lin10['cont_nome']; ?> ::..</title>

<link rel="stylesheet" href="css.css" type="text/css"/>
<link rel="icon" href="../assets/_img/favicon.png" type="image/x-icon"/>
<script type="text/javascript" src="for/jquery-1.7.min.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>

</head>
<body>
<div id="wrapper">
<div id="logo"><img src="imagens/logo-galanti.png" alt="<?php echo $lin10['cont_nome']; ?>"/></div>
<div id="box-login">
<form id="login" name="login" method="post" autocomplete="off" action="logando.php" onsubmit="return validarAcesso()">
<ul>
<li><label>EMAIL:</label><input type="text" name="usuario" id="usuario" autofocus/></li>
<li><label>SENHA:</label><input type="password" name="senha" id="senha"/></li>
</ul>
<div id="esqueceu"><a href="#janela" class="modal">ESQUECEU SUA SENHA?</a></div>
<input type="image" src="imagens/logar.png" class="logar" alt="Logar"/>
</form>
</div>
</div>
<?php include "rodape.php"; ?>
<?php include "resete.php"; ?>
<?php include "destruidor.php"; ?>
</body>
</html>