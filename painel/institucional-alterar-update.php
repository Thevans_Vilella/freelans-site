<?php
session_start();
if(isset($_SESSION["login_ses"])) {
?>

<?php include "alerta/scripts.php"; ?>

<?php
include "conexao.php";
include "classes.php";

$item			=	(int) $_POST['item'];

$exibir			=	(int) $_POST['exibir'];
$nome_pagina	=	ConverteItem(anti_injection($_POST['nome_pagina']));
$previa			=	ConvertePrevia(anti_injection($_POST['previa']));
$chave			=	ConverteItem(anti_injection($_POST['chave']));
$descricao		=	anti_injection_Desc($_POST['descricao']);

//===================================================================================================================================================
if ( $nome_pagina == "" or $previa == "" or $chave == "" or $descricao == "" )  { 

	echo "<script type='text/javascript'>alert('Por Favor, Preencha os Campos Obrigatórios!', 'institucional-cad');</script>";
	include "destruidor.php";
	exit() ;

} 
//===================================================================================================================================================
// Corrigindo o nome da Imagem
date_default_timezone_set('America/Sao_Paulo');
$tempo_image = md5(date("Ymd-His"));
$time_image = substr($tempo_image, 10);

$namefoto 	= 	trim(CorrigirNome($nome_pagina)."-".$time_image);
/*==================================================================================================================================================*/ 
//================ Daqui pra baixo INSERE FOTOS NO FTP (Arquivo) =====================================================================================

if ($_FILES['arquivo']['size'] > 0) {   // Verificando Se Foi Colocado Foto para Download ---------------


				// Seleciono no banco o caminho que devo excluir as fotos do FTP.
				$sql_verif	=	"select * from institucional where inst_codigo = '$item'";
				$res_verif	=	mysqli_query($cn, $sql_verif);
				$lin_verif	=	mysqli_fetch_array($res_verif);
			
				unlink($lin_verif['inst_fotop']); // Entrar na Pasta do FTP e Excluir Foto Antiga
				unlink($lin_verif['inst_fotog']); // Entrar na Pasta do FTP e Excluir Foto Antiga


				include "verificar_extensao.php";
				
				// AGORA DEVO INSERIR FOTOS NOVAS NO FTP ---------------------
				$arq   = $_FILES['arquivo'];
				
				
				
				$arq2    = $arq['tmp_name'];
				$nome    = $arq['name'];
				$tipo    = $arq['type'];
				$tamanho = $arq['size'];
				
				
				$ptr_arq = fopen($arq2, "r");
				$lido    = fread($ptr_arq, filesize($arq2));
				$foto    = addslashes($lido);
				fclose($ptr_arq);
				
				/********SCRIPT PARA REDIMENSIONAMENTO DA IMAGEM*********/
				/********CRIANDO A IMAGEM GRANDE REDIMENSIONADA**********/
				
				// dados sobre a imagem fonte
				$img_fonte    = imagecreatefromjpeg("$arq2"); //abre a imagem (do disco para a memória))
				$largura      = imagesx($img_fonte);            //pega a largura
				$altura       = imagesy($img_fonte);            //pega a altura
				
				// dados para a nova imagem - FOTO GRANDE
				$nova_largura = 1520;                            //define nova largura
				$nova_altura  = $altura*$nova_largura/$largura; //define nova altura
				$img_origem   = $img_fonte;
				$img_destino  = imagecreatetruecolor($nova_largura,$nova_altura) or die("Cannot Initialize new GD image stream");
				
				// redimensiona
				imagecopyresampled($img_destino,$img_origem,0,0,0,0,$nova_largura,$nova_altura,$largura,$altura); 
				
				// mostra a imagem 
				//header("content-type: image/jpeg");
				//imagejpeg($img_fonte);
				
				$nomeimagem = $namefoto; //gera nome do arquivo com data e hora
				
				//Verifica se o diretório existe
				if(!file_exists("fotos/institucional/grandes")){
				 mkdir("fotos/institucional/grandes");
				}
				
				$end_foto = "fotos/institucional/grandes/".$nomeimagem.".jpg";
				imagejpeg($img_destino,$end_foto,80);
				
				chmod("$end_foto", 0755);
				
				// libera a memória 
				imagedestroy($img_fonte);
				imagedestroy($img_destino);
				
				/********CRIANDO A IMAGEM THUMBS REDIMENSIONADA**********/
				$img_fonte    = imagecreatefromjpeg("$arq2"); //abre a imagem (do disco para a memória))
				$largura      = imagesx($img_fonte);            //pega a largura
				$altura       = imagesy($img_fonte);            //pega a altura
				
				// dados para a nova imagem - FOTO PEQUENA
				$nova_largura = 200;                            //define nova largura
				$nova_altura  = $altura*$nova_largura/$largura; //define nova altura
				$img_origem   = $img_fonte;
				$img_destino  = imagecreatetruecolor($nova_largura,$nova_altura) or die("Cannot Initialize new GD image stream");
				
				// redimensiona
				imagecopyresampled($img_destino,$img_origem,0,0,0,0,$nova_largura,$nova_altura,$largura,$altura); 
				
				// mostra a imagem 
				//header("content-type: image/jpeg");
				
				//imagejpeg($img_fonte);
				
				if(!file_exists("fotos/institucional/pequenas")){
				 mkdir("fotos/institucional/pequenas");
				}
				
				$end_thumbs = "fotos/institucional/pequenas/".$nomeimagem.".jpg";
				imagejpeg($img_destino,$end_thumbs,80);
				
				chmod("$end_thumbs", 0755);
				
				// libera a memória 
				imagedestroy($img_fonte);
				imagedestroy($img_destino);





} else { // Não trouxe foto nenhuma

	// Seleciono no banco o caminho que devo excluir as fotos do FTP.
	$sql_verif	=	"select * from institucional where inst_codigo = '$item'";
	$res_verif	=	mysqli_query($cn, $sql_verif);
	$lin_verif	=	mysqli_fetch_array($res_verif);
	
	

	

$end_thumbs		=	$lin_verif['inst_fotop'];
$end_foto		=	$lin_verif['inst_fotog'];

} 
//====================================================================================================================================================

$sql	=	"UPDATE institucional SET 

inst_exi_codigo			= '$exibir',
inst_nome				= '$nome_pagina',
inst_previa				= '$previa',
inst_palavras_chaves	= '$chave',
inst_descricao			= '$descricao',
inst_fotop				= '$end_thumbs',
inst_fotog				= '$end_foto'


WHERE inst_codigo = '$item'";

mysqli_query($cn, $sql);

?>

<?php
echo "<script type='text/javascript'>alert('Página Alterado com Sucesso!', 'institucional-alterar/$item');</script>";
include "destruidor.php";
?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>