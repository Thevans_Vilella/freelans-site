<?php  if (isset($_SESSION["login_ses"])) { ?>
    <div class="menu_div">
        <div class="navigation">
            <div class="menu">
                <ul class="nav">

                    <li><a>Banners</a>
                        <ul>
                            <li><a href="banner-cad">Banner</a></li>
                            <li><a href="banner-principal-cad">Banner Principal</a></li>
                        </ul>
                    </li>

                    <div class="barrinha">|</div>

                    <li><a>Perfil</a>
                        <ul>
                             
                        <li><a href="categorias-cad">Categorias de Atuação</a></li>
							
                        <li><a>Planos</a>
                             <ul>
                                <li><a href="planos-cad">Cadastro de Planos</a></li>
                                <li><a href="planos-listar">Listar Planos</a></li>
                             </ul>
                        </li>
							
						<li><a href="atuacao-listar">Áreas de Atuação</a></li>
						<li><a href="cidades-cad">Cidades de Atuação</a></li>
						<li><a href="destaques-cad">Destaques</a></li>
						<li><a href="midias-contato-cad">Redes Sociais</a></li>
						<li><a href="perfil-contato-cad">Contatos Perfil</a></li>
                             
                        </ul>
                    </li>

                    <div class="barrinha">|</div>

                    <li><a>Usuários</a>
                        <ul>
                             
							<li><a>Listar usuários por Status</a>
								 <ul>
									<?php
									$sql1150         =   "select * from perfil_status order by per_sta_ordem";
									$res1150         =   mysqli_query($cn, $sql1150);
									while($lin1150   =   mysqli_fetch_array($res1150))  {
									?>
                                	<li><a href="perfil-listar/<?php echo $lin1150['per_sta_codigo']; ?>"><?php echo $lin1150['per_sta_nome']; ?></a></li>
									<?php } ?>
								 </ul>
							</li>

							<li><a href="avaliacoes-listar">Listar Avaliações</a></li>
							<li><a href="avaliacoes-atualizar">Atualizar Avaliações</a></li>
                        </ul>
                    </li>

                    <div class="barrinha">|</div>                    
                                        

                    <li><a>Mídias</a>
                        <ul>
                            <li><a href="midias-cad">Cadastro de Mídias</a></li>
                        </ul>
                    </li>

                    <div class="barrinha">|</div>

                    <li><a>Institucional</a>
                        <ul>
                             
                        <li><a>Área de Textos</a>
                             <ul>
                                <?php
                                $sql115         =   "select * from textos order by txt_nome";
                                $res115         =   mysqli_query($cn, $sql115);
                                while ($lin115  =   mysqli_fetch_array($res115))  {
                                ?>
                                <li><a href="textos-alterar/<?php echo $lin115['txt_codigo']; ?>"><?php echo $lin115['txt_nome']; ?></a></li>
                                <?php } ?>
                             </ul>
                        </li>
                             
                        <li><a>Área Institucional</a>
                             <ul>
                                <?php
                                $sql        =   "select * from institucional order by inst_nome";
                                $res        =   mysqli_query($cn, $sql);
                                while ($lin =   mysqli_fetch_array($res))  {
                                ?>
                                <li><a href="institucional-alterar/<?php echo $lin['inst_codigo']; ?>"><?php echo $lin['inst_nome']; ?></a></li>
                                <?php } ?>
                             </ul>
                        </li>
                        <li><a href="faq-cad">Cadastro FAQ</a></li>
                        <li><a href="faq-listar">Listar FAQ</a></li>
                             
                        </ul>
                    </li>

                    <div class="barrinha">|</div>                    
                                        
                    <li><a>Relatórios</a>
                        <ul>
                            <li><a href="atuacao-busca">Busca de Atuações</a></li>
                        </ul>
                    </li>


                    <div class="barrinha">|</div>                    
                                        
                    <li><a>Tutoriais</a>
                        <ul>
                            <?php
                            $sqltut         =   "select * from tutoriais order by tut_nome";
                            $restut         =   mysqli_query($cn, $sqltut);
                            while($lintut   =   mysqli_fetch_array($restut))  {
                            ?>
                            <li><a href="tutoriais-alterar/<?php echo $lintut['tut_codigo']; ?>"><?php echo $lintut['tut_nome']; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>

                    <div class="barrinha">|</div>                    
                                        
                    <li><a>Empresa</a>
                        <ul>
                            <li><a href="empresa-dados-alterar">Dados da Empresa</a></li>
                            <li><a href="administrador-cad">Cadastro de Administrador</a></li>
                            <li><a href="gerar-lista-emails">Gerar Lista de Emails</a></li>
                            <li><a href="upload-cad" target="_blank">Cadastro de Imagens para Upload</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </div>
<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>