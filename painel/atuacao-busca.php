﻿<?php  if (isset($_SESSION["login_ses"])) { ?>

<div class="tit">Relatório de Busca de Áreas de Atuação</div>
<div class="sombra-tit"></div>
<div class="sub-tit">Selecione a data inicial e a data final da busca.</div>

<form id="cadastro" method="post" action="atuacao-busca-dados">
    <ul>
    <li><div class="dad"><label>Data Inicial:</label><div class="dado"><input type="text" name="inicial" id="datepicker" class="validate[required]" maxlength="10"/></div></div></li>
    <li><div class="dad"><label>Data Final:</label><div class="dado"><input type="text" name="final" id="datesicker" class="validate[required]" maxlength="10"/></div></div></li>
  	</ul>
  
    <input type="image" src="imagens/buscar.png" class="cadastrar" />
</form>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
