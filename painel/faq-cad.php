﻿<?php  if(isset($_SESSION["login_ses"])) { ?>

<div class="tit">
	Cadastro de FAQ
</div>
<div class="sombra-tit"></div>

<form id="cadastro" method="post" action="faq-inserir.php">
    <ul>
    	<li>
    		<div class="dad">
    			<label>Pergunta:</label>
    			<div class="dado">
    				<input type="text" name="pergunta" class="validate[required]" autofocus/>
    			</div>
    		</div>
    	</li>
    </ul>
    
    <div class="sub-item">Resposta</div>
    <div class="desc-cadastro">
    	<div class="desc-cad"><textarea id="editor1" name="descricao" cols="1" rows="10" class="validate[required]"></textarea></div>
	</div>

    <input type="image" src="imagens/cadastrar.png" class="cadastrar" />
</form>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>