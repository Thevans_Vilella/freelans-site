﻿<?php  if(isset($_SESSION["login_ses"])) {  ?>

<div class="tit">Cadastro de Página Institucional</div>
<div class="sombra-tit"></div>

<form id="cadastro" name="form" method="post" enctype="multipart/form-data" action="institucional-inserir.php">
    <div class="dad"><label>Exibir Foto:</label>
    <div class="dado">
    <select name="exibir" class="list-menu">
    <?php
    $sql03	=	"select * from institucional_exibir order by inst_exi_codigo desc";
    $res03	=	mysqli_query($cn, $sql03);
    while ($lin03	=	mysqli_fetch_array($res03))  {
    ?>
    <option value="<?php echo $lin03['inst_exi_codigo']; ?>"><?php echo $lin03['inst_exi_nome']; ?></option>
    <?php } ?>
    </select>
    </div>
    </div>
    
    <ul>
    <li>
    <div class="dad">
    <label>Nome:</label>
    <div class="dado"><input type="text" name="nome_pagina" id="nome_pagina" class="validate[required]" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" /></div>
    </div>
    </li>

    <div class="sub-item">O Campo Prévia deve conter 160 Caracteres</div>
    <li>
    <div class="dad">
    <label>Prévia:</label>
    <div class="dado"><input type="text" name="previa" id="previa" class="validate[required]" onKeyup="CaixaBaixa(this),excesso(this)" onBlur="CaixaBaixa(this),vazio(this),excesso(this)" maxlength="160"/></div>
    </div>
    </li>

    <div class="sub-item">O limite de caracteres no campo palavras chaves é de 180 caracteres. Lembrando que, as palavras devem ser separadas por vírgula.</div>
    <li>
    <div class="dad">
    <label>Palavras Chaves:</label>
    <div class="dado"><input type="text" name="chave" id="chave" class="validate[required]" onKeyup="KeyChaves(this),excesso(this)" onBlur="KeyChaves(this),vazio(this),excesso(this)" maxlength="180"/></div>
    </div>
    </li>

    <div class="sub-item">A Imagem deve ter 600 x 450 pixel no formato jpg.</div>
    <li><div class="dad-fot"><label>Foto:</label><div class="dado"><input name="arquivo" type="file" class="validate[required]" id="arquivo" onChange="validaimagem();"/></div></div></li>
    </ul>
    
    <div class="sub-item">Descrição</div>
    <div class="desc-cadastro">
    <div class="desc-cad"><textarea id="editor1" name="descricao" cols="1" rows="10"></textarea></div>
	</div>
    
    <input type="image" src="imagens/cadastrar.png" class="cadastrar" />
</form>



<div class="sec-cadastrados">
<div class="tit-cadastrados">Páginas Cadastradas</div>
</div>

<?php
$sql	=	"select * from institucional order by inst_nome";
$res	=	mysqli_query($cn, $sql);
while ($lin	=	mysqli_fetch_array($res))  {
?>
<div class="box-cad">
<div class="nome-admin"><?php echo $lin['inst_nome']; ?></div>
<div class="adm-alterar-adm"><a href="institucional-alterar/<?php echo $lin['inst_codigo']; ?>"><img src="imagens/altera.png" alt="empresa"></a></div>
<form id="form1" class="adm-alterar-adm" method="post" action="institucional-excluir-update.php" onClick="return confirmation()">
    <input type="image" src="imagens/exclui.png"/>
    <input type="hidden" name="item" value="<?php echo $lin['inst_codigo']; ?>"/>
</form>
</div>
<?php } ?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
