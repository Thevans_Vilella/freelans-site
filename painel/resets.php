<?php
include "conexao.php"; 
include "classes.php"; 
?>
<?php
$sql10	=	"SELECT * from contato
INNER JOIN estado on estado.est_codigo	=	contato.est_codigo";
$res10	=	mysqli_query($cn, $sql10);
$lin10	=	mysqli_fetch_array($res10);	

$fone1 = substr($lin10['cont_telefone'], 0, 4);
$fone2 = substr($lin10['cont_telefone'], 5, 9);
?>
<?php
$sql11	=	"SELECT * from metas_google";
$res11	=	mysqli_query($cn, $sql11);
$lin11	=	mysqli_fetch_array($res11);	
$meturl	=	$lin11['tag_url'];
?>
<?php
$sql12	=	"SELECT * from autor";
$res12	=	mysqli_query($cn, $sql12);
$lin12	=	mysqli_fetch_array($res12);	
?>
<?php
$useres				=	CorrigirUseres(anti_injection($_GET['CodigAbriR']));
$codaltera			=	CorrigirUseres(anti_injection($_GET['altera']));
$dataclialtera		=	CorrigirUseres(anti_injection($_GET['date']));
$horaclialtera		=	CorrigirUseres(anti_injection($_GET['time']));
$data 				= 	date("Y-m-d");
$hora 				= 	strtotime(date("H:i:s"));
$ipuser				=	$_SERVER["REMOTE_ADDR"];
//===============================================================================================================
$sql88		=	"SELECT * FROM administrador where adm_login = '$useres' and adm_cod_altera = '$codaltera'";
$res88		=	mysqli_query($cn, $sql88);
$cnt88		=	mysqli_num_rows($res88);
$lin88		=	mysqli_fetch_array($res88);

$user_identifica	=	$lin88['adm_codigo'];
$user_login			=	$lin88['adm_login'];
$cod_alterar		=	$lin88['adm_cod_altera'];
$data_cliente		=	md5($lin88['adm_data']);
$hora_cliente		=	md5($lin88['adm_hora']);
$ip_alterar			=	$lin88['adm_ip'];

// Calculando Tempo de Validade do Token
$hora_inicia		=	strtotime($lin88['adm_hora']);
$hora_expira		=	date('H:i:s', strtotime("+5 minute",strtotime($lin88['adm_hora'])));
$hora_expirou		=	strtotime($hora_expira);
?>
<!doctype html>
<html lang="pt-br">
<head>
<meta charset="UTF-8"/>
<meta name="keywords" content="<?php echo $lin10['cont_nome']; ?>, <?php echo $lin11['tag_palavras_chaves']; ?>" />
<meta name="description" content="<?php echo $lin10['cont_nome']; ?> - <?php echo $lin11['tag_descricao']; ?>" />
<meta name="geo.placename" content="<?php echo $lin10['cont_nome']; ?> - <?php echo $lin10['cont_endereco']; ?>, <?php echo $lin10['cont_bairro']; ?>, <?php echo $lin10['cont_cidade']; ?>/<?php echo $lin10['est_sigla']; ?>, CEP: <?php echo $lin10['cont_cep'];?>"/>
<meta name="geo.region" content="BR-<?php echo $lin10['est_nome']; ?>"/>
<meta name="author" content="<?php echo $lin11['tag_autor']; ?>" />
<meta property="og:title" content="<?php echo $lin10['cont_nome']; ?>" />
<meta property="og:type" content="website"/>
<meta property="og:url" content="<?php echo $lin11['tag_url']; ?>" />
<meta property="og:site_name" content="<?php echo $lin10['cont_nome']; ?>"/>
<meta property="og:description" content="<?php echo $lin11['tag_descricao']; ?>" />
<meta property="busca:title" content="<?php echo $lin10['cont_nome']; ?> - <?php echo $lin11['tag_palavras_chaves']; ?>" />
<meta property="busca:species" content="Home" />
<meta name="viewport" content="width=1000"/>
<base href="<?php echo $lin11['tag_url']; ?>/painel/" />
<title>..:: <?php echo $lin10['cont_nome']; ?> ::..</title>

<link rel="stylesheet" href="css.css" type="text/css"/>
<link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-png"/>
<link rel="stylesheet" type="text/css" media="all" href="for/css.css" />
<script type="text/javascript" src="for/jquery-1.7.min.js"></script>
<script type="text/javascript" src="for/jquery.validationEngine.js"></script>
<script type="text/javascript" src="for/jquery.validationEngine-pt.js"></script>
<script type="text/javascript" src="js/reset.js"></script>

</head>
<body>
<?php if(
		$ipuser == $ip_alterar and 
		$useres == $user_login and 
		$dataclialtera == $data_cliente and 
		$horaclialtera == $hora_cliente and 
		$codaltera == $cod_alterar and 
		$data == $lin88['adm_data'] and 
		$hora < $hora_expirou and 
		$hora > $hora_inicia
		) {
// Se os Dados de Verificação Bater Faz Abaixo ===================================================================
?>
<div id="wrapper">
<div id="logo"><img src="imagens/logo-galanti.png" alt="<?php echo $lin10['cont_nome']; ?>"/></div>
<div id="box-loga">
<div class="txt">
<strong><span style="color:#FF0000">Instru&ccedil;&otilde;es para nova senha:</span></strong><br />
Deve conter de 8 a 20 caracteres<br />
Deve iniciar com uma letra<br />
Pode conter @ ! * - #<br />
Não pode ser igual ao usu&aacute;rio<br />
</div>
<form id="login" name="form" method="post" autocomplete="off" action="sends.php" onsubmit="return ConfereResetSe()">
<ul>
<li><label>SENHA:</label><input type="password" name="sesn" id="sesn" onKeyup="ResetSe(this)" onBlur="ResetSes(this)" class="validate[required]" maxlength="20" autofocus/></li>
<li><label>REPETIR SENHA:</label><input type="password" name="sern" id="sern" onKeyup="ResetSe(this)" onBlur="ResetSes(this)" class="validate[required]" maxlength="20"/></li>
</ul>
<input type="image" src="imagens/cadastrar-senha.png" class="logar" alt="Logar"/>
<input type="hidden" name="user" size="5" value="<?php echo $user_identifica; ?>" />
</form>
</div>
</div>
<?php } else { // Se os Dados de Verificação Não Bater Faz Abaixo =================================================================== ?>
<?php echo "<script>window.location.href='$meturl/painel';</script>"; ?>
<?php } // Termina Verificação Aqui ================================================================================================= ?>
<?php include "rodape.php"; ?>
<?php include "destruidor.php"; ?>
</body>
</html>