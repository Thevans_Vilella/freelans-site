﻿<?php  if(isset($_SESSION["login_ses"])) {  ?>

<div class="tit">Avaliações de Usuários Cadastradas</div>
<div class="sombra-tit"></div>


<div id="itemContainer">
<?php
$sql		=	"select * from avaliacoes_usuario 
				INNER JOIN perfil	  	on		perfil.perf_codigo		=	avaliacoes_usuario.perf_codigo
				ORDER BY 
				ava_user_data_created DESC, 
				ava_user_hora_created DESC
				LIMIT 0, 50
				";
$res		=	mysqli_query($cn, $sql);
while($lin	=	mysqli_fetch_array($res))  {
?>
<div class="box-cad">
<div class="cliente-nome"><?php echo CorrigirListar($lin['perf_nome']); ?></div>
<div class="cliente-nome"><?php echo CorrigirListar($lin['ava_user_descricao']); ?></div>
<div class="fat-ano"><?php echo CorrigirData($lin['ava_user_data_created']); ?></div>
<div class="fat-ano"><?php echo CorrigirHora($lin['ava_user_hora_created']); ?></div>

	<form id="form1" class="adm-alterar-adm" method="post" action="avaliacoes-excluir-update.php">
		<input type="image" src="imagens/exclui.png"/>
		<input type="hidden" name="item" value="<?php echo $lin['ava_user_codigo']; ?>"/>
		<input type="hidden" name="perfcod" value="<?php echo $lin['perf_cod_ref']; ?>"/>
	</form>

</div>
<?php } ?>

	
</div>
<div class="holder"></div>
<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
