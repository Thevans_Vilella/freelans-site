﻿<?php  if(isset($_SESSION["login_ses"])) {  ?>

<?php
$banner		=	(int) $_POST['banner'];

$sql	=	"select * from banner_principal where ban_prin_codigo = '$banner'";
$res	=	mysqli_query($cn, $sql);
$lin	=	mysqli_fetch_array($res);
?>

<div class="tit">Alterar Cadastro de Banner Principal</div>
<div class="sombra-tit"></div>

<form id="cadastro" name="form" method="post" enctype="multipart/form-data" action="banner-principal-alterar-update.php">
    <ul>
    <li>
    <div class="dad">
    <label>Link:</label>
    <div class="dado"><input type="text" name="site" id="site" onKeyup="vazio(this),excesso(this)" onBlur="vazio(this),excesso(this)" value="<?php echo $lin['ban_prin_site']; ?>"/></div>
    </div>
    </li>

    <div class="sub-item">A Imagem deve ter 1425 x 420 pixel no formato jpg.</div>
    <li><div class="dad-fot"><label>Foto Desktop:</label><div class="dado"><input name="arquivo" type="file" id="arquivo" onChange="validaimagemdesk();"/></div></div></li>

    <div class="sub-item">A Imagem deve ter 770 x 250 pixel no formato jpg.</div>
    <li><div class="dad-fot"><label>Tablet:</label><div class="dado"><input name="arquivotab" type="file" id="arquivotab" onChange="validaimagemtablet();"/></div></div></li>

    <div class="sub-item">A Imagem deve ter 400 x 150 pixel no formato jpg.</div>
    <li><div class="dad-fot"><label>Mobile:</label><div class="dado"><input name="arquivomob" type="file" id="arquivomob" onChange="validaimagemmobile();"/></div></div></li>
    </ul>
    
    <input type="image" src="imagens/alterar.png" class="cadastrar" />
    <input type="hidden" name="banner" size="5" value="<?php echo $banner; ?>" />
</form>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
