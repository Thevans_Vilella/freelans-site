<?php
session_start();
if(isset($_SESSION["login_ses"])) {
?>

<?php include "alerta/scripts.php"; ?>

<?php
include "conexao.php";

echo $item		=	(int) $_POST['item'];
$perfcod	=	(int) $_POST['perfcod'];

//================================================================================================================================================================
if ( $item == "" or $perfcod == "" )  { 
?>

<?php
echo "<script type='text/javascript'>alert('Por favor, é necessário o prenchimento de todos os campos obrigatórios!', 'avaliacoes-listar');</script>";
include "destruidor.php";
exit() ;

} 
//================================================================================================================================================================

$sql02	=	"DELETE FROM avaliacoes_usuario WHERE ava_user_codigo = '$item'";
mysqli_query($cn, $sql02);

//===========================================================================================================================================================
// Aqui verificamos quantos votos o prestador já teve.

$presatend		= 	0;	
$presquali		= 	0;	
$prespontu		= 	0;	
$presprec		= 	0;	
$presagili		= 	0;	
$medgeral		= 	0;	
	
$sqlqtde		=	"select * from avaliacoes_usuario WHERE perf_cod_ref = '$perfcod'";
$resqtde		=	mysqli_query($cn, $sqlqtde);
$cntqtde		=	mysqli_num_rows($resqtde);
while($linvqtde	=	mysqli_fetch_array($resqtde)) {
	
	// Aqui encontramos os valores no banco de dados e somamos
	$presatend	=	$presatend + $linvqtde['ava_user_atendimento'];
	$presquali	=	$presquali + $linvqtde['ava_user_qualidadade'];
	$prespontu	=	$prespontu + $linvqtde['ava_user_pontualidade'];
	$presprec	=	$presprec + $linvqtde['ava_user_preco'];
	$presagili	=	$presagili + $linvqtde['ava_user_agilidade'];
	$medgeral	=	$medgeral + $linvqtde['ava_user_media_estrelas'];
	
}

$qtdvotos	=	$cntqtde;
$medgeral	=	$medgeral  / $qtdvotos;
$presatend	=	$presatend / $qtdvotos;
$presquali	=	$presquali / $qtdvotos;
$prespontu	=	$prespontu / $qtdvotos;
$presprec	=	$presprec  / $qtdvotos;
$presagili	=	$presagili / $qtdvotos;

$medgeral	=	(int)$medgeral;
$presatend	=	(int)$presatend;
$presquali	=	(int)$presquali;
$prespontu	=	(int)$prespontu;
$presprec	=	(int)$presprec;
$presagili	=	(int)$presagili;

//===========================================================================================================================================================

$sqlconf	=	"select * from avaliacoes_usuario WHERE perf_cod_ref = '$perfcod'";
$resconf	=	mysqli_query($cn, $sqlconf);
$cntconf	=	mysqli_num_rows($resconf);
$linconf	=	mysqli_fetch_array($resconf);
							   
								   
   if($cntconf == 0){ 
	   
	$qtdvotnovo		=	"0";
    $medgeralnovo	=	"0";
	$presatendnovo	=	"0";
	$presqualinovo	=	"0";
	$prespontunovo	=	"0";
	$presprecnovo	=	"0";
	$presagilinovo	=	"0";
	   
   } else {
	   
	$qtdvotnovo		=	$cntqtde; 
    $medgeralnovo	=	$medgeral;
	$presatendnovo	=	$presatend;
	$presqualinovo	=	$presquali;
	$prespontunovo	=	$prespontu;
	$presprecnovo	=	$presprec;
	$presagilinovo	=	$presagili;
	   
   }
	
//===========================================================================================================================================================

		$sqlvot		=	"UPDATE avaliacoes SET
		
		ava_atendimento		= 	'$presatendnovo',
		ava_qualidadade		= 	'$presqualinovo',
		ava_pontualidade	= 	'$prespontunovo',
		ava_preco			= 	'$presprecnovo',
		ava_agilidade		= 	'$presagilinovo',
		ava_media_estrelas	= 	'$medgeralnovo',
		ava_qtde_votos		= 	'$qtdvotnovo'

		WHERE perf_codigo 	= '$perfcod'";

		mysqli_query($cn, $sqlvot);

//===========================================================================================================================================================
								   
?>

<?php
echo "<script type='text/javascript'>alert('Excluído com Sucesso!', 'avaliacoes-listar');</script>";
include "destruidor.php";
?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>