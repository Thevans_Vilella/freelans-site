<?php
session_start();
include "conexao.php";
include "classes.php"; 

$pg = isset($_GET['IniSeSsiAdm']) ? $_GET['IniSeSsiAdm'] : "home";
$pagina = "$pg.php";
if ( !file_exists($pagina) ) $pagina = "erro.php";
?>
<?php include "padrao.php"; ?>
<!doctype html>
<html lang="pt-br">
<head>
<meta charset="utf-8"/>
<meta name="keywords" content="<?php echo $lin10['cont_nome']; ?>, <?php echo $lin11['tag_palavras_chaves']; ?>" />
<meta name="description" content="<?php echo $lin10['cont_nome']; ?> - <?php echo $lin11['tag_descricao']; ?>" />
<meta name="geo.placename" content="<?php echo $lin10['log_nome']; ?>: <?php echo $lin10['cont_endereco']; ?>, <?php echo $lin10['cont_numero']; ?> - <?php echo $lin10['cont_bairro']; ?> - CEP: <?php echo $lin10['cont_cep']; ?> - <?php echo $lin10['cont_cidade']; ?>-<?php echo $lin10['est_sigla']; ?>"/>
<meta name="geo.region" content="BR-<?php echo $lin10['est_nome']; ?>"/>
<meta name="author" content="<?php echo $lin11['tag_autor']; ?>" />
<meta property="og:title" content="<?php echo $lin10['cont_nome']; ?>" />
<meta property="og:type" content="website"/>
<meta property="og:url" content="<?php echo $lin11['tag_url']; ?>" />
<meta property="og:site_name" content="<?php echo $lin10['cont_nome']; ?>"/>
<meta property="og:description" content="<?php echo $lin11['tag_descricao']; ?>" />
<meta property="busca:title" content="<?php echo $lin10['cont_nome']; ?> - <?php echo $lin11['tag_palavras_chaves']; ?>" />
<meta property="busca:species" content="Home" />
<meta name="viewport" content="width=1000"/>
<base href="<?php echo $lin11['tag_url']; ?>/painel/" />
<title>..:: <?php echo $lin10['cont_nome']; ?> ::..</title>

<link rel="icon" href="../assets/_img/favicon.png" type="image/x-icon"/>

<link rel="stylesheet" type="text/css" href="botoes/estilos.css"/>
<link rel="stylesheet" type="text/css" href="estilos.css" />

<link rel="stylesheet" type="text/css" media="all" href="for/css.css" />
<script type="text/javascript" src="for/jquery-1.7.min.js"></script>
<script type="text/javascript" src="for/jquery.validationEngine.js"></script>
<script type="text/javascript" src="for/jquery.validationEngine-pt.js"></script>

<link rel="stylesheet" href="data/css/jquery-ui.css" />
<script type="text/javascript" src="data/js/jquery-ui.js"></script>
<script>
$(function() {
	$( "#datepicker" ).datepicker();
	$( "#datesicker" ).datepicker();
	$( ".dateficker" ).datepicker();
});
</script>

<link rel="stylesheet" type="text/css" media="all" href="ordenar/css.css" />
<!--<script type="text/javascript" src="ordenar/jquery-ui.js"></script>-->
<script type="text/javascript">
$(function() {
	$( "#sortable" ).sortable();
	$( "#sortable" ).disableSelection();
	
	$( "#sortable2" ).sortable();
	$( "#sortable2" ).disableSelection();	
});
</script>

<link rel="stylesheet" href="pages/css/jPages.css">
<script type="text/javascript" src="pages/js/highlight.pack.js"></script>
<script type="text/javascript" src="pages/js/tabifier.js"></script>
<script type="text/javascript" src="pages/js/js.js"></script>
<script type="text/javascript" src="pages/js/jPages.js"></script>
<script type="text/javascript">
  $(function() {
	$("div.holder").jPages({
		containerID: "itemContainer"
	});
  });
</script>


<script type="text/javascript" src="js/jquery.filestyle.js"></script>
<script type="text/javascript" src="js/jquery.maskMoney.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.abre').click(function(){
		$(this).parent().find('div.acorde').slideToggle("slow");
	});
});
</script>

<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="ckeditor/funcoes.js"></script>
<script type="text/javascript" src="js/ajax.js"></script>

<link rel="stylesheet" href="shadowbox/style.css" type="text/css" />
<link rel="stylesheet" href="shadowbox/assinatura.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="shadowbox/shadowbox.css" />
<script type="text/javascript" src="shadowbox/shadowbox.js"></script>
<script type="text/javascript"> 
Shadowbox.init({ 
handleOversize: "drag", 
modal: true 
}); 
</script> 

</head>
<body>
<?php  if (isset($_SESSION["login_ses"])) { ?>
<div id="wrapper">
<header>
<div id="topo">
<?php include "autor.php"; ?>
<div id="logo"><a href="home"><img src="imagens/logo.png" alt="<?php echo $lin10['cont_nome']; ?>"/></a></div>
<div id="controle">
<div id="painel">PAINEL DE CONTROLE <span class="hifen">-</span></div>
<div id="sair"><a href="sair.php"></a></div>
</div>
<?php include "utilizando.php"; ?>
<?php include "botoes-tudo.php"; ?>
</div>
</header>
<div id="conteudo"><?php include $pagina; ?></div>
</div>
<?php include "rodape.php"; ?>
<?php include "destruidor.php"; ?>
<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>
</body>
</html>
