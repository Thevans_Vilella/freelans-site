<?php

if(!isset($_POST['formrelacionada'])) { require_once("alerta.php"); }


if(isset($_POST['city'])) { 
	
	$city	=	(int)$_POST['city'];
	
}

if(isset($_POST['cat'])) { 
	
	$cat	=	(int)$_POST['cat'];
	
}

if(isset($_POST['dest'])) { 
	
	$string 	= 	'"' . implode('","', $_POST['dest']) . '"';
	$result 	= 	count($_POST['dest']);
	
}


if(($city != "" and $cat == "") and (!isset($_POST['dest']))) { 
	$tipbusca = "1"; // Só trouxe cidade
}

if(($city != "" and $cat != "") and (!isset($_POST['dest']))) { 
	$tipbusca = "2"; // Trouxe cidade e hastag
}

if(($city != "" and $cat == "") and (isset($_POST['dest']))) { 
	$tipbusca = "3"; // Trouxe cidade e destaques
}

if(($city != "" and $cat != "") and (isset($_POST['dest']))) { 
	$tipbusca = "4"; // Trouxe cidade, hastag e destaques
}



if(($city == "" and $cat != "") and (!isset($_POST['dest']))) { 
	$tipbusca = "5"; // Só trouxe hastag
}


if(($city == "" and $cat != "") and (isset($_POST['dest']))) { 
	$tipbusca = "6"; // Trouxe hastag e destaques
}


if(($city == "" and $cat == "") and (isset($_POST['dest']))) { 
	$tipbusca = 7; // Só trouxe destaques
}

$sqlcity	=	"select * from cidades WHERE cid_codigo = $city";
$rescity	=	mysqli_query($cn, $sqlcity);
$lincity	=	mysqli_fetch_array($rescity);
		

?>
<div class="boxfull">
	<div class="titfxtop">Busca de profissional: <span class="titfxtop-sub"></span></div>

		<div class="boxfull cinza">
			<div class="blg">
				
				<?php require_once("banner-busca.php"); ?>


				
		<div class="bl-prof">
			<div id="itemContainer">
				
				<div class="titcity">Cidade Buscada: <span class="titfxtop-sub"><?php echo $lincity['cid_nome']; ?></span></div>
				
				
				<?php 
                switch($tipbusca){
                
				case "1"; // Só trouxe cidade 
					
					$sql99			=	"select * from perfil
										INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
										INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
										INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
										INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
										WHERE 
										cidades.cid_codigo = $city
										AND
										per_sta_codigo = 1
										AND
										per_tip_codigo = 2
										ORDER BY
										pla_ordem DESC, ava_media_estrelas DESC, perf_nome
										";
				break;
					
				
				case "2"; // Trouxe cidade e hastag 
					
					$sql99		=	"select * from atuacao_perfil
									INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	atuacao_perfil.atua_codigo
									INNER JOIN perfil	  		on		perfil.perf_codigo			=	atuacao_perfil.perf_codigo
									INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
									INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
									INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
									INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
									WHERE 
									cidades.cid_codigo = $city
									AND
									atuacao_perfil.atua_codigo = $cat
									AND
									per_sta_codigo = 1
									AND
									per_tip_codigo = 2
									ORDER BY
									pla_ordem DESC, ava_media_estrelas DESC, perf_nome
									";

                break;
					
				
				case "3"; // Trouxe cidade e destaques 
					
							$sql99				=	"select * from perfil_destaques 
													INNER JOIN destaques	  	on		destaques.dest_codigo		=	perfil_destaques.dest_codigo
													INNER JOIN perfil	  		on		perfil.perf_codigo			=	perfil_destaques.perf_codigo
													INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
													INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
													INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
													INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
													WHERE 
													perfil_destaques.dest_codigo IN ($string) 
													AND 
													perfil.cid_codigo = $city
													AND
													per_sta_codigo = 1
													AND
													per_tip_codigo = 2
													GROUP BY
													perfil_destaques.perf_codigo 
													ORDER BY
													pla_ordem DESC, ava_media_estrelas DESC, perf_nome
													";
					
				break;
					
				
				
				
				case "4"; // Trouxe cidade, hastag e destaques
					
					$sql99				=	"select * from atuacao_perfil 
											INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	atuacao_perfil.atua_codigo
											INNER JOIN perfil	  		on		perfil.perf_codigo			=	atuacao_perfil.perf_codigo
											INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
											INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
											INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
											INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
											WHERE 
											perfil.cid_codigo = $city
											AND
											atuacao_perfil.atua_codigo = $cat
											AND
											per_sta_codigo = 1
											AND
											per_tip_codigo = 2
											ORDER BY
											pla_ordem DESC, ava_media_estrelas DESC, perf_nome
											";

                break;
					
				
				
				case "5"; // Trouxe hastag 
					
							$sql99		=	"select * from atuacao_perfil
											INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	atuacao_perfil.atua_codigo
											INNER JOIN perfil	  		on		perfil.perf_codigo			=	atuacao_perfil.perf_codigo
											INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
											INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
											INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
											INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
											WHERE 
											atuacao_perfil.atua_codigo = $cat
											AND
											per_sta_codigo = 1
											AND
											per_tip_codigo = 2
											ORDER BY
											pla_ordem DESC, ava_media_estrelas DESC, perf_nome
											";
				break;
				
				
				
		
				
				case "6"; // Trouxe hastag e destaques 
					
							$sql99		=	"select * from atuacao_perfil
											INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	atuacao_perfil.atua_codigo
											INNER JOIN perfil	  		on		perfil.perf_codigo			=	atuacao_perfil.perf_codigo
											INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
											INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
											INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
											INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
											WHERE 
											atuacao_perfil.atua_codigo = $cat
											AND
											per_sta_codigo = 1
											AND
											per_tip_codigo = 2
											ORDER BY
											pla_ordem DESC, ava_media_estrelas DESC, perf_nome
											";
				
				break;
				
				
				
				
				case "7"; // Trouxe destaques 
				   
						$sql99				=	"select * from perfil_destaques 
												INNER JOIN destaques	  	on		destaques.dest_codigo		=	perfil_destaques.dest_codigo
												INNER JOIN perfil	  		on		perfil.perf_codigo			=	perfil_destaques.perf_codigo
												INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
												INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
												INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
												INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
												WHERE 
												perfil_destaques.dest_codigo IN ($string) 
												AND
												per_sta_codigo = 1
												AND
												per_tip_codigo = 2
												GROUP BY
												perfil_destaques.perf_codigo
												ORDER BY
												pla_ordem DESC, ava_media_estrelas DESC, perf_nome
												";
					
				break;
                        
            } // Termina Switch que verifica $tipbusca
				
					
					$res99			=	mysqli_query($cn, $sql99);
					$cnt99			=	mysqli_num_rows($res99);
					while($lin99	=	mysqli_fetch_array($res99))  {

						$perfcode	=	$lin99['perf_codigo'];


									if( $tipbusca == "3" or $tipbusca == "4" or $tipbusca == "6" or $tipbusca == "7" ) { // Trouxe destaques 	

											$sqldest			=	"select * from perfil_destaques 
																	WHERE 
																	dest_codigo IN ($string) 
																	AND
																	perf_codigo = $perfcode
																	";
											$resdest			=	mysqli_query($cn, $sqldest);
											$cntdest			=	mysqli_num_rows($resdest);
											$lindest			=	mysqli_fetch_array($resdest);

									}
						
						
					$sqlaval	=	"select * from avaliacoes WHERE perf_codigo = '$perfcode'";
					$resaval	=	mysqli_query($cn, $sqlaval);
					$linaval	=	mysqli_fetch_array($resaval);
						
						
						
					if($lin99['pla_codigo'] == 4) {
						
						$clas	=	"boxit";
						
						} else {
						
						$clas	=	"boxit-dest";
						
					}
						
						
					if($lin99['opc_codigo'] == 2) {
						
						$classfot	=	"boxit-fot";
						$classplain	=	"boxit-tipo";
						$nomeplain	=	ConverteTexto($lin99['pla_nome'], 1);
						
						} else {
						
						$classfot	=	"boxit-fotss";
						$classplain	=	"";
						$nomeplain	=	"";
						
					}
						
						
						
						

				?>
				
					<?php if( 
								$tipbusca == "3" and $result == $cntdest 
								or $tipbusca == "4" and $result == $cntdest 
								or $tipbusca == "6" and $result == $cntdest 
								or $tipbusca == "7" and $result == $cntdest 
							) { // Trazem destaques na busca
					?>
					<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
						<div class="<?php echo $clas; ?>">
							<a href="perfil/<?php echo $lin99['perf_codigo']; ?>/<?php echo CorrigirNome($lin99['perf_nome']); ?>" target="_blank">
								<div class="<?php echo $classplain; ?>"><?php echo $nomeplain; ?></div>
								<div class="<?php echo $classfot; ?>">
									<img src="painel/<?php echo $lin99['perf_fotop']; ?>" alt="Favoritos"/>
								</div>
								<div class="boxit-tit"><?php echo $lin99['perf_nome']; ?></div>
								<div class="boxit-esp"><?php echo $lin99['cat_nome']; ?></div>
								<div class="boxit-estrela">
									<?php 
										for($contador = 1; $contador <= 5; $contador++) {

												if($contador <= $linaval['ava_media_estrelas']){

													echo "<i class='fas fa-star estyellow'></i>";

												} else {

													echo "<i class='fas fa-star'></i>";

												}

										}
									?>
								</div>
								<div class="boxit-nota">(<?php echo $linaval['ava_qtde_votos']; ?>)</div>
							</a>	
						</div>
					</div>
					<?php } ?>
				
					<?php if( $tipbusca == "1" or $tipbusca == "2" or $tipbusca == "5" ) { ?>
					<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
						<div class="<?php echo $clas; ?>">
							<a href="perfil/<?php echo $lin99['perf_codigo']; ?>/<?php echo CorrigirNome($lin99['perf_nome']); ?>" target="_blank">
								<div class="<?php echo $classplain; ?>"><?php echo $nomeplain; ?></div>
								<div class="<?php echo $classfot; ?>">
									<img src="painel/<?php echo $lin99['perf_fotop']; ?>" alt="Favoritos"/>
								</div>
								<div class="boxit-tit"><?php echo $lin99['perf_nome']; ?></div>
								<div class="boxit-esp"><?php echo $lin99['cat_nome']; ?></div>
								<div class="boxit-estrela">
									<?php 
										for($contador = 1; $contador <= 5; $contador++) {

												if($contador <= $linaval['ava_media_estrelas']){

													echo "<i class='fas fa-star estyellow'></i>";

												} else {

													echo "<i class='fas fa-star'></i>";

												}

										}
									?>
								</div>
								<div class="boxit-nota">(<?php echo $linaval['ava_qtde_votos']; ?>)</div>
							</a>	
						</div>
					</div>
					<?php } ?>

					<?php } // Final do While de repetição ?>

				
			</div><!-- paginação -->
			<div class="holder"></div>
		</div>
				
	
	
	<div class="vazio"></div>
</div>