<?php 

if(!isset($_GET['CodigAbriR'])) { require_once("alerta.php"); }
	
$item		=	$_GET['CodigAbriR'];

$sqluser	=	"select * from perfil where perf_codigo = $item";
$resuser	=	mysqli_query($cn, $sqluser);
$linuser	=	mysqli_fetch_array($resuser);	

$perfcod	=	$linuser['perf_codigo'];
					
					
$sqlavaluser	=	"select * from avaliacoes_usuario WHERE perf_cod_ref = '$perfcod'";
$resavaluser	=	mysqli_query($cn, $sqlavaluser);
$cntavaluser	=	mysqli_num_rows($resavaluser);
$linavaluser	=	mysqli_fetch_array($resavaluser);	


$sqlaval	=	"select * from avaliacoes WHERE perf_codigo = '$perfcod'";
$resaval	=	mysqli_query($cn, $sqlaval);
$linaval	=	mysqli_fetch_array($resaval);	



$sqlcat_verif_perf		=	"select * from promocao 
							INNER JOIN perfil	  		on		perfil.perf_codigo			=	promocao.perf_codigo
							INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
							INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	promocao.atua_codigo
							WHERE perfil.perf_codigo = $item";
$rescat_verif_perf		=	mysqli_query($cn, $sqlcat_verif_perf);
$contacat_verif_perf	=	mysqli_num_rows($rescat_verif_perf);
$lincat_verif_perf		=	mysqli_fetch_array($rescat_verif_perf);

	if($contacat_verif_perf == 0) {
		
		$class	=	"";
		
	} else {
		
		$class	=	"black";

	}

?>
<div class="boxfull">
	<div class="perftop cinza-escuro">
		Você está em: <span class="titfxtop-sub">Promoções <?php echo $linuser['perf_nome']; ?></span>
		<div class="mobil">CLIQUE NO MENU ABAIXO PARA MAIS INFORMAÇÕES DESSE PERFIL: <br/><span class="titfxtop-sub"><?php echo $linuser['perf_nome']; ?></span></div>
		<?php require_once("botoes-perfil-prestador.php"); ?>
	</div>

	
		<div class="boxfull <?php echo $class; ?>">
			<div class="blg">
				
				
				<?php if($contacat_verif_perf == 0) { // Se não tiver promoção faz isso ?>
				<div class="bl ">
					<div class="titfxdest-meio">Não existem promoções cadastradas para este prestador neste momento.</div>
					<div class="subtititem-meio"></div>
				</div>
				
				<?php }  else { // Se tiver promoção faz abaixo. ?>
				
				
				<div class="blpromo">

						<?php
						$sql01			=	"select * from promocao 
											INNER JOIN perfil	  		on		perfil.perf_codigo			=	promocao.perf_codigo
											INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
											INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	promocao.atua_codigo
											WHERE perfil.perf_codigo = $item
											ORDER BY 
											pro_desconto DESC";
						$res01			=	mysqli_query($cn, $sql01);
						while($lin01	=	mysqli_fetch_array($res01))  {
						?>
						<div class="grdGRL-4 grdDSK-6 grdSMPr-12">
							<div class="cx-promocao">
								<div class="promo-fot" style="background-image: url(painel/<?php echo $lin01['pro_fotog']; ?>)">
									<a href="perfil/<?php echo $lin01['perf_codigo']; ?>/<?php echo CorrigirNome($lin01['perf_nome']); ?>">
										<div class="promo-mask"></div>
										<!--<div class="boxit-tipo">PREMIUM</div>-->
										<div class="promo-name"><?php echo $lin01['perf_nome']; ?></div>
										<div class="promo-name-serv"><?php echo $lin01['cat_nome']; ?></div>
										<div class="boxit-estrela-promo">
											<?php 
												for($contador = 1; $contador <= 5; $contador++) {

														if($contador <= $linaval['ava_media_estrelas']){

															echo "<i class='fas fa-star estyellow'></i>";

														} else {

															echo "<i class='fas fa-star'></i>";

														}

												}
											?>
										</div>
										<div class="boxit-promo-nota">(<?php echo $cntavaluser; ?>)</div>
										<div class="boxit-promo-love"><i class="fas fa-heart estred"></i></div>
									</a>
								</div>
								<div class="promo-desc"><?php echo $lin01['pro_desconto']; ?>% <span class="promo-desconto">DESCONTO</span></div>
								<div class="bx-dad-tit-promo">
									<div class="promo-tit-serv"><?php echo $lin01['atua_nome']; ?></div>
									<div class="promo-desc-serv"><?php echo $lin01['pro_descricao']; ?></div>
								</div>
							</div>
						</div>
						<?php } ?>



				</div><!-- bl -->
				<?php } // Termina IF que verifica se tem promoção  ?>
			
			</div>
		</div>


	
</div>