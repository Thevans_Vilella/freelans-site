<div class="boxfull">
	<div class="titfxtop">Conheça nossos: <span class="titfxtop-sub">Profissionais</span></div>

		<div class="boxfull cinza">
			<div class="blg">

				<?php require_once("banner.php"); ?>
				<?php require_once("busca-avancada.php"); ?>
	
				
		<!--
		<div class="bl">
			<div class="titfxdest-meio">Todos os profissionais</div>
			<div class="subtititem-meio"></div>
		</div>
		-->


	
		<div class="bl-prof">
			<div id="itemContainer">
				<?php 
				$sql		=	"select * from perfil
								INNER JOIN planos		  	on		planos.pla_codigo			=	perfil.pla_codigo
								INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
								INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
								WHERE 
								per_tip_codigo = 2
								AND 
								per_sta_codigo = 1
								ORDER BY 
								ava_media_estrelas DESC, pla_ordem, perf_nome";
				$res		=	mysqli_query($cn, $sql);
				while($lin	=	mysqli_fetch_array($res))  {
					
					$perfavacod	=	$lin['perf_codigo'];
					
					
					$sqlavaluser	=	"select * from avaliacoes_usuario WHERE perf_cod_ref = '$perfavacod'";
					$resavaluser	=	mysqli_query($cn, $sqlavaluser);
					$cntavaluser	=	mysqli_num_rows($resavaluser);
					$linavaluser	=	mysqli_fetch_array($resavaluser);	
					
					
					$sqlaval	=	"select * from avaliacoes WHERE perf_codigo = '$perfavacod'";
					$resaval	=	mysqli_query($cn, $sqlaval);
					$linaval	=	mysqli_fetch_array($resaval);	
				?>
				<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
					<div class="boxit">
						<a href="perfil/<?php echo $lin['perf_codigo']; ?>/<?php echo CorrigirNome($lin['perf_nome']); ?>">
							<div class="boxit-fot">
								<img src="painel/<?php echo $lin['perf_fotop']; ?>" alt="Favoritos"/>
								<!--<div class="boxit-tipo">PREMIUM</div>-->
							</div>
							<div class="boxit-tit"><?php echo $lin['perf_nome']; ?></div>
							<div class="boxit-esp"><?php echo $lin['cat_nome']; ?></div>
							<div class="boxit-estrela">
								<?php 
									for($contador = 1; $contador <= 5; $contador++) {

											if($contador <= $linaval['ava_media_estrelas']){

												echo "<i class='fas fa-star estyellow'></i>";

											} else {

												echo "<i class='fas fa-star'></i>";

											}

									}
					
									if($cntavaluser > 0){ 
										$cntavaluser	=	"(".$cntavaluser.")"; 
									} else {
										$cntavaluser	=	""; 
									}
					
								?>
							</div>
							<div class="boxit-nota"><?php echo $cntavaluser; ?></div>
						</a>	
					</div>
				</div>
				<?php } ?>

				
			</div><!-- paginação -->
			<div class="holder"></div>
		</div>


	
	
	<div class="vazio"></div>
</div>