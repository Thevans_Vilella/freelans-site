<section id="login-message">
    <div class="container no-padding-left-right">
        <div class="row align-items-center justify-content-center no-margin-left-right">
            <p id="message">Faça <a href="login">login</a> e veja ofertas incríveis escolhidas só para você</p>
        </div>
    </div>
</section>