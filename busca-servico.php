<?php

if(!isset($_POST['servico'])) { require_once("alerta.php"); }

$item		=	$_POST['servico'];

$sqlcli		=	"select * from atuacao where atua_codigo = $item";
$rescli		=	mysqli_query($cn, $sqlcli);
$lincli		=	mysqli_fetch_array($rescli);	

$atuaname	=	$lincli['atua_nome'];


?>
<div class="boxfull">
	<div class="titfxtop">Busca de profissional por: <span class="titfxtop-sub"><?php echo $atuaname; ?></span></div>

		<div class="boxfull cinza">
			<div class="blg">
				
				<?php require_once("banner.php"); ?>
				<?php require_once("busca-avancada.php"); ?>

		<!--
		<div class="bl">
			<div class="titfxdest-meio">Profissionais Encontrados</div>
			<div class="subtititem-meio"></div>
		</div>
		-->


				
		<div class="bl-prof">
			<div id="itemContainer">
				<?php 
				$sql		=	"select * from atuacao_perfil
								INNER JOIN perfil	  		on		perfil.perf_codigo			=	atuacao_perfil.perf_codigo
								INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
								where 
								atua_codigo = $item
								and
								per_sta_codigo = 1
								ORDER BY 
								ava_media_estrelas DESC, perf_nome";
				$res		=	mysqli_query($cn, $sql);
				while($lin	=	mysqli_fetch_array($res))  {
					
				$perfcodig	=	$lin['perf_codigo'];	
					
					$sqlavaluser	=	"select * from avaliacoes_usuario WHERE perf_cod_ref = '$perfcodig'";
					$resavaluser	=	mysqli_query($cn, $sqlavaluser);
					$cntavaluser	=	mysqli_num_rows($resavaluser);
					$linavaluser	=	mysqli_fetch_array($resavaluser);	


					$sqlaval	=	"select * from avaliacoes WHERE perf_codigo = '$perfcodig'";
					$resaval	=	mysqli_query($cn, $sqlaval);
					$linaval	=	mysqli_fetch_array($resaval);	
				?>
				<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
					<div class="boxit">
						<a href="perfil/<?php echo $lin['perf_codigo']; ?>/<?php echo CorrigirNome($lin['perf_nome']); ?>" target="_blank">
							<div class="boxit-fot">
								<img src="painel/<?php echo $lin['perf_fotop']; ?>" alt="Favoritos"/>
								<!--<div class="boxit-tipo">PREMIUM</div>-->
							</div>
							<div class="boxit-tit"><?php echo $lin['perf_nome']; ?></div>
							<div class="boxit-esp"><?php echo $atuaname; ?></div>
							<div class="boxit-estrela">
								<?php 
									for($contador = 1; $contador <= 5; $contador++) {

											if($contador <= $linaval['ava_media_estrelas']){

												echo "<i class='fas fa-star estyellow'></i>";

											} else {

												echo "<i class='fas fa-star'></i>";

											}

									}
								?>
							</div>
							<div class="boxit-nota">(<?php echo $cntavaluser; ?>)</div>
						</a>	
					</div>
				</div>
				<?php } ?>

				
			</div><!-- paginação -->
			<div class="holder"></div>
		</div>


	
	
	<div class="vazio"></div>
</div>