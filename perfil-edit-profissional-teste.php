<?php  if(isset($_SESSION["login_ses"]) and ($_SESSION["status_ses"] == "1") and ($_SESSION["tipo_ses"] == "2")) { ?>
<?php 

	$user	=	$_SESSION["login_ses"];

	$sqluser	=	"select * from perfil
					INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
					INNER JOIN estado	  		on		estado.est_codigo			=	cidades.est_codigo
					INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
					INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
					where perf_login = '$user'";
	$resuser	=	mysqli_query($cn, $sqluser);
	$linuser	=	mysqli_fetch_array($resuser);	

	$perfcod	=	$linuser['perf_codigo'];
	$catcod		=	$linuser['cat_codigo'];
	$perfcat	=	$linuser['cat_nome'];
	$placod		=	$linuser['pla_codigo'];
	$qtdarea	=	$linuser['pla_qtd_atuacao'];
	$qtdfotos	=	$linuser['pla_qtd_fotos'];
	$qtdvideo	=	$linuser['pla_qtd_video'];

	
	
	$sqldsec	=	"select * from perfil_sobre where perf_codigo = '$perfcod'";
	$resdsec	=	mysqli_query($cn, $sqldsec);
	$lindsec	=	mysqli_fetch_array($resdsec);	

	$sqlcat_verif_perf		=	"SELECT * FROM atuacao_perfil WHERE perf_codigo = '$perfcod'";
	$rescat_verif_perf		=	mysqli_query($cn, $sqlcat_verif_perf);
	$contacat_verif_perf	=	mysqli_num_rows($rescat_verif_perf);
	$lincat_verif_perf		=	mysqli_fetch_array($rescat_verif_perf);
	
	$sqlaval	=	"select * from avaliacoes WHERE perf_codigo = '$perfcod'";
	$resaval	=	mysqli_query($cn, $sqlaval);
	$linaval	=	mysqli_fetch_array($resaval);	
	
	

	$sql79		=	"SELECT * from textos WHERE txt_codigo = '17'";
	$res79		=	mysqli_query($cn, $sql79);
	$lin79		=	mysqli_fetch_array($res79);
	
	
	
	
// Verificando se existem dados adicionais cadastrados em planos pagos anteriormente
// =====================================================================================================================================================
$sql69		=	"SELECT * from textos WHERE txt_codigo = '16'";
$res69		=	mysqli_query($cn, $sql69);
$lin69		=	mysqli_fetch_array($res69);

$sql3000		=	"SELECT * FROM atuacao_perfil WHERE perf_codigo = '$perfcod'";
$res3000		=	mysqli_query($cn, $sql3000);
$cnt3000		=	mysqli_num_rows($res3000);
$lin3000		=	mysqli_fetch_array($res3000);

	
$sql3010		=	"select * from perfil_fotos where perf_codigo = $perfcod";
$res3010		=	mysqli_query($cn, $sql3010);
$cnt3010		=	mysqli_num_rows($res3010);
$lin3010		=	mysqli_fetch_array($res3010);

	
$sql3020		=	"select * from perfil_videos where perf_codigo = $perfcod";
$res3020		=	mysqli_query($cn, $sql3020);
$cnt3020		=	mysqli_num_rows($res3020);
$lin3020		=	mysqli_fetch_array($res3020);
	
	if($cnt3000 > $qtdarea or $cnt3010 > $qtdfotos or $cnt3020 > $qtdvideo) {
		
		$alert	=	"1";
		
	} else {
		
		$alert	=	"2";
	
}
		
// =====================================================================================================================================================

?>

<div class="boxfull">
	<div class="titfxtop">Meu perfil: <span class="titfxtop-sub"><?php echo $linuser['perf_nome']; ?></span></div>

	
		<div class="boxfull cinza">
			<div class="blg perf-bloco">

				
				<?php if($alert == "1") { ?>
					<div class="blalert">
						<?php echo $lin69['txt_descricao']; ?>
						
						<div class="altplannov"><a href="alterar-plano" target="_blank">Alterar para Premium</a></div>
					</div>
				<?php } ?>
				
				

				
				<?php if($placod == "5") { ?>
					<div class="blalertpre">
						<?php echo $lin79['txt_descricao']; ?>
					</div>
				<?php } ?>
				
				
				
				<div class="grdGRL-4 grdDSK-6 grdTBLp-12">
				<div class="bloco-perfil">
					<div class="perfil-cx-foto">
						<div class="perfil-foto-grande"  style="background-image: url(painel/<?php echo $linuser['perf_fotog']; ?>);"></div>
						<div class="perfil-foto-mask"></div>
						<div class="perfil-foto-mini"><img src="painel/<?php echo $linuser['perf_fotop']; ?>" alt=""/></div>
							<div class="icons-avaliacoes">
								<div class="icons-avalia-stars">
									<i class="fas fa-star estyellow"></i>
									<i class="fas fa-star estyellow"></i>
									<i class="fas fa-star estyellow"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>	
								<div class="qtdavalia"><?php echo $linaval['ava_qtde_votos']; ?> avaliações</div>
								<div class="perf-nome"><?php echo $linuser['perf_nome']; ?></div>
								<div class="perf-funcao"><?php echo ConverteTexto($linuser['cat_nome'], 1); ?></div>
							</div>						
					</div>

					
						<div class="bl-atua">
							<div class="bl-atua-tit">DADOS CADASTRAIS</div>
							<div class="bl-atua-linha"></div>

							<form class="cadastro" id="cadastro1" method="post" enctype="multipart/form-data"  action="perfil-edit-profissional-altera-foto.php">
								<ul>
									<li>
										<div class="dad">
											<label>CADASTRE SUA FOTO DE PERFIL: 460 x 330 pixel jpg.</label>
											<div class="dad-fot">
												<input type="file" id="upload" name="arquivo" onChange="this.form.submit()">
												<label class ="new-button" for="upload">Buscar Foto</label>
											</div>
										</div>
									</li>
									
									<div class="contnome">
										Cidade: 
										<div class="contitem"><?php echo $linuser['cid_nome']; ?>/<?php echo $linuser['est_sigla']; ?></div>
									</div>
									
									<div class="contnome">
										Nome: 
										<div class="contitem"><?php echo $linuser['perf_nome']; ?></div>
									</div>
									
									<div class="contnome">
										Email: 
										<div class="contitem"><?php echo $linuser['perf_email']; ?></div>
									</div>
									
									<div class="contnome">
										Telefone: 
										<div class="contitem"><?php echo $linuser['perf_telefone']; ?></div>
									</div>
									
									<div class="contnome">
										Celular: 
										<div class="contitem"><?php echo $linuser['perf_celular']; ?></div>
									</div>

								<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
							</form>
							
							<div class="altpoup"><a href="#ex4" rel="modal:open">Alterar Dados Cadastrais</a></div>
							
						</div>

					
					
					<div class="bl-atua">
						<div class="bl-atua-tit">ÁREA DE ATUAÇÃO</div>
						<div class="bl-atua-mais-palavras"><a>Você pode Cadastrar até <?php echo $linuser['pla_qtd_atuacao']; ?> palavras</a></div>
						<div class="bl-atua-linha"></div>
						
						<div class="bl-perf-avalia-link">
							<a data-fancybox href="assets/_img/tut1.jpeg">
								Veja o passo a passo de como fazer
							</a>
						</div>
						<?php
						$sql02			=	"select * from atuacao_perfil 
											INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	atuacao_perfil.atua_codigo
											WHERE 
											perf_codigo = $perfcod 
											ORDER BY
											atua_nome";
						$res02			=	mysqli_query($cn, $sql02);
						while($lin02	=	mysqli_fetch_array($res02))  {
							
						$atuacat	=	$lin02['atua_nome'];
							
							
						$sql01		=	"select * from categorias WHERE cat_nome = '$atuacat'";
						$res01		=	mysqli_query($cn, $sql01);
						$conta01	=	mysqli_num_rows($res01);
						$lin01		=	mysqli_fetch_array($res01);
						?>
							<div class="bl-atua-area">&#8226; <?php echo $lin02['atua_nome']; ?></div>
							<?php if ( $atuacat != $perfcat ) { ?>
							<form method="post" action="perfil-edit-profissional-atua.php">
								<input type="submit" class="closex" value="x" />
								<input type="hidden" name="area" size="5" value="<?php echo $lin02['atua_codigo']; ?>" />
								<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
							</form>
							<?php } ?>
						<?php } ?>

						
						<?php if($contacat_verif_perf >= $qtdarea and $placod == 4) { ?>
						<div class="contnome">Para adicionar mais áreas de atuação é necessário mudar para o perfil Premium</div>
						<div class="altplannov"><a href="alterar-plano" target="_blank">Alterar para Premium</a></div>
						<?php } else { ?>
						<div class="altpoup"><a href="#ex2" rel="modal:open">Alterar Áreas</a></div>
						<?php } ?>
					</div>
					
					
					<div class="bl-atua">
						<div class="bl-atua-tit">DESTAQUE</div>
						<div class="bl-atua-linha"></div>
						
						<form name="cadastro"  class="cadastro" method="post" action="perfil-edit-destaque.php">
								<?php
								$sql999			=	"select * from destaques order by dest_ordem";
								$res999			=	mysqli_query($cn, $sql999);
								while($lin999	=	mysqli_fetch_array($res999))  {

								$destcob		=	$lin999['dest_codigo'];
								?>
									<?php
									$sql77	=	"select * from perfil_destaques where dest_codigo = '$destcob' and perf_codigo = '$perfcod'";
									$res77	=	mysqli_query($cn, $sql77);
									$cnt77	=	mysqli_num_rows($res77);
									$lin77	=	mysqli_fetch_array($res77);
									?>
										<div class="bl-atua-destaque">
											<?php if($cnt77 >0) { ?>
												<input type="checkbox" name="modulo[]" id="modulo[]" checked value="<?php echo $destcob; ?>" />
											<?php } else { ?>
												<input type="checkbox" name="modulo[]" id="modulo[]" value="<?php echo $destcob; ?>" />
											<?php } ?>
												<?php echo $lin999['dest_nome']; ?>
										</div>
								<?php } ?>

							<input type="submit" class="regdados" value="Cadastrar Destaque" />
							<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
						</form>
					</div>
					
					
				</div>
				</div>
				
				
				
				<div class="grdGRL-4 grdDSK-6 grdTBLp-12">
				<div class="bloco-perfil">
					<div class="bl-atua-bloco">
						<div class="bl-atua-tit">SOBRE</div>
						<div class="bl-atua-linha"></div>
						<div class="bl-atua-txt">
							<form name="cadastro" class="cadastro" method="post" action="perfil-edit-profissional-sobre.php">
								<ul>
									<li>
										<div class="dad">
											<label>Fale um pouco sobre seu trabalho:</label>
											<textarea name="descricao" cols="1" rows="10" placeholder="Conte-nos um pouco sobre seus trabalhos! Ex: Experiências profissionais, tempo de trabalho, cursos etc..."><?php echo $lindsec['perf_sob_descricao']; ?></textarea>
										</div>
									</li>
								</ul>

								<input type="submit" class="regdados" value="Salvar" />
								<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
							</form>
						</div>
					</div>
					
					
					
					<div class="bl-atua-bloco">
						<div class="bl-atua-tit">CONTATOS</div>
						<div class="bl-atua-linha"></div>
						
						<form name="cadastro" class="cadastro" method="post" action="perfil-edit-contato.php">
							<?php
							$sql999			=	"select * from perfil_contato order by per_cont_ordem";
							$res999			=	mysqli_query($cn, $sql999);
							while($lin999	=	mysqli_fetch_array($res999))  {

							$destcob		=	$lin999['per_cont_codigo'];


								$sql77	=	"select * from perfil_contato_dados where per_cont_codigo = '$destcob' and perf_codigo = '$perfcod'";
								$res77	=	mysqli_query($cn, $sql77);
								$cnt77	=	mysqli_num_rows($res77);
								$lin77	=	mysqli_fetch_array($res77);
							?>
								<div class="contnome">
									<?php echo $lin999['per_cont_nome']; ?>: 
									<div class="contitem"><?php echo $lin77['perf_cont_dad_item']; ?></div>
								</div>
							<?php } ?>
						</form>	

						<div class="altpoup"><a href="#ex1" rel="modal:open">Alterar Contato</a></div>
						
					</div>
					
					
				
					<div class="bl-atua-bloco">
						<div class="bl-atua-tit">REDES SOCIAIS</div>
						<div class="bl-atua-linha"></div>
							
						<div class="bl-perf-avalia-link">
							<a data-fancybox href="assets/_img/tut3.jpg">
								Veja o passo a passo de como fazer
							</a>
						</div>
						
						<form name="cadastro" class="cadastro" method="post" action="perfil-edit-redes-sociais.php">
								<?php
								$sql767			=	"select * from midias_contato order by mid_cont_ordem";
								$res767			=	mysqli_query($cn, $sql767);
								while($lin767	=	mysqli_fetch_array($res767))  {
									
								$midcod		=	$lin767['mid_cont_codigo'];
									

									$sql875	=	"select * from perfil_midias_dados where mid_cont_codigo = '$midcod' and perf_codigo = '$perfcod'";
									$res875	=	mysqli_query($cn, $sql875);
									$cnt875	=	mysqli_num_rows($res875);
									$lin875	=	mysqli_fetch_array($res875);
								?>
								<div class="contnome">
									<?php echo $lin767['mid_cont_nome']; ?>: 
									<div class="contitem"><?php echo $lin875['perf_cont_dad_item']; ?></div>
								</div>
								<?php } ?>
						</form>	
						
						<div class="altpoup"><a href="#ex3" rel="modal:open">Cadastrar Redes Sociais</a></div>
					</div>
					
					
				</div>
				</div>

				
				
				
				<div class="grdGRL-4 grdDSK-12">
				<div class="bloco-perfil">
					<div class="bl-atua-bloco">
						<div class="bl-atua-bloco">
							<div class="bl-atua-tit">LOCALIZAÇÃO</div>
							<div class="bl-atua-linha"></div>
							
							<div class="bl-perf-avalia-link">
								<a data-fancybox href="assets/_img/tut2.jpeg">
									Veja o passo a passo de como fazer
								</a>
							</div>
						</div>
						<div class="bl-perf-cont-mapa">
							<a data-fancybox href="<?php echo $lindsec['perf_sob_mapa']; ?>">
								<img src="assets/_img/mapa.jpg" alt=""/>
							</a>
						</div>

						<form class="cadastro" method="post" action="perfil-edit-localiza.php">
							<ul>
								<li>
									<div class="dad">
										<br>
										<label>Link do Google Maps:</label>
										<input type="text" name="maps" id="maps" value="<?php echo $lindsec['perf_sob_mapa']; ?>"/>
									</div>
								</li>
							</ul>

							<input type="submit" class="regdados" value="Alterar Mapa de Localização" />
							<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
						</form>
					</div>
					
					
					
					<div class="bl-atua-bloco">
						<div class="bl-atua-tit">FOTOS</div>
						<div class="bl-atua-mais-palavras"><a>Você pode Cadastrar até <?php echo $qtdfotos; ?> fotos</a></div>
						<div class="bl-atua-linha"></div>
						<?php
						$sql444			=	"select * from perfil_fotos where perf_codigo = $perfcod";
						$res444			=	mysqli_query($cn, $sql444);
						$cnt444			=	mysqli_num_rows($res444);
						while($lin444	=	mysqli_fetch_array($res444))  {
						?>
						<div class="grdGRL-4 grdDSK-3 grdSMPr-4 grdMOPr-6">
							<form class="cadastro" method="post" action="perfil-edit-profissional-fotos-exclui.php">
								<div class="perf-fot-add">
									<img src="painel/<?php echo $lin444['perf_fot_fotop']; ?>" alt=""/>
									<input type="submit" class="closexx" value="x" />
								</div>
								<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
								<input type="hidden" name="closefot" size="5" value="<?php echo $lin444['perf_fot_codigo']; ?>" />
							</form>
						</div>
						<?php } ?>

						<?php if($cnt444 < $qtdfotos) { ?>
						<form name="cadastro" class="cadastro" method="post" enctype="multipart/form-data" action="perfil-edit-profissional-fotos.php">
							<ul>
								<li>
									<div class="dad">
										<label>Foto: 600 x 600 pixel jpg.<br/>Cadastre 1 foto por vez</label>
										<div class="dad-fot">
											<input type="file" id="upload2" name="arquivo" onChange="this.form.submit()">
											<label class="new-button" for="upload2">Buscar Foto</label>
										</div>
									</div>
								</li>
							</ul>

						    <!--<input type="submit" class="regdados" value="Cadastrar Fotos" />-->
							<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
						</form>
						<?php } else { ?>
						<div class="contnome">Para adicionar mais fotos é necessário mudar para o perfil Premium</div>
						<div class="altplannov"><a href="alterar-plano" target="_blank">Alterar para Premium</a></div>
						<?php } ?>
					</div>
					
					
					<div class="bl-atua-bloco">
						<div class="bl-atua-tit">VÍDEOS</div>
						<div class="bl-atua-mais-palavras"><a>Você pode Cadastrar até <?php echo $qtdvideo; ?> vídeos</a></div>
						<div class="bl-atua-linha"></div>
						<?php
						$sql555			=	"select * from perfil_videos where perf_codigo = $perfcod";
						$res555			=	mysqli_query($cn, $sql555);
						$cnt555			=	mysqli_num_rows($res555);
						while($lin555	=	mysqli_fetch_array($res555))  {
						?>
						<div class="grdGRL-4 grdDSK-3 grdSMPr-4 grdMOPr-6">
							<form class="cadastro" method="post" action="perfil-edit-profissional-videos-exclui.php">
								<div class="perf-fot-add">
									<a data-fancybox href="<?php echo $lin555['perf_vid_link']; ?>">
										<img src="http://i4.ytimg.com/vi/<?php echo $lin555['perf_vid_tag']; ?>/default.jpg" alt=""/>
									</a>
									<input type="submit" class="closexx" value="x" />
								</div>
								<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
								<input type="hidden" name="closevid" size="5" value="<?php echo $lin555['perf_vid_codigo']; ?>" />
							</form>
						</div>
						<?php } ?>
						
						<?php if($cnt555 < $qtdvideo) { ?>
						<form name="cadastro" class="cadastro" method="post" action="perfil-edit-profissional-video.php">
							<ul>
								<li>
									<div class="dad">
										<label>Link do vídeo:<br>Ex: https://www.youtube.com/watch?v=glucM47muh5</label>
										<input type="text" name="link" id="link" class="validate[required]"/>
									</div>
								</li>
							</ul>

							<input type="submit" class="regdados" value="Cadastrar Vídeos" />
							<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
						</form>
						<?php } else { ?>
						<div class="contnome">Para adicionar mais vídeos é necessário mudar para o perfil Premium</div>
						<div class="altplannov"><a href="alterar-plano" target="_blank">Alterar para Premium</a></div>
						<?php } ?>
					</div>
					
					
					<div class="bl-perf-avalia-link">
						<a class="puxa" href="perfil-avaliacoes-profissional/<?php echo $perfcod; ?>/<?php echo CorrigirNome($linuser['perf_nome']); ?>">
							Ver Minhas Avaliações
						</a>
					</div>
					
				</div>
				</div>

			</div>
			<div class="vazio"></div>	
		</div>


	
</div>

<!-- jQuery Modal -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

<?php
require_once("poup-pap-perfil-profissional.php"); 
require_once("poup-pap-contatos.php"); 
require_once("poup-pap-areas-atuacao.php"); 
require_once("poup-pap-redes-sociais.php"); 
?>



<?php } else { require_once("alerta.php");}// Termina IF de Login Aqui ============= ?>