<?php 

if(!isset($_GET['CodigAbriR'])) { require_once("alerta.php"); }


$item		=	$_GET['CodigAbriR'];

if(isset($_SESSION["cod_ses"])) {
	
	$clitem		=	$_SESSION["cod_ses"];
	
	$sqlaval	=	"select * from avaliacoes_usuario WHERE perf_codigo = '$clitem' AND perf_cod_ref = '$item'";
	$resaval	=	mysqli_query($cn, $sqlaval);
	$linaval	=	mysqli_fetch_array($resaval);	

	$avaldes	=	$linaval['ava_user_descricao'];
	
}
	

$sqluser	=	"select * from perfil
				INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
				INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
				WHERE perf_codigo = $item";
$resuser	=	mysqli_query($cn, $sqluser);
$linuser	=	mysqli_fetch_array($resuser);	

$perfcod	=	$linuser['perf_codigo'];
$perfname	=	$linuser['perf_nome'];
$catcod		=	$linuser['cat_codigo'];
$perfcat	=	$linuser['cat_nome'];

$perfnames	=	CorrigirNome($perfname);
$avalia		=	"perfil-avaliacoes/".$perfcod."/".$perfnames;



$sqldsec	=	"select * from perfil_sobre WHERE perf_codigo = '$item'";
$resdsec	=	mysqli_query($cn, $sqldsec);
$lindsec	=	mysqli_fetch_array($resdsec);	


$sqlavaluser	=	"select * from avaliacoes WHERE perf_codigo = '$item'";
$resavaluser	=	mysqli_query($cn, $sqlavaluser);
$linavaluser	=	mysqli_fetch_array($resavaluser);	

$cntavuser		=	$linavaluser['ava_media_estrelas'];

if($cntavuser > 0){ 
	
		$cntavuser	=	$cntavuser; 
	
	} else {
	
		$cntavuser	=	""; 
}

?>
<div class="boxfull">
	<div class="titfxtop">Você está em: <span class="titfxtop-sub">Avaliações <?php echo $linuser['perf_nome']; ?></span></div>

	
		<div class="boxfull cinza">
			<div class="blg perf-bloco">

				<div class="grdGRL-6 grdDSK-12">
					<div class="bloco-perfil">
						<div class="bl-atua-bloco">
							<div class="bl-atua-tit">CLASSIFICAÇÃO</div>
							<div class="bl-perf-avalia">Pontuação média <?php echo $cntavuser; ?></div>
							<div class="bl-atua-linha"></div>
								<div class="grdGRL-12">
									<div class="bl-perf-avalia-tit">ATENDIMENTO</div>
									<div class="boxit-estrela-avaliacoes">
										<?php 
											for($contador = 1; $contador <= 5; $contador++) {

													if($contador <= $linavaluser['ava_atendimento']){

														echo "<i class='fas fa-star estyellow'></i>";

													} else {

														echo "<i class='fas fa-star'></i>";

													}

											}
										?>
									</div>
								</div>
						</div>
					</div>
				</div>

				
				

				<?php if(isset($_SESSION["cod_ses"])) { ?>
				<div class="grdGRL-6 grdDSK-12">
					<div class="bloco-perfil">
						<div class="bl-atua-bloco">
							<div class="bl-atua-tit">SUA AVALIAÇÃO</div>
							<div class="bl-perf-avalia">Pontuação <?php echo $linaval['ava_user_media_estrelas']; ?></div>
							<div class="bl-atua-linha"></div>
								<div class="grdGRL-12">
									<div class="bl-perf-avalia-tit">ATENDIMENTO</div>
									<div class="boxit-estrela-avaliacoes">
										<?php 
											for($contador = 1; $contador <= 5; $contador++) {

													if($contador <= $linaval['ava_user_atendimento']){

														echo "<i class='fas fa-star estyellow'></i>";

													} else {

														echo "<i class='fas fa-star'></i>";

													}

											}
										?>
									</div>
								</div>
						</div>
					</div>
				</div>

				
				


				<div class="grdGRL-12">
					<div class="bloco-perfil">
						<div class="bl-atua-bloco">
							<div class="bl-atua-tit">AVALIAR PRESTADOR</div>
							<div class="bl-perf-avalia"></div>
							<div class="bl-atua-linha"></div>
							
								<div class="grdGRL-6  grdDSK-12">
									<div class="grdGRL-4 grdDSK-3 grdSMPr-4">
										<div class="bl-perf-avalia-tit">ATENDIMENTO</div>
										<div class="boxit-estrela-avaliacoes">
											<form name="avaliacoes" class="avaliacao" method="post" enctype="multipart/form-data"  action="avalia.php" onSubmit="return validarAvaliacao()">
												<div class="estrelas">
													<input type="radio" id="radio" name="atendimento" value="" checked>

													<label for="estrela_um"><i class="fa estpx"></i></label>
													<input type="radio" id="estrela_um" name="atendimento" value="1">

													<label for="estrela_dois"><i class="fa estpx"></i></label>
													<input type="radio" id="estrela_dois" name="atendimento" value="2">

													<label for="estrela_tres"><i class="fa estpx"></i></label>
													<input type="radio" id="estrela_tres" name="atendimento" value="3">

													<label for="estrela_quatro"><i class="fa estpx"></i></label>
													<input type="radio" id="estrela_quatro" name="atendimento" value="4">

													<label for="estrela_cinco"><i class="fa estpx"></i></label>
													<input type="radio" id="estrela_cinco" name="atendimento" value="5">
												</div>
										</div>
									</div>

								</div>
							
									<div class="grdGRL-6  grdDSK-12">
										<div class="dad">
											<div class="bl-atua-tit">Escreva seu comentário</div>
											<textarea name="descricao" cols="1" rows="10"><?php echo $avaldes; ?></textarea>
										</div>

										<input type="hidden" name="dir" value="<?php echo $avalia; ?>" />
										<input type="hidden" name="item" value="<?php echo $item; ?>" />
										<input type="hidden" name="clitem" value="<?php echo $clitem; ?>" />
										<input type="submit" class="bl-avalia-user-avalia" value="Avaliar Prestador" />
										<!-- <div class="bl-avalia-user-avalia"><a href="#">Alterar Avaliação</a></div>-->
									</div>
								</form>
						</div>
					</div>
				</div>
				<?php } // Fecha IF que verifica se usuário está logado ?>
				
				

				<div class="grdGRL-12 grdDSK-12">
				<div class="bloco-perfil">
					<div class="bl-atua-bloco">
						<div class="bl-atua-tit">RECOMENDAÇÕES</div>
							<div class="bl-reconda-scroll">
						
							<?php
							$sqlrecom		=	"select * from avaliacoes_usuario 
												INNER JOIN perfil	  	on		perfil.perf_codigo		=	avaliacoes_usuario.perf_codigo
												WHERE 
												perf_cod_ref = '$item'
												ORDER BY
												ava_user_data_created DESC, 
												ava_user_hora_created DESC
												";
							$resrecom		=	mysqli_query($cn, $sqlrecom);
							while($linrecom	=	mysqli_fetch_array($resrecom)) {
							?>
							<div class="bl-recomendacoes">
							<?php if($linrecom['perf_fotop'] == ""){ ?>
								<div class="bl-avalia-user-foto"><i class="fas fa-user user-nofoto"></i></div>
							<?php } else { ?>
								<div class="bl-avalia-user-foto"><img src="painel/<?php echo $linrecom['perf_fotop']; ?>" alt=""/></div>
							<?php } ?>
									<div class="bl-avalia-user-dad">
										<div class="bl-avalia-user-nome"><?php echo $linrecom['perf_nome']; ?></div>
										<div class="bl-avalia-user-txt"><?php echo $linrecom['ava_user_descricao']; ?></div>
										<div class="bl-avalia-user-estrela">
											<?php 
												for($contador = 1; $contador <= 5; $contador++) {

														if($contador <= $linrecom['ava_user_media_estrelas']){

															echo "<i class='fas fa-star estyellow'></i>";

														} else {

															echo "<i class='fas fa-star'></i>";

														}

												}
											?>
										</div>
									</div>
							</div>
							<?php } ?>	
								
						</div>
					</div>
					
				</div>
				</div>
				
				

			</div>
			<div class="vazio"></div>	
		</div>


	
</div>