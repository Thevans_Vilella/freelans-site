<div class="boxfull">
	<div class="titfxtop cinza">Perguntas Frequentes <span class="titfxtop-sub"></span></div>
	
		<div class="boxfull">
			<div class="blg">
				<div class="inst-tit">Tire suas dúvidas sobre os temas abaixo</div>
				<div class="bl-faq">
                    <?php
                    $sql		=	"select * from faq order by faq_ordem";
                    $res		=	mysqli_query($cn, $sql);
                    while($lin	=	mysqli_fetch_array($res))  {
                    ?>
                    <button class="accordion"><?php echo $lin['faq_ordem']; ?>. <?php echo $lin['faq_pergunta']; ?></button>
                    <div class="panel">
                        <?php echo $lin['faq_resposta']; ?>
                    </div>
                    <?php } ?>
                </div>
			</div>
		</div>

</div>
