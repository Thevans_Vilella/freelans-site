<div id="ex4" class="modal">
	<div class="modform">
		
		<div class="closefor"><a href="#" rel="modal:close"><i class="fas fa-times-circle closeforx"></i></a></div>

		<div class="bl-atua-tit">DADOS CADASTRAIS</div>
		<div class="bl-atua-linha"></div>

		<form class="cadastro" id="cadastro1" method="post" enctype="multipart/form-data"  action="perfil-edit-profissional-altera.php">
			<ul>
				<li>
					<div class="dad">
						<label>Cidade de Atuação:</label>
						<select name="city" class="select" id="city">
							<option value="<?php echo $linuser['cid_codigo']; ?>"><?php echo $linuser['cid_nome']; ?>/<?php echo $linuser['est_sigla']; ?></option>
							<option value="<?php echo $linuser['cid_codigo']; ?>">----------------------------</option>
							<?php
							$sqlcid			=	"select * from cidades
												INNER JOIN estado	  	on		estado.est_codigo		=	cidades.est_codigo
												order by est_nome, cid_nome";
							$rescid			=	mysqli_query($cn, $sqlcid);
							while($lincid	=	mysqli_fetch_array($rescid))  {
							?>
							<option value="<?php echo $lincid['cid_codigo']; ?>"><?php echo $lincid['cid_nome']; ?>/ <?php echo $lincid['est_sigla']; ?></option>
							<?php } ?>
						</select>
					</div>
				</li>
				<li>
					<div class="dad">
						<label>Categoria de Atuação:</label>
						<select name="categoria" class="select" id="categoria">
							<option value="<?php echo $linuser['cat_codigo']; ?>"><?php echo $linuser['cat_nome']; ?></option>
							<option value="<?php echo $linuser['cat_codigo']; ?>">----------------------------</option>
							<?php
							$sqlcat			=	"select * from categorias order by cat_nome";
							$rescat			=	mysqli_query($cn, $sqlcat);
							while($lincat	=	mysqli_fetch_array($rescat))  {
							?>
							<option value="<?php echo $lincat['cat_codigo']; ?>"><?php echo $lincat['cat_nome']; ?></option>
							<?php } ?>
						</select>
					</div>
				</li>
				<li>
					<div class="dad">
						<label>Nome:</label>
						<input type="text" name="username" id="username" required value="<?php echo $linuser['perf_nome']; ?>"/>
					</div>
				</li>
				<li>
					<div class="dad">
						<label>Email:</label>
						<input type="email" name="email" id="email" required value="<?php echo $linuser['perf_email']; ?>"/>
					</div>
				</li>
				<li>
					<div class="dad">
						<label>Telefone:</label>
						<input type="tel" name="fone" id="fone" onkeyup="maskIt(this,event,'(##) ####-#####')" maxlength="14" required value="<?php echo $linuser['perf_telefone']; ?>"/>
					</div>
				</li>
				<li>
					<div class="dad">
						<label>Celular:</label>
						<input type="tel" name="celular" id="celular" onkeyup="maskIt(this,event,'(##) ####-#####')" required maxlength="15" value="<?php echo $linuser['perf_celular']; ?>"/>
					</div>
				</li>
			</ul>

			<input type="submit" class="regdados" value="Salvar" />
			<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
		</form>

	</div>
</div>