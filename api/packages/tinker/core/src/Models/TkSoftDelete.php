<?php

namespace Tinker\Core\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

trait TkSoftDelete
{
    use SoftDeletes;

    public function deleted(BaseModel $model)
    {
        if (array_search("", $model->getFillable()) !== false) {
            $model->status = 0;
            $model->save();
        }
    }
}
