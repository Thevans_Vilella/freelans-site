<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcos
 * Date: 2019-01-14
 * Time: 11:24
 */

namespace Tinker\Core\Models;

use Illuminate\Database\Eloquent\Collection;
use Log;
use stdClass;

trait Mapper
{
    public static function modelToData($model)
    {
        $data = new StdClass();
        $includeMutated = true;
        if (isset($model->includeMutated)) {
            $includeMutated = $model->includeMutated;
        }

        foreach ($model->getFillable() as $fillable) {
            if (array_search($fillable, $model->getHidden()) === false) {
                $data->$fillable = $model->$fillable;
            }
        }
        if ($includeMutated) {
            foreach ($model->getMutatedAttributes() as $append) {
                $data->$append = $model->$append;
            }
        }

        return $data;
    }

    public static function dataToModel($data)
    {
        $model = new static();
        $includeMutated = true;
        if (isset($model->includeMutated)) {
            $includeMutated = $model->includeMutated;
        }

        foreach ($model->getFillable() as $fillable) {
            if (array_search($fillable, $model->getHidden()) === false) {
                $model->$fillable = $data->$fillable;
            }
        }
        if ($includeMutated) {
            foreach ($model->getMutatedAttributes() as $append) {
                if (isset($data->$append)) {
                    $model->$append = $data->$append;
                }
            }
        }

        return $model;
    }

    public static function modelToDataList($models)
    {
        $datas = array();
        if (!is_array($models) && !($models instanceof Collection))
        {
            array_push($datas, static::modelToData($models));
            return $datas;
        }

        foreach ($models as $model)
        {
            array_push($datas, static::modelToData($model));
        }

        return $datas;
    }

    public static function dataToModelList($datas)
    {
        $models = array();
        if (!is_array($datas))
        {
            array_push($models, static::dataToModel($datas));
            return $datas;
        }

        foreach ($datas as $data)
        {
            array_push($models, static::dataToModel($data));
        }

        return $models;
    }
}
