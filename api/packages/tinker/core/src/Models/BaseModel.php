<?php

namespace Tinker\Core\Models;

trait BaseModel
{
    public static function autoFill(array $request) {
        $model = new static();
        foreach ($model->getFillable() as $fillable) {
            if (isset($request[$fillable])) {
                $model->$fillable = $request[$fillable];
            }
        }
        return $model;
    }

    public function fillAttributes(array $request) {
        foreach ($this->getMutatedAttributes() as $append) {
            if (isset($request[$append])) {
                $this->$append = $request[$append];
            }
        }
    }
}
