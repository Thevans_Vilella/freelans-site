<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcos
 * Date: 2019-02-20
 * Time: 11:48
 */

namespace Tinker\Core\Exceptions;

use Exception;
use Throwable;

abstract class BaseException extends Exception implements IBaseException
{

    protected const EXCEPTION_CODE = 0;
    protected $httpCode = 0;
    protected $data = null;
    protected $message = 'Unknown exception';
    protected $code = 0;
    protected $file;
    protected $line;

    public function __construct($data = null, string $message = "", int $httpCode = HttpCodes::InternalServerError, Throwable $previous = null)
    {
        if (!$message) {
            throw new $this('Unknown '. get_class($this));
        }
        parent::__construct($message, static::EXCEPTION_CODE, $previous);
        $this->httpCode = $httpCode;
        $this->data = $data;
    }

    public function __toString()
    {
        return get_class($this) . " '{$this->message}' in {$this->file}({$this->line})\n"
            . "{$this->getTraceAsString()}";
    }

    public function getHttpCode()
    {
        return $this->httpCode;
    }

    public function getData()
    {
        return $this->data;
    }

    public function hasData()
    {
        return $this->data != null;
    }

}
