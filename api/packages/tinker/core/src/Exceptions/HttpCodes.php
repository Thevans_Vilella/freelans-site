<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcos
 * Date: 2019-04-10
 * Time: 11:51
 */

namespace Tinker\Core\Exceptions;

/**
 * Class HttpCodes
 * @package App\Exceptions
 */
// https://restfulapi.net/http-status-codes/
abstract class HttpCodes
{
    /**
     * It indicates that the REST API successfully carried out whatever action the client requested, and that no more
     * specific code in the 2xx series is appropriate
     **/
    const OK = 200;
    /**
     * A REST API responds with the 201 status code whenever a resource is created inside a collection. There may also
     * be times when a new resource is created as a result of some controller action, in which case 201 would also be
     * an appropriate response.
     */
    const Created = 201;
    /**
     * A 202 response is typically used for actions that take a long while to process. It indicates that the request
     * has been accepted for processing, but the processing has not been completed. The request might or might not be
     * eventually acted upon, or even maybe disallowed when processing occurs.
     */
    const Accepted = 202;
    /**
     * The 204 status code is usually sent out in response to a PUT, POST, or DELETE request when the REST API declines
     * to send back any status message or representation in the response message’s body.
     */
    const NoContent = 204;
    /**
     * The 301 status code indicates that the REST API’s resource model has been significantly redesigned and a new
     * permanent URI has been assigned to the client’s requested resource. The REST API should specify the new URI in
     * the response’s Location header and all future requests should be directed to the given URI.
     */
    const MovedPermanently = 301;
    /**
     * The HTTP response status code 302 Found is a common way of performing URL redirection. An HTTP response with
     * this status code will additionally provide a URL in the location header field. The user agent
     * (e.g. a web browser) is invited by a response with this code to make a second, otherwise identical, request to
     * the new URL specified in the location field.
     */
    const Found = 302;
    /**
     * A 303 response indicates that a controller resource has finished its work, but instead of sending a potentially
     * unwanted response body, it sends the client the URI of a response resource. This can be the URI of a temporary
     * status message, or the URI to some already existing, more permanent, resource.
     */
    const SeeOther = 303;
    /**
     * This status code is similar to 204 (“No Content”) in that the response body must be empty. The key distinction
     * is that 204 is used when there is nothing to send in the body, whereas 304 is used when the resource has not
     * been modified since the version specified by the request headers If-Modified-Since or If-None-Match.
     */
    const NotModified = 304;
    /**
     * A 307 response indicates that the REST API is not going to process the client’s request. Instead, the client
     * should resubmit the request to the URI specified by the response message’s Location header. However, future
     * requests should still use the original URI.
     */
    const TemporaryRedirect = 307;
    /**
     * 400 is the generic client-side error status, used when no other 4xx error code is appropriate. Errors can be
     * like malformed request syntax, invalid request message parameters, or deceptive request routing etc.
     */
    const BadRequest = 400;
    /**
     * A 401 error response indicates that the client tried to operate on a protected resource without providing the
     * proper authorization. It may have provided the wrong credentials or none at all. The response must include a
     * WWW-Authenticate header field containing a challenge applicable to the requested resource.
     */
    const Unauthorized = 401;
    const PaymentRequired = 402;
    /**
     * A 403 error response indicates that the client’s request is formed correctly, but the REST API refuses to honor
     * it i.e. the user does not have the necessary permissions for the resource. A 403 response is not a case of
     * insufficient client credentials; that would be 401 (“Unauthorized”).
     */
    const Forbidden = 403;
    /**
     * The 404 error status code indicates that the REST API can’t map the client’s URI to a resource but may be
     * available in the future. Subsequent requests by the client are permissible.
     */
    const NotFound = 404;
    /**
     * The API responds with a 405 error to indicate that the client tried to use an HTTP method that the resource does
     * not allow. For instance, a read-only resource could support only GET and HEAD, while a controller resource might
     * allow GET and POST, but not PUT or DELETE.
     */
    const MethodNotAllowed = 405;
    /**
     * The 406 error response indicates that the API is not able to generate any of the client’s preferred media types,
     * as indicated by the Accept request header. For example, a client request for data formatted as application/xml
     * will receive a 406 response if the API is only willing to format data as application/json.
     */
    const NotAcceptable = 406;
    /**
     * The 412 error response indicates that the client specified one or more preconditions in its request headers,
     * effectively telling the REST API to carry out its request only if certain conditions were met. A 412 response
     * indicates that those conditions were not met, so instead of carrying out the request, the API sends this status
     * code.
     */
    const PreconditionFailed = 412;
    /**
     * The 415 error response indicates that the API is not able to process the client’s supplied media type, as
     * indicated by the Content-Type request header. For example, a client request including data formatted as
     * application/xml will receive a 415 response if the API is only willing to process data formatted as
     * application/json.
     */
    const UnsupportedMediaType = 415;
    /**
     * The 422 error response indicates that given data was invalid.
     */
    const UnprocessableEntity = 422;
    /**
     * 500 is the generic REST API error response. Most web frameworks automatically respond with this response status
     * code whenever they execute some request handler code that raises an exception.
     */
    const InternalServerError = 500;
    /**
     * The server either does not recognize the request method, or it lacks the ability to fulfill the request. Usually,
     * this implies future availability (e.g., a new feature of a web-service API).
     */
    const NotImplemented = 501;
}
