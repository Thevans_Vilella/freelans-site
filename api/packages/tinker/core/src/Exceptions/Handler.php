<?php
namespace Tinker\Core\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException as LaravelValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tinker\Core\Exceptions\ValidationException as AppValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        LaravelValidationException::class,
        AppValidationException::class
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        if ($exception instanceof LaravelValidationException) {
            $errors = "";
            foreach ($exception->errors() as $errorKey => $errorValue) {
                if (is_array($errorValue)) {
                    foreach ($errorValue as $messageKey => $messageValue) {
                        if ($errors != "") {
                            $errors .= "\n";
                        }
                        $errors .= $messageValue;
                    }
                } else {
                    if ($errors != "") {
                        $errors .= "\n";
                    }
                    $errors .= $errorValue;
                }
            }
            throw new AppValidationException(null, $exception->getMessage() . "\n" . $errors, HttpCodes::UnprocessableEntity, $exception);
        }

        parent::report($exception);
    }

    private function sendReport($exception, $request = null) {
        $context = stream_context_create([
            'http' => [
                'ignore_errors' => true
            ]
        ]);

        $environment = app()->environment();
        $errorMessage = "Erro desconhecido!";
        if ($exception instanceof NotFoundHttpException) {
            $errorMessage = "Rota Inexistente!";
        }
        if ($request != null) {
            $errorMessage .= "\nRota: " . $request->getRequestUri();
        }
        if ($exception->getMessage() != "") {
            $errorMessage .= "\n" . $exception->getMessage();
        }
        $filename = str_replace(base_path(), "", $exception->getFile());
        $ip = app('request')->ip() ?: "-";
        $agent = app('request')->header("User-Agent") ?: "-";
        $extra = base64_encode($filename . ":" . $exception->getLine() . "|ip:" . $ip . "|agent:" . $agent);
        if (env("TK_CUSTOMER", 0) != 0) {
            file_get_contents("https://tinkertecnologia.com/islequi.php?ambient=$environment&customer=" . env("TK_CUSTOMER", 0) . "&message=" . base64_encode($errorMessage) . "&extra=$extra", false, $context);
        }
        return $errorMessage;
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $exception
     * @return JsonResponse|RedirectResponse|Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->wantsJson() || preg_match("/v\d+/", $request->url())) {
            if ($exception instanceof IBaseException || $exception instanceof AppValidationException) {
                $response = [
                    "status" => false,
                    "errorCode" => $exception->getCode(),
                    "message" => $exception->getMessage()
                ];
                if ($exception->hasData()) {
                    $response["data"] = $exception->getData();
                }

                return response()->json($response, $exception->getHttpCode());
            } else {
                error_log($exception->getMessage());
                $errorMessage = $this->sendReport($exception, $request);
                return response()->json([
                    "status" => false,
                    "errorCode" => "-2000",
                    "message" => $errorMessage
                ], 500);
            }
        }

        if (!app()->environment("local")) {
            $data = [];
            if ($exception instanceof IBaseException) {
                $data["error_code"] = $exception->getCode();
            }
            if ($exception instanceof LaravelValidationException) {
                return redirect()->back()->withErrors($exception->validator->getMessageBag()->toArray())->withInput();
            }

            $this->sendReport($exception, $request);
            return response(view("errors.500", $data), 500);
        } else {
            return parent::render($request, $exception);
        }
    }
}
