<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcos
 * Date: 2019-02-20
 * Time: 12:19
 */

namespace Tinker\Core\Exceptions;

use Throwable;

interface IBaseException
{
    public function getMessage();
    public function getCode();
    public function getFile();
    public function getLine();
    public function getTrace();
    public function getTraceAsString();

    public function __toString();
    public function __construct($data = null, string $message = "", int $httpCode = HttpCodes::InternalServerError, Throwable $previous = null);
    public function getData();
    public function hasData();
    public function getHttpCode();
}
