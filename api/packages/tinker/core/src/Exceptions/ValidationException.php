<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcos
 * Date: 2019-02-20
 * Time: 12:33
 */

namespace Tinker\Core\Exceptions;

use Throwable;

class ValidationException extends BaseException
{
    protected const EXCEPTION_CODE = 0;

    public function __construct($data = null, string $message = "Parâmetros da requisição inválidos.", int $httpCode = HttpCodes::UnprocessableEntity, Throwable $previous = null)
    {
        parent::__construct($data, $message, $httpCode, $previous);
    }
}
