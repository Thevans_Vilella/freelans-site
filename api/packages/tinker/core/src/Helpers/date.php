<?php

if (!function_exists("printDate")) {
    function printDate($date, $includeTime = true) {
        if (!isset($date) || empty($date)) {
            return "";
        }

        $returns = substr($date, 8, 2) . "/" . substr($date, 5, 2) . "/" . substr($date, 0, 4);
        if (strlen($date) > 10 && $includeTime) {
            $returns .= " " . substr($date, 11, 2) . ":" . substr($date, 14, 2);
            if (strlen($date) > 17) {
                $returns .= ":" . substr($date, 17, 2);
            }
        }
        return $returns;
    }
}
