<?php

if (!function_exists('randomString'))
{
    function randomString($source, $length)
    {
        if (!isset($source) || empty($source))
        {
            return "";
        }

        $result = "";
        for ($i = 1; $i <= $length; $i++)
        {
            $result .= $source[rand(0, strlen($source)-1)];
        }
        return $result;
    }
}
