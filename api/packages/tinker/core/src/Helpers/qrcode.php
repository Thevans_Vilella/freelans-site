<?php

use SimpleSoftwareIO\QrCode\Facades\QrCode;

if (!function_exists('qrcode')) {
    function qrcode($data) {
        return base64_encode(
            QrCode::format('png')
                ->size(600)
                ->margin(0)
                ->errorCorrection('H')
                ->generate(UserFactoryHelper::generateQrCode($data))
        );
    }
}
