<?php

namespace Tinker\Core;

use Illuminate\Support\ServiceProvider;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //$this->app->make('tinker\core\TodolistController');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /*$this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadViewsFrom(__DIR__.'/views', 'tinker/core');
        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/tinker/core'),
        ]);*/
    }
}
