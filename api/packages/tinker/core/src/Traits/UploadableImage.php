<?php

namespace Tinker\Core\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

trait UploadableImage
{
    private static $DEFAULT_MAX_WIDTH = 1920;
    private static $DEFAULT_MAX_HEIGHT = 1920;

    /**
     * @var int Max image width
     */
    protected $imageMaxWidth;
    /**
     * @var int Max image height
     */
    protected $imageMaxHeight;

    /**
     * @var bool|string If false, keeps incoming file extension. If string, will use that string as file extension
     */
    protected $forceImageExtension = false;

    /**
     * UploadableImage constructor.
     */
    public function bootUploadableImageTrait()
    {
        $this->imageMaxWidth = static::$DEFAULT_MAX_WIDTH;
        $this->imageMaxHeight = static::$DEFAULT_MAX_HEIGHT;
    }

    private function defaultUploadableOptions($options) {
        if (!isset($options["fieldName"])) {
            $options["fieldName"] = "image";
        }
        if (!isset($options["imageMaxWidth"])) {
            $options["imageMaxWidth"] = $this->imageMaxWidth;
        }
        if (!isset($options["imageMaxheight"])) {
            $options["imageMaxheight"] = $this->imageMaxHeight;
        }
        if (!isset($options["forceImageExtension"])) {
            $options["forceImageExtension"] = $this->forceImageExtension;
        }
        if (!isset($options["fileName"])) {
            $options["fileName"] = Str::uuid();
        }
        return $options;
    }

    public function uploadImage(Request $request, string $path, array $options = null)
    {
        $options = $this->defaultUploadableOptions($options);
        if ($request->hasFile($options["fieldName"]) && $request->file($options["fieldName"])->isValid())
        {
            $imageSize = getimagesize($request->file($options["fieldName"]));
            $width = $imageSize[0];
            $height = $imageSize[1];

            $imageResize = Image::make($request->file($options["fieldName"])->getRealPath());
            if ($width > $options["imageMaxWidth"] || $height > $options["imageMaxHeight"]) {
                $maxWidth = $options["imageMaxWidth"];
                $maxHeight = $options["imageMaxHeight"];
                $imageResize->resize($maxWidth, $maxHeight, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            $fileName = $options["fileName"];
            if (!$options["forceImageExtension"]) {
                $extension = $request->file($options["fieldName"])->extension();
                $fileName .= ".{$extension}";
            } else {
                $fileName .= ".$options[forceImageExtension]";
            }
            $imageResize->save(storage_path("app/public") . "/{$path}{$fileName}", 80, 'jpg');

            return $fileName;
        }

        return null;
    }
}
