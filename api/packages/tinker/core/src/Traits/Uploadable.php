<?php

namespace Tinker\Core\Traits;

use Illuminate\Http\Request;

trait Uploadable
{
    use UploadableImage, UploadableVideo;

    public static function upload(Request $request, string $path, string $fieldName = "media")
    {
        if (isset($request->media_type)) {
            if ($request->media_type == "image") {
                return UploadableImage::uploadImage($request, $path, $fieldName);
            } else {
                return UploadableVideo::uploadVideo($request, $path, $fieldName);
            }
        }

        return "";
    }
}
