<?php

namespace Tinker\Core\Traits;

use Pbmedia\LaravelFFMpeg\FFMpegFacade as FFMpeg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

trait UploadableVideo
{
    public static function generateThumb(string $fileName, int $sec = 10)
    {
        $thumbFileName = explode(".", $fileName)[0] . ".png";
        FFMpeg::fromDisk("local")
            ->open("public/" . config("filesystems.ads_videos_folder") . $fileName)
            ->getFrameFromSeconds($sec)
            ->export()
            ->toDisk("local")
            ->save("public/" . config("filesystems.ads_images_folder") . $thumbFileName);
    }

    public static function uploadVideo(Request $request, string $path, string $fieldName = "video")
    {
        if ($request->hasFile($fieldName) && $request->file($fieldName)->isValid())
        {
            $uuid = Str::uuid();
            $extension = $request->file($fieldName)->extension();

            $fileName = "{$uuid}.{$extension}";

            $request->$fieldName->storeAs("public/" . $path . "/", $fileName);
            static::generateThumb($fileName);

            return $fileName;
        }
    }
}
