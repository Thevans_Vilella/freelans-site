<?php

namespace Tinker\Core\Traits;

use Tinker\Core\Exceptions\HttpCodes;

trait HasJsonResponse
{
    protected function json_response($data, $state) {
        return response()->json($data, $state);
    }

    protected function json_success($message, $data = null, $state = HttpCodes::OK) {
        if (!is_string($message)) {
            if (!$data) {
                $data = $message;
            }
            $message = "OK";
        }

        $response = [];
        if (!is_null($data)) {
            $response["data"] = $data;
        }

        $response["message"] = $message;
        if (!key_exists('status', $response)) {
            $response['status'] = true;
        }

        return response()->json($response, $state);
    }

    protected function json_error(string $message, $data = null, $state = HttpCodes::InternalServerError) {
        $response = [];
        if ($data) {
            $response["data"] = $data;
        }

        $response["message"] = $message;
        if (!key_exists('status', $response)) {
            $response['status'] = false;
        }

        return response()->json($response, $state);
    }
}
