<?php

namespace App\Events;

use App\Models\PushMessage;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewPushMessageEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var PushMessage
     */
    public $pushMessage;

    /**
     * Create a new event instance.
     *
     * @param PushMessage $pushMessage
     * @return void
     */
    public function __construct(PushMessage $pushMessage)
    {
        $this->pushMessage = $pushMessage;
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return PrivateChannel
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
