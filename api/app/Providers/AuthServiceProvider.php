<?php
namespace App\Providers;

use Auth;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Auth::provider('custom_jwt', function ($app, $config) {
            return new JWTAuthProvider($this->app['hash'], $config['model']);
        });

        Auth::provider('custom_admin', function ($app, $config) {
            return new AdminAuthProvider($this->app['hash'], $config['model']);
        });
    }
}
