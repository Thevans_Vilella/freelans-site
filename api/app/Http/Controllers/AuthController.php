<?php

namespace App\Http\Controllers;

use App\Models\Enum\Gender;
use App\Models\Profile;
use App\Models\ProfileType;
use App\Models\Referral;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Tinker\Core\Exceptions\HttpCodes;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use function copy;
use function date;
use function md5;
use function storage_path;
use function substr;


class AuthController extends UploadableController
{
    use ValidatesRequests;

    protected $forceImageExtension = "jpg";
    protected $imageMaxWidth = 460;
    protected $imageMaxHeight = 330;
    //protected $imageMaxWidth = 170;
    //protected $imageMaxHeight = 122;

    public function authenticate(Request $request)
    {
        $reqCredentials = $request->all();
        try {
            $credentials = [
                "perf_email" => $reqCredentials["email"],
                "perf_senha" => $reqCredentials["password"]
            ];
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->json_error('invalid_credentials', null, HttpCodes::Unauthorized);
            }
        } catch (JWTException $e) {
            return $this->json_error('could_not_create_token', null, HttpCodes::InternalServerError);
        }

        $user = auth()->user();
        $user->fbid = isset($reqCredentials["fbid"]) ? $reqCredentials["fbid"] : null;
        $user->save();
        $user->token = $token;
        return $this->json_success($user);
    }

    public function register(Request $request)
    {
        $request->validate([
            "name" => "required|min:3|max:255",
            "phone" => "nullable",
            "mobile" => "required|min:3|max:255",
            "email" => "required|min:3|max:255",
            "password" => "required|confirmed",
            "policy" => "required|boolean",
            "birth_date" => "nullable|string",
            "gender" => "nullable|between:0,1",
            "profile_type" => "required|exists:perfil_tipo,per_tip_codigo",
            "image" => "nullable",
            "fbid" => "nullable",
        ]);

        if ($request->profile_type == ProfileType::PROVIDER) {
            $request->validate([
                "city" => "required|exists:cidades,cid_codigo",
                "job" => "required|exists:categorias,cat_codigo",
                "plan" => "required|exists:planos,pla_codigo"
            ]);
        }
        $requestData = $request->all();
        $requestData["gender"] = isset($requestData["gender"]) ? Gender::toDatabase($requestData["gender"]) : null;
        $requestData["birth_date"] = isset($requestData["birth_date"]) ? date_create_from_format("d/m/Y", $requestData["birth_date"])->format("Y-m-d") : null;
        $requestData["phone"] = isset($requestData["phone"]) ? $requestData["phone"] : "";
        $requestData["status"] = 1;
        $requestData["policy"] = 1;

        if ($request->profile_type == ProfileType::CUSTOMER) {
            $requestData["category"] = 0;
            $requestData["city"] = 0;
            $requestData["plan"] = 0;
        }

        $profile = Profile::dataToModel((object)$requestData);
        $profile->fbid = isset($requestData["fbid"]) ? $requestData["fbid"] : null;
        $profile->perf_login = md5($profile->email);
        $profile->perf_senha = md5($requestData["password"]);
        $profile->perf_ip = $request->ip();
        $profile->perf_key1 = "";
        $profile->perf_key2 = "";
        $profile->perf_key3 = "";

        $img_checksum = substr(md5(date("Ymd-His")), 0, 5);
        $fileName = Str::slug($profile->name) . "-" . $img_checksum;
        if (isset($requestData['image'])) {
            $profile->large_avatar = $this->uploadImage($request, config('filesystems.user_images_folder_large'), [
                "fileName" => $fileName
            ]);
            $profile->small_avatar = $this->uploadImage($request, config('filesystems.user_images_folder_small'), [
                "fileName" => $fileName,
                "imageMaxWidth" => 170,
                "imageMaxHeight" => 122
            ]);
        } else {
            copy(storage_path('app/public') . "/" . config("filesystems.user_images_folder_default") . "/avataruserg.jpg", storage_path('app/public') . "/" . config("filesystems.user_images_folder_large") . "/$fileName.jpg");
            copy(storage_path('app/public') . "/" . config("filesystems.user_images_folder_default") . "/avataruserp.jpg", storage_path('app/public') . "/" . config("filesystems.user_images_folder_small") . "/$fileName.jpg");
            $profile->large_avatar = "fotos/perfil/grandes/$fileName.jpg";
            $profile->small_avatar = "fotos/perfil/pequenas/$fileName.jpg";
        }
        $profile->save();

        if (isset($requestData['referrer_uid'])) {
            Referral::makeReferralWithReferrerUid($requestData['referrer_uid'], $profile->id);
        }

        return $this->json_success("OK");
    }

    public function check()
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response(['authenticated' => false]);
        }
        return response(['authenticated' => true]);
    }

    public function logout()
    {
        try {
            $token = JWTAuth::getToken();

            if ($token) {
                JWTAuth::invalidate($token);
            }

        } catch (JWTException $e) {
            return response()->json($e->getMessage(), 401);
        }
        return $this->json_success("OK", true);
    }
}
