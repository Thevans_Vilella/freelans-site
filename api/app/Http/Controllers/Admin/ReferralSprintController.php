<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ReferralSprint;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Tinker\Core\Exceptions\HttpCodes;

class ReferralSprintController extends Controller
{
    public function index()
    {
        $sprints = ReferralSprint::orderBy("start", "desc")
            ->get();
        if ($sprints->isEmpty()) {
            $newSprint = ReferralSprint::autoFill([
                "start" => now(),
                "finish" => Carbon::now()->addWeek(),
            ]);
            $newSprint->save();
            $sprints = ReferralSprint::orderBy("start", "desc")
                ->get();
        }

        return view("admin.referral-sprints.index")->with([
            "referralSprints" => $sprints
        ]);
    }

    /**
     * @param Request $request
     * @param ReferralSprint $referralSprint
     * @return RedirectResponse
     */
    public function update(Request $request, ReferralSprint $referralSprint)
    {
        $referralSprint->update($request->all());
        return back(HttpCodes::OK)->with("flash_message", "Sprint atualizada!");
    }

    public function updateCurrent(Request $request)
    {
        $requestData = $this->validate($request, [
            "finish" => "required"
        ]);

        $referralSprint = ReferralSprint::getCurrentSprint();
        $referralSprint->finish = $requestData["finish"];
        $referralSprint->save();

        return redirect(url("/admin/referral-sprint"));
    }

    public function finish(Request $request)
    {
        ReferralSprint::finishCurrent();

        return redirect(url("/admin/referral-sprint"));
    }

    public function show(Request $request, ReferralSprint $referralSprint)
    {
        return view("admin.referral-sprints.show")->with([
            "referralSprint" => $referralSprint,
            "referrals" => $referralSprint->getFullRanking()//->paginate(25)
        ]);
    }
}
