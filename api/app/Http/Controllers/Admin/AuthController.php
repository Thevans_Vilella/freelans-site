<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AuthController extends Controller
{
    use ValidatesRequests, RedirectsUsers;

    /**
     * @return View
     */
    public function login(): View
    {
        return view('admin.sessions.login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function authenticate(Request $request): RedirectResponse
    {
        $this->validate($request, [
            "email" => "required|email",
            "password" => "required"
        ]);

        $credentials = $this->getCredentials($request);

        if (Auth::guard('admin')->attempt($credentials)) {
            return redirect()->to('/admin');
        }

        return redirect()->back()
            ->withInput($request->only('eamil'))
            ->withErrors([
                'status' => 'Não encontramos registros com suas credenciais.'
            ]);
    }

    /**
     * @param Request $request
     * @return array
     */
    private function getCredentials(Request $request): array
    {
        return [
            "adm_email" => $request["email"],
            "adm_senha" => md5($request["password"]),
        ];
    }

    /**
     * @return mixed
     */
    public function guard()
    {
        return Auth::guard('admin');
    }
}
