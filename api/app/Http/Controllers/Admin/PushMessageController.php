<?php

namespace App\Http\Controllers\Admin;

use App\Models\PushMessage;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PushMessageController extends Controller
{
    use ValidatesRequests;

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $pushMessages = PushMessage::select()->where('id', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('message', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $pushMessages = PushMessage::select()->latest()->paginate($perPage);
        }

        return view('admin.push-message.index')->with([
            "pushMessages" => $pushMessages,
        ]);
    }

    /**
     * @return View
     */
    public function create() : View
    {
        return view("admin.push-message.create");
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "title" => "required",
            "message" => "required",
            "link" => "required"
        ]);

        $data = $request->all();
        $data["sender_id"] = $request->user("admin")->id;

        PushMessage::create($data);
    }
}
