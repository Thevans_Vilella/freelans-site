<?php
namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\Referral;

class DashboardController extends Controller
{
    public function index()
    {
        return redirect()->route('admin.dashboard');
    }

    public function basic()
    {
        $usersReferred = [
            "today" => [],
            "week" => [],
            "month" => [],
            "total" => [],
        ];
        $usersReferred["today"] = Referral::selectRaw("count(1) as amount")
            ->whereBetween("created_at", [date("Y-m-d 00:00:00"), date("Y-m-d 23:59:59")])
            ->get();
        $day = date('w');
        $weekStart = date('Y-m-d 00:00:00', strtotime('-' . $day . ' days'));
        $weekEnd = date('Y-m-d 23:59:59', strtotime('+' . (6 - $day) . ' days'));
        $usersReferred["week"] = Referral::selectRaw("count(1) as amount")
            ->whereBetween("created_at", [$weekStart, $weekEnd])
            ->get();
        $usersReferred["month"] = Referral::selectRaw("count(1) as amount")
            ->whereBetween("created_at", [date("Y-m-01 00:00:00"), date("Y-m-t 23:59:59")])
            ->get();
        $usersReferred["total"] = Profile::where("created_at");
        return view('admin.dashboard.basic', compact('usersReferred'));
    }
}
