<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Promotion;
use Auth;
use Illuminate\Http\JsonResponse;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $onlyMine = request()->has("mine") && request()->get("mine", "false") == "true";
        $promos = Promotion::select("promocao.*")
            ->join("perfil", "promocao.perf_codigo", "=", "perfil.perf_codigo")
            ->join("avaliacoes", "perfil.perf_codigo", "=", "avaliacoes.perf_codigo")
            ->when($onlyMine, function ($query) {
                $query->where("promocao.perf_codigo", "=", Auth::user()->id);
            })
            ->where("per_tip_codigo", 2)
            ->where("per_sta_codigo", 1)
            ->orderBy("pro_desconto", "desc")
            ->orderBy("ava_media_estrelas", "desc")
            ->paginate();

        return $this->json_success($promos);
    }
}
