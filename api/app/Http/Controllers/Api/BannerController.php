<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        return $this->json_success(Banner::selectRaw("distinct banner.*")
            ->join("banner_categorias", "banner.ban_codigo", "=", "banner_categorias.ban_codigo")
            ->when($request->has("category"), function ($query) use ($request) {
                $query->where("cat_codigo", $request->category);
            })->when($request->has("city"), function ($query) use ($request) {
                $query->where("cid_codigo", $request->city);
            })->orderBy("ban_ordem")->get());
    }
}
