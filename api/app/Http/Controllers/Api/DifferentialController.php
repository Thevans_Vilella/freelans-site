<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Differential;
use Illuminate\Http\Response;

class DifferentialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->json_success(Differential::orderBy("dest_ordem")->get());
    }
}
