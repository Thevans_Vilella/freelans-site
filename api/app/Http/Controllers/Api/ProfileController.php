<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\UploadableController;
use App\Models\Enum\Gender;
use App\Models\Favorite;
use App\Models\Profile;
use App\Models\ProfileType;
use App\Models\ProfileVisualization;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\DelegatesToResource;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ProfileController extends UploadableController
{
	use DelegatesToResource;

	/**
	 * Display a listing of the resource.
	 *
	 * @return JsonResponse|Response
	 */
	public function index(Request $request)
	{
		$keyword = $request->get('search');
		$id = null;
		$category = $request->get("category", 0);
		$city = $request->get("city", 0);
		$differentials = $request->get("differential", "");
		if (empty($keyword)) {
			$id = $request->get('id');
		}
		$perPage = $request->per_page ?: 12;
		$plan = $request->get("type", "ALL");

		$profiles = Profile::selectRaw("distinct perfil.*, planos.pla_ordem, avaliacoes.ava_media_estrelas" . (!empty($id) ? ", '' as large_version_for_json" : ""))
		                   ->join("planos", "perfil.pla_codigo", "=", "planos.pla_codigo")
		                   ->join("avaliacoes", "avaliacoes.perf_codigo", "=", "perfil.perf_codigo", "left")
		                   ->join("perfil_destaques", "perfil.perf_codigo", "=", "perfil_destaques.perf_codigo")
		                   ->where("per_tip_codigo", 2)
		                   ->where("per_sta_codigo", 1)
		                   ->when(!empty($id), function ($query) use ($id) {
			                   $query->where("perfil.perf_codigo", "=", $id);
		                   })
		                   ->when(!empty($keyword), function ($query) use ($keyword) {
			                   $query->where("perf_nome", "like", "%$keyword%");
		                   })
		                   ->when($category > 0, function ($query) use ($category) {
			                   $query->where("cat_codigo", "=", $category);
		                   })
		                   ->when($city > 0, function ($query) use ($city) {
			                   $query->where("cid_codigo", "=", $city);
		                   })
		                   ->when($differentials != "", function ($query) use ($differentials) {
			                   $query->where("perfil_destaques.dest_codigo", "in", explode($differentials, ";"));
		                   })
		                   ->when(($plan != "ALL") && ($plan != "0"), function ($query) use ($plan, &$perPage) {
			                   if ($plan != "FREE" && $plan != 4) {
				                   $query->where("planos.pla_codigo", "!=", 4);
				                   $perPage = 9;
			                   } elseif ($plan == "FREE" || $plan == 4) {
				                   $query->where("planos.pla_codigo", "=", 4);
			                   }
		                   })
			//->orderBy("pla_ordem desc")
			               ->orderBy("ava_media_estrelas", "desc")
		                   ->orderBy("perf_nome")
		                   ->paginate($perPage);
		if (!empty($id) && $id == Auth::id()) {
			$exists = ProfileVisualization::where("perf_ip", $request->ip())
			                              ->where("perf_codigo", $id)
			                              ->where("perf_data", date("Y-m-d"))
			                              ->exists();
			if (!$exists) {
				$visualization = new ProfileVisualization();
				$visualization->profile_id = $id;
				$visualization->date = date("Y-m-d");
				$visualization->time = date("H:i:s");
				$visualization->ip = $request->ip();
				$visualization->save();
			}
		}

		return $this->json_success($profiles);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param int $id
	 * @return JsonResponse|Response
	 */
	public function update(Request $request, $id)
	{
		$request->validate([
			"name" => "required|min:3|max:255",
			"phone" => "nullable",
			"mobile" => "required|min:3|max:255",
			"email" => "required|min:3|max:255",
			"policy" => "required|boolean",
			"birth_date" => "nullable|date",
			"gender" => "nullable|between:0,1",
			"profile_type" => "required|exists:perfil_tipo,per_tip_codigo",
			"image" => "nullable"
		]);

		if ($request->profile_type == ProfileType::PROVIDER) {
			$request->validate([
				"city" => "required|exists:cidades,cid_codigo",
				"job" => "required|exists:categorias,cat_codigo",
				"plan" => "required|exists:planos,pla_codigo"
			]);
		}

		$requestData = $request->all();
		$requestData["gender"] = isset($requestData["gender"]) ? Gender::toDatabase($requestData["gender"]) : null;
		$requestData["birth_date"] = isset($requestData["birth_date"]) ? date_create_from_format("d/m/Y", $requestData["birth_date"])->format("Y-m-d") : null;
		$requestData["phone"] = isset($requestData["phone"]) ? $requestData["phone"] : "";
		$requestData["status"] = 1;
		$requestData["policy"] = 1;

		if ($request->profile_type == ProfileType::CUSTOMER) {
			$requestData["category"] = 0;
			$requestData["city"] = 0;
			$requestData["plan"] = 0;
		}

		$profile = Profile::findOrFail($id);
		$profile->fillAttributes($requestData);
		$profile->fbid = isset($requestData["fbid"]) ? $requestData["fbid"] : null;
		$img_checksum = substr(md5(date("Ymd-His")), 0, 5);
		$fileName = Str::slug($profile->name) . "-" . $img_checksum;

		if (isset($requestData['image'])) {
			$this::sendFile($requestData['image'], config('filesystems.user_images_folder_small'), $fileName, 170, 127);
			$this::sendFile($requestData['image'],config('filesystems.user_images_folder_large'), $fileName, 460, 345);
		}

		$profile->save();

		return $this->json_success($profile);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param int $id
	 * @return JsonResponse|Response
	 */
	public function updateProfilephoto(Request $request, $id)
	{
		$request->validate([
			"image" => "required"
		]);

		$requestData = $request->all();

		$profile = Profile::findOrFail($id);
		$profile->fbid = isset($requestData["fbid"]) ? $requestData["fbid"] : null;
		$img_checksum = substr(md5(date("Ymd-His")), 0, 5);
		$fileName = Str::slug($profile->name) . "-" . $img_checksum;

		if (isset($requestData['image'])) {
			$this::sendFile($requestData['image'], config('filesystems.user_images_folder_small'), $fileName, 170, 127);
			$this::sendFile($requestData['image'], config('filesystems.user_images_folder_large'), $fileName, 460, 345);
		}

		$profile->small_avatar = config('filesystems.user_images_folder_small') . "/" . $fileName . ".jpg";
		$profile->large_avatar = config('filesystems.user_images_folder_large') . "/" . $fileName . ".jpg";
		$profile->save();

		return $this->json_success($profile);
	}

	/**
	 * sendFile: Método responsável por enviar o arquivo no formato base64
	 */
	private function sendFile($image, $path, $fileName, $widthToRezize = 0, $heighToRezize = 0)
	{
		$base64 = $image;
		$extension = '.jpg';
		$file = $base64;
		$fullFileName = $path . "/". $fileName . $extension;

		Storage::disk('public')->put($fullFileName, base64_decode($file));

		$storagePathWithFileName = Storage::disk('public')->path($fullFileName);

		$imageSize = getimagesize($storagePathWithFileName);
		$width = $imageSize[0];
		$height = $imageSize[1];

		$imageResize = Image::make($storagePathWithFileName);
		if ($width > $widthToRezize || $height >  $heighToRezize) {
			$maxWidth = $widthToRezize;
			$maxHeight = $heighToRezize;
			$imageResize->resize($maxWidth, $maxHeight, function ($constraint) {
				$constraint->aspectRatio();
			});
		}
		$imageResize->save(storage_path("app/public") . "/{$fullFileName}", 100, 'jpg');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return JsonResponse
	 */
	public function destroy($id)
	{
		$user = Profile::findOrFail($id);
		$user->status = 3;
		$user->name = "Excluído - " . $user->name;
		$user->save();

		return $this->json_success(true);
	}

	public function toggleFavorite($id)
	{
		$favorite = Favorite::where("perf_codigo", Auth::user()->id)
		                    ->where("perf_cod_usuario", $id)
		                    ->first();
		if (empty($favorite)) {
			$favorite = new Favorite();
			$favorite->perf_codigo = Auth::user()->id;
			$favorite->perf_cod_usuario = $id;
			$favorite->save();

			return $this->json_success(true);
		} else {
			$favorite->delete();

			return $this->json_success(false);
		}
	}

	/**
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function favorites(Request $request): JsonResponse
	{
		$perPage = $request->per_page ?: 12;
		$favorites = auth()->guard('api')->user()
		                   ->favoritesModel()
		                   ->join("avaliacoes", "avaliacoes.perf_codigo", "=", "perfil.perf_codigo", "left")
		                   ->orderBy("ava_media_estrelas", "desc")
		                   ->orderBy("perf_nome")
		                   ->paginate($perPage);
		return $this->json_success("Ok", $favorites);
	}
}
