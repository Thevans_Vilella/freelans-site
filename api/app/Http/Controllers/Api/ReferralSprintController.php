<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ReferralSprint;

class ReferralSprintController extends Controller
{
    public function index()
    {
        $currentSprint = ReferralSprint::getCurrentSprint();
        $ranking = array();
        if (!empty($currentSprint)) {
            $ranking['ranking'] = $currentSprint->getRanking();
            $userPosition = $currentSprint->getProfileRanking(auth()->user()->id);
            $ranking['current_user'] = $userPosition;
        } else {
            $ranking['ranking'] = [];
            $ranking['current_user'] = null;
        }

        return $this->json_success("Ok", $ranking);
    }
}
