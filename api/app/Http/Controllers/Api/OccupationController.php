<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Occupation;
use Auth;
use Illuminate\Http\JsonResponse;

class OccupationController extends Controller
{
    /**
     * OccupationController constructor.
     */
    public function __construct()
    {
        if (request("mine")) {
            $this->middleware("api.auth");
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        if (Auth::user() && \request("mine")) {
            return $this->json_success(Occupation::select("atuacao.*")
                ->join("atuacao_perfil", "atuacao.atua_codigo", "=", "atuacao_perfil.atua_codigo")
                ->where("atuacao_perfil.perf_codigo", "=", Auth::user()->id)
                ->orderBy("atua_nome")->get());
        }

        return $this->json_success(Occupation::orderBy("atua_nome")->get());
    }
}
