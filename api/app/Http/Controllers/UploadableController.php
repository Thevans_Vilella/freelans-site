<?php

namespace App\Http\Controllers;

use Tinker\Core\Traits\UploadableImage;

class UploadableController extends Controller
{
    use UploadableImage;
}
