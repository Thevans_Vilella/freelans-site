<?php
namespace App\Http\Middleware;

use App\Exceptions\JWTInvalidException;
use Closure;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ApiAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (JWTAuth::getToken() && JWTAuth::parseToken()->authenticate()) {
            $user = JWTAuth::parseToken()->authenticate();
        } else {
            $test = JWTAuth::toUser(JWTAuth::getToken());

            return response()->json(['error' => 'user_not_found'], 404);
        }
        return $next($request);
    }
}
