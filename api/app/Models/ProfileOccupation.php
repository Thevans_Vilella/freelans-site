<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileOccupation extends Model
{
    protected $table = 'atuacao_perfil';
    protected $primaryKey = 'atua_perf_codigo';

    protected $hidden = ['atua_perf_codigo', 'atua_codigo', 'perf_codigo'];
}
