<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string name
 */
class Differential extends Model
{
    protected $table = "destaques";

    protected $primaryKey = "dest_codigo";

    protected $hidden = [
        'dest_codigo', 'dest_ordem', 'dest_nome', 'laravel_through_key'
    ];

    protected $appends = [
        'id', 'name'
    ];

    public function getIdAttribute()
    {
        return $this->dest_codigo;
    }

    public function setIdAttribute($value) {
        $this->dest_codigo = $value;
    }

    public function getNameAttribute() {
        return $this->dest_nome;
    }

    public function setNameAttribute($value) {
        $this->dest_nome = $value;
    }
}
