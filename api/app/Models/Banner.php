<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

/**
 * @property int id
 * @property string url
 * @property string image_small
 * @property string image_medium
 * @property string image_large
 */
class Banner extends Model
{
    protected $table = "banner";

    protected $hidden = [
        'ban_codigo', 'ban_ordem', 'ban_site', 'ban_fotop', 'ban_fotom', 'ban_fotog'
    ];

    protected $appends = [
        'id', 'url', 'image_small', 'image_medium', 'image_large'
    ];

    public function getIdAttribute() {
        return $this->ban_codigo;
    }

    public function setIdAttribute($value) {
        $this->id = $value;
    }

    public function getUrlAttribute() {
        return $this->ban_site;
    }

    public function setUrlAttribute($value) {
        $this->ban_site = $value;
    }

    public function getImageSmallAttribute() {
        return Storage::disk('public')->url('freelans/' . $this->ban_fotop);
    }

    public function setImageSmallAttribute($value) {
        $this->ban_fotop = $value;
    }

    public function getImageMediumAttribute() {
        return Storage::disk('public')->url('freelans/' . $this->ban_fotom);
    }

    public function setImageMediumAttribute($value) {
        $this->ban_fotom = $value;
    }

    public function getImageLargeAttribute() {
        return Storage::disk('public')->url('freelans/' . $this->ban_fotog);
    }

    public function setImageLargeAttribute($value) {
        $this->ban_fotog = $value;
    }
}
