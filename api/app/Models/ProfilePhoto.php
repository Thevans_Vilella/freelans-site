<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfilePhoto extends Model
{
    protected $table = 'perfil_fotos';
    protected $primaryKey = 'perf_fot_codigo';

    protected $hidden = ['perf_fot_codigo', 'perf_codigo', 'perf_fot_fotop', 'perf_fot_fotog'];

    protected $appends = ['photo_large', 'photo_small'];

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    public function getPhotoLargeAttribute()
    {
        return $this->perf_fot_fotog;
    }

    public function setPhotoLargeAttribute($value)
    {
        $this->perf_fot_fotog = $value;
    }

    public function getPhotoSmallAttribute()
    {
        return $this->perf_fot_fotop;
    }

    public function setPhotoSmallAttribute($value)
    {
        $this->perf_fot_fotop = $value;
    }
}
