<?php

namespace App\Models;

use Auth;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Kreait\Firebase;
use Laravel\Passport\HasApiTokens;
use Storage;
use Tinker\Core\Models\BaseModel;
use Tinker\Core\Models\Mapper;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class Profile
 * @package App\Models
 * @property mixed id
 * @property mixed name
 * @property mixed phone
 * @property mixed mobile
 * @property mixed email
 * @property mixed small_avatar
 * @property mixed large_avatar
 */
class Profile extends Authenticatable implements JWTSubject
{
    use BaseModel, Mapper, Notifiable, HasApiTokens, MustVerifyEmail;

    public $timestamps = false;
    protected $table = 'perfil';
    protected $primaryKey = "perf_codigo";
    protected $fillable = ["fbid"];
    protected $hidden = [
        'perf_codigo', 'per_tip_codigo', 'per_sta_codigo', 'pol_codigo', 'cid_codigo', 'cat_codigo', 'pla_codigo',
        'perf_nome', 'perf_telefone', 'perf_celular', 'perf_email', 'perf_login', 'perf_senha', 'perf_data',
        'perf_hora', 'perf_ip', 'perf_fotop', 'perf_fotog', 'password', 'perf_key1', 'perf_key2', 'perf_key3',
        'fbid'
    ];
    protected $appends = [
        'id', 'profile_type', 'name', 'phone', 'mobile', 'email', 'small_avatar', 'large_avatar', 'status', 'policy',
        'city', 'category', 'plan', 'referral_url'
    ];
    private $factory;
//    protected $casts = [ 'profile_type', 'perf_codigo' => 'integer' ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->factory = new Firebase\Factory();
    }

    public static function boot()
    {
        static::saving(function ($callback) {
            if ($callback->perf_data == null) {
                $callback->perf_data = date("Y-m-d");
            }

            if ($callback->perf_hora == null) {
                $callback->perf_hora = date("H:i:s");
            }
        });

        parent::boot();
    }

    public function toArray()
    {
        $result = parent::toArray();
        $result["is_premium"] = isset($this->planModel) ? $this->planModel->id != 4 : 4;
        $result["birth_date"] = $this->birth_date;
        $result['gender'] = $this->gender;
        $result["category"] = $this->jobModel;
        $result["rate"] = $this->rateModel;
        $result["plan"] = $this->planModel;
        $result["city"] = $this->cityModel;

        if (!isset($result["large_version_for_json"])) {
            return $result;
        }
        unset($result["large_version_for_json"]);

        if (isset($this->aboutModel)) {
            $result['about'] = $this->aboutModel->description;
            $result['map'] = $this->aboutModel->map;
        }
        $result["is_favorite"] = $this->favoritesIamModel()->where("perf_codigo", Auth::user()->id)->count() > 0;
        $result["visualizations"] = $this->visualizationModel()->count();
        $result["occupation"] = $this->occupationModel()->get();
        $result["differential"] = $this->differentialModel;
        $result["contacts"] = $this->contactModel;
        $result["social"] = $this->socialModel;
        $result["photos"] = $this->photosModel;
        $result["videos"] = $this->videosModel;
        return $result;
    }

    public function favoritesIamModel()
    {
        return $this->hasMany(Favorite::class, 'perf_cod_usuario');
    }

    public function visualizationModel()
    {
        return $this->hasMany(ProfileVisualization::class, "perf_codigo");
    }

    public function occupationModel()
    {
        return $this->hasManyThrough(Occupation::class, ProfileOccupation::class,
            'perf_codigo', 'atua_codigo', 'perf_codigo', 'atua_codigo');
    }

    function getProfileTypeAttribute()
    {
        return $this->per_tip_codigo;
    }

    function setProfileTypeAttribute($value)
    {
        $this->per_tip_codigo = $value;
    }

    function getStatusAttribute()
    {
        return $this->per_sta_codigo;
    }

    function setStatusAttribute($value)
    {
        $this->per_sta_codigo = $value;
    }

    function getPolicyAttribute()
    {
        return $this->pol_codigo;
    }

    function setPolicyAttribute($value)
    {
        $this->pol_codigo = $value;
    }

    function getCityAttribute()
    {
        return $this->cid_codigo;
    }

    function setCityAttribute($value)
    {
        $this->cid_codigo = $value;
    }

    function getCategoryAttribute()
    {
        return $this->cat_codigo;
    }

    function setCategoryAttribute($value)
    {
        $this->cat_codigo = $value;
    }

    function getPlanAttribute()
    {
        return $this->pla_codigo;
    }

    function setPlanAttribute($value)
    {
        $this->pla_codigo = $value;
    }

    function getAuthIdentifierName()
    {
        return "perf_codigo";
    }

    function getAuthIdentifier()
    {
        return $this->perf_email;
    }

    public function getAuthPassword()
    {
        return $this->perf_senha;
    }

    function getIdAttribute()
    {
        return $this->perf_codigo;
    }

    function getNameAttribute()
    {
        return $this->perf_nome;
    }

    function getPhoneAttribute()
    {
        return $this->perf_telefone;
    }

    function getMobileAttribute()
    {
        return $this->perf_celular;
    }

    function getEmailAttribute()
    {
        return $this->perf_email;
    }

    function getSmallAvatarAttribute()
    {
        return Storage::disk('public')->url($this->perf_fotop);
    }

    function getLargeAvatarAttribute()
    {
        return Storage::disk('public')->url($this->perf_fotog);
    }

    function getReferralUrlAttribute(): string
    {
        $uid = $this->uid ?? $this->generateUid();
        return sprintf(env("DYNAMIC_INVITE_LINK_URL"), $uid);
    }

    /**
     * @return string
     */
    private function generateUid(): string
    {
        $uid = Str::uuid();

        Profile::where("perf_codigo", $this->id)
            ->update([
                "uid" => $uid
            ]);

        return $uid;
    }

    function setIdAttribute($value)
    {
        $this->perf_codigo = $value;
    }

    function setNameAttribute($value)
    {
        $this->perf_nome = $value;
    }

    function setPhoneAttribute($value)
    {
        $this->perf_telefone = $value;
    }

    function setMobileAttribute($value)
    {
        $this->perf_celular = $value;
    }

    function setEmailAttribute($value)
    {
        $this->perf_email = $value;
    }

    function setSmallAvatarAttribute($value)
    {
        $this->perf_fotop = $value;
    }

    function setLargeAvatarAttribute($value)
    {
        $this->perf_fotog = $value;
    }

    function aboutModel()
    {
        return $this->hasOne(ProfileAbout::class, 'perf_codigo', 'perf_codigo');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function cityModel()
    {
        return $this->hasOne(City::class, 'cid_codigo', 'cid_codigo');
    }

    public function jobModel()
    {
        return $this->hasOne(Category::class, 'cat_codigo', 'cat_codigo');
    }

    public function planModel()
    {
        return $this->hasOne(Plan::class, 'pla_codigo', 'pla_codigo');
    }

    public function rateModel()
    {
        return $this->hasOne(Rating::class, 'perf_codigo', 'perf_codigo');
    }

    public function contactModel()
    {
        return $this->hasMany(ProfileContactData::class, 'perf_codigo');
    }

    public function socialModel()
    {
        return $this->hasMany(ProfileMediaData::class, 'perf_codigo');
    }

    public function differentialModel()
    {
        return $this->hasManyThrough(Differential::class, ProfileDifferential::class,
            'perf_codigo', 'dest_codigo', 'perf_codigo', 'dest_codigo');
    }

    public function photosModel()
    {
        return $this->hasMany(ProfilePhoto::class, 'perf_codigo');
    }

    public function videosModel()
    {
        return $this->hasMany(ProfileVideo::class, 'perf_codigo');
    }

    public function favoritesModel()
    {
        return $this->belongsToMany(Profile::class, "favoritos", "perf_codigo", "perf_cod_usuario");
    }

    public function routeNotificationForFcm()
    {
        return $this->fbid;
    }

    public function save(array $options = [])
    {
        if (!isset($this->uid)) {
            $this->uid = Str::uuid();
        }

        return parent::save($options);
    }
}
