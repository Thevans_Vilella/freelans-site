<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class ContactMedia extends Model
{
    protected $table = 'midias_contato';
    protected $primaryKey = 'mid_cont_codigo';

    protected $hidden = ['mid_cont_codigo', 'mid_cont_ordem', 'mid_cont_nome', 'mid_cont_exemplo', 'mid_cont_foto'];

    protected $appends = ['name', 'icon'];

    public function getNameAttribute()
    {
        return $this->mid_cont_nome;
    }

    public function setNameAttribute($value)
    {
        $this->mid_cont_nome = $value;
    }

    public function getIconAttribute()
    {
        return Storage::disk('public')->url('freelans/' . $this->mid_cont_foto);
    }

    public function setIconAttribute($value)
    {
        $this->mid_cont_foto = $value;
    }
}
