<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    protected $table = "referrals";

    protected $fillable = [
        "referral_sprint_id", "referrer_id", "referred_id"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function referralSprint()
    {
        return $this->belongsTo(ReferralSprint::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function referrer()
    {
        return $this->hasOne(
            Profile::class,
            "referrer_id",
            "perf_codigo"
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function referred()
    {
        return $this->hasOne(
            Profile::class,
            "referred_id",
            "perf_codigo"
        );
    }

    /**
     * @param string $referrerUid
     * @param int $referredId
     * @return Referral
     */
    public static function makeReferralWithReferrerUid(string $referrerUid, int $referredId): Referral
    {
        $referrer = Profile::where('uid', $referrerUid)
            ->first();

        return Referral::create([
            'referrer_id' => $referrer->id,
            'referred_id' => $referredId
        ]);
    }

    /**
     * @param array $data
     * @return Referral
     */
    public static function create(array $data): Referral
    {
        $data['referral_sprint_id'] = $data['referral_sprint_id'] ?? ReferralSprint::getCurrentSprint()->id;
        return static::query()->create($data);
    }
}
