<?php

namespace App\Models;

use App\Events\ReferralSprintFinishedEvent;
use DB;
use Illuminate\Database\Eloquent\Model;
use Tinker\Core\Models\BaseModel;

class ReferralSprint extends Model
{
    use BaseModel;

    protected $table = "referral_sprints";

    protected $fillable = [
        "start", "finish", "status", "winner_id"
    ];

    /**
     * @param array $data
     * @return ReferralSprint
     */
    public static function create(array $data): ReferralSprint
    {
        $oldSprint = ReferralSprint::where("status", true)->get();
        $oldSprint->status = false;
        $oldSprint->save();
        return static::query()->create($data);
    }

    /**
     * @return ReferralSprint
     */
    public static function getCurrentSprint()
    {
        return ReferralSprint::where("status", true)->first();
    }

    public static function finishCurrent()
    {
        event(new ReferralSprintFinishedEvent());
    }

    public function referrals()
    {
        return $this->hasMany(Referral::class);
    }

    public function winner()
    {
        return $this->belongsTo(
            Profile::class,
            "winner_id",
            "perf_codigo"
        );
    }

    /**
     * @return string
     */
    public function getTitleTime(): string
    {
        return date("d/m/Y", strtotime($this->start)) . " - " .
            date("d/m/Y", strtotime($this->finish));
    }

    public function getWinner()
    {
        $ranking = $this->getRanking();

        if (count($ranking) == 0) {
            return null;
        }

        return $this->getRanking()[0];
    }

    public function getRanking()
    {
        $queryResult = $this->getFullRanking();
        return array_slice($queryResult, 0, 10);
    }

    public function getFullRanking()
    {
        return DB::select(DB::raw("SELECT 
            @row:=@row + 1 AS position, hax.*
            FROM
                (SELECT 
                    referrer_id, perf_nome AS name, COUNT(*) AS total
                FROM
                    referrals
                INNER JOIN `perfil` ON `referrals`.`referrer_id` = `perf_codigo`
                WHERE referrals.referral_sprint_id = $this->id
                GROUP BY `referrer_id`
                ORDER BY `total` DESC) AS hax,
                (SELECT @row:=0) AS position"));
    }

    public function getProfileRanking(int $profileId)
    {
        $ranking = $this->getFullRanking();

        foreach ($ranking as $position) {
            if ($position->referrer_id == $profileId) {
                return $position;
            }
        }

        return null;
    }

    /**
     * @param array $attributes
     * @param array $options
     * @return bool
     */
    public function update(array $attributes = Array(), array $options = Array()): bool
    {
        if (isset($attributes["status"]) && !$attributes["status"]) {
            event(new ReferralSprintFinishedEvent());
        }

        return parent::update($attributes, $options);
    }
}
