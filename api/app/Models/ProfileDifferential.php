<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileDifferential extends Model
{
    protected $table = 'perfil_destaques';
    protected $primaryKey = 'perf_dest_codigo';

    protected $hidden = ['perf_dest_codigo', 'dest_codigo', 'perf_codigo'];
}
