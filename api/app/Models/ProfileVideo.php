<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed link
 */
class ProfileVideo extends Model
{
    protected $table = 'perfil_videos';
    protected $primaryKey = 'perf_vid_codigo';

    protected $hidden = ['perf_vid_codigo', 'perf_codigo', 'perf_vid_tag', 'perf_vid_link'];

    protected $appends = ['link'];

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    private function getThumbnail()
    {
        $matches = null;
        if (preg_match("/^(http(s?):\/\/)?(www\.)?youtu(be)?\.([a-z])+\/(watch(.*?)(\?|&)v=)?(.*?)(&(.)*)?$/",
                $this->link, $matches) == true) {
            return "https://img.youtube.com/vi/$matches[9]/default.jpg";
        }

        return $this->link;
    }

    public function toArray()
    {
        return [
            "link" => $this->link,
            "thumb" => $this->getThumbnail()
        ];
    }

    public function getLinkAttribute()
    {
        return $this->perf_vid_link;
    }

    public function setLinkAttribute($value)
    {
        $this->perf_vid_link = $value;
    }
}
