<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rating
 * @package App\Models
 * @property int id
 * @property int service
 * @property int quality
 * @property int punctuality
 * @property int price
 * @property int agility
 * @property int average
 * @property int votes
 */
class Rating extends Model
{
    protected $table = "avaliacoes";

    protected $primaryKey = "ava_codigo";

    protected $hidden = ['ava_codigo', 'perf_codigo', 'ava_atendimento', 'ava_qualidadade', 'ava_pontualidade',
        'ava_preco', 'ava_agilidade', 'ava_media_estrelas', 'ava_qtde_votos'];

    protected $appends = ['id', 'service', 'quality', 'punctuality', 'price', 'agility', 'average', 'votes'];

    function getIdAttribute()
    {
        return $this->ava_codigo;
    }

    function getServiceAttribute()
    {
        return $this->ava_atendimento;
    }

    function getQualityAttribute()
    {
        return $this->ava_qualidade;
    }

    function getPunctualityAttribute()
    {
        return $this->ava_pontualidade;
    }

    function getPriceAttribute()
    {
        return $this->ava_preco;
    }

    function getAgilityAttribute()
    {
        return $this->ava_agilidade;
    }

    function getAverageAttribute()
    {
        return $this->ava_media_estrelas;
    }

    function getVotesAttribute()
    {
        return $this->ava_qtde_votos;
    }

    function setIdAttribute($value)
    {
        $this->ava_codigo = $value;
    }

    function setServiceAttribute($value)
    {
        $this->ava_atendimento = $value;
    }

    function setQualityAttribute($value)
    {
        $this->ava_qualidade = $value;
    }

    function setPunctualityAttribute($value)
    {
        $this->ava_pontualidade = $value;
    }

    function setPriceAttribute($value)
    {
        $this->ava_preco = $value;
    }

    function setAgilityAttribute($value)
    {
        $this->ava_agilidade = $value;
    }

    function setAverageAttribute($value)
    {
        $this->ava_media_estrelas = $value;
    }

    function setVotesAttribute($value)
    {
        $this->ava_qtde_votos = $value;
    }
}
