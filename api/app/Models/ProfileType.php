<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProfileType
 * @package App\Models
 * @property int id
 * @property string name
 */
class ProfileType extends Model
{
    const CUSTOMER = 1;
    const PROVIDER = 2;

    protected $table = 'perfil_tipo';

    protected $primaryKey = "per_tip_codigo";

    protected $hidden = ['per_tip_codigo', 'per_tip_ordem', 'per_tip_nome'];

    protected $appends = ['id', 'name'];

    function getIdAttribute()
    {
        return $this->per_tip_codigo;
    }

    function getNameAttribute()
    {
        return $this->per_tip_nome;
    }

    function setIdAttribute($value)
    {
        $this->per_tip_codigo = $value;
    }

    function setNameAttribute($value)
    {
        $this->per_tip_nome = $value;
    }
}
