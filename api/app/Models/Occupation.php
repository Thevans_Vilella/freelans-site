<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string name
 */
class Occupation extends Model
{
    protected $table = "atuacao";

    protected $primaryKey = "atua_codigo";

    protected $hidden = [
        'atua_codigo', 'atua_nome', 'cat_codigo', 'laravel_through_key'
    ];

    protected $appends = [
        'id', 'name'
    ];

    public function getIdAttribute() {
        return $this->atua_codigo;
    }

    public function setIdAttribute($value) {
        $this->atua_codigo = $value;
    }

    public function getNameAttribute() {
        return $this->atua_nome;
    }

    public function setNameAttribute($value) {
        $this->atua_nome = $value;
    }
}
