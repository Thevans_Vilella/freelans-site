<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categorias";

    protected $primaryKey = "cat_codigo";

    protected $hidden = [
        'cat_codigo', 'cat_ordem', 'cat_nome'
    ];

    protected $appends = [
        'id', 'name'
    ];

    public function getIdAttribute() {
        return $this->cat_codigo;
    }

    public function setIdAttribute($value) {
        $this->cat_codigo = $value;
    }

    public function getNameAttribute() {
        return $this->cat_nome;
    }

    public function setNameAttribute($value) {
        $this->cat_nome = $value;
    }
}
