<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileMediaData extends Model
{
    protected $table = 'perfil_midias_dados';
    protected $primaryKey = 'perf_mid_soc_codigo';

    public function toArray()
    {
        return [
            "type" => $this->contactMediaModel->name,
            "icon" => $this->contactMediaModel->icon,
            "item" => $this->url
        ];
    }

    public function getUrlAttribute()
    {
        return $this->perf_cont_dad_item;
    }

    public function setUrlAttribute($value)
    {
        $this->perf_cont_dad_item = $value;
    }

    public function contactMediaModel()
    {
        return $this->hasOne(ContactMedia::class, 'mid_cont_codigo', 'mid_cont_codigo');
    }
}
