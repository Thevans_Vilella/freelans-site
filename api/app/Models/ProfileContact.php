<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class ProfileContact extends Model
{
    protected $table = 'perfil_contato';
    protected $primaryKey = 'per_cont_codigo';

    protected $hidden = ["per_cont_codigo", "per_cont_ordem", "per_cont_nome", "per_cont_foto"];

    protected $appends = ["name", "icon"];

    public function getNameAttribute()
    {
        return $this->per_cont_nome;
    }

    public function setNameAttribute($value)
    {
        $this->per_cont_nome = $value;
    }

    public function getIconAttribute()
    {
        return Storage::disk('public')->url('freelans/' . $this->per_cont_foto);
    }

    public function setIconAttribute($value)
    {
        $this->per_cont_foto = $value;
    }
}
