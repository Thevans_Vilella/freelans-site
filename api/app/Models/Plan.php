<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $table = "planos";

    protected $primaryKey = "pla_codigo";

    protected $hidden = [
        'pla_codigo', 'opc_codigo', 'pla_ordem', 'pla_nome', 'pla_titulo', 'pla_valor', 'pla_qtd_atuacao',
        'pla_qtd_fotos', 'pla_qtd_video', 'pla_qtd_promo', 'pla_desc'
    ];

    protected $appends = [
        'id', 'name', 'title', 'description', 'value', 'qt_occupations', 'qt_photos', 'qt_videos', 'qt_promos'
    ];

    public function getIdAttribute() {
        return $this->pla_codigo;
    }

    public function setIdAttribute($value) {
        $this->pla_codigo = $value;
    }

    public function getNameAttribute() {
        return $this->pla_nome;
    }

    public function setNameAttribute($value) {
        $this->pla_nome = $value;
    }

    public function getTitleAttribute() {
        return $this->pla_titulo;
    }

    public function setTitleAttribute($value) {
        $this->pla_titulo = $value;
    }

    public function getDescriptionAttribute() {
        return $this->pla_desc;
    }

    public function setDescriptionAttribute($value) {
        $this->pla_desc = $value;
    }

    public function getValueAttribute() {
        return $this->pla_valor;
    }

    public function setValueAttribute($value) {
        $this->pla_valor = $value;
    }

    public function getQtOccupationsAttribute() {
        return $this->pla_qtd_atuacao;
    }

    public function setQtOccupationsAttribute($value) {
        $this->pla_qtd_atuacao = $value;
    }

    public function getQtPhotosAttribute() {
        return $this->pla_qtd_fotos;
    }

    public function setQtPhotosAttribute($value) {
        $this->pla_qtd_fotos = $value;
    }

    public function getQtVideosAttribute() {
        return $this->pla_qtd_video;
    }

    public function setQtVideosAttribute($value) {
        $this->pla_qtd_video = $value;
    }

    public function getQtPromosAttribute() {
        return $this->pla_qtd_promo;
    }

    public function setQtPromosAttribute($value) {
        $this->pla_qtd_promo = $value;
    }
}
