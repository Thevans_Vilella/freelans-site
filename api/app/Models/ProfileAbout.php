<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProfileAbout
 * @package App\Models
 * @property mixed map
 * @property mixed description
 */
class ProfileAbout extends Model
{
    protected $table = 'perfil_sobre';

    protected $primaryKey = "perf_sob_codigo";

    protected $hidden = ['perf_sob_codigo', 'perf_codigo', 'perf_sob_mapa', 'perf_sob_descricao'];

    protected $appends = ['map', 'description'];

    function getMapAttribute()
    {
        return $this->perf_sob_mapa;
    }

    function getDescriptionAttribute()
    {
        return $this->perf_sob_descricao;
    }

    function setMapAttribute($value)
    {
        $this->perf_sob_mapa = $value;
    }

    function setDescriptionAttribute($value)
    {
        $this->perf_sob_descricao = $value;
    }
}
