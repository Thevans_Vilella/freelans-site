<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

/**
 * @property mixed id
 * @property mixed description
 * @property mixed discount
 * @property mixed photo_small
 * @property mixed photo_large
 * @property mixed profile
 */
class Promotion extends Model
{
    protected $table = "promocao";

    protected $fillable = ['gender', 'birth_date'];

    protected $hidden = [
        'pro_codigo', 'perf_codigo', 'atua_codigo', 'pro_desconto', 'pro_descricao', 'pro_fotop', 'pro_fotog'
    ];

    protected $appends = ['id', 'description', 'discount', 'photo_small', 'photo_large', 'profile'];

    public function toArray()
    {
        return [
            "id" => $this->id,
            "description" => $this->description,
            "discount" => $this->discount,
            "photo_small" => $this->photo_small,
            "photo_large" => $this->photo_large,
            "profile" => $this->profile
        ];
    }

    public function getIdAttribute()
    {
        return $this->pro_codigo;
    }

    public function setIdAttribute($value)
    {
        $this->pro_codigo = $value;
    }

    public function getDescriptionAttribute()
    {
        return $this->pro_descricao;
    }

    public function setDescriptionAttribute($value) {
        $this->pro_descricao = $value;
    }

    public function getDiscountAttribute() {
        return $this->pro_desconto;
    }

    public function setDiscountAttribute($value) {
        $this->pro_desconto = $value;
    }

    public function getPhotoSmallAttribute() {
        return Storage::disk('public')->url('freelans/' . $this->pro_fotop);
    }

    public function setPhotoSmallAttribute($value) {
        $this->pro_fotop = $value;
    }

    public function getPhotoLargeAttribute() {
        return Storage::disk('public')->url('freelans/' . $this->pro_fotog);
    }

    public function setPhotoLargeAttribute($value) {
        $this->pro_fotog = $value;
    }

    public function getProfileAttribute()
    {
        return $this->profileModel;
    }

    public function getGenderAttribute()
    {
        return $this->gender;
    }

    public function setGenderAttribute($value)
    {
        $this->gender = $value;
    }

    public function profileModel() {
        return $this->belongsTo(Profile::class, 'perf_codigo', 'perf_codigo');
    }

    public function occupationModel() {
        return $this->belongsTo(Occupation::class, 'atua_codigo', 'atual_codigo');
    }
}
