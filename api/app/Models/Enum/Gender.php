<?php

namespace App\Models\Enum;

use Tinker\Core\Models\BasicEnum;

class Gender extends BasicEnum
{
    const Male = 0;
    const Female = 1;

    public static function toDatabase($value)
    {
        switch ($value) {
            case 0:
                return 'M';
            case 1:
                return 'F';
            default:
                return null;
        }
    }
}
