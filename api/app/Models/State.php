<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = "estado";

    protected $primaryKey = "est_codigo";

    protected $hidden = [
        'est_codigo', 'est_sigla', 'est_nome'
    ];

    protected $appends = [
        'id', 'name', 'initial'
    ];

    public function getIdAttribute()
    {
        return $this->est_codigo;
    }

    public function setIdAttribute($value)
    {
        $this->est_codigo = $value;
    }

    public function getNameAttribute()
    {
        return $this->est_nome;
    }

    public function setNameAttribute($value)
    {
        $this->est_nome = $value;
    }

    public function getInitialAttribute()
    {
        return $this->est_sigla;
    }

    public function setInitialAttribute($value)
    {
        $this->est_sigla = $value;
    }
}
