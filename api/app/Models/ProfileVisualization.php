<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileVisualization extends Model
{
    protected $table = 'perfil_visualizacoes';
    protected $primaryKey = 'perf_vis_codigo';

    protected $attributes = [
        'profile_id', 'date', 'time', 'ip'
    ];

    public function getProfileId()
    {
        return $this->perf_codigo;
    }

    public function getDate()
    {
        return $this->perf_data;
    }

    public function getTime()
    {
        return $this->perf_hora;
    }

    public function getIp()
    {
        return $this->perf_ip;
    }

    public function setProfileId($value)
    {
        $this->perf_codigo = $value;
    }

    public function setDate($value)
    {
        $this->perf_data = $value;
    }

    public function setTime($value)
    {
        $this->perf_hora = $value;
    }

    public function setIp($value)
    {
        $this->perf_ip = $value;
    }
}
