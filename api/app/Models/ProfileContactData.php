<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed type
 * @property mixed item
 */
class ProfileContactData extends Model
{
    protected $table = 'perfil_contato_dados';
    protected $primaryKey = 'perf_cont_dad_codigo';

    protected $hidden = ["perf_cont_dad_codigo", "per_cont_codigo", "perf_codigo", "perf_cont_dad_item"];

    protected $appends = ["type", "item"];

    public function toArray()
    {
        return [
            "type" => $this->typeModel->name,
            "icon" => $this->typeModel->icon,
            "item" => $this->item
        ];
    }

    public function typeModel()
    {
        return $this->hasOne(ProfileContact::class, 'per_cont_codigo', 'per_cont_codigo');
    }

    public function getTypeAttribute()
    {
        return $this->per_cont_codigo;
    }

    public function setTypeAttribute($value)
    {
        $this->per_cont_codigo = $value;
    }

    public function getItemAttribute()
    {
        return $this->perf_cont_dad_item;
    }

    public function setItemAttributeAttribute($value)
    {
        $this->perf_cont_dad_item = $value;
    }
}
