<?php

namespace App\Models;

use App\Events\NewPushMessageEvent;
use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PushMessage
 * @package App\Models
 *
 * @property int id
 * @property int sender_id
 * @property string title
 * @property string message
 * @property string link
 * @property int category_id
 * @property int profile_type
 * @property int city_id
 */
class PushMessage extends Model
{
    protected $table = "push_messages";

    protected $fillable = [
        "id", "sender_id", "title", "message", "link"
    ];

    protected $hidden = [
        "created_at", "updated_at"
    ];

    public function categoriesModel()
    {
        return $this->belongsToMany(Category::class, "category_push_message", "push_message_id", "category_id");
    }

    public function profileTypesModel()
    {
        return $this->belongsToMany(ProfileType::class, "profile_type_push_message", "push_message_id", "profile_type_id");
    }

    public function citiesModel()
    {
        return $this->belongsToMany(City::class, "city_push_message", "push_message_id", "city_id");
    }

    /**
     * @return array
     */
    public function getCategoriesIds() : array
    {
        return $this->categoriesModel->pluck("id")->toArray();
    }

    /**
     * @return array
     */
    public function getProfileTypesIds() : array
    {
        return $this->profileTypesModel->pluck("id")->toArray();
    }

    /**
     * @return array
     */
    public function getCitiesIds() : array
    {
        return $this->citiesModel->pluck("id")->toArray();
    }

    /**
     * @param array $data
     * @return PushMessage
     */
    public static function create(array $data): PushMessage
    {
        $pushMessage = static::query()->create($data);

        if (isset($data["profile_types"])) {
            $profileTypes = ProfileType::whereIn("per_tip_codigo", $data["profile_types"])->get();
        } else {
            $profileTypes = ProfileType::all();
        }

        if (isset($data["category_ids"])) {
            $categories = Category::whereIn("cat_codigo", $data["category_ids"])->get();
        } else {
            $categories = Category::all();
        }

        if (isset($data["city_ids"])) {
            $cities = City::whereIn("cid_codigo", $data["city_ids"])->get();
        } else {
            $cities = City::all();
        }

        $pushMessage->profileTypesModel()->attach($profileTypes->pluck("id"));
        $pushMessage->categoriesModel()->attach($categories->pluck("id"));
        $pushMessage->citiesModel()->attach($cities->pluck("id"));

        event(new NewPushMessageEvent($pushMessage));
        return $pushMessage;
    }
}
