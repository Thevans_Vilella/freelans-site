<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = "cidades";

    protected $primaryKey = "cid_codigo";

    protected $hidden = [
        'cid_codigo', 'est_codigo', 'cid_referencia', 'cid_nome', 'stateModel'
    ];

    protected $appends = [
        'id', 'name', 'state'
    ];

    public function getIdAttribute()
    {
        return $this->cid_codigo;
    }

    public function setIdAttribute($value)
    {
        $this->cid_codigo = $value;
    }

    public function getNameAttribute()
    {
        return $this->cid_nome;
    }

    public function setNameAttribute($value)
    {
        $this->cid_nome = $value;
    }

    public function getStateAttribute()
    {
        return $this->stateModel->initial;
    }

    public function stateModel()
    {
        return $this->hasOne(State::class, 'est_codigo', 'est_codigo');
    }
}
