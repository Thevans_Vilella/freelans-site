<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Administrator extends Authenticatable
{
    protected $table = "administrador";

    protected $primaryKey = "adm_codigo";

    protected $fillable = [
        "adm_codigo", "adm_nome", "adm_email", "adm_login", "adm_senha", "adm_data", "adm_hora",
        "adm_ip", "adm_data_altera", "adm_hora_altera", "adm_ip_altera", "adm_cod_altera"
    ];

    protected $hidden = [
        "adm_senha", "adm_ip", "adm_login", "adm_data", "adm_hora", "adm_data_altera", "adm_hora_altera",
        "adm_ip_altera", "adm_cod_altera"
    ];

    protected $appends = [
        "id", "name", "email", "password"
    ];

    public function getIdAttribute(): int
    {
        return $this->adm_codigo;
    }

    public function setIdAttribute(int $id)
    {
        $this->adm_codigo = $id;
    }

    public function getNameAttribute(): string
    {
        return $this->adm_nome;
    }

    public function setNameAttribute(string $name)
    {
        $this->adm_nome = $name;
    }

    public function getEmailAttribute(): string
    {
        return $this->adm_email;
    }

    public function setEmailAttribute(string $value)
    {
        $this->adm_email = $value;
    }

    public function getPasswordAttribute(): string
    {
        return $this->adm_senha;
    }

    public function setPasswordAttribute(string $value)
    {
        $this->adm_senha = $value;
    }
}
