<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    public $timestamps = false;

    protected $table = 'favoritos';
    protected $primaryKey = 'fav_codigo';

    protected $hidden = ['fav_codigo', 'perf_codigo', 'perf_cod_usuario'];
}
