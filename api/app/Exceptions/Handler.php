<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tinker\Core\Exceptions\Handler as ExceptionHandler;
use Tinker\Core\Exceptions\HttpCodes;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $exception
     * @return JsonResponse|RedirectResponse|Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof TokenExpiredException) {
            return response()->json([
                "status" => false,
                "errorCode" => $exception->getCode(),
                "message" => $exception->getMessage()
            ], HttpCodes::Unauthorized);
        } else if ($exception instanceof TokenInvalidException) {
            return response()->json([
                "status" => false,
                "errorCode" => $exception->getCode(),
                "message" => $exception->getMessage()
            ], HttpCodes::Unauthorized);
        }

        return parent::render($request, $exception);
    }
}
