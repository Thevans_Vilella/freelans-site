<?php

namespace App\Listeners;

use App\Events\ReferralSprintFinishedEvent;
use App\Models\ReferralSprint;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReferralSprintFinishedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReferralSprintFinishedEvent  $event
     * @return void
     */
    public function handle(ReferralSprintFinishedEvent $event)
    {
        $currentSprint = ReferralSprint::getCurrentSprint();
        $currentSprint->winner_id = $currentSprint->getWinner() != null ? $currentSprint->getWinner()->referrer_id : 0;
        $currentSprint->status = false;
        $currentSprint->finish = now();
        $currentSprint->save();

        $newSprint = ReferralSprint::autoFill([
            "start" => now(),
            "finish" => Carbon::now()->addWeek(),
        ]);
        $newSprint->save();
    }
}
