<?php

namespace App\Listeners;

use App\Events\NewPushMessageEvent;
use App\Jobs\SendPushMessageJob;
use App\Models\Profile;
use App\Notifications\PushMessageNotification;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewPushMessageListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewPushMessageEvent  $event
     * @return void
     */
    public function handle(NewPushMessageEvent $event)
    {
        SendPushMessageJob::dispatch($event->pushMessage);
    }
}
