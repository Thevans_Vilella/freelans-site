<?php

namespace App\Jobs;

use App\Models\Profile;
use App\Models\PushMessage;
use App\Notifications\PushMessageNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendPushMessageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var PushMessage
     */
    public $pushMessage;

    /**
     * Create a new job instance.
     *
     * @param PushMessage $pushMessage
     * @return void
     */
    public function __construct(PushMessage $pushMessage)
    {
        $this->pushMessage = $pushMessage;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $message = $this->pushMessage;
        $targets = Profile::when(count($message->getCitiesIds()) > 0, function ($query) use ($message) {
            return $query->whereIn("cid_codigo", $message->getCitiesIds());
        })->when(count($message->getProfileTypesIds()) > 0, function ($query) use ($message) {
            return $query->whereIn("per_tip_codigo", $message->getProfileTypesIds());
        })->when(count($message->getCategoriesIds()) > 0, function ($query) use ($message) {
            return $query->whereIn("cat_codigo", $message->getCategoriesIds());
        })->get();

        foreach ($targets as $target) {
            $target->notify(new PushMessageNotification($message));
        }
    }
}
