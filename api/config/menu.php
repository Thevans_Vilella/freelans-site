<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Navigation Menu
    |--------------------------------------------------------------------------
    |
    | This array is for Navigation menus of the backend.  Just add/edit or
    | remove the elements from this array which will automatically change the
    | navigation.
    |
    */

    // SIDEBAR LAYOUT - MENU

    'sidebar' => [
        [
            'title' => 'Principal',
            'link' => '/admin/dashboard',
            'active' => 'admin/dashboard*',
            'icon' => 'icon-fa icon-fa-dashboard'
        ],
        [
            'title' => 'Sprints de indicações',
            'link' => '/admin/referral-sprint',
            'active' => 'admin/referral-sprint*',
            'icon' => 'icon-fa icon-fa-retweet'
        ],
        [
            'title' => 'Notificações',
            'link' => '/admin/push-message',
            'active' => 'admin/push-message*',
            'icon' => 'icon-fa icon-fa-bell'
        ]
    ]
];
