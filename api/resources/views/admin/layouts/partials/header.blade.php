<header class="site-header">
  <a href="#" class="brand-main">
    <img src="{{asset('/assets/admin/img/logo-desk.svg')}}" id="logo-desk" alt="Freelans Logo" class="d-none d-md-inline ">
    <img src="{{asset('/assets/admin/img/logo-mobile.png')}}" id="logo-mobile" alt="Freelans Logo" class="d-md-none">
  </a>
  <a href="#" class="nav-toggle">
    <div class="hamburger hamburger--htla">
      <span>toggle menu</span>
    </div>
  </a>
</header>
