<footer class="site-footer">
    <div class="text-right">
        <p>{{ config('app.name') }} © {{ date('Y') }}</p>
    </div>
</footer>
