@extends('admin.layouts.layout-basic')

@section('content')
    <div class="main-content" id="dashboardPage">
        <div class="row">
            <div class="col-md-12 col-lg-6 col-xl-3">
                <a class="dashbox" href="#">
                    <i class="icon-fa icon-fa-user text-primary"></i>
                    <span class="title">
                      {{ $usersReferred["today"]->first()->amount }}
                    </span>
                    <span class="desc">
                      Usuários indicados hoje
                    </span>
                </a>
            </div>
            <div class="col-md-12 col-lg-6 col-xl-3">
                <a class="dashbox" href="#">
                    <i class="icon-fa icon-fa-user text-success"></i>
                    <span class="title">
                      {{ $usersReferred["week"]->first()->amount }}
                    </span>
                    <span class="desc">
                      Usuários indicados semana
                    </span>
                </a>
            </div>
            <div class="col-md-12 col-lg-6 col-xl-3">
                <a class="dashbox" href="#">
                    <i class="icon-fa icon-fa-user text-danger"></i>
                    <span class="title">
                      {{ $usersReferred["month"]->first()->amount }}
                    </span>
                    <span class="desc">
                      Usuários indicados este mês
                    </span>
                </a>
            </div>
            <div class="col-md-12 col-lg-6 col-xl-3">
                <a class="dashbox" href="#">
                    <i class="icon-fa icon-fa-user text-info"></i>
                    <span class="title">
                      --
                    </span>
                    <span class="desc">
                      --
                    </span>
                </a>
            </div>
        </div>
    </div>
@stop
