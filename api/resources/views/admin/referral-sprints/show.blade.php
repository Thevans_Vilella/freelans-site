@extends("admin.layouts.layout-basic")

@section("content")
    <div class="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark">
                        <h6 class="card-title">{{ $referralSprint->getTitleTime() }}</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Posição</th><th>Nome</th><th>Indicações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($referrals as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->total }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ url("/admin/referral-sprint") }}" title="Voltar">
                            <button class="btn btn-warning btn-sm" type="button">
                                <i class="icon-fa icon-fa-arrow-left" aria-hidden="true"></i> Voltar
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
