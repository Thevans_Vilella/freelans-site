@extends('admin.layouts.layout-basic')

@section('content')
    <div class="main-content">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-dark">
                    <h6 class="card-title">Sprints de indicações</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Período</th>
                                <th>Ações</th>
                                <th>Situação</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($referralSprints as $referralSprint)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ date("d/m/Y", strtotime($referralSprint->start)) . " - " . date("d/m/Y", strtotime($referralSprint->finish)) }}</td>
                                    <td>
                                        <a href="{{ url("/admin/referral-sprint/$referralSprint->id") }}"
                                           class="btn btn-outline-primary btn-sm">
                                            Ver ranking</a>
                                        @if($referralSprint->status)
                                            <button type="button" id="btEditSprint"
                                                    class="btn btn-outline-warning btn-sm" data-toggle="modal"
                                                    data-target="#modalEdit">
                                                Editar
                                            </button>
                                            <button type="button"
                                                    class="btn btn-outline-danger btn-sm bt-finish-sprint">
                                                Finalizar
                                            </button>
                                        @endif
                                    </td>
                                    <td>{{ $referralSprint->status ? "Ativa" : "Finalizada" }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalEditLabel">Editar Sprint</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inFinishDate" class="control-label">Término da sprint</label>
                        <input type="date" name="inFinishDate" id="inFinishDate" class="form-control"
                               value="{{ date("Y-m-d", strtotime(\App\Models\ReferralSprint::getCurrentSprint()->finish)) }}"
                               min="{{ date("Y-m-d", strtotime(\App\Models\ReferralSprint::getCurrentSprint()->start)) }}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btModalSave">Salvar
                        mudanças
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(() => {
            $('.bt-finish-sprint').on('click', () => {
                axios.post("{{ url("/admin/referral-sprint/finish") }}")
                    .then(() => {
                        window.location.reload()
                    })
            })

            $(`#btModalSave`).unbind('click').bind('click', () => {
                axios.post("{{ url("/admin/referral-sprint") }}", {
                    _token: "{{ csrf_token() }}",
                    finish: $('#inFinishDate').val()
                }).then(() => {
                    window.location.reload()
                })
            })

            {{--$("#inFinishDate").val({{ date("d/m/Y", strtotime(\App\Models\ReferralSprint::getCurrentSprint()->finish)) }})--}}
        })
    </script>
@endsection
