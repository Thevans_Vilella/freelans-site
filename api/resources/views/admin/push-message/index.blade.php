@extends('admin.layouts.layout-basic')

@section('content')
    <div class="main-content">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-dark">
                    <h6 class="card-title">Mensagens enviadas</h6>
                </div>
                <div class="card-body">
                    <a href="{{ url('/admin/push-message/create') }}" class="btn btn-success btn-sm"
                       title="Nova mensagem">
                        <i class="icon-fa icon-fa-plus" aria-hidden="true"></i> Nova mensagem
                    </a>

                    <form method="GET" action="{{ url('/admin/push-message') }}" accept-charset="UTF-8"
                          class="form-inline ml-3 float-right" role="search">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" name="search" placeholder="Pesquisar..."
                                   value="{{ request('search') }}" aria-label="Pesquisar...">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <em class="icon-fa icon-fa-search"></em>
                                </button>
                            </span>
                        </div>
                    </form>
                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th><th>Título</th><th>Mensagem</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pushMessages as $pushMessage)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $pushMessage->title }}</td>
                                    <td>{{ $pushMessage->message }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
