@extends('admin.layouts.layout-basic')

@section('content')
    <div class="main-content">
        <div class="col-md-12">
            <div class="card">
                <civ class="card-header bg-dark">
                    <h6>Enviar Mensagem</h6>
                </civ>
                <form method="POST" action="{{ url('/admin/push-message') }}" accept-charset="UTF-8"
                      role="form" autocomplete="off" enctype="multipart/form-data" id="push-message-form">
                    <div class="card-body">
                        @if($errors->any())
                            <ul class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="title" class="control-label">Título</label>
                                <input type="text" class="form-control" name="title" id="title" required>
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group col-md-4">
                                <label for="message" class="control-label">Mensagem</label>
                                <input type="text" class="form-control" name="message" id="message" required>
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group col-md-4">
                                <label for="link" class="control-label">Link</label>
                                <input type="text" class="form-control" name="link" id="link" required>
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="profile_types[]" class="control-label">Tipo de perfil</label>
                                <select class="custom-select select2" name="profile_types[]" id="profile_type" multiple>
                                    @foreach(\App\Models\ProfileType::all() as $profileType)
                                        <option value="{{ $profileType->id }}">{{ $profileType->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="category_ids[]" class="control-label">Categoria</label>
                                <select class="custom-select select2" name="category_ids[]" id="category_id" multiple>
                                    @foreach(\App\Models\Category::all() as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="city_ids[]" class="control-label">Cidade</label>
                                <select class="custom-select select2" name="city_ids[]" id="city_id" multiple>
                                    @foreach(\App\Models\City::all() as $city)
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary bnt-sm">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(() => {
          $("#profile_type").select2({
            placeholder: "Todos",
            allowClear: true,
            language: {
              noResults: function () {
                return "Nenhum resultado encontrado";
              },
            },
          });

          $('#category_id').select2({
            placeholder: "Todas",
            allowClear: true,
            language: {
              noResults: function () {
                return "Nenhum resultado encontrado";
              },
            },
          });

          $('#city_id').select2({
            placeholder: "Todas",
            allowClear: true,
            language: {
              noResults: function () {
                return "Nenhum resultado encontrado";
              },
            },
          });
        });
    </script>
@endsection
