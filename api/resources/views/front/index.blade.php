@extends('front.layouts.front')

@section('content')
    <section class="section section-hero-area">
        <div class="container text-sm-center">
            <h1>{{ config('app.name') }}</h1>
            <p class="lead">Use this document as a way to quickly start any new project.<br></p>
        </div>
    </section>
@endsection
