<?php

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
| Define the routes for your Frontend pages here
|
*/
Route::get('/', function() {
    return redirect()->to("https://freelans.com.br");
});


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
| Route group for Backend prefixed with "admin".
| To Enable Authentication just remove the comment from Admin Middleware
|
*/

Route::group([
    'prefix' => 'admin',
    'middleware' => 'admin'
], function () {

    // Dashboard
    //----------------------------------

    Route::get('/', [
        'as' => 'admin.dashboard', 'uses' => 'DashboardController@index'
    ]);

    Route::get('/dashboard', [
        'as' => 'admin.dashboard', 'uses' => 'DashboardController@basic'
    ]);

    Route::resource('users', 'UsersController');
    Route::resource('push-message', 'Admin\PushMessageController');
    Route::resource('referral-sprint', 'Admin\ReferralSprintController');
    Route::post('referral-sprint', 'Admin\ReferralSprintController@updateCurrent');
    Route::post('referral-sprint/finish', 'Admin\ReferralSprintController@finish');
});

/*
|--------------------------------------------------------------------------
| Guest Routes
|--------------------------------------------------------------------------
| Guest routes cannot be accessed if the user is already logged in.
| He will be redirected to '/" if he's logged in.
|
*/

Route::group(['middleware' => ['guest']], function () {

    Route::get('login', [
        'as' => 'login', 'uses' => 'Admin\AuthController@login'
    ]);

    Route::get('register', [
        'as' => 'register', 'uses' => 'Admin\AuthController@register'
    ]);

    Route::post('login', [
        'as' => 'login.post', 'uses' => 'Admin\AuthController@authenticate'
    ]);

    Route::get('forgot-password', [
        'as' => 'forgot-password.index', 'uses' => 'ForgotPasswordController@getEmail'
    ]);

    Route::post('/forgot-password', [
        'as' => 'send-reset-link', 'uses' => 'ForgotPasswordController@postEmail'
    ]);

    Route::get('/password/reset/{token}', [
        'as' => 'password.reset', 'uses' => 'ForgotPasswordController@GetReset'
    ]);

    Route::post('/password/reset', [
        'as' => 'reset.password.post', 'uses' => 'ForgotPasswordController@postReset'
    ]);

    Route::get('auth/{provider}', 'Admin\AuthController@redirectToProvider');

    Route::get('auth/{provider}/callback', 'Admin\AuthController@handleProviderCallback');
});

Route::get('logout', [
    'as' => 'logout', 'uses' => 'Admin\AuthController@logout'
]);

Route::get('install', [
    'as' => 'logout', 'uses' => 'Admin\AuthController@logout'
]);

Route::get('/clear-cache', function() {
	Artisan::call('cache:clear');
	Artisan::call('config:clear');
	Artisan::call('view:clear');
	return "Cache is cleared";
});
