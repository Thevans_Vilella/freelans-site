<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v1'
], function () {
    Route::get('banner', 'Api\\BannerController@index');
    Route::get('category', 'Api\\CategoryController@index');
    Route::get('occupation', 'Api\\OccupationController@index');
    Route::get('differential', 'Api\\DifferentialController@index');
    Route::get('plan', 'Api\\PlanController@index');
    Route::get('city', 'Api\\CityController@index');
    Route::get('favorites', 'Api\\ProfileController@favorites');

    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'AuthController@authenticate');
        Route::post('register', 'AuthController@register');
        Route::post('logout', 'AuthController@logout');
        Route::get('check', 'AuthController@check');
    });

    Route::group(['middleware' => 'api.auth'], function () {
        Route::get('promotion', 'Api\\PromotionController@index');
        Route::get('provider', 'Api\\ProfileController@index');
        Route::get('favorite_provider/{id}', 'Api\\ProfileController@toggleFavorite');
        Route::get("referral-sprint", "Api\ReferralSprintController@index");
	    Route::post('provider/update_profile_photo/{id}', 'Api\\ProfileController@updateProfilephoto');
	    Route::put('provider/{id}', 'Api\\ProfileController@update');
    });
});

