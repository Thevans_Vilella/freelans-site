<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReferralSprintTableAddWinnerField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("referral_sprints", function (Blueprint $table) {
            $table->unsignedInteger("winner_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("referral_sprints", function (Blueprint $table) {
            $table->dropColumn("winner_id");
        });
    }
}
