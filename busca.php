<?php

if(!isset($_POST['formbusca'])) { require_once("alerta.php"); }


if(isset($_POST['city'])) { 
	
	$city	=	(int)$_POST['city'];
	
}

if(isset($_POST['cat'])) { 
	
	$cat	=	(int)$_POST['cat'];
	
}

if(isset($_POST['dest'])) { 
	
	$string 	= 	'"' . implode('","', $_POST['dest']) . '"';
	echo $result 	= 	count($_POST['dest']);
	
}


if(($city != "" and $cat == "") and (!isset($_POST['dest']))) { 
	$tipbusca = "1"; // Só trouxe cidade
}

if(($city != "" and $cat != "") and (!isset($_POST['dest']))) { 
	$tipbusca = "2"; // Trouxe cidade e hastag
}

if(($city != "" and $cat == "") and (isset($_POST['dest']))) { 
	$tipbusca = "3"; // Trouxe cidade e destaques
}

if(($city != "" and $cat != "") and (isset($_POST['dest']))) { 
	$tipbusca = "4"; // Trouxe cidade, hastag e destaques
}



if(($city == "" and $cat != "") and (!isset($_POST['dest']))) { 
	$tipbusca = "5"; // Só trouxe hastag
}


if(($city == "" and $cat != "") and (isset($_POST['dest']))) { 
	$tipbusca = "6"; // Trouxe hastag e destaques
}


if(($city == "" and $cat == "") and (isset($_POST['dest']))) { 
	$tipbusca = 7; // Só trouxe destaques
}

$sqlcity	=	"select * from cidades WHERE cid_codigo = $city";
$rescity	=	mysqli_query($cn, $sqlcity);
$lincity	=	mysqli_fetch_array($rescity);
		



//===================================================================================================================
// Aqui inserimos os dados da busca no banco
// para gerarmos os relatórios

date_default_timezone_set('America/Sao_Paulo');
$datahoje	=	date('Y-m-d');
$horahoje	=	date('H:i:s');


$sqlverfrel	=	"select * from busca_atua_cidade 
				WHERE 
				bus_atu_cid_data = '$datahoje' AND atua_codigo = $cat AND cid_codigo = $city
				ORDER BY 
				bus_atu_cid_hora DESC
				LIMIT 0 , 1
				";
$resverfrel	=	mysqli_query($cn, $sqlverfrel);
$cntverfrel	=	mysqli_num_rows($resverfrel);
$linverfrel	=	mysqli_fetch_array($resverfrel);

$horavery	  = strtotime("+10 minutes", strtotime($linverfrel['bus_atu_cid_hora'])); // Aqui adicionamos 10 minutos na hora inserida no banco
$time	  = date('H:i:s', $horavery); // Aqui formatamos a hora corretamente





if(($cntverfrel == 0) or ($cntverfrel > 0 and $horahoje > $time)) { // Começa IF de verificação inserir no banco de dados

	$sqlrel	=	"insert into busca_atua_cidade 
	(atua_codigo, cid_codigo, bus_atu_cid_data, bus_atu_cid_hora)

	VALUES 
	('$cat', '$city', '$datahoje', '$horahoje')";

	mysqli_query($cn, $sqlrel);
	
} 
// Termina IF de verificação de tempo
//===================================================================================================================
?>
<div class="boxfull">
	<div class="titfxtop">Busca de profissional: <span class="titfxtop-sub"></span></div>

		<div class="boxfull cinza">
			<div class="blg">
				
				<?php require_once("banner-busca.php"); ?>

				<!--
				<div class="bl">
					<div class="titfxdest-meio">Profissionais Encontrados</div>
					<div class="subtititem-meio"></div>
				</div>
				-->


				
		<div class="bl-prof">
			<div id="itemContainer">
				
				<div class="titcity">Cidade Buscada: <span class="titfxtop-sub"><?php echo $lincity['cid_nome']; ?></span></div>
				
				
				<?php 
                switch($tipbusca){
                
				case "1"; // Só trouxe cidade 
					
					$sql99			=	"select * from perfil
										INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
										INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
										INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
										INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
										WHERE 
										cidades.cid_codigo = $city
										AND
										per_sta_codigo = 1
										AND
										per_tip_codigo = 2
										ORDER BY
										pla_ordem DESC, ava_media_estrelas DESC, perf_nome
										";
				break;
					
				
				case "2"; // Trouxe cidade e hastag 
					
					$sql99		=	"select * from atuacao_perfil
									INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	atuacao_perfil.atua_codigo
									INNER JOIN perfil	  		on		perfil.perf_codigo			=	atuacao_perfil.perf_codigo
									INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
									INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
									INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
									INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
									WHERE 
									cidades.cid_codigo = $city
									AND
									atuacao_perfil.atua_codigo = $cat
									AND
									per_sta_codigo = 1
									AND
									per_tip_codigo = 2
									ORDER BY
									pla_ordem DESC, ava_media_estrelas DESC, perf_nome
									";

                break;
					
				
				case "3"; // Trouxe cidade e destaques 
					
							$sql99				=	"select * from perfil_destaques 
													INNER JOIN destaques	  	on		destaques.dest_codigo		=	perfil_destaques.dest_codigo
													INNER JOIN perfil	  		on		perfil.perf_codigo			=	perfil_destaques.perf_codigo
													INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
													INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
													INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
													INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
													WHERE 
													perfil_destaques.dest_codigo IN ($string) 
													AND 
													perfil.cid_codigo = $city
													AND
													per_sta_codigo = 1
													AND
													per_tip_codigo = 2
													GROUP BY
													perfil_destaques.perf_codigo 
													ORDER BY
													pla_ordem DESC, ava_media_estrelas DESC, perf_nome
													";
					
				break;
					
				
				
				
				case "4"; // Trouxe cidade, hastag e destaques
					
					$sql99				=	"select * from atuacao_perfil 
											INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	atuacao_perfil.atua_codigo
											INNER JOIN perfil	  		on		perfil.perf_codigo			=	atuacao_perfil.perf_codigo
											INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
											INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
											INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
											INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
											WHERE 
											perfil.cid_codigo = $city
											AND
											atuacao_perfil.atua_codigo = $cat
											AND
											per_sta_codigo = 1
											AND
											per_tip_codigo = 2
											ORDER BY
											pla_ordem DESC, ava_media_estrelas DESC, perf_nome
											";

                break;
					
				
				
				case "5";// Trouxe hastag 
					
							$sql99		=	"select * from atuacao_perfil
											INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	atuacao_perfil.atua_codigo
											INNER JOIN perfil	  		on		perfil.perf_codigo			=	atuacao_perfil.perf_codigo
											INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
											INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
											INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
											INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
											WHERE 
											atuacao_perfil.atua_codigo = $cat
											AND
											per_sta_codigo = 1
											AND
											per_tip_codigo = 2
											ORDER BY
											pla_ordem DESC, ava_media_estrelas DESC, perf_nome
											";
				break;
				
				
				
		
				
				case "6"; // Trouxe hastag e destaques 
					
							$sql99		=	"select * from atuacao_perfil
											INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	atuacao_perfil.atua_codigo
											INNER JOIN perfil	  		on		perfil.perf_codigo			=	atuacao_perfil.perf_codigo
											INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
											INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
											INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
											INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
											WHERE 
											atuacao_perfil.atua_codigo = $cat
											AND
											per_sta_codigo = 1
											AND
											per_tip_codigo = 2
											ORDER BY
											pla_ordem DESC, ava_media_estrelas DESC, perf_nome
											";
				
				break;
				
				
				
				
				
				
		
				
				case "7"; // Trouxe destaques 
				   
						$sql99				=	"select * from perfil_destaques 
												INNER JOIN destaques	  	on		destaques.dest_codigo		=	perfil_destaques.dest_codigo
												INNER JOIN perfil	  		on		perfil.perf_codigo			=	perfil_destaques.perf_codigo
												INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
												INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
												INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
												INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
												WHERE 
												perfil_destaques.dest_codigo IN ($string) 
												AND
												per_sta_codigo = 1
												AND
												per_tip_codigo = 2
												GROUP BY
												perfil_destaques.perf_codigo
												ORDER BY
												pla_ordem DESC, ava_media_estrelas DESC, perf_nome
												";
					
				break;
                        
            } // Termina Switch que verifica $tipbusca
                
					$res99			=	mysqli_query($cn, $sql99);
					$cnt99			=	mysqli_num_rows($res99);
					while($lin99	=	mysqli_fetch_array($res99))  {

						$perfcode	=	$lin99['perf_codigo'];


									if( $tipbusca == "3" or $tipbusca == "4" or $tipbusca == "6" or $tipbusca == "7" ) { // Trouxe destaques 	

											$sqldest			=	"select * from perfil_destaques 
																	WHERE 
																	dest_codigo IN ($string) 
																	AND
																	perf_codigo = $perfcode
																	";
											$resdest			=	mysqli_query($cn, $sqldest);
											$cntdest			=	mysqli_num_rows($resdest);
											$lindest			=	mysqli_fetch_array($resdest);

									}
						
						
					$sqlaval	=	"select * from avaliacoes WHERE perf_codigo = '$perfcode'";
					$resaval	=	mysqli_query($cn, $sqlaval);
					$linaval	=	mysqli_fetch_array($resaval);
						
						
						
					if($lin99['pla_codigo'] == 4) {
						
						$clas	=	"boxit";
						
						} else {
						
						$clas	=	"boxit-dest";
						
					}
						
						
					if($lin99['opc_codigo'] == 2) {
						
						$classfot	=	"boxit-fot";
						$classplain	=	"boxit-tipo";
						$nomeplain	=	ConverteTexto($lin99['pla_nome'], 1);
						
						} else {
						
						$classfot	=	"boxit-fotss";
						$classplain	=	"";
						$nomeplain	=	"";
						
					}
						
						
						
						

				?>
				
					<?php if( 
								$tipbusca == "3" and $result == $cntdest 
								or $tipbusca == "4" and $result == $cntdest 
								or $tipbusca == "6" and $result == $cntdest 
								or $tipbusca == "7" and $result == $cntdest 
							) { // Trazem destaques na busca
					?>
					<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
						<div class="<?php echo $clas; ?>">
							<a href="perfil/<?php echo $lin99['perf_codigo']; ?>/<?php echo CorrigirNome($lin99['perf_nome']); ?>" target="_blank">
								<div class="<?php echo $classplain; ?>"><?php echo $nomeplain; ?></div>
								<div class="<?php echo $classfot; ?>">
									<img src="painel/<?php echo $lin99['perf_fotop']; ?>" alt="Favoritos"/>
								</div>
								<div class="boxit-tit"><?php echo $lin99['perf_nome']; ?></div>
								<div class="boxit-esp"><?php echo $lin99['cat_nome']; ?></div>
								<div class="boxit-estrela">
									<?php 
										for($contador = 1; $contador <= 5; $contador++) {

												if($contador <= $linaval['ava_media_estrelas']){

													echo "<i class='fas fa-star estyellow'></i>";

												} else {

													echo "<i class='fas fa-star'></i>";

												}

										}
									?>
								</div>
								<div class="boxit-nota">(<?php echo $linaval['ava_qtde_votos']; ?>)</div>
							</a>	
						</div>
					</div>
					<?php } ?>
				
					<?php if( $tipbusca == "1" or $tipbusca == "2" or $tipbusca == "5" ) { ?>
					<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
						<div class="<?php echo $clas; ?>">
							<a href="perfil/<?php echo $lin99['perf_codigo']; ?>/<?php echo CorrigirNome($lin99['perf_nome']); ?>" target="_blank">
								<div class="<?php echo $classplain; ?>"><?php echo $nomeplain; ?></div>
								<div class="<?php echo $classfot; ?>">
									<img src="painel/<?php echo $lin99['perf_fotop']; ?>" alt="Favoritos"/>
								</div>
								<div class="boxit-tit"><?php echo $lin99['perf_nome']; ?></div>
								<div class="boxit-esp"><?php echo $lin99['cat_nome']; ?></div>
								<div class="boxit-estrela">
									<?php 
										for($contador = 1; $contador <= 5; $contador++) {

												if($contador <= $linaval['ava_media_estrelas']){

													echo "<i class='fas fa-star estyellow'></i>";

												} else {

													echo "<i class='fas fa-star'></i>";

												}

										}
									?>
								</div>
								<div class="boxit-nota">(<?php echo $linaval['ava_qtde_votos']; ?>)</div>
							</a>	
						</div>
					</div>
					<?php } ?>

					<?php } // Final do While de repetição ?>

				
					<?php if( $cnt99 == 0 ) { // Não encontrou nenhum resultado ?>
							<div class="titfxdest-meio">Nenhum profissional foi encontrado para a busca solicitada</div>
					<?php } ?>
				
				
			</div><!-- paginação -->
			<div class="holder"></div>
		</div>


				
				
				
				
				
			<?php
			$sqlcid			=	"select * from cidades_relacionadas
								INNER JOIN cidades	  	on		cidades.cid_codigo		=	cidades_relacionadas.cid_codigo
								WHERE 
								cidades_relacionadas.cid_referencia = $city
								order by 
								cidades.cid_nome
								";
			$rescid			=	mysqli_query($cn, $sqlcid);
			while($lincid	=	mysqli_fetch_array($rescid))  {
				
				
				$cidref		=	$lincid['cid_codigo'];
				
			?>
			<div class="titcity">
				Cidade próxima: <span class="titfxtop-sub"><?php echo $lincid['cid_nome']; ?></span>
				
				<form name="buscarelacionada" method="post" action="buscaa">
					<input type="hidden" name="city" size="5" value="<?php echo $cidref; ?>" />
					<input type="hidden" name="cat" size="5" value="<?php echo $cat; ?>" />
					
					<?php if(isset($_POST['dest'])) { ?>
					<input type="hidden" name="adm" size="5" value="<?php echo $adm; ?>" />
					<?php } ?>
					
					<input type="hidden" name="formrelacionada"/>
					<input type="submit" class="destvcity" value="Visualizar todos" />
				</form>
			</div>
				
				<?php // Aqui trazemos as cidades relacionadas a cidade da busca ============
				if( $tipbusca == "2" ) { // Trouxe cidade e hastag 
					
					$sql1219		=	"select * from atuacao_perfil
										INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	atuacao_perfil.atua_codigo
										INNER JOIN perfil	  		on		perfil.perf_codigo			=	atuacao_perfil.perf_codigo
										INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
										INNER JOIN cidades	  		on		cidades.cid_codigo			=	perfil.cid_codigo
										INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
										INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
										WHERE 
										cidades.cid_codigo = $cidref
										AND
										atuacao_perfil.atua_codigo = $cat
										AND
										per_sta_codigo = 1
										AND
										per_tip_codigo = 2
										ORDER BY
										pla_ordem DESC, ava_media_estrelas DESC, perf_nome 
										LIMIT 0 , 6
										";
				}
					
				
				
				
				if( $tipbusca == "4" ) { // Trouxe cidade, hastag e destaques
					
					$sql1219		=	"select * from atuacao_perfil 
									INNER JOIN atuacao	  					on		atuacao.atua_codigo						=	atuacao_perfil.atua_codigo
									INNER JOIN perfil	  					on		perfil.perf_codigo						=	atuacao_perfil.perf_codigo
									INNER JOIN planos	  					on		planos.pla_codigo						=	perfil.pla_codigo
									INNER JOIN cidades	  					on		cidades.cid_codigo						=	perfil.cid_codigo
									INNER JOIN categorias	  				on		categorias.cat_codigo					=	perfil.cat_codigo
									INNER JOIN avaliacoes	  				on		avaliacoes.perf_codigo					=	perfil.perf_codigo
									WHERE 
									perfil.cid_codigo = $cidref
									AND
									atuacao_perfil.atua_codigo = $cat
									AND
									per_sta_codigo = 1
									AND
									per_tip_codigo = 2
									ORDER BY
									pla_ordem DESC, ava_media_estrelas DESC, perf_nome 
									LIMIT 0 , 6
									";
					
				}
				// Busca de cidade relacionadas a cidade da busca
				
				
				
					$res1219		=	mysqli_query($cn, $sql1219);
					$cnt1219		=	mysqli_num_rows($res1219);
					while($lin1219	=	mysqli_fetch_array($res1219))  {
								
						$citycity		=	$lin1219['cid_nome'];
						$perfcodecity	=	$lin1219['perf_codigo'];

								 
									if($tipbusca == "4") { // Trouxe destaques 	

											$sqldestcity			=	"select * from perfil_destaques 
																		WHERE 
																		dest_codigo IN ($string) 
																		AND
																		perf_codigo = $perfcodecity
																		";
											$resdestcity			=	mysqli_query($cn, $sqldestcity);
											$cntdestcity			=	mysqli_num_rows($resdestcity);
											$lindestcity			=	mysqli_fetch_array($resdestcity);

									}
						
						
					$sqlavalcity	=	"select * from avaliacoes WHERE perf_codigo = '$perfcodecity'";
					$resavalcity	=	mysqli_query($cn, $sqlavalcity);
					$linavalcity	=	mysqli_fetch_array($resavalcity);
						
						
						
					if($lin1219['pla_codigo'] == 4) {
						
						$clas	=	"boxit";
						
						} else {
						
						$clas	=	"boxit-dest";
						
					}
						
						
					if($lin1219['opc_codigo'] == 2) {
						
						$classfot	=	"boxit-fot";
						$classplain	=	"boxit-tipo";
						$nomeplain	=	ConverteTexto($lin1219['pla_nome'], 1);
						
						} else {
						
						$classfot	=	"boxit-fotss";
						$classplain	=	"";
						$nomeplain	=	"";
						
					}
				?>				
					<?php if( $tipbusca == "4" and $result == $cntdestcity  ) { // Trazem destaques na busca ?>
					<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
						<div class="<?php echo $clas; ?>">
							<a href="perfil/<?php echo $lin1219['perf_codigo']; ?>/<?php echo CorrigirNome($lin1219['perf_nome']); ?>" target="_blank">
								<div class="<?php echo $classplain; ?>"><?php echo $nomeplain; ?></div>
								<div class="<?php echo $classfot; ?>">
									<img src="painel/<?php echo $lin1219['perf_fotop']; ?>" alt="Favoritos"/>
								</div>
								<div class="boxit-tit"><?php echo $lin1219['perf_nome']; ?></div>
								<div class="boxit-esp"><?php echo $lin1219['cat_nome']; ?></div>
								<div class="boxit-estrela">
									<?php 
										for($contador = 1; $contador <= 5; $contador++) {

												if($contador <= $linavalcity['ava_media_estrelas']){

													echo "<i class='fas fa-star estyellow'></i>";

												} else {

													echo "<i class='fas fa-star'></i>";

												}

										}
									?>
								</div>
								<div class="boxit-nota">(<?php echo $linavalcity['ava_qtde_votos']; ?>)</div>
							</a>	
						</div>
					</div>
				
					<?php } ?>
				
				
				<?php if( $tipbusca == "2" ) { // Trazem destaques na busca ?>
					<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
						<div class="<?php echo $clas; ?>">
							<a href="perfil/<?php echo $lin1219['perf_codigo']; ?>/<?php echo CorrigirNome($lin1219['perf_nome']); ?>" target="_blank">
								<div class="<?php echo $classplain; ?>"><?php echo $nomeplain; ?></div>
								<div class="<?php echo $classfot; ?>">
									<img src="painel/<?php echo $lin1219['perf_fotop']; ?>" alt="Favoritos"/>
								</div>
								<div class="boxit-tit"><?php echo $lin1219['perf_nome']; ?></div>
								<div class="boxit-esp"><?php echo $lin1219['cat_nome']; ?></div>
								<div class="boxit-estrela">
									<?php 
										for($contador = 1; $contador <= 5; $contador++) {

												if($contador <= $linavalcity['ava_media_estrelas']){

													echo "<i class='fas fa-star estyellow'></i>";

												} else {

													echo "<i class='fas fa-star'></i>";

												}

										}
									?>
								</div>
								<div class="boxit-nota">(<?php echo $linavalcity['ava_qtde_votos']; ?>)</div>
							</a>	
						</div>
					</div>
					<?php } // Final do IF de verificação se traz destaque ?>
				
					<?php } // Final do While de repetição ?>
				
				<?php } // Final do While de repetição de cidades relacionadas ?>
				
	
	
	<div class="vazio"></div>
</div>