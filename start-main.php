<!-- Modal -->
<div class="modal fade" id="modal-search" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			
			
				<button type="button" class="close closemod" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>

				<div class="cxmod">
					<div class="titmodal">Explique o que você precisa</div>
					<div class="sub-titmodal">Listaremos os melhores profissionais para sua necessidade.</div>

					<form name="form" id="filbusca" method="post" action="#">
						<ul>
							<li>
								<div class="itemcont">Qual é o tipo de profissional que você precisa? </div>
								
					<select name="servico" class="search-professional validate[required]" placeholder="Qual o tipo de profissional que você procura?" id="country-selectorrr" autofocus="autofocus" autocorrect="off" autocomplete="off">
					  <option value="" selected="selected">Qual é o tipo de profissional que você precisa?</option>
						<?php 
						$sql		=	"select * from atuacao ORDER BY atua_nome";
						$res		=	mysqli_query($cn, $sql);
						while($lin	=	mysqli_fetch_array($res))  {
						?>
						<option value="<?php echo $lin['atua_codigo']; ?>" data-alternative-spellings="<?php echo $lin['atua_nome']; ?>" data-relevancy-booster="0.5"><?php echo $lin['atua_nome']; ?></option>
						<?php } ?>
					</select>
								
							</li>
							<li>
								<div class="titmodal">Escolha os diferenciais.</div>
								<div class="box-itemdifere">
									<?php 
									$sql01			=	"select * from destaques ORDER BY dest_ordem";
									$res01			=	mysqli_query($cn, $sql01);
									while($lin01	=	mysqli_fetch_array($res01))  {
									?>
									<div class="itemdifere">
										<div class="itemdifcheck"><input type="checkbox" class="validate[required]" name="horns"></div>
										<?php echo $lin01['dest_nome']; ?> 
									</div>
									<?php } ?>
								</div>
							</li>
						</ul>
						<input type="hidden" name="formfilbusca"/>
						<input type="submit" class="envia" value="Buscar Profissional" />
					</form>
				</div>
			
			
        </div>
    </div>
</div>