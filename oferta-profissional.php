<?php
session_start();


if(isset($_SESSION["login_ses"]) and ($_SESSION["status_ses"] == "1")) {
	
require_once("painel/conexao.php");
require_once("painel/classes.php");


$vgoft		=	(int)$_POST['ofet'];
$perfcod	=	(int)$_POST['user'];
	
date_default_timezone_set('America/Sao_Paulo');
$data_atual = date("Y-m-d");
$hora_atual	= date('H:i:s');

//============================================================================================================
if ( $vgoft == "" or $perfcod == "" )  { 

	echo "<script type='text/javascript'>alert('Por Favor, Preencha todos os Campos!');</script>";
    echo "<script>javascript:parent.jQuery.fancybox.close();</script>";
	include "painel/destruidor.php";
	exit();

} 
//================ Verificando se já se cadastrou para esta vaga ============================================
$res_verifica = mysqli_query($cn, "SELECT * FROM ofertas_profissionais WHERE ofe_tra_codigo = $vgoft AND perf_codigo = $perfcod");
$conta_verifica = mysqli_num_rows($res_verifica);
if ( $conta_verifica <> 0 ) {
	?>

	<?php	
	echo "<script type='text/javascript'>alert('Você já se cadastrou para esta vaga!');</script>";
    echo "<script>javascript:parent.jQuery.fancybox.close();</script>";
	include "painel/destruidor.php";
	exit();
}
//============================================================================================================


$sqlatua	=	"INSERT into ofertas_profissionais 
(ofe_tra_codigo, perf_codigo, ofe_pro_data, ofe_pro_hora)

VALUES 
('$vgoft', '$perfcod', '$data_atual', '$hora_atual')";

mysqli_query($cn, $sqlatua);

//============================================================================================================
// Aqui verificamos qual o perfil que registrou a vaga e enviamos email avisando que tem um novo candidato.
    
   
$sqluser	=	"select * from ofertas_trabalho
                INNER JOIN perfil	  	on		perfil.perf_codigo		=	ofertas_trabalho.perf_codigo
                where
                ofe_tra_codigo = $vgoft
                ";
$resuser	=	mysqli_query($cn, $sqluser);
$linuser	=	mysqli_fetch_array($resuser);	

$perfcod	=	$linuser['perf_codigo'];
$perfname	=	$linuser['perf_nome'];
$perfmail	=	$linuser['perf_email'];
//============================================================================================================
//============================================================================================================
//============================================================================================================
 
$sql11 = "SELECT * FROM metas_google";
$res11 = mysqli_query($cn, $sql11);
$lin11 = mysqli_fetch_array($res11);

$key	 = $lin11['tag_alt'];
$urlproj = $lin11['tag_url'];



$sql10 = "SELECT * 
			FROM contato
	  INNER JOIN estado 		
			  ON estado.est_codigo = contato.est_codigo
	  INNER JOIN logradouro 	
			  ON logradouro.log_codigo = contato.log_codigo";
$res10 = mysqli_query($cn, $sql10);
$lin10 = mysqli_fetch_array($res10);	

$fone  = substr($lin10['cont_telefone'], 0, 14);
$fone0 = substr($lin10['cont_telefone'], 1, 2);
$fone1 = substr($lin10['cont_telefone'], 0, 4);
$fone2 = substr($lin10['cont_telefone'], 5, 9);



$contnome  = $lin10['cont_nome'];   
$namestop  = "Freelans";  
$empfone   = $lin10['cont_telefone'];
$empemail  = $lin10['cont_email'];
$empcep    = $lin10['cont_cep'];
$emploca   = $lin10['cont_endereco'].", ".$lin10['cont_numero'];
$emplocal  = $lin10['log_sigla']." ".$lin10['cont_endereco'].", ".$lin10['cont_numero'];
$empbairro = $lin10['cont_bairro'];
$empcidade = $lin10['cont_cidade'];
$empestado = $lin10['est_nome'];
//==================================================================================================================

//===============================================================================================================
// Passando os dados obtidos pelo formulário para as variáveis abaixo
$nomeremetente     = $namestop;
$emailremetente    = $user_mail;
$emaildestinatario = $perfmail;
$titulo            = 'Candidato para Oferta de Trabalho Pelo Site Freelans';
//===============================================================================================================

//===============================================================================================================
require_once("phpmailer/PHPMailerAutoload.php");
// Inicia a classe PHPMailer
//===============================================================================================================
$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               	// Enable verbose debug output

$mail->isSMTP();                                      	// Define que a mensagem será SMTP
//$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Especifique os servidores SMTP principal e backup
$mail->Host = $serv_sis;  								// Especifique os servidores SMTP principal e backup
$mail->SMTPAuth = true;                               	// Usa autenticação SMTP? (opcional)
$mail->Username = $user_sis;     						// Usuário do servidor SMTP - Mesmo email da linha 45
$mail->Password = $pass_sis;            				// Senha do servidor SMTP - Se não tiver de Serviço de SMTP Contrato colocar a senha do email que está sendo utilizada para envio.
$mail->SMTPSecure = $user_crip;                         // Ativar a criptografia TLS, `também ssl` aceita
$mail->Port = $user_porta;                              // Porta TCP para conectar-se

$mail->From = $emailremetente;					                      // Email do Remetente - Colocar email do servidor Locaweb (Site)
$mail->FromName = $nomeremetente;									  // Nome do Remetente - Colocar Seu Nome ou Nome de sua Empresa
$mail->addAddress($emaildestinatario, $perfname);         			  // Adicionar um destinatário - Nome é opcional
$mail->addReplyTo($emaildestinatario, $perfname);	  				  // Responder Para - Aqui define para quem vão a respósta do email



// Define os dados Técnicos da Mensagem
//===============================================================================================================
$mail->IsHTML(true); 								    // Define que o e-mail será¡ enviado como HTML
$mail->CharSet = 'utf-8'; 						        // Charset da mensagem (opcional)

//===============================================================================================================
// Aqui é a Mensagem do Contato que vai no Email - Ou seja o HTML da Mensagem
//===============================================================================================================
$txt = "

        <table width='550' border='0' style='background-color:#FFF;border:25px solid #E7E2DC' align='center' cellpadding='0' cellspacing='0'>
          <tr>
            <td colspan='3' valign='top' bgcolor='#F9F8F7'>
                <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align='center'><a href='$urlproj' target='_blank'><img src='$urlproj/assets/_img/logo-freelans.png' border='0' alt='$contnome'/></a></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                </table>
            </td>
          </tr>
          <tr>
            <td height='3' colspan='3' bgcolor='#005FA6'></td>
          </tr>
          <tr>
            <td height='40' colspan='3' bgcolor='#FFFFFF'>&nbsp;</td>
          </tr>
          <tr>
            <td width='5%' bgcolor='#FFFFFF'>&nbsp;</td>
            <td width='100%' bgcolor='#FFFFFF'>
                <table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>
                  <tr>
                    <td style='color:#000; font-size: 13px; font-family:Arial, Helvetica, sans-serif;'>
                        Olá <span style='color:#FF0000; font-size: 14px;'>$perfname!</span><br/><br/>
                        Um novo candidato se cadastrou em nosso site para sua oferta de trabalho!!!<br/><br/>
                        Para acessar nosso site e consultar os dados do candidato clique no link abaixo.<br/><br/>
                        
						<table border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td>
								<a href='$urlproj' style='text-decoration:none; color: #FFFFFF; display:block; font-size:15px; background:#005FA6; padding: 10px 15px 10px 15px;'>
								Acessar Site</a>
								</td>
                            </tr>
                        </table>
						
                    </td>
                  </tr>
                </table>
            </td>
            <td width='5%' bgcolor='#FFFFFF'>&nbsp;</td>
          </tr>
          <tr>
            <td height='40' colspan='3' bgcolor='#FFFFFF'>&nbsp;</td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#FFCA03' height='3'></td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#8BC53E' height='3'></td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#005FA6'>&nbsp;</td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#005FA6' align='center' style='color:#F9F9F9; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>
                Telefone: <a style='color:#F9F9F9; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$empfone</a><br/>
                Email: <a style='color:#F9F9F9; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$empemail</a><br/>
                $emplocal - $empbairro | CEP: <a style='color:#F9F9F9; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$empcep</a><br/>
                $empcidade - $empestado
            </td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#005FA6'>&nbsp;</td>
          </tr>
        </table>

";
//===============================================================================================================
// Define a mensagem (Texto e Assunto)
//===============================================================================================================
$mail->Subject = $titulo;				    // Assunto da mensagem
$mail->Body    = $txt;					    // Texto da mensagem se for HTML
$mail->AltBody = $txt;						// Texto da mensagem se Não for HTML
//$mail->Body = "Este Ã© o corpo da mensagem de teste, em <b>HTML</b>!  :)";
//$mail->AltBody = "Este Ã© o corpo da mensagem de teste, em Texto Plano! \r\n :)";


if(!$mail->send()) {
    echo 'NÃ£o foi possível enviar o e-mail!!!';
    echo 'Informações do erro: ' . $mail->ErrorInfo;
	
}
    
//============================================================================================================
//============================================================================================================
//============================================================================================================

echo "<script type='text/javascript'>alert('Candidatura feita com sucesso!');</script>";
echo "<script>javascript:parent.jQuery.fancybox.close();</script>";
include "painel/destruidor.php";
?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>