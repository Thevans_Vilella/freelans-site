<div class="boxfull">
	<div class="titfxtop">Contato <span class="titfxtop-sub"></span></div>
	
		<div class="boxfull cinza">
			<div class="blg">
				<div class="inst-tit">Fale Conosco</div>
				<div class="bl-faq">
                
                    <div class="grdGRL-6 grdTBLp-12">
                        <div class="txt-contato">Preencha o formulário para que possa entrar em contato.</div>
                        <form id="contatos" method="post" action="enviar.php" autocomplete="off">
                            <ul>
                                <li><div class="campos"><input type="text" name="nome" id="pes" class="validate[required]" placeholder="Nome"/></div></li>
                                <li><div class="campos"><input type="tel" name="telefone" id="telfone" class="validate[required,custom[phone]]" placeholder="Telefone" onkeyup="maskIt(this,event,'(##) ####-#####')" maxlength="15"/></div></li>
                                <li><div class="campos"><input type="email" name="email" id="orige" class="validate[required,custom[email]]" placeholder="Email" /></div></li>
                                <li><div class="campos"><input type="text" name="assunto" id="assunto" class="validate[required]" placeholder="Assunto" /></div></li>
                                <li><div class="campos"><input type="text" name="cidade" id="cidade" class="validate[required]" placeholder="Cidade" /></div></li>
                                <li><div class="campos"><input type="text" name="estado" id="estado" class="validate[required]" placeholder="Estado" maxlength="2" /></div></li>
                                <li><div id="msgconts"><textarea name="mensagem" id="textarea" cols="1" rows="5" class="validate[required]" placeholder="Mensagem"></textarea></div></li>
                            </ul>

                            <input type="submit" class="envias" value="Enviar" />
                        </form>
                    </div>
                
                
                
                    <div class="grdGRL-6 grdTBLp-12">
                        <div class="lt-contato">
                            <div class="cnt-endereco-tit">ENDEREÇO</div>  
                            <div class="cnt-endereco-dados"><?php echo $emplocal; ?> - <?php echo $empbairro; ?>, <?php echo $empcidade; ?> - <?php echo $empsigla; ?>, <?php echo $empcep; ?>.</div>  
                            <div class="cnt-atendimento">ATENDIMENTO</div> 
                            <div class="cnt-atendimento-tit">Contato</div> 
                            <div class="cnt-atendimento-dados"><?php echo $fone1; ?> <span class="cnt-atendimento-dados-bold"><?php echo $fone2; ?></span></div> 
                            <div class="cnt-atendimento-tit">Email</div> 
                            <div class="cnt-atendimento-dados"><span class="cnt-atendimento-dados-bold"><?php echo $empemail; ?></span></div> 
                        </div>
                    </div>

                </div>
			</div>
		</div>

</div>
