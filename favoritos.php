<?php

if(isset($_SESSION["login_ses"]) and ($_SESSION["status_ses"] == "1")) { 

$users	=	$_SESSION["cod_ses"];

$sql101	=	"select * from favoritos WHERE perf_cod_usuario = '$users'";
$res101	=	mysqli_query($cn, $sql101);
$cnt101	=	mysqli_num_rows($res101);
$lin101	=	mysqli_fetch_array($res101);	
?>
<div class="boxfull">
	<div class="blg">

		<div class="tit">Meus favoritos</div>
		<div class="totprof"><?php echo $cnt101; ?> profissionais selecionados</div>
	
		<?php 
		$sqlfav			=	"select * from favoritos
							INNER JOIN perfil		  	on		perfil.perf_codigo			=	favoritos.perf_codigo
							INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
							INNER JOIN avaliacoes	  	on		avaliacoes.perf_codigo		=	perfil.perf_codigo
							WHERE 
							favoritos.perf_cod_usuario = '$users'
							ORDER BY 
							pla_ordem DESC, 
							ava_media_estrelas DESC, 
							perf_nome
							";
		$resfav			=	mysqli_query($cn, $sqlfav);
		$cntfav			=	mysqli_num_rows($resfav);
		while($linfav	=	mysqli_fetch_array($resfav))  {
			
			$perfref	=	$linfav['perf_codigo'];
			
			
			
				$sqluser	=	"select * from perfil
								INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
								INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
								where 
								perf_codigo = $perfref
								AND
								per_tip_codigo = 2
								AND 
								per_sta_codigo = 1
								";
				$resuser	=	mysqli_query($cn, $sqluser);
				$linuser	=	mysqli_fetch_array($resuser);	

				$perfnames	=	CorrigirNome($linuser['perf_nome']);

			
			
			$sqlavaluser	=	"select * from avaliacoes_usuario WHERE perf_cod_ref = '$perfref'";
			$resavaluser	=	mysqli_query($cn, $sqlavaluser);
			$cntavaluser	=	mysqli_num_rows($resavaluser);
			$linavaluser	=	mysqli_fetch_array($resavaluser);	


			$sqlaval	=	"select * from avaliacoes WHERE perf_codigo = '$perfref'";
			$resaval	=	mysqli_query($cn, $sqlaval);
			$linaval	=	mysqli_fetch_array($resaval);	
		?>
		<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
			<div class="boxit">
				<a href="perfil/<?php echo $linuser['perf_codigo']; ?>/<?php echo $perfnames; ?>">
					<div class="boxit-fot">
						<img src="painel/<?php echo $linuser['perf_fotop']; ?>" alt="<?php echo $perfnames; ?>"/>
						<!--<div class="boxit-tipo">PREMIUM</div>-->
					</div>
					<div class="boxit-tit"><?php echo $linuser['perf_nome']; ?></div>
					<div class="boxit-esp"><?php echo $linuser['cat_nome']; ?></div>
					<div class="boxit-estrela">
						<?php 
							for($contador = 1; $contador <= 5; $contador++) {

									if($contador <= $linaval['ava_media_estrelas']){

										echo "<i class='fas fa-star estyellow'></i>";

									} else {

										echo "<i class='fas fa-star'></i>";

									}

							}
						?>
					</div>
					<div class="boxit-nota">(<?php echo $cntavaluser; ?>)</div>
				</a>	
			</div>
		</div>
		<?php } ?>
		

		<div class="vazio"></div>
	</div>
</div>
<?php } else { require_once("alerta.php"); }// Termina IF de Login Aqui ============= ?>