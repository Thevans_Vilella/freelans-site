<?php
session_start();


if(isset($_SESSION["login_ses"]) and ($_SESSION["status_ses"] == "1")) {
	
require_once("painel/conexao.php");
require_once("painel/classes.php");

$ofet	    =	(int)$_POST['ofet'];
$perfcod	=	(int)$_POST['user'];
$status		=	(int)$_POST['stat'];
$city		=	(int)$_POST['city'];
$category	=	(int)$_POST['category'];
$titulo		=	ConverteItem(anti_injection($_POST['titulo']));
$descricao	=	ConvertePrevia(anti_injection(ConverteTexto($_POST['descricao'], 0)));
	
date_default_timezone_set('America/Sao_Paulo');
$data_atual = date("Y-m-d");
$hora_atual	= date('H:i:s');

	$sqluser	=	"select * from ofertas_trabalho WHERE ofe_tra_codigo = $ofet AND perf_codigo = $perfcod";
	$resuser	=	mysqli_query($cn, $sqluser);
	$linuser	=	mysqli_fetch_array($resuser);
    
    $data_cria  =   $linuser['ofe_tra_data'];

    $dias   =   CompararDatas($data_atual, $data_cria);

//============================================================================================================
if ( $ofet == "" or $perfcod == ""  or $status == ""  or $city == "" or $category == "" or $titulo == "" or $descricao == "" )  { 

	echo "<script type='text/javascript'>alert('Por Favor, Preencha todos os Campos!', 'ofertas');</script>";
	include "painel/destruidor.php";
	exit();

} 
//============================================================================================================
if ( $status == "1" )  { // Se o Status for Aberto faz abaixo
    
        $sqloft	=	"UPDATE ofertas_trabalho SET

        cat_codigo				= 	'$category',
        cid_codigo				= 	'$city',
        ofe_tra_nome			= 	'$titulo',
        ofe_tra_descricao		= 	'$descricao'

        WHERE
        ofe_tra_codigo 		=   $ofet
        AND
        perf_codigo 		=   $perfcod";

        mysqli_query($cn, $sqloft);
    
    
} else { // Se Status for diferente de Aberto faz abaixo
    
    
                $sqloft	=	"UPDATE ofertas_trabalho SET

                ofe_tra_stat_codigo		= 	'$status',
                cat_codigo				= 	'$category',
                cid_codigo				= 	'$city',
                ofe_tra_dias			= 	'$dias',
                ofe_tra_data_encerra	= 	'$data_atual',
                ofe_tra_hora_encerra	= 	'$hora_atual',
                ofe_tra_nome			= 	'$titulo',
                ofe_tra_descricao		= 	'$descricao'

                WHERE
                ofe_tra_codigo 		=   $ofet
                AND
                perf_codigo 		=   $perfcod";

                mysqli_query($cn, $sqloft);
    
} // Termina IF aqui

//============================================================================================================
	
echo "<script>window.location.href='ofertas';</script>";
include "painel/destruidor.php";

?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>