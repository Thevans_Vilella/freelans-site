<?php  if(isset($_SESSION["login_ses"]) and ($_SESSION["status_ses"] == "1")) { ?>
<?php 

	$perfcod	=	(int)$_POST["user"];
	$ofet	    =	(int)$_POST["ofet"];

	$sqluser	=	"select * from ofertas_trabalho
                    INNER JOIN perfil		  	            on		perfil.perf_codigo			                    =	ofertas_trabalho.perf_codigo
                    INNER JOIN categorias	  	            on		categorias.cat_codigo		                    =	ofertas_trabalho.cat_codigo
                    INNER JOIN cidades	  	                on		cidades.cid_codigo		                        =	ofertas_trabalho.cid_codigo
                    INNER JOIN estado	  	                on		estado.est_codigo		                        =	cidades.est_codigo
                    INNER JOIN ofertas_trabalho_status	  	on		ofertas_trabalho_status.ofe_tra_stat_codigo		=	ofertas_trabalho.ofe_tra_stat_codigo
                    WHERE
                    ofe_tra_codigo = $ofet
                    AND
                    perfil.perf_codigo = $perfcod
                    AND
                    ofertas_trabalho_status.ofe_tra_stat_codigo = 1
                    ";
	$resuser	=	mysqli_query($cn, $sqluser);
	$linuser	=	mysqli_fetch_array($resuser);	
?>

<div class="boxfull">
	<div class="titfxtop">Oferta de Serviço: <span class="titfxtop-sub"><?php echo $linuser['perf_nome']; ?></span></div>

	
		<div class="boxfull cinza">
			<div class="blg perf-bloco">
                
            <?php require_once("banner-oferta.php"); ?>
                
                
					<div class="grdGRL-6 grdDSK-12">
					<div class="bloco-perfil">
							<div class="bl-atua-bloco">
								<div class="bl-atua-tit">DETALHES DA OFERTA DE SERVIÇO</div>
								<div class="bl-atua-linha"></div>

								<form name="cadastro" class="cadastro" method="post" action="oferta-altera.php">
									<ul>
									<li>
										<div class="dad">
											<label>Status da oferta:</label>
											<select name="stat" class="select validate[required]" id="stat" style="background: #f44336; color: #ffffff;">
												<option value="<?php echo $linuser['ofe_tra_stat_codigo']; ?>"><?php echo $linuser['ofe_tra_stat_nome']; ?></option>
												<option value="<?php echo $linuser['ofe_tra_stat_codigo']; ?>">----------------</option>
												<?php
												$sqlofs		    =	"select * from ofertas_trabalho_status WHERE ofe_tra_stat_codigo < '4'";
												$resofs		    =	mysqli_query($cn, $sqlofs);
												while($linofs	=	mysqli_fetch_array($resofs))  {
												?>
												<option value="<?php echo $linofs['ofe_tra_stat_codigo']; ?>"><?php echo $linofs['ofe_tra_stat_nome']; ?></option>
												<?php } ?>
											</select>
										</div>
									</li>
									<li>
										<div class="dad">
											<label>Cidade:</label>
											<select name="city" class="select validate[required]" id="city">
												<option value="<?php echo $linuser['cid_codigo']; ?>"><?php echo $linuser['cid_nome']; ?>/ <?php echo $linuser['est_sigla']; ?></option>
												<option value="<?php echo $linuser['cid_codigo']; ?>">----------------</option>
												<?php
												$sql		=	"select * from cidades
																INNER JOIN estado	  	on		estado.est_codigo		=	cidades.est_codigo
																order by est_nome, cid_nome";
												$res		=	mysqli_query($cn, $sql);
												while($lin	=	mysqli_fetch_array($res))  {
												?>
												<option value="<?php echo $lin['cid_codigo']; ?>"><?php echo $lin['cid_nome']; ?>/ <?php echo $lin['est_sigla']; ?></option>
												<?php } ?>
											</select>
										</div>
									</li>
									<li>
										<div class="dad">
											<label>Categoria do Serviço:</label>
											<select name="category" class="select validate[required]" id="category">
												<option value="<?php echo $linuser['cat_codigo']; ?>"><?php echo $linuser['cat_nome']; ?></option>
												<option value="<?php echo $linuser['cat_codigo']; ?>">----------------</option>
												<?php
												$sql01	        =	"select * from categorias order by cat_ordem";
												$res01	        =	mysqli_query($cn, $sql01);
												while($lin01	=	mysqli_fetch_array($res01))  {
												?>
												<option value="<?php echo $lin01['cat_codigo']; ?>"><?php echo $lin01['cat_nome']; ?></option>
												<?php } ?>
											</select>
										</div>
									</li>
									<li>
										<div class="dad">
											<label>Título do serviço: Limite de 80 caracteres.</label>
											<input type="text" name="titulo" id="titulo" maxlength="80" class="validate[required]" placeholder="Título objetivo. Ex: Trocar torneira - pintar parede - arrumar curto elétrico..." value="<?php echo $linuser['ofe_tra_nome']; ?>"/>
										</div>
									</li>
									<li>
										<div class="dad">
											<label>Fale um pouco sobre o trabalho: Limite de 300 caracteres.</label>
											<textarea name="descricao" cols="1" rows="10" maxlength="300" class="validate[required]" placeholder="Fale um pouco sobre data, horário para execução e serviço a ser realizado..."><?php echo $linuser['ofe_tra_descricao']; ?></textarea>
										</div>
									</li>
									</ul>

									<input type="submit" class="regdados" value="Alterar" />
									<input type="hidden" name="ofet" size="5" value="<?php echo $ofet; ?>" />
                                    <input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
								</form>
							</div>
					</div>
					</div>
				

					<div class="grdGRL-6 grdDSK-12">
                    <div class="bl-inter-bloco">
                        <div class="tit-inter-dest">Profissionais Interessados</div>
                            
                        <?php 
                        $sql		=	"select * from ofertas_profissionais
                                        INNER JOIN perfil		  	     on		perfil.perf_codigo			=	ofertas_profissionais.perf_codigo
                                        INNER JOIN planos		  	     on		planos.pla_codigo			=	perfil.pla_codigo
                                        INNER JOIN categorias	  	     on		categorias.cat_codigo		=	perfil.cat_codigo
                                        INNER JOIN avaliacoes	  	     on		avaliacoes.perf_codigo		=	perfil.perf_codigo
                                        WHERE 
                                        ofe_tra_codigo = $ofet
                                        AND 
                                        per_sta_codigo = 1
                                        ORDER BY 
                                        pla_ordem DESC, ava_media_estrelas DESC, perf_nome";
                        $res		=	mysqli_query($cn, $sql);
                        while($lin	=	mysqli_fetch_array($res))  {

                            $perfavacod	=	$lin['perf_codigo'];


                            $sqlavaluser	=	"select * from avaliacoes_usuario WHERE perf_cod_ref = '$perfavacod'";
                            $resavaluser	=	mysqli_query($cn, $sqlavaluser);
                            $cntavaluser	=	mysqli_num_rows($resavaluser);
                            $linavaluser	=	mysqli_fetch_array($resavaluser);	


                            $sqlaval	=	"select * from avaliacoes WHERE perf_codigo = '$perfavacod'";
                            $resaval	=	mysqli_query($cn, $sqlaval);
                            $linaval	=	mysqli_fetch_array($resaval);
                            
                            if($lin['opc_codigo'] == 2) { // IF que Verifica se Exibi nome do plano
                                    
                                    $clas	    =	"boxit-dest";
                                    $classfot	=	"boxit-fot";
                                    $classplain	=	"boxit-tipo";
                                    $nomeplain	=	ConverteTexto($lin['pla_nome'], 1);

                                } else {

                                    $clas	    =	"boxit";
                                    $classfot	=	"boxit-fotss";
                                    $classplain	=	"";
                                    $nomeplain	=	"";

                            } // Termina IF que Verifica se Exibi nome do plano

                        ?>
                        <div class="grdGRL-6 grdDSK-4 grdTBLp-6 grdTBLr-4 grdSMPr-6 grdMOPr-12">
						<div class="<?php echo $clas; ?>">
							<a href="perfil/<?php echo $lin['perf_codigo']; ?>/<?php echo CorrigirNome($lin['perf_nome']); ?>" target="_blank">
								<div class="<?php echo $classplain; ?>"><?php echo $nomeplain; ?></div>
								<div class="<?php echo $classfot; ?>">
									<img src="painel/<?php echo $lin['perf_fotop']; ?>" alt="Favoritos"/>
								</div>
								<div class="boxit-tit"><?php echo $lin['perf_nome']; ?></div>
								<div class="boxit-esp"><?php echo $lin['cat_nome']; ?></div>
								<div class="boxit-estrela">
									<?php 
										for($contador = 1; $contador <= 5; $contador++) {

												if($contador <= $linaval['ava_media_estrelas']){

													echo "<i class='fas fa-star estyellow'></i>";

												} else {

													echo "<i class='fas fa-star'></i>";

												}

										}
									?>
								</div>
								<div class="boxit-nota">(<?php echo $linaval['ava_qtde_votos']; ?>)</div>
							</a>	
						</div>
                        </div>
                        <?php } ?>
                            
                    </div>
					</div>
				
			</div>
			<div class="vazio"></div>	
		</div>


	
</div>
<?php } else { require_once("alerta.php");}// Termina IF de Login Aqui ============= ?>