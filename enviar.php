<?php
include "painel/conexao.php";
include "painel/classes.php";
//===============================================================================================================
// Aqui Seleciona os Dados de Contato da Empresa ===================================================================
$sql10	=	"SELECT * from contato
			INNER JOIN estado 		on 	estado.est_codigo		=	contato.est_codigo
			INNER JOIN logradouro 	on 	logradouro.log_codigo	=	contato.log_codigo";
$res10	=	mysqli_query($cn, $sql10);
$lin10	=	mysqli_fetch_array($res10);	

$fone   = substr($lin10['cont_telefone'], 0, 14);
$fone0  = substr($lin10['cont_telefone'], 1, 2);
$fone1  = substr($lin10['cont_telefone'], 0, 4);
$fone2  = substr($lin10['cont_telefone'], 5, 9);

$contnome	=	$lin10['cont_nome'];   
$namestop	=	"J7 Construtora";  
$empfone	=	$lin10['cont_telefone'];
$empemail 	=	$lin10['cont_email'];
$empcep 	=	$lin10['cont_cep'];
$emplocal 	=	$lin10['log_sigla']." ".$lin10['cont_endereco'].", ".$lin10['cont_numero'];
$empbairro 	=	$lin10['cont_bairro'];
$empcidade 	=	$lin10['cont_cidade'];
$empestado =	$lin10['est_nome'];
$recebe		=	$lin10['cont_email_formulario'];
//==================================================================================================================
$sql11 		= 	"SELECT * FROM metas_google";
$res11 		= 	mysqli_query($cn, $sql11);
$lin11 		= 	mysqli_fetch_array($res11);

$urlproj	=	$lin11['tag_url'];
//===============================================================================================================
//===============================================================================================================
$nome		=	ConverteItem(ConverteTexto(anti_injection($_POST['nome']), 0));
$telefone	=	CorrigirTelefone(anti_injection($_POST['telefone']));
$email		=	ConverteEmail(anti_injection($_POST['email']));
$cidade		=	ConverteItem(ConverteTexto(anti_injection($_POST['cidade']), 0));
$estado		=	ConverteTexto(anti_injection($_POST['estado']), 1);
$assunto	=	ConverteItem(ConverteTexto(anti_injection($_POST['assunto']), 0));
$mensagem	=	ConvertePrevia(anti_injection($_POST['mensagem']));
//===============================================================================================================
if ( $_POST['nome'] == "" or $_POST['telefone'] == "" or $_POST['email'] == ""  or $_POST['assunto'] == "" or $_POST['mensagem'] == ""  )  {
	
	echo "Por favor, Todos os Campos são de Preenchimento Obrigatório!"; 
    echo "<script type='text/javascript'>window.location.href='contato';</script>";
	include "painel/destruidor.php";
	exit();
} 
//===============================================================================================================
// Passando os dados obtidos pelo formulário para as variáveis abaixo
//===============================================================================================================
$nomeremetente     = 	$contnome;
$emailremetente    = 	$user_mail;
$emaildestinatario = 	$recebe; 
$titulo            = 	"Contato Pelo Site $contnome";
//===============================================================================================================
//===============================================================================================================
require_once("phpmailer/PHPMailerAutoload.php");
// Inicia a classe PHPMailer
//===============================================================================================================
$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               	// Enable verbose debug output

$mail->isSMTP();                                      	// Define que a mensagem será SMTP
//$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Especifique os servidores SMTP principal e backup
$mail->Host = $serv_sis;  								// Especifique os servidores SMTP principal e backup
$mail->SMTPAuth = true;                               	// Usa autenticação SMTP? (opcional)
$mail->Username = $user_sis;     						// Usuário do servidor SMTP - Mesmo email da linha 45
$mail->Password = $pass_sis;            				// Senha do servidor SMTP - Se não tiver de Serviço de SMTP Contrato colocar a senha do email que está sendo utilizada para envio.
$mail->SMTPSecure = $user_crip;                         // Ativar a criptografia TLS, `também ssl` aceita
$mail->Port = $user_porta;                              // Porta TCP para conectar-se

$mail->From = $emailremetente;					                      // Email do Remetente - Colocar email do servidor Locaweb (Site)
$mail->FromName = $nomeremetente;									  // Nome do Remetente - Colocar Seu Nome ou Nome de sua Empresa
$mail->addAddress($emaildestinatario, $nome);           	      	  // Adicionar um destinatário - Nome é opcional
$mail->addReplyTo($email, $nome);	  								  // Responder Para - Aqui define para quem vao a respósta do email



// Define os dados Técnicos da Mensagem
//===============================================================================================================
$mail->IsHTML(true); 								    // Define que o e-mail será¡ enviado como HTML
$mail->CharSet = 'utf-8'; 						        // Charset da mensagem (opcional)

//===============================================================================================================
// Aqui é a Mensagem do Contato que vai no Email - Ou seja o HTML da Mensagem
//===============================================================================================================
$txt = "

        <table width='550' border='0' style='background-color:#FFF;border:25px solid #E7E2DC' align='center' cellpadding='0' cellspacing='0'>
          <tr>
            <td colspan='3' valign='top' bgcolor='#F9F8F7'>
                <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align='center'><a href='$urlproj' target='_blank'><img src='$urlproj/assets/_img/logo-freelans.png' border='0' alt='$contnome'/></a></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                </table>
            </td>
          </tr>
          <tr>
            <td height='3' colspan='3' bgcolor='#005FA6'></td>
          </tr>
          <tr>
            <td height='40' colspan='3' bgcolor='#FFFFFF'>&nbsp;</td>
          </tr>
          <tr>
            <td width='5%' bgcolor='#FFFFFF'>&nbsp;</td>
            <td width='100%' bgcolor='#FFFFFF'>
                <table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>
                  <tr>
                    <td style='color:#B20000; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>Nome:</td>
                    <td style='color:#000000; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$nome</td>
                  </tr>
                  <tr>
                    <td style='color:#B20000; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>Telefone:</td>
                    <td><a style='color:#000000; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$telefone</a></td>
                  </tr>
                  <tr>
                    <td style='color:#B20000; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>Email:</td>
                    <td><a style='color:#000000; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$email</a></td>
                  </tr>
                  <tr>
                    <td style='color:#B20000; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>Cidade:</td>
                    <td style='color:#000000; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$cidade</td>
                  </tr>
                  <tr>
                    <td style='color:#B20000; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>Estado:</td>
                    <td style='color:#000000; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$estado</td>
                  </tr>
                  <tr>
                    <td style='color:#B20000; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>Assunto:</td>
                    <td style='color:#000000; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$assunto</td>
                  </tr>
                  <tr>
                    <td colspan='2' style='color:#B20000; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>Mensagem:</td>
                  </tr>
                  <tr>
                    <td  colspan='2' style='color:#000000; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif; text-align:justify'>$mensagem</td>
                  </tr>
                </table>
            </td>
            <td width='5%' bgcolor='#FFFFFF'>&nbsp;</td>
          </tr>
          <tr>
            <td height='40' colspan='3' bgcolor='#FFFFFF'>&nbsp;</td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#FFCA03' height='3'></td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#8BC53E' height='3'></td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#005FA6'>&nbsp;</td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#005FA6' align='center' style='color:#F9F9F9; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>
                Telefone: <a style='color:#F9F9F9; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$empfone</a><br/>
                Email: <a style='color:#F9F9F9; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$empemail</a><br/>
                $emplocal - $empbairro | CEP: <a style='color:#F9F9F9; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$empcep</a><br/>
                $empcidade - $empestado
            </td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#005FA6'>&nbsp;</td>
          </tr>
        </table>

";
//===============================================================================================================
// Define a mensagem (Texto e Assunto)
//===============================================================================================================
$mail->Subject = $titulo;				    // Assunto da mensagem
$mail->Body    = $txt;					    // Texto da mensagem se for HTML
$mail->AltBody = $txt;						// Texto da mensagem se Não for HTML
//$mail->Body = "Este Ã© o corpo da mensagem de teste, em <b>HTML</b>!  :)";
//$mail->AltBody = "Este Ã© o corpo da mensagem de teste, em Texto Plano! \r\n :)";


if(!$mail->send()) {
    echo 'NÃ£o foi possível enviar o e-mail!!!';

    echo 'Informações do erro: ' . $mail->ErrorInfo;
	include "painel/destruidor.php";
	
}
	
?>


<?php
	echo "<script type='text/javascript'>alert('Mensagem Enviada com Sucesso!!!');</script>";
	echo "<script type='text/javascript'>window.location.href='contato';</script>";

	include "painel/destruidor.php";
?>
