<?php 

if(isset($_SESSION["login_ses"]) and ($_SESSION["status_ses"] == "1") and ($_SESSION["tipo_ses"] == "2")) {

$item		=	$_SESSION["cod_ses"];

	
$sqluser	=	"select * from perfil
				INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
				INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
				WHERE perf_codigo = $item";
$resuser	=	mysqli_query($cn, $sqluser);
$linuser	=	mysqli_fetch_array($resuser);	

$perfcod	=	$linuser['perf_codigo'];
$perfname	=	$linuser['perf_nome'];
$catcod		=	$linuser['cat_codigo'];
$perfcat	=	$linuser['cat_nome'];

$perfnames	=	CorrigirNome($perfname);
$avalia		=	"perfil-avaliacoes/".$perfcod."/".$perfnames;



$sqlavaluser	=	"select * from avaliacoes WHERE perf_codigo = '$item'";
$resavaluser	=	mysqli_query($cn, $sqlavaluser);
$linavaluser	=	mysqli_fetch_array($resavaluser);	

$cntavuser		=	$linavaluser['ava_media_estrelas'];

if($cntavuser > 0){ 
	
		$cntavuser	=	$cntavuser; 
	
	} else {
	
		$cntavuser	=	""; 
}

?>
<div class="boxfull">
		<div class="titfxtop">Avaliações: <span class="titfxtop-sub"><?php echo $linuser['perf_nome']; ?></span></div>

	
		<div class="boxfull cinza">
			<div class="blg perf-bloco">

				<div class="grdGRL-12">
					<div class="bloco-perfil">
						<div class="bl-atua-bloco">
							<div class="bl-atua-tit">CLASSIFICAÇÃO</div>
							<div class="bl-perf-avalia">Pontuação média <?php echo $cntavuser; ?></div>
							<div class="bl-atua-linha"></div>
								<div class="grdGRL-4 grdDSK-3 grdSMPr-4">
									<div class="bl-perf-avalia-tit">ATENDIMENTO</div>
									<div class="boxit-estrela-avaliacoes">
										<?php 
											for($contador = 1; $contador <= 5; $contador++) {

													if($contador <= $linavaluser['ava_atendimento']){

														echo "<i class='fas fa-star estyellow'></i>";

													} else {

														echo "<i class='fas fa-star'></i>";

													}

											}
										?>
									</div>
								</div>
						</div>
					</div>
				</div>

				
				


				<div class="grdGRL-12">
				<div class="bloco-perfil">
					<div class="bl-atua-bloco">
						<div class="bl-atua-tit">RECOMENDAÇÕES</div>
							<div class="bl-reconda-scroll">
						
							<?php
							$sqlrecom		=	"select * from avaliacoes_usuario 
												INNER JOIN perfil	  	on		perfil.perf_codigo		=	avaliacoes_usuario.perf_codigo
												WHERE 
												perf_cod_ref = '$item'
												ORDER BY
												ava_user_data_created, 
												ava_user_hora_created
												DESC
												";
							$resrecom		=	mysqli_query($cn, $sqlrecom);
							while($linrecom	=	mysqli_fetch_array($resrecom)) {
							?>
							<div class="bl-recomendacoes">
							<?php if($linrecom['perf_fotop'] == ""){ ?>
								<div class="bl-avalia-user-foto"><i class="fas fa-user user-nofoto"></i></div>
							<?php } else { ?>
								<div class="bl-avalia-user-foto"><img src="painel/<?php echo $linrecom['perf_fotop']; ?>" alt=""/></div>
							<?php } ?>
									<div class="bl-avalia-user-dad">
										<div class="bl-avalia-user-nome"><?php echo $linrecom['perf_nome']; ?></div>
										<div class="bl-avalia-user-txt"><?php echo $linrecom['ava_user_descricao']; ?></div>
										<div class="bl-avalia-user-estrela">
											<?php 
												for($contador = 1; $contador <= 5; $contador++) {

														if($contador <= $linrecom['ava_user_media_estrelas']){

															echo "<i class='fas fa-star estyellow'></i>";

														} else {

															echo "<i class='fas fa-star'></i>";

														}

												}
											?>
										</div>
									</div>
							</div>
							<?php } ?>	
								
						</div>
					</div>
					
				</div>
				</div>
				
				

			</div>
			<div class="vazio"></div>	
		</div>


	
</div>
<?php } else { require_once("alerta.php");}// Termina IF de Login Aqui ============= ?>