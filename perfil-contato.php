<?php 

if(!isset($_GET['CodigAbriR'])) { require_once("alerta.php"); }
	
if(isset($_SESSION["login_ses"])) {
	
	$clitem		=	$_SESSION["login_ses"];
	
} else {
	
	$clitem		=	"";
	
}

$item		=	$_GET['CodigAbriR'];

$sqlcli		=	"select * from perfil where perf_login = '$clitem'";
$rescli		=	mysqli_query($cn, $sqlcli);
$lincli		=	mysqli_fetch_array($rescli);	

$cliname	=	$lincli['perf_nome'];



$sqluser	=	"select * from perfil
				INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
				INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
				where perf_codigo = $item";
$resuser	=	mysqli_query($cn, $sqluser);
$linuser	=	mysqli_fetch_array($resuser);	

$perfcod	=	$linuser['perf_codigo'];
$perfname	=	$linuser['perf_nome'];
$catcod		=	$linuser['cat_codigo'];
$perfcat	=	$linuser['cat_nome'];



$sqldsec	=	"select * from perfil_sobre where perf_codigo = '$item'";
$resdsec	=	mysqli_query($cn, $sqldsec);
$lindsec	=	mysqli_fetch_array($resdsec);	
	
?>
<div class="boxfull">
	<div class="perftop cinza-escuro">
		Você está em: <span class="titfxtop-sub">Contato <?php echo $linuser['perf_nome']; ?></span>
		<div class="mobil">CLIQUE NO MENU ABAIXO PARA MAIS INFORMAÇÕES DESSE PERFIL: <br/><span class="titfxtop-sub"><?php echo $linuser['perf_nome']; ?></span></div>
		<?php require_once("botoes-perfil-prestador.php"); ?>
	</div>

	
		<div class="boxfull cinza">
			<div class="blg perf-bloco">
				
				<?php  if(!isset($_SESSION["login_ses"])) {  ?>
				<div class="bl ">
					<div class="titfxdest-meio">Para acessar os dados de contato deste prestador, é necessário estar logado.<br/><br/><br/></div>
					<div class="subtititem-meio"></div>
					
					<div class="caddados"><a href="login">Faça seu login</a></div>
					<div class="caddados"><a href="register-account">Cadastre-se</a></div>
				</div>
				<?php } ?>
				
				
				

				<?php  if(isset($_SESSION["login_ses"])) {  ?>
				<div class="grdGRL-4 grdDSK-6 grdTBLp-12">
				<div class="bloco-perfil">
					<div class="bl-atua-bloco">
						<div class="bl-atua-tit">CONTATOS</div>
						<div class="bl-atua-linha"></div>
						<?php
						$sql999			=	"select * from perfil_contato_dados 
											INNER JOIN perfil_contato	  	on		perfil_contato.per_cont_codigo		=	perfil_contato_dados.per_cont_codigo
											WHERE 
											perf_codigo = $perfcod 
											ORDER BY 
											per_cont_ordem";
						$res999			=	mysqli_query($cn, $sql999);
						while($lin999	=	mysqli_fetch_array($res999))  {
							
						$cel			=	CorrigirNumeros($lin999['perf_cont_dad_item']);
						?>
						<div class="bl-perf-cont">
							<div class="afasta">
								<a href="https://api.whatsapp.com/send?phone=55<?php echo $cel; ?>&text=Olá,%20tudo%20bem%20<?php echo $perfname; ?>!%20Meu%20nome%20é%20<?php echo $cliname; ?>%20vi%20seu%20anúncio%20no%20site%20da%20Freelans.%20Preciso%20de%20um%20orçamento!" target="_blank">
									<img src="painel/<?php echo $lin999['per_cont_foto']; ?>" alt=""/>
								</a>
							</div>
							<?php echo $lin999['perf_cont_dad_item']; ?>
						</div>
						<?php } ?>
					</div>
				</div>
				</div>
				
				

				<div class="grdGRL-4 grdDSK-6 grdTBLp-12">
				<div class="bloco-perfil">
					<div class="bl-atua-bloco">
						<div class="bl-atua-tit">LOCALIZAÇÃO</div>
						<div class="bl-atua-linha"></div>
					</div>
					<div class="bl-perf-cont-mapa"><a href="<?php echo $lindsec['perf_sob_mapa']; ?>" target="_blank"><img src="assets/_img/mapa.jpg"></a></div>
					<div class="bl-perf-cont-mapa-link"><a href="<?php echo $lindsec['perf_sob_mapa']; ?>" target="_blank">Acessar Mapa</a></div>
				</div>
				</div>

				
				

				<div class="grdGRL-4 grdDSK-12 grdTBLp-12">
				<div class="bloco-perfil">
					<div class="bl-atua-bloco">
						<div class="bl-atua-tit">REDES SOCIAIS</div>
						<div class="bl-atua-linha"></div>
						<?php
						$sql767			=	"select * from perfil_midias_dados 
											INNER JOIN midias_contato	  	on		midias_contato.mid_cont_codigo		=	perfil_midias_dados.mid_cont_codigo
											WHERE perf_codigo = $perfcod 
											ORDER BY 
											mid_cont_ordem";
						$res767			=	mysqli_query($cn, $sql767);
						while($lin767	=	mysqli_fetch_array($res767))  {

						?>
						<div class="bl-perf-cont-icon"><a href="<?php echo $lin767['perf_cont_dad_item']; ?>"><img src="painel/<?php echo $lin767['mid_cont_foto']; ?>" alt=""/></div>
						<?php } ?>
					</div>
				</div>
				</div>
				<?php } ?>

			</div>
			<div class="vazio"></div>	
		</div>

</div>