<?php
$sql6		=	"SELECT * from textos WHERE txt_codigo = '6'";
$res6		=	mysqli_query($cn, $sql6);
$lin6		=	mysqli_fetch_array($res6);

$sql7		=	"SELECT * from textos WHERE txt_codigo = '7'";
$res7		=	mysqli_query($cn, $sql7);
$lin7		=	mysqli_fetch_array($res7);

$sql8		=	"SELECT * from textos WHERE txt_codigo = '8'";
$res8		=	mysqli_query($cn, $sql8);
$lin8		=	mysqli_fetch_array($res8);

$sql9		=	"SELECT * from textos WHERE txt_codigo = '9'";
$res9		=	mysqli_query($cn, $sql9);
$lin9		=	mysqli_fetch_array($res9);

$sql10		=	"SELECT * from textos WHERE txt_codigo = '10'";
$res10		=	mysqli_query($cn, $sql10);
$lin10		=	mysqli_fetch_array($res10);
?>
<section id="how_works">
    <div class="container no-padding-left-right">
        <div class="row no-margin-left-right">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <h4 id="how_works_title">Como funciona</h4>
                <h5 id="how_works_subtitle"><?php echo $lin6['txt_descricao']; ?></h5>
                <p id="how_works_faq">Tem alguma dúvida? confira as <a class="underscore_effect" href="">Dúvidas Frequentes</a></p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <p id="how_works_text"><?php echo $lin7['txt_descricao']; ?></p>
            </div>
        </div>
        <div class="row no-margin-left-right">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <figure class="how_works_figure">
                    <figcaption class="how_works_item_title">Encontre o <br class="d-none d-md-block"> profissional ideal</figcaption>
                    <span class="how_works_item_icon">
                        <img class="img-fluid" src="assets/_img/icons/candidates.png" alt="Encontre o profissional ideal">
                    </span>
                </figure>
                <p class="how_works_item_text"><?php echo $lin8['txt_descricao']; ?></p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <figure class="how_works_figure">
                    <figcaption class="how_works_item_title">Feche <br class="d-none d-md-block"> negócio</figcaption>
                    <span class="how_works_item_icon">
                        <img class="img-fluid" src="assets/_img/icons/handshake.png" alt="Feche negócio">
                    </span>
                </figure>
                <p class="how_works_item_text"><?php echo $lin9['txt_descricao']; ?></p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <figure class="how_works_figure">
                    <figcaption class="how_works_item_title">Avalie o <br class="d-none d-md-block"> profissional</figcaption>
                    <span class="how_works_item_icon">
                        <img class="img-fluid" src="assets/_img/icons/evaluation.png" alt="Avalie o profissional">
                    </span>
                </figure>
                <p class="how_works_item_text"><?php echo $lin10['txt_descricao']; ?></p>
            </div>
        </div>
    </div>
</section>