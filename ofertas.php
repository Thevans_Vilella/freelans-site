<?php  if(isset($_SESSION["login_ses"]) and ($_SESSION["status_ses"] == "1")) { ?>
<?php 

	$user	=	$_SESSION["login_ses"];

	$sqluser	=	"select * from perfil
                    INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
                    where
                    perfil.perf_login = '$user'
                    ";
	$resuser	=	mysqli_query($cn, $sqluser);
	$linuser	=	mysqli_fetch_array($resuser);	

	$perfcod	=	$linuser['perf_codigo'];
	$perfcat	=	$linuser['cat_codigo'];
    
    
    $sqltut	    =	"select * from tutoriais where tut_codigo = '1'";
    $restut	    =	mysqli_query($cn, $sqltut);
    $lintut	    =	mysqli_fetch_array($restut);    
?>
<div class="boxfull">
	<div class="titfxtop">Ofertas de Serviços: <span class="titfxtop-sub"><?php echo $linuser['perf_nome']; ?></span></div>

	
		<div class="boxfull cinza">
			<div class="blg perf-bloco">
                
            <?php require_once("banner.php"); ?>
            <div class="bl-cadastros">
                
                <div class="grdGRL-12">
                    <div class="vidtut"><a data-fancybox href="https://www.youtube.com/embed/<?php echo $lintut['tut_link']; ?>?rel=0">Vídeo Tutorial</a></div> 
                </div> 
                
                
                    <div class="grdGRL-6 grdDSK-12">
                    <div class="bloco-perfil">
                        <div class="accordebl">CADASTRAR OPORTUNIDADE DE SERVIÇO</div>
                        <div class="panel">
                            <div class="bl-atua-bloco">
                                <div class="bl-atua-tit">CADASTRAR OPORTUNIDADE DE SERVIÇO</div>
                                <div class="bl-atua-linha"></div>

                                <form name="cadastro" class="cadastro" method="post" action="oferta-insere.php">
                                    <ul>
                                    <li>
                                        <div class="dad">
                                            <label>Selecione a cidade para se cadastrar:</label>
                                            <select name="city" class="select validate[required]" id="city">
                                                <option value="">Selecione uma Cidade</option>
                                                <option value="">----------------------------</option>
                                                <?php
                                                $sql		=	"select * from cidades
                                                                INNER JOIN estado	  	on		estado.est_codigo		=	cidades.est_codigo
                                                                order by est_nome, cid_nome";
                                                $res		=	mysqli_query($cn, $sql);
                                                while($lin	=	mysqli_fetch_array($res))  {
                                                ?>
                                                <option value="<?php echo $lin['cid_codigo']; ?>"><?php echo $lin['cid_nome']; ?>/ <?php echo $lin['est_sigla']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="dad">
                                            <label>Selecione a Categoria do Serviço:</label>
                                            <select name="category" class="select validate[required]" id="category">
                                                <option value="">Selecione a Categoria do Serviço</option>
                                                <option value="">----------------------------</option>
                                                <?php
                                                $sql01	=	"select * from categorias order by cat_ordem";
                                                $res01	=	mysqli_query($cn, $sql01);
                                                while($lin01	=	mysqli_fetch_array($res01))  {
                                                ?>
                                                <option value="<?php echo $lin01['cat_codigo']; ?>"><?php echo $lin01['cat_nome']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="dad">
                                            <label>Título do serviço: Limite de 80 caracteres.</label>
                                            <input type="text" name="titulo" id="titulo" maxlength="80" class="validate[required]" placeholder="Título objetivo. Ex: Trocar torneira - pintar parede - arrumar curto elétrico..."/>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="dad">
                                            <label>Fale um pouco sobre o trabalho: Limite de 300 caracteres.</label>
                                            <textarea name="descricao" cols="1" rows="10" maxlength="300" class="validate[required]" placeholder="Fale um pouco sobre data, horário para execução e serviço a ser realizado..."></textarea>
                                        </div>
                                    </li>
                                    </ul>

                                    <input type="submit" class="regdados" value="Cadastrar" />
                                    <input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>



                    <div class="grdGRL-6 grdDSK-12">
                    <div class="bloco-perfil">
                        <div class="accordebl">MINHAS OPORTUNIDADES</div>
                        <div class="panel">
                            <div class="bl-atua-bloco">
                                <div class="bl-atua-tit">MINHAS OPORTUNIDADES</div>
                                <div class="bl-atua-linha"></div>
                                    <?php
                                    $sqlofta			=	"select * from ofertas_trabalho 
                                                            WHERE 
                                                            perf_codigo = $perfcod 
                                                            AND 
                                                            ofe_tra_stat_codigo = 1
                                                            ORDER BY
                                                            ofe_tra_data";
                                    $resofta			=	mysqli_query($cn, $sqlofta);
                                    while($linofta		=	mysqli_fetch_array($resofta))  {
                                    ?>
                                    <div class="grdGRL-12">
                                        <div class="oft-data"><?php echo CorrigirData($linofta['ofe_tra_data']); ?></div>
                                        <form name="cadastro" class="oft-detalhes" method="post" action="oferta-detalhes">
                                            <input type="submit" class="oft-detail" value="Ver detalhes" />
                                            <!--<button><i class="fas fa-check detail"></i></button>-->
                                            <input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
                                            <input type="hidden" name="ofet" size="5" value="<?php echo $linofta['ofe_tra_codigo']; ?>" />
                                        </form>	
                                        <div class="oft-nome"><?php echo $linofta['ofe_tra_nome']; ?></div>
                                        <div class="oft-linha"></div>
                                    </div>
                                    <?php } ?>
                            </div>
                        </div>
                    </div>
                    </div>
            </div>
				
                
                    <?php  if(isset($_SESSION["login_ses"]) and ($_SESSION["tipo_ses"] == "2")) { // Coomeça IF que verifica se é profissional  ?>
                    <?php
                        if(isset($_POST['cattrabalho']) and ($_POST['cattrabalho'] > 0)) {
                            
                            $oftcat     =   $_POST['cattrabalho'];
                            
                                $sqlcatoft		=	"select * from categorias WHERE cat_codigo = $oftcat";
                                $rescatoft		=	mysqli_query($cn, $sqlcatoft);
                                $lincatoft	    =	mysqli_fetch_array($rescatoft);
                            
                                    $namecat    =   $lincatoft['cat_nome'];

                
                        } else {
                            
                                $namecat    =   ""; 
                            
                        }
                
                    ?>
                    <!--<div class="titcity">Ofertas de Serviços: <span class="titfxtop-sub"><?php echo $linuser['cat_nome']; ?></span></div>-->
                    <div class="titcity">Ofertas de Serviços: <span class="titfxtop-sub"><?php echo $namecat; ?></span></div>
                    
                    <div class="titcity">
                        <form method="post" name="catoferta" action="ofertas">
                            <label>Selecione a Categoria para ver as vagas:</label>
                            <select name="cattrabalho" class="select" id="cattrabalho" onchange="document.forms['catoferta'].submit();">
                                <option value="">Selecione a Categoria</option>
                                <option value="">----------------------------</option>
                                <?php
                                $sqlcat			=	"select * from categorias order by cat_nome";
                                $rescat			=	mysqli_query($cn, $sqlcat);
                                while($lincat	=	mysqli_fetch_array($rescat))  {
                                ?>
                                <option value="<?php echo $lincat['cat_codigo']; ?>"><?php echo $lincat['cat_nome']; ?></option>
                                 <?php } ?>
                               <option value="">Todas as Vagas</option>
                            </select>
                        </form>
                
                    </div>
                    
                
                        <?php 
                                                                                                 
                        // Se quiser que a pessoa veja apenas as vagas da categoria que pertence incluir essa linha na sql da consulta.
                        // AND
                        // categorias.cat_codigo = $perfcat
                                                                                                 
                        // Colocar abaixo desse trecho de código
                        // perfil.perf_codigo != $perfcod
                                                                                                 
                                                                                                 
//                        $sql		=	"select * from ofertas_trabalho
//                                        INNER JOIN perfil		  	   on		perfil.perf_codigo			    =	ofertas_trabalho.perf_codigo
//                                        INNER JOIN categorias	  	   on		categorias.cat_codigo		    =	ofertas_trabalho.cat_codigo
//                                        INNER JOIN cidades	  	       on		cidades.cid_codigo		        =	ofertas_trabalho.cid_codigo
//                                        INNER JOIN estado	  	       on		estado.est_codigo		        =	cidades.est_codigo
//                                        WHERE
//                                        perfil.perf_codigo != $perfcod
//                                        
//                                        AND
//                                        ofe_tra_stat_codigo = 1
//                                        ORDER BY 
//                                        cid_nome, ofe_tra_nome
//                                        ";
//                                        
                         
                                        
                            if(isset($_POST['cattrabalho']) and ($_POST['cattrabalho'] > 0)) {

                            $oftcat     =   $_POST['cattrabalho'];

                            $sql		=	"select * from ofertas_trabalho
                                            INNER JOIN perfil		  	   on		perfil.perf_codigo			    =	ofertas_trabalho.perf_codigo
                                            INNER JOIN categorias	  	   on		categorias.cat_codigo		    =	ofertas_trabalho.cat_codigo
                                            INNER JOIN cidades	  	       on		cidades.cid_codigo		        =	ofertas_trabalho.cid_codigo
                                            INNER JOIN estado	  	       on		estado.est_codigo		        =	cidades.est_codigo
                                            WHERE
                                            perfil.perf_codigo != $perfcod

                                            AND
                                            ofe_tra_stat_codigo = 1
                                            AND
                                            categorias.cat_codigo = $oftcat
                                            ORDER BY 
                                            cid_nome, ofe_tra_nome
                                            ";


                            } else {

                            $sql		=	"select * from ofertas_trabalho
                                            INNER JOIN perfil		  	   on		perfil.perf_codigo			    =	ofertas_trabalho.perf_codigo
                                            INNER JOIN categorias	  	   on		categorias.cat_codigo		    =	ofertas_trabalho.cat_codigo
                                            INNER JOIN cidades	  	       on		cidades.cid_codigo		        =	ofertas_trabalho.cid_codigo
                                            INNER JOIN estado	  	       on		estado.est_codigo		        =	cidades.est_codigo
                                            WHERE
                                            perfil.perf_codigo != $perfcod

                                            AND
                                            ofe_tra_stat_codigo = 1
                                            ORDER BY 
                                            cid_nome, ofe_tra_nome
                                            ";

                            }
                        $res		=	mysqli_query($cn, $sql);
                        while($lin	=	mysqli_fetch_array($res))  {
                        ?>
                        <div data-same-height="group" class="grdGRL-4 grdTBLp-6 grdTBLr-4 grdSMPr-6 grdMOPr-12 box-oft">
                            <div class="boxit">
                                <a data-fancybox data-type="iframe" href="oferta/<?php echo $lin['ofe_tra_codigo']; ?>">
                                    <div class="oftit-tit"><?php echo $lin['ofe_tra_nome']; ?></div>
                                    <div class="oftit-prev"><?php echo CorrigirListarPrevia($lin['ofe_tra_descricao']); ?></div>
                                    <div class="oftit-city"><?php echo $lin['cid_nome']; ?>/<?php echo $lin['est_sigla']; ?></div>
                                </a>	
                            </div>
                        </div>
                        <?php } ?>
                
                    <?php } // Termina IF que verifica se é Profissional ?>
                <div class="vazio"></div>
            </div>
		</div>


	
</div>
<script type="text/javascript">
var acc = document.getElementsByClassName("accordebl");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>

<?php } else { require_once("alerta.php");}// Termina IF de Login Aqui ============= ?>