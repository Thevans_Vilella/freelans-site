<?php
session_start();
require_once("painel/conexao.php");
require_once("painel/classes.php");

$pg = isset($_GET['IniSeSsiSit']) ? $_GET['IniSeSsiSit'] : "empresa";

$pagina = "$pg.php";
if(!file_exists($pagina)){
	$pagina = "erro.php";
}

require_once("padrao.php"); 
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<meta name="keywords" content="<?= $name ?> - <?= Corrigirkeywords($busca_chave) ?>" />
<meta name="description" content="<?= $name ?> - <?= CorrigirDescription($description) ?>" />
<meta name="geo.placename" content="<?= $name ?> - <?= $lin10['log_sigla'] ?> <?= $lin10['cont_endereco'] ?>, <?= $lin10['cont_bairro'] ?>, <?= $lin10['cont_cidade'] ?>/<?= $lin10['est_sigla'] ?>, CEP: <?= $lin10['cont_cep'] ?>"/>
<meta name="geo.region" content="BR-<?= $lin10['est_nome'] ?>"/>
<meta name="author" content="<?= $lin11['tag_autor'] ?>" />
<meta property="og:image" content="<?= $lin11['tag_url'] ?>/assets/_img/logomarca.png"/>
<meta property="og:title" content="<?= $name ?>" />
<meta property="og:phone_number" content="<?= $lin10['cont_telefone'] ?>" />
<meta property="og:street-address" content="<?= $lin10['log_sigla'] ?> <?= $lin10['cont_endereco'] ?>, <?= $lin10['cont_numero'] ?> - <?= $lin10['cont_bairro'] ?>" />
<meta property="og:locality" content="<?= $lin10['cont_cidade'] ?>" />
<meta property="og:region" content="<?= $lin10['est_sigla'] ?>" />
<meta property="og:email" content="<?= $lin10['cont_email'] ?>" />
<meta property="og:type" content="website"/>
<meta property="og:url" content="<?= $lin11['tag_url'] ?>" />
<meta property="og:site_name" content="<?= $name ?>"/>
<meta property="og:description" content="<?= CorrigirDescription($lin11['tag_descricao']) ?>" />
<meta property="busca:title" content="<?= $name ?> - <?php if(isset($pagname)){	echo ($pagname); } ?>" />
<meta property="busca:species" content="<?php if(isset($pagname)){	echo ($pagname); } ?>" />
<meta name="format-detection" content="address=no; email=no; telephone=no" />

<base href="<?= $lin11['tag_url'] ?>"/>

<title><?= $namestop ?> | <?= $titulo ?></title>

<link rel="canonical" href="<?= $lin11['tag_url'] ?>" />
<link rel="icon" href="assets/_img/favicon.png" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" media="screen" href="assets/_css/styles.css" />
<link rel="stylesheet" type="text/css" media="screen" href="assets/_css/estilo.css" />
<link rel="stylesheet" type="text/css" media="screen" href="assets/_css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="assets/_css/estilo-fontes.css" />
<link rel="stylesheet" type="text/css" media="screen" href="assets/_css/responsive.css" />
<link rel="stylesheet" type="text/css" media="screen" href="assets/nav/css/style.css" />
<link rel="stylesheet" type="text/css" media="screen" href="assets/pages/css/jPages.css">
<link rel="stylesheet" type="text/css" media="screen" href="assets/for/css.css">
<link rel="stylesheet" type="text/css" media="screen" href="assets/select/jquery-ui.css">
<link rel="stylesheet" type="text/css" media="screen" href="assets/filestyle/jquery-filestyle.css">
<link rel="stylesheet" type="text/css" media="screen" href="assets/filestyle/jquery-filestyle.min.css">

<link rel="stylesheet" href="assets/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="assets/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="assets/owl-carousel/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="assets/slick/slick.css" />
<link rel="stylesheet" type="text/css" href="assets/slick/slick-theme.css" />
<link rel="stylesheet" href="assets/font-awesome/all.css">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
<link href="assets/lightbox/ekko-lightbox.css" rel="stylesheet">
<link rel="stylesheet" href="assets/flexslider/flexslider.css"> 
<link rel="stylesheet" href="assets/fancybox/jquery.fancybox.min.css">
<script type="application/javascript" src="assets/jquery/jquery-3.4.0.min.js"></script>
<script src="https://kit.fontawesome.com/967572049d.js"></script>
	
<!-- Global site tag (gtag.js) - Google Analytics -->
<script type="application/javascript" async src="https://www.googletagmanager.com/gtag/js?id=UA-147164363-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-147164363-1');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 701640832 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-701640832"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-701640832');
</script>    
    
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K9LPBG2');</script>
<!-- End Google Tag Manager -->
    
    
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- Inicio Compatibilidade HTML5 para IE -->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<![endif]-->


<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->

	<style>
	</style>
	
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K9LPBG2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    
<?php require_once("topo.php"); ?>


<div class="boxfull cnt">
	<div class="grdGRL-2 grdDSK-3 grdTBLp-3 grdTBLr-12">
	<?php 
		require_once("menu-lateral.php");
		require_once("start-main.php");
	?>
	</div>	

	<div class="grdGRL-10 grdDSK-9 grdTBLp-9 grdTBLr-12 abpg">
		<?php  require_once($pagina); ?>
	</div>
</div>


<?php 	
require_once("footer.php"); 
require_once("painel/destruidor.php"); 
?>

<script type="application/javascript" src="assets/nav/js/jquery-1.10.2.min.js"></script>
<script type="application/javascript" src="assets/nav/js/jquery.easing.1.3.js"></script>
<script type="application/javascript" src="assets/nav/js/jquery.slicknav.js"></script>
	
<script type="application/javascript" src="assets/facebook/facebook.js"></script>
<script type="application/javascript" src="assets/popper/popper.min.js"></script>
<script type="application/javascript" src="assets/bootstrap/bootstrap.min.js"></script>
<script type="application/javascript" src="assets/owl-carousel/owl.carousel.min.js"></script>
<script type="application/javascript" src="assets/owl-carousel/owl-carousel.js"></script>
<script type="application/javascript" src="assets/owl-carousel/funcoes.js"></script>
<script type="application/javascript" src="assets/_js/scripts.js"></script>
	
<script type="text/javascript" src="assets/slick/slick.js"></script>
<script type="text/javascript" src="assets/slick/scripts.js"></script>

<script type="application/javascript" src="assets/flexslider/funcoes.js"></script>
<script type="application/javascript" src="assets/flexslider/jquery.flexslider.js"></script>
<script type="application/javascript" src="assets/flexslider/jquery.flexslider-min.js"></script>
	
<!--<script type="text/javascript" src="assets/for/jquery-2.1.4.min.js"></script>-->
<script type="text/javascript" src="assets/for/jquery.validationEngine.js"></script>
<script type="text/javascript" src="assets/for/jquery.validationEngine-pt.js"></script>

<script type="text/javascript" src="assets/filestyle/jquery-filestyle.js"></script>
<script type="text/javascript" src="assets/filestyle/jquery-filestyle.min.js"></script>


<script type="application/javascript" src="assets/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="assets/pages/js/highlight.pack.js"></script>
<script type="text/javascript" src="assets/pages/js/tabifier.js"></script>
<script type="text/javascript" src="assets/pages/js/js.js"></script>
<script type="text/javascript" src="assets/pages/js/jPages.js"></script>
<script type="text/javascript">
  $(function() {
	$("div.holder").jPages({
		containerID: "itemContainer"
	});
  });
</script>

<!--<script type="text/javascript" src="assets/select/jquery-1.11.1.min.js"></script>-->
<script type="text/javascript" src="assets/select/jquery-ui.min.js"></script>
<script type="text/javascript" src="assets/select/jquery.select-to-autocomplete.js"></script>
<script>
  (function($){
	$(function(){
	  $('.search-professional').selectToAutocomplete();
	  $('.bus-area-select').selectToAutocomplete();
//	  $('form').submit(function(){
//		alert( $(this).serialize() );
//		return false;
//	  });
	});
  })(jQuery);
</script>

    
<!-- Facebook Pixel Code -->
<script type="text/javascript">
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
     fbq('init', '742815162918006'); 
    fbq('track', 'PageView');
</script>
    
<noscript>
 <img height="1" width="1" src="https://www.facebook.com/tr?id=742815162918006&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
    
    
</body>
</html>