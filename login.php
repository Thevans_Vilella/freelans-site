<main id="register-customer">
    <div class="d-flex flex-wrap container-fluid no-padding-left-right">
        <div id="register-background" style="background-image: linear-gradient(rgba(1,70,116,0.2), rgba(1,70,116,0.2)), url(assets/_img/helpers/img-professional-profile.png);" 
             class="col-md-6 d-none d-md-block no-padding-left-right">
            <a class="link-logo" href=""><img class="logo register-logo" src="assets/_img/icons/freelans-logo.png" alt="Freelans"></a>
        </div>
        <div class="col-md-6 col-sm-12 no-padding-left-right">

            <div class="return-container col-12">
                <a href="" class="return-button">
                    <img class="return-image" src="assets/_img/icons/chevron-left.svg" alt="Voltar"> Voltar
                </a>
            </div>
            <div class="half-container">
                <h1 class="register-title">Acessar minha conta</h1>
                <form name="cadastro" method="post" action="log.php">
                    <div class="form-group">
                        <label class="register-label" for="email">E-mail</label>
						<input type="email" class="form-control register-input" id="email" name="email" placeholder="Ex: nome@email.com.br" required>
                    </div>
                    <div class="form-group">
                        <label class="register-label" for="password">Senha</label>
                        <input type="password" class="form-control register-input" id="password" name="password" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" required>
                    </div>
					
				
					<div class="captc">
						<div class="g-recaptcha" data-sitekey="6Leb4dMUAAAAAJlbSrE64_bJo93QxpAvOLkbQiUG"></div>
					</div>
				
					
					<div class="lbot">
						<div class="form-group button-register-container">
							<input type="submit" class="register-button" value="Acessar Conta" />
						</div>
                    </div>
                    <div class="esqueci"><a href="reset">Esqueci minha senha</a></div>
                </form>

            </div>

        </div>
    </div>
</main>
