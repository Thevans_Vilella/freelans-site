<?php

$perflog	=	CorrigirUseres(anti_injection($_GET['CodUser']));
$perfdate	=	CorrigirUseres(anti_injection($_GET['CodItem']));
$perfhora	=	CorrigirUseres(anti_injection($_GET['CodConfirm']));
$perfip		=	CorrigirUseres(anti_injection($_GET['CodTime']));

//===============================================================================================================
$sql88		=	"SELECT * FROM perfil WHERE perf_login = '$perflog' AND perf_key1 = '$perfdate' AND perf_key2 = '$perfhora' AND perf_key3 = '$perfip'";
$res88		=	mysqli_query($cn, $sql88);
$cnt88		=	mysqli_num_rows($res88);
$lin88		=	mysqli_fetch_array($res88);

$perfcod	=	$lin88['perf_codigo'];
?>
<main id="register-customer">
    <div class="d-flex flex-wrap container-fluid no-padding-left-right">
        <div id="register-background" style="background-image: linear-gradient(rgba(1,70,116,0.2), rgba(1,70,116,0.2)), url(assets/_img/helpers/img-professional-profile.png);" 
             class="col-md-6 d-none d-md-block no-padding-left-right">
            <a class="link-logo" href=""><img class="logo register-logo" src="assets/_img/icons/freelans-logo.png" alt="Freelans"></a>
        </div>
        <div class="col-md-6 col-sm-12 no-padding-left-right">

            <div class="return-container col-12">
                <a href="login" class="return-button">
                    <img class="return-image" src="assets/_img/icons/chevron-left.svg" alt="Voltar"> Voltar
                </a>
            </div>
            <div class="half-container">
                <h1 class="register-title">Alterar minha senha</h1>
				
				<?php if ( $cnt88 == "1" )  { ?>
                <form name="cadastro" method="post" action="recuperando.php" onSubmit="return validarSenha()">
                    <div class="form-group">
                        <label class="register-label" for="password">Senha</label>
                        <input type="password" class="form-control register-input" id="password" name="password" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" required>
                    </div>
                    <div class="form-group">
                        <label class="register-label" for="confirm-password">Confirme sua senha</label>
                        <input type="password" class="form-control register-input" id="passwordconf" name="passwordconf" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" required>
                    </div>
					<div class="lbot">
						<div class="form-group button-register-container">
							<input type="hidden" name="mttp" size="5" value="<?php echo $perfcod; ?>" />
							<input type="hidden" name="mttt" size="5" value="<?php echo $perfdate; ?>" />
							<input type="hidden" name="mttc" size="5" value="<?php echo $perfhora; ?>" />
							<input type="hidden" name="mttm" size="5" value="<?php echo $perfip; ?>" />
							<input type="submit" class="register-button" value="Gerar nova senha" />
						</div>
                    </div>
                </form>
				<?php } else { ?>
				
					<div class="register-title">Dados incorretos, favor solicitar novo link para redefinição de senha.<br/><br/></div>
					<div class="esqueci"><a href="reset">Esqueci minha senha</a></div>
				
				<?php } ?>
            </div>

        </div>
    </div>
</main>
