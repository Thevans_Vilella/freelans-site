FROM php:7.4-apache
RUN a2enmod rewrite
RUN docker-php-ext-install mysqli

RUN apt-get update -y && apt-get install -y libpng-dev libjpeg-dev zlib1g-dev libzip-dev libfreetype6-dev

RUN docker-php-ext-install zip
RUN docker-php-ext-install gd

RUN docker-php-ext-configure gd --with-jpeg --with-freetype \
    && docker-php-ext-install -j$(nproc) gd

RUN chown www-data:www-data /var/www/html