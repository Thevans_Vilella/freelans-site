<?php 

if(!isset($_GET['CodigAbriR'])) { require_once("alerta.php"); }


$item		=	$_GET['CodigAbriR'];

if(isset($_SESSION["cod_ses"])) {
	
	$clitem		=	$_SESSION["cod_ses"];
	
	$sqlaval	=	"select * from avaliacoes_usuario WHERE perf_codigo = '$clitem' AND perf_cod_ref = '$item'";
	$resaval	=	mysqli_query($cn, $sqlaval);
	$linaval	=	mysqli_fetch_array($resaval);	

	$avaldes	=	$linaval['ava_user_descricao'];
	
}
	

$sqluser	=	"select * from perfil
				INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
				INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
				WHERE perf_codigo = $item";
$resuser	=	mysqli_query($cn, $sqluser);
$linuser	=	mysqli_fetch_array($resuser);	

$perfcod	=	$linuser['perf_codigo'];
$perfname	=	$linuser['perf_nome'];
$catcod		=	$linuser['cat_codigo'];
$perfcat	=	$linuser['cat_nome'];

$perfnames	=	CorrigirNome($perfname);
$avalia		=	"perfil/".$perfcod."/".$perfnames;



$sqldsec	=	"select * from perfil_sobre WHERE perf_codigo = '$item'";
$resdsec	=	mysqli_query($cn, $sqldsec);
$lindsec	=	mysqli_fetch_array($resdsec);	


$sqlavaluser	=	"select * from avaliacoes WHERE perf_codigo = '$item'";
$resavaluser	=	mysqli_query($cn, $sqlavaluser);
$linavaluser	=	mysqli_fetch_array($resavaluser);	

$cntavuser		=	$linavaluser['ava_media_estrelas'];

if($cntavuser > 0){ 
	
		$cntavuser	=	$cntavuser; 
	
	} else {
	
		$cntavuser	=	""; 
}

?>
<div id="ex10" class="modal">
	<div class="modform">
		
		<div class="closefor"><a href="#" rel="modal:close"><i class="fas fa-times-circle closeforx"></i></a></div>

				<div class="grdGRL-12">
					<div class="bloco-perfil">
						<div class="bl-atua-bloco">
							<div class="bl-atua-tit">AVALIAR PRESTADOR</div>
							<div class="bl-perf-avalia"></div>
							<div class="bl-atua-linha"></div>
							
								<div class="grdGRL-6  grdDSK-12">
									<div class="grdGRL-12">
										<div class="bl-perf-avalia-tit">ATENDIMENTO</div>
										<div class="boxit-estrela-avaliacoes">
											<form name="avaliacoes" class="avaliacao" method="post" enctype="multipart/form-data"  action="avalia.php" onSubmit="return validarAvaliacao()">
												<div class="estrelas">
													<input type="radio" id="radio" name="atendimento" value="" checked>

													<label for="estrela_um"><i class="fa estpx"></i></label>
													<input type="radio" id="estrela_um" name="atendimento" value="1">

													<label for="estrela_dois"><i class="fa estpx"></i></label>
													<input type="radio" id="estrela_dois" name="atendimento" value="2">

													<label for="estrela_tres"><i class="fa estpx"></i></label>
													<input type="radio" id="estrela_tres" name="atendimento" value="3">

													<label for="estrela_quatro"><i class="fa estpx"></i></label>
													<input type="radio" id="estrela_quatro" name="atendimento" value="4">

													<label for="estrela_cinco"><i class="fa estpx"></i></label>
													<input type="radio" id="estrela_cinco" name="atendimento" value="5">
												</div>
										</div>
									</div>

								</div>
							
									<div class="grdGRL-12">
										<div class="dad">
											<div class="bl-atua-tit">Escreva seu comentário</div>
											<textarea name="descricao" cols="1" rows="10"><?php echo $avaldes; ?></textarea>
										</div>

										<input type="hidden" name="dir" value="<?php echo $avalia; ?>" />
										<input type="hidden" name="item" value="<?php echo $item; ?>" />
										<input type="hidden" name="clitem" value="<?php echo $clitem; ?>" />
										<input type="submit" class="bl-avalia-user-avalia" value="Avaliar Prestador" />
										<!-- <div class="bl-avalia-user-avalia"><a href="#">Alterar Avaliação</a></div>-->
									</div>
								</form>
						</div>
					</div>
				</div>

	</div>
</div>