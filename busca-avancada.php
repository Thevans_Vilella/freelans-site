		<div class="bl-filtro">
            <form name="buscaavancada" id="buscaavancada" action="busca" method="post" onSubmit="return validarBuscaAvancada()">

				<div class="dados-cliente">
					<div class="abre">
						<div class="opcao-dados"> 
							<div class="buscprof"><img src="assets/_img/icons/search-icon.png" alt=""/> Buscar profissional</div>
						</div>
					</div>
					
					
					
					<div class="grdGRL-12 acorde">
						<div class="grdGRL-6 grdSMPr-12">
							<div class="busca-bl">
								<div class="bus-tit">Selecione a cidade:</div>
								<select name="city" class="bus-select" required>
									<option value="">Selecione uma Cidade</option>
									<option value="">----------------------------</option>
									<?php
									$sql		=	"select * from cidades
													INNER JOIN estado	  	on		estado.est_codigo		=	cidades.est_codigo
													ORDER BY est_nome, cid_nome";
									$res		=	mysqli_query($cn, $sql);
									while($lin	=	mysqli_fetch_array($res))  {
									?>
									<option value="<?php echo $lin['cid_codigo']; ?>"><?php echo $lin['cid_nome']; ?>/ <?php echo $lin['est_sigla']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						
						<div class="grdGRL-6 grdSMPr-12">
							<div class="busca-bl">
								<div class="bus-tit">Selecione o tipo de serviço:</div>
								<select name="cat" class="bus-area-select" placeholder="Qual o tipo de profissional que você procura?" id="country-selector" autofocus="autofocus" autocorrect="off" autocomplete="off" required>
									<option value="" selected="selected">Qual o tipo de profissional que você procura?</option>
									<?php
									$sql01			=	"select * from atuacao ORDER BY atua_nome";
									$res01			=	mysqli_query($cn, $sql01);
									while($lin01	=	mysqli_fetch_array($res01))  {
									?>
									<option value="<?php echo $lin01['atua_codigo']; ?>"><?php echo $lin01['atua_nome']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						
						
						<div class="grdGRL-12">
							<div class="busca-bl">
								<div class="bus-tit">Escolha os diferenciais:</div>
									<?php 
									$sql02			=	"select * from destaques ORDER BY dest_ordem";
									$res02			=	mysqli_query($cn, $sql02);
									while($lin02	=	mysqli_fetch_array($res02))  {
									?>
									<div class="buscdifere">
										<div class="buscdifcheck"><input type="checkbox" name="dest[]" id="dest[]" value="<?php echo $lin02['dest_codigo']; ?>"></div>
										<?php echo $lin02['dest_nome']; ?> 
									</div>
									<?php } ?>
							</div>
						</div>
						

						<input type="hidden" name="formbusca"/>
						<input type="submit" class="buscdados" value="Buscar Profissional" />
					</div><!-- Acorde -->
				</div>
				
			</form>			
		</div>
