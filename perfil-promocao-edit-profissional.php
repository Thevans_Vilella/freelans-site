<?php  if(isset($_SESSION["login_ses"]) and ($_SESSION["status_ses"] == "1") and ($_SESSION["tipo_ses"] == "2")) { ?>
<?php 

	$user	=	$_SESSION["login_ses"];

	$sqluser	=	"select * from perfil
					INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
					INNER JOIN planos	  		on		planos.pla_codigo			=	perfil.pla_codigo
					where perf_login = '$user'";
	$resuser	=	mysqli_query($cn, $sqluser);
	$linuser	=	mysqli_fetch_array($resuser);	

	$perfcod	=	$linuser['perf_codigo'];
	$catcod		=	$linuser['cat_codigo'];
	$perfcat	=	$linuser['cat_nome'];
	$qtdarea	=	$linuser['pla_qtd_atuacao'];
	$qtdfotos	=	$linuser['pla_qtd_fotos'];
	$qtdvideo	=	$linuser['pla_qtd_video'];
	$qtdpromo	=	$linuser['pla_qtd_promo'];
	
	
	$sqlcat_verif_perf		=	"SELECT * FROM promocao WHERE perf_codigo = '$perfcod'";
	$rescat_verif_perf		=	mysqli_query($cn, $sqlcat_verif_perf);
	$contacat_verif_perf	=	mysqli_num_rows($rescat_verif_perf);
	$lincat_verif_perf		=	mysqli_fetch_array($rescat_verif_perf);

?>

<div class="boxfull">
	<div class="titfxtop">Minhas Promoções: <span class="titfxtop-sub"><?php echo $linuser['perf_nome']; ?></span></div>

	
		<div class="boxfull cinza">
			<div class="blg perf-bloco">

				<?php if ( $contacat_verif_perf < $qtdpromo ) { ?>
				<div class="grdGRL-4 grdDSK-6 grdTBLp-12">
				<div class="bloco-perfil">
						<div class="bl-atua-bloco">
							<div class="bl-atua-tit">CADASTRAR PROMOÇÃO</div>
							<div class="bl-atua-linha"></div>

							<form class="cadastro" id="cadastro1" method="post" enctype="multipart/form-data"  action="perfil-edit-promocao-insere.php">
								<ul>
									<li>
										<div class="dad">
											<label>Selecione um serviço para colocar na promoção:</label>
											<select name="service" class="select validate[required]" id="service">
												<option value="">Selecione um serviço</option>
												<option value="">----------------------------</option>
												<?php
												$sql	=	"select * from atuacao_perfil 
															INNER JOIN atuacao	  	on		atuacao.atua_codigo		=	atuacao_perfil.atua_codigo
															WHERE 
															perf_codigo = $perfcod 
															ORDER BY 
															atua_nome";
												$res	=	mysqli_query($cn, $sql);
												while($lin	=	mysqli_fetch_array($res))  {
												?>
												<option value="<?php echo $lin['atua_codigo']; ?>"><?php echo $lin['atua_nome']; ?></option>
												<?php } ?>
											</select>
										</div>
									</li>
									<li>
										<div class="dad">
											<label>Valor em porcentagem do desconto:</label>
											<input type="text" name="desconto" id="desconto" maxlength="2" placeholder="Desconto %" class="validate[required]"/>
										</div>
									</li>
									<li>
										<div class="dad">
											<label>Descrição: máximo 80 caracteres</label>
											<input type="text" name="descr" id="descr" maxlength="80" class="validate[required]"/>
										</div>
									</li>
									<li>
										<div class="dad">
											<label>Foto: 545 x 380 pixel jpg.</label>
												<div class="dad-fot">
													<input type="file" id="upload" name="arquivo" onChange="validaimagem();" required>
													<label class ="new-button" for="upload">Buscar Foto</label>
												</div>
										</div>
									</li>
								</ul>

								<input type="submit" class="regdados" value="Alterar Dados" />
								<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
							</form>
						</div>
					
				</div>
				</div>
				<?php } ?>
				

						<?php
						$sql01			=	"select * from promocao 
											INNER JOIN perfil	  		on		perfil.perf_codigo			=	promocao.perf_codigo
											INNER JOIN categorias	  	on		categorias.cat_codigo		=	perfil.cat_codigo
											INNER JOIN atuacao	  		on		atuacao.atua_codigo			=	promocao.atua_codigo
											WHERE perfil.perf_codigo = $perfcod 
											ORDER BY 
											pro_desconto";
						$res01			=	mysqli_query($cn, $sql01);
						while($lin01	=	mysqli_fetch_array($res01))  {
						?>
						<div class="grdGRL-4 grdDSK-6 grdSMPr-12">
							<div class="cx-promo blpromo">
								<div class="promo-fot" style="background-image: url(painel/<?php echo $lin01['pro_fotog']; ?>)">
										<form class="cadastro" method="post" action="perfil-promocao-exclui.php">
											<div class="perf-fot-add">
												<input type="submit" class="closexx" value="x" />
											</div>
											<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
											<input type="hidden" name="item" size="5" value="<?php echo $lin01['pro_codigo']; ?>" />
										</form>
									
										<div class="promo-mask"></div>
										<!--<div class="boxit-tipo">PREMIUM</div>-->
										<div class="promo-name"><?php echo $lin01['perf_nome']; ?></div>
										<div class="promo-name-serv"><?php echo $lin01['cat_nome']; ?></div>
										<div class="boxit-estrela-promo">
											<i class="fas fa-star estyellow"></i>
											<i class="fas fa-star estyellow"></i>
											<i class="fas fa-star estyellow"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
										</div>
										<div class="boxit-promo-nota">3,2</div>
										<div class="boxit-promo-love"><i class="fas fa-heart estred"></i></div>
								</div>
								<div class="promo-desc"><?php echo $lin01['pro_desconto']; ?>% <span class="promo-desconto">DESCONTO</span></div>
								<div class="bx-dad-tit-promo">
									<div class="promo-tit-serv"><?php echo $lin01['atua_nome']; ?></div>
									<div class="promo-desc-serv"><?php echo $lin01['pro_descricao']; ?></div>
								</div>
							</div>
						</div>
						<?php } ?>
				
				
				

			</div>
			<div class="vazio"></div>	
		</div>


	
</div>
<?php } else { require_once("alerta.php");}// Termina IF de Login Aqui ============= ?>