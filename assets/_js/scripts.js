/*================================================================================================================================================================*/
$(document).ready(function(){
	$('.abre').click(function(){
		$(this).parent().find('.acorde').slideToggle("slow");
	});
});
/*================================================================================================================================================================*/

var acc = document.getElementsByClassName("accordion");
var i;

    for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight){
        panel.style.maxHeight = null;
        } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
        } 
    });
}

$(document).ready(function() {
    var equalize = function () {
        var disableOnMaxWidth = 0; // 767 for bootstrap

        var grouped = {};
        var elements = $('*[data-same-height]');

        elements.each(function () {
            var el = $(this);
            var id = el.attr('data-same-height');

            if (!grouped[id]) {
                grouped[id] = [];
            }

            grouped[id].push(el);
        });

        $.each(grouped, function (key) {
            var elements = $('*[data-same-height="' + key + '"]');

            elements.css('height', '');

            var winWidth = $(window).width();

            if (winWidth <= disableOnMaxWidth) {
                return;
            }

            var maxHeight = 0;

            elements.each(function () {
                var eleq = $(this);
                maxHeight = Math.max(eleq.height(), maxHeight);
            });

            elements.css('height', maxHeight + "px");
        });
    };

    var timeout = null;

    $(window).resize(function () {
        if (timeout) {
            clearTimeout(timeout);
            timeout = null;
        }

        timeout = setTimeout(equalize, 250);
    });
    equalize();
});


function validarSenha() {
	var cadastro = document.cadastro;

	if (cadastro.password.value != cadastro.passwordconf.value) {
		alert('O campo [ Senha ] e [ Confirme sua Senha ] não são iguais.');
		cadastro.password.focus();
		return false;
	}
}

function validarBusca() {
	var cadastro = document.cadastro;

	if (cadastro.password.value != cadastro.passwordconf.value) {
		alert('O campo [ Senha ] e [ Confirme sua Senha ] não são iguais.');
		cadastro.password.focus();
		return false;
	}
}

function validarBuscaAvancada() {
	var buscaavancada = document.buscaavancada;

	if (buscaavancada.city.value.length == 0 && buscaavancada.cat.value.length == 0 && buscaavancada.dest.value.length == 0) {
		alert('É necessário selecionar pelo menos uma opção do filtro para pesquisa!');
		buscaavancada.city.focus();
		return false;
	}
}

$(document).ready(function() {
    
	$('#toggle').click(function() {
		$(this).toggleClass('active');
		$('#overlay').toggleClass('open');
	});

});


$('#switch').on("click", function() {

	if($('#switch-button').prop("checked")) {
		$('#switch-label').text("perfil profissional");
	} else {
		$('#switch-label').text("perfil cliente");
	}

});


	$(document).ready(function() {
		$('#carousel-produto').flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			itemWidth: 72,
			itemMargin: 9,
			asNavFor: '#slider-produto',
			prevText: "",
			nextText: "",
		});

		$('div.anchors a.anchor-link').each(function(index) {
			var link = $(this).attr('href');
			$(this).on("click", function() {

				$('html, body').animate({
					scrollTop: $(link).offset().top
				}, 1000);
				return false;
			});

		});


	});

	$(document).mouseup(function(e) {
		var container = $("ul#area-list");

		if(!container.is(e.target) && container.has(e.target).length === 0){
			container.hide();
		}
	});

	var search_pro = $("input#search-professional"); 

	search_pro.on("click", function() {
		$("ul#area-list").css("display", "flex");
	});

	search_pro.on("keyup", function() {
		var value = $(this).val().toLowerCase();
		$("ul#area-list li.list-category a").filter(function() {
			$(this).parent().toggle($(this).text().toLowerCase().indexOf(value) > -1);
		});
	});

	window.onscroll = function() {

		var fixed_header = $("div#fixed_header");
		var mobile_header = $("div#mobile_header");

		if($(window).scrollTop() > fixed_header.height() / 2) {
			mobile_header.css({"display": "flex"});
		} else {
			mobile_header.css({"display": "none"});
		}
	}




/*=======================================================================================================================================================================*/
/**  
 * Função para aplicar máscara em campos de texto
  */
function maskIt(w,e,m,r,a){
        
        // Cancela se o evento for Backspace
        if (!e) var e = window.event
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;
        
        // Variáveis da função
        var txt  = (!r) ? w.value.replace(/[^\d]+/gi,'') : w.value.replace(/[^\d]+/gi,'').reverse();
        var mask = (!r) ? m : m.reverse();
        var pre  = (a ) ? a.pre : "";
        var pos  = (a ) ? a.pos : "";
        var ret  = "";

        if(code == 9 || code == 8 || txt.length == mask.replace(/[^#]+/g,'').length) return false;

        // Loop na máscara para aplicar os caracteres
        for(var x=0,y=0, z=mask.length;x<z && y<txt.length;){
                if(mask.charAt(x)!='#'){
                        ret += mask.charAt(x); x++;
                } else{
                        ret += txt.charAt(y); y++; x++;
                }
        }
        
        // Retorno da função
        ret = (!r) ? ret : ret.reverse()        
        w.value = pre+ret+pos;
}

// Novo método para o objeto 'String'
String.prototype.reverse = function(){
        return this.split('').reverse().join('');
};


/*============= Função Para Validar Extensão de Foto ===================================================================================================================*/
function validaimagem() {
	var extensoesOk = ",.jpg,.jpeg,";
	var extensao	= "," + document.form.arquivo.value.substr( document.form.arquivo.value.length - 4 ).toLowerCase() + ",";
	var tipoimg	    = "A Imagem Selecionada tem o Formato " + "[" + document.form.arquivo.value.substr( document.form.arquivo.value.length - 4 ).toUpperCase() + "]";
		
		if (document.form.arquivo.value == "") {
			alert("Favor Inserir Uma Imagem!!!")
		  }
		  
		  
		 else if( extensoesOk.indexOf( extensao ) == -1 ) {
		    alert( "O Formato da Imagem Não é Compatível!!!\n" + tipoimg + "\n\nFavor Inserir uma Imagem no Formato JPG!!!" );
			document.getElementById("arquivo").value = "";
		   }

   }

/*================================================================================================================================================================*/

(function($) {
    
    $.fn.filestyle = function(options) {
                
        /* TODO: This should not override CSS. */
        var settings = {
            right : 0
        };
                
        if(options) {
            $.extend(settings, options);
        };
                        
        return this.each(function() {
            
            var self = this;
            var wrapper = $("<div>")
                            .css({
                                "width": settings.imagewidth + "px",
                                "height": settings.imageheight + "px",
                                "margin-top": settings.imagetop + "px",
                                "margin-left": settings.imageleft + "px",
                                "background": "url(" + settings.image + ") 0 0 no-repeat",
                                "background-position": "right",
                                "display": "inline",
                                "position": "absolute",
                                "overflow": "hidden"
                            });
                            
            var filename = $('<input class="file">')
                             .addClass($(self).attr("class"))
                             .css({
                                 "display": "inline",
                                 "width": settings.width + "px"
                             });

            $(self).before(filename);
            $(self).wrap(wrapper);

            $(self).css({
                        "position": "relative",
                        "height": settings.imageheight + "px",
                        "width": settings.width + "px",
                        "display": "inline",
                        "cursor": "pointer",
                        "opacity": "0.0"
                    });

            if ($.browser.mozilla) {
                if (/Win/.test(navigator.platform)) {
                    $(self).css("margin-left", "-142px");                    
                } else {
                    $(self).css("margin-left", "-168px");                    
                };
            } else {
                $(self).css("margin-left", settings.imagewidth - settings.width + "px");                
            };

            $(self).bind("change", function() {
                filename.val($(self).val());
            });
      
        });
        

    };
    
})(jQuery);




$(document).ready(function(){
    $("input[type=file]").filestyle({ 
        image: "assets/_img/bt_upload.png",
        imagetop : -5,
        imageleft : -10,
        imageheight : 0,
        imagewidth : 0,
    });
});

/*================================================================================================================================================================*/

function validarAvaliacao() {
	var avaliacoes = document.avaliacoes;

	if (avaliacoes.atendimento.value == 0) {
		alert('Selecione pelo menos [ 1 estrela ] em atendimento!');
		return false;
	}

	if (avaliacoes.qualidade.value == 0) {
		alert('Selecione pelo menos [ 1 estrela ] em qualidade!');
		return false;
	}

	if (avaliacoes.pontualidade.value == 0) {
		alert('Selecione pelo menos [ 1 estrela ] em pontualidade!');
		return false;
	}

	if (avaliacoes.preco.value == 0) {
		alert('Selecione pelo menos [ 1 estrela ] em preço!');
		return false;
	}

	if (avaliacoes.agilidade.value == 0) {
		alert('Selecione pelo menos [ 1 estrela ] em agilidade!');
		return false;
	}
	if (avaliacoes.descricao.value == 0) {
		alert('É necessário  [ escrever seu comentário ] para poder votar!');
		return false;
	}
}

//
/*================================================================================================================================================================*/

   var janela = new ModalShow('id_que_voce_quiser', false);
  /*
  parâmetros: 
  1 - "id_que_voce_quiser" : É o id da estrutura HTML
  2 - "false" : É o conteúdo interno, mesmo que feche a janela o conteúdo não será apagado
  */

  janela.open('O título que você quiser', true);
  /*
  parâmetros: 
  1 - "O título que você quiser" : Título da janela
  2 - "true" : Quer dize que o botão de fechar deve ser exibido
  */
  
/*================================================================================================================================================================*/


/*================================================================================================================================================================*/
