<?php
$sql1		=	"SELECT * from textos WHERE txt_codigo = '1'";
$res1		=	mysqli_query($cn, $sql1);
$lin1		=	mysqli_fetch_array($res1);

$sql2		=	"SELECT * from textos WHERE txt_codigo = '2'";
$res2		=	mysqli_query($cn, $sql2);
$lin2		=	mysqli_fetch_array($res2);

$sql3		=	"SELECT * from textos WHERE txt_codigo = '3'";
$res3		=	mysqli_query($cn, $sql3);
$lin3		=	mysqli_fetch_array($res3);

$sql4		=	"SELECT * from textos WHERE txt_codigo = '4'";
$res4		=	mysqli_query($cn, $sql4);
$lin4		=	mysqli_fetch_array($res4);

$sql5		=	"SELECT * from textos WHERE txt_codigo = '5'";
$res5		=	mysqli_query($cn, $sql5);
$lin5		=	mysqli_fetch_array($res5);
?>

<div id="mobile_header" class="header container-fluid">
<div class="col-12 col-md-7 no-padding-left-right anchors">
	  <nav role="navigation">
		<div id="menuToggle">
		  <input type="checkbox" />
			<span></span>
			<span></span>
			<span></span>
				<ul id="menu">
				  <li><a href="#customer_profile">Cadastro Cliente</a></li>
				  <li><a href="#professional_profile">Cadastro Profissional</a></li>
				  <li><a href="blog/">Blog</a></li>

				  <?php 
				  if(isset($_SESSION["login_ses"])) {

				  $seperfm		=	$_SESSION["login_ses"];

				  $sqldestm	=	"select * from perfil WHERE perf_login = '$seperfm'";
				  $resdestm	=	mysqli_query($cn, $sqldestm);
				  $lindestm	=	mysqli_fetch_array($resdestm);
				  ?>
				  <li><a href="perfil/<?php echo $lindestm['perf_codigo']; ?>/<?php echo CorrigirNome($lindestm['perf_nome']); ?>">Meu Perfil</a></li>
				  <?php } else { ?>
				  <li><a href="login">Login</a></li>
				  <?php } ?>
				</ul>
	   </div>
	  </nav>
</div>	
</div>
<header id="home">
    <div id="fixed_header" class="header container-fluid">
        <div class="container-logo col-12 col-md-5 no-padding-left-right">
            <a class="link-logo" href="">
                <img class="logo" src="assets/_img/icons/freelans-logo.png" alt="Freelans">
            </a>
        </div>
        <div class="anchors col-12 col-md-7 no-padding-left-right">
            <a class="anchor-link" href="#how_works">Como funciona</a>
            <a class="anchor-link" href="#customer_profile">Cadastro Cliente</a>
            <a class="anchor-link" href="#professional_profile">Cadastro Profissional</a>
            <a class="anchor-link" href="blog/">Blog</a>
			
			<?php 
			if(isset($_SESSION["login_ses"])) {
				
			$seperf		=	$_SESSION["login_ses"];
				
			$sqldest	=	"select * from perfil WHERE perf_login = '$seperf'";
			$resdest	=	mysqli_query($cn, $sqldest);
			$lindest	=	mysqli_fetch_array($resdest);
			?>
			<a class="anchor-link" href="perfil/<?php echo $lindest['perf_codigo']; ?>/<?php echo CorrigirNome($lindest['perf_nome']); ?>">Meu Perfil</a>
			<?php } else { ?>
			<a class="anchor-link" href="login">Login</a>
			<?php } ?>
        </div>
    </div>
    <div class="container no-padding-left-right">
        <div class="row no-margin-left-right">
            <h1 id="solution"><?php echo $lin1['txt_descricao']; ?></h1>
            <div class="col-12 no-padding-left-right">
				<div class="bl-filtro-home">
					<?php require_once("busca-avancada.php"); ?>
				</div>
			</div>
        </div>
        <div class="row no-margin-left-right">
            <h2 id="benefit">Benefícios</h2>
            <div class="col-xl-3 col-md-3 col-sm-6 col-12">
                <h3 class="benefit-title">Comodidade</h3>
                <p class="benefit-text"><?php echo $lin2['txt_descricao']; ?></p>
            </div>
            <div class="col-xl-3 col-md-3 col-sm-6 col-12">
                <h3 class="benefit-title">Contrate sem complicação</h3>
                <p class="benefit-text"><?php echo $lin3['txt_descricao']; ?></p>
            </div>
            <div class="col-xl-3 col-md-3 col-sm-6 col-12">
                <h3 class="benefit-title">Avalie e Recomende</h3>
                <p class="benefit-text"><?php echo $lin4['txt_descricao']; ?></p>
            </div>
            <div class="col-xl-3 col-md-3 col-sm-6 col-12">
                <h3 class="benefit-title">Filtros de localização</h3>
                <p class="benefit-text"><?php echo $lin5['txt_descricao']; ?></p>
            </div>
        </div>
    </div>

    <?php require_once("contact-shortcut.php"); ?>

</header>
