<?php
session_start();
if(isset($_SESSION["login_ses"]) and ($_SESSION["status_ses"] == "1") and ($_SESSION["tipo_ses"] == "1")) { 
	
require_once("painel/conexao.php");
require_once("painel/classes.php");

$perfcod	=	(int)$_POST['user'];
$username	=	ConverteItem(anti_injection($_POST['username']));
$fone		=	CorrigirTelefone(anti_injection($_POST['fone']));
$celular	=	CorrigirTelefone(anti_injection($_POST['celular']));
$email		=	ConverteEmail(anti_injection($_POST['email']));
$usuario	=	md5($email);
//===================================================================================================================================================
// Corrigindo o nome da Imagem
date_default_timezone_set('America/Sao_Paulo');
$tempo_image = md5(date("Ymd-His"));
$time_image = substr($tempo_image, 0, 5);

$namefoto 	= 	trim(CorrigirNome($username)."-".$time_image);
/*==================================================================================================================================================*/ 
//============================================================================================================
if ( $username == "" or $email == "" or $fone == "" or $celular == "" )  { 

	echo "<script type='text/javascript'>alert('Por Favor, Preencha todos os Campos!', 'perfil-edit-cliente');</script>";
	include "painel/destruidor.php";
	exit();

} 
//================ Verificando se já Existe Nome Cadastrado ==================================================
$res_verifica = mysqli_query($cn, "SELECT * FROM perfil WHERE perf_nome = '$username' and perf_codigo != '$perfcod'");
$conta_verifica = mysqli_num_rows($res_verifica);
if ( $conta_verifica <> 0 ) {
	?>

	<?php	
	echo "<script>alert('Nome já Existente!');</script>";
	echo "<script>window.location.href='perfil-edit-cliente';</script>";
	include "painel/destruidor.php";
	exit();
}
//============================================================================================================
//================ Verificando se já Existe Email Cadastrada =================================================
$verifica = mysqli_query($cn, "SELECT * FROM perfil WHERE perf_email = '$email' and perf_codigo != '$perfcod'");
$conta_very = mysqli_num_rows($verifica);
if ( $conta_very <> 0 ) {
	?>

	<?php	
	echo "<script>alert('Email já Existente!');</script>";
	echo "<script>window.location.href='perfil-edit-cliente';</script>";
	include "painel/destruidor.php";
	exit();
}
//=========================================================================================================================================================
//================ Daqui pra baixo INSERE FOTOS NO FTP (Arquivo) ==========================================================================================

if ($_FILES['arquivo']['size'] > 0) {   // Verificando Se Foi Colocado Foto para Download ---------------

				include "verificar_extensao2.php"; 

				// Seleciono no banco o caminho que devo excluir as fotos do FTP.
				$sql_verif	=	"select * from perfil where perf_codigo = '$perfcod'";
				$res_verif	=	mysqli_query($cn, $sql_verif);
				$lin_verif	=	mysqli_fetch_array($res_verif);
			
				unlink("painel/".$lin_verif['perf_fotop']); // Entrar na Pasta do FTP e Excluir Foto Antiga
				unlink("painel/".$lin_verif['perf_fotog']); // Entrar na Pasta do FTP e Excluir Foto Antiga



				
				// AGORA DEVO INSERIR FOTOS NOVAS NO FTP ---------------------
				$arq   = $_FILES['arquivo'];
				
				
				
				$arq2    = $arq['tmp_name'];
				$nome    = $arq['name'];
				$tipo    = $arq['type'];
				$tamanho = $arq['size'];
				
				
				$ptr_arq = fopen($arq2, "r");
				$lido    = fread($ptr_arq, filesize($arq2));
				$foto    = addslashes($lido);
				fclose($ptr_arq);
				
				/********SCRIPT PARA REDIMENSIONAMENTO DA IMAGEM*********/
				/********CRIANDO A IMAGEM GRANDE REDIMENSIONADA**********/
				
				// dados sobre a imagem fonte
				$img_fonte    = imagecreatefromjpeg("$arq2"); //abre a imagem (do disco para a memória))
				$largura      = imagesx($img_fonte);            //pega a largura
				$altura       = imagesy($img_fonte);            //pega a altura
				
				// dados para a nova imagem - FOTO GRANDE
				$nova_largura = 460;                            //define nova largura
				$nova_altura  = $altura*$nova_largura/$largura; //define nova altura
				$img_origem   = $img_fonte;
				$img_destino  = imagecreatetruecolor($nova_largura,$nova_altura) or die("Cannot Initialize new GD image stream");
				
				// redimensiona
				imagecopyresampled($img_destino,$img_origem,0,0,0,0,$nova_largura,$nova_altura,$largura,$altura); 
				
				// mostra a imagem 
				//header("content-type: image/jpeg");
				//imagejpeg($img_fonte);
				
				$nomeimagem = $namefoto; //gera nome do arquivo com data e hora
				
				//Verifica se o diretório existe
				if(!file_exists("painel/fotos/perfil/grandes")){
				 mkdir("painel/fotos/perfil/grandes");
				}
				
				$end_foto = "painel/fotos/perfil/grandes/".$nomeimagem.".jpg";
				imagejpeg($img_destino,$end_foto,80);
				
				chmod("$end_foto", 0755);
				
				// libera a memória 
				imagedestroy($img_fonte);
				imagedestroy($img_destino);
				
				/********CRIANDO A IMAGEM THUMBS REDIMENSIONADA**********/
				$img_fonte    = imagecreatefromjpeg("$arq2"); //abre a imagem (do disco para a memória))
				$largura      = imagesx($img_fonte);            //pega a largura
				$altura       = imagesy($img_fonte);            //pega a altura
				
				// dados para a nova imagem - FOTO PEQUENA
				$nova_largura = 170;                            //define nova largura
				$nova_altura  = $altura*$nova_largura/$largura; //define nova altura
				$img_origem   = $img_fonte;
				$img_destino  = imagecreatetruecolor($nova_largura,$nova_altura) or die("Cannot Initialize new GD image stream");
				
				// redimensiona
				imagecopyresampled($img_destino,$img_origem,0,0,0,0,$nova_largura,$nova_altura,$largura,$altura); 
				
				// mostra a imagem 
				//header("content-type: image/jpeg");
				
				//imagejpeg($img_fonte);
				
				if(!file_exists("painel/fotos/perfil/pequenas")){
				 mkdir("painel/fotos/perfil/pequenas");
				}
				
				$end_thumbs = "painel/fotos/perfil/pequenas/".$nomeimagem.".jpg";
				imagejpeg($img_destino,$end_thumbs,80);
				
				chmod("$end_thumbs", 0755);
				
				// libera a memória 
				imagedestroy($img_fonte);
				imagedestroy($img_destino);


	
				$end_thumbs		=	CorrigirCaminhoPerfil($end_thumbs);
				$end_foto		=	CorrigirCaminhoPerfil($end_foto);




} else { // Não trouxe foto nenhuma

	// Seleciono no banco o caminho que devo excluir as fotos do FTP.
	$sql_verif	=	"select * from perfil where perf_codigo = '$perfcod'";
	$res_verif	=	mysqli_query($cn, $sql_verif);
	$lin_verif	=	mysqli_fetch_array($res_verif);
	
	
$end_thumbs		=	$lin_verif['perf_fotop'];
$end_foto		=	$lin_verif['perf_fotog'];

} 
//=========================================================================================================================================================
//=========================================================================================================================================================
	
$sql	=	"UPDATE perfil SET

perf_nome				= 	'$username',
perf_telefone			= 	'$fone',
perf_celular			= 	'$celular',
perf_email				= 	'$email',
perf_login				= 	'$usuario',
perf_fotop				= 	'$end_thumbs',
perf_fotog				= 	'$end_foto'

WHERE perf_codigo 		= '$perfcod'";

mysqli_query($cn, $sql);

	
//=========================================================================================================================================================
//=========================================================================================================================================================

echo "<script>window.location.href='sair';</script>";
include "painel/destruidor.php";

?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>