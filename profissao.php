<div class="boxfull">
	<div class="titfxtop">Filtrado por: <span class="titfxtop-sub">Técnico de informática</span></div>

		<div class="boxfull cinza">
			<div class="blg">

				<?php require_once("banner.php"); ?>

				<div class="bl">
					<div class="titfxdest">Profissionais em Destaque</div>
					<div class="destv"><a href="destaque">Visualizar todos</a></div>
				</div>


				<div class="bloco bloco-carro">
					<div class="grdGRL-12">
						
						<div class="slider responsive">
							<div class="boxitcar">
								<a href="#">
									<div class="boxit-fot">
										<img src="assets/_img/excluir/1.jpg" alt="Favoritos"/>
										<div class="boxit-tipo">PREMIUM</div>
									</div>
									<div class="boxit-tit">Danilo Fazolo</div>
									<div class="boxit-esp">Especialista de eletrônicos</div>
									<div class="boxit-estrela">
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
									</div>
									<div class="boxit-nota">5,0</div>
								</a>
							</div>
							
							<div class="boxitcar">
								<a href="#">
									<div class="boxit-fot">
										<img src="assets/_img/excluir/2.jpg" alt="Favoritos"/>
										<div class="boxit-tipo">PREMIUM</div>
									</div>
									<div class="boxit-tit">Danilo Fazolo</div>
									<div class="boxit-esp">Especialista de eletrônicos</div>
									<div class="boxit-estrela">
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
									</div>
									<div class="boxit-nota">5,0</div>
								</a>
							</div>
							
							<div class="boxitcar">
								<a href="#">
									<div class="boxit-fot">
										<img src="assets/_img/excluir/3.jpg" alt="Favoritos"/>
										<div class="boxit-tipo">PREMIUM</div>
									</div>
									<div class="boxit-tit">Danilo Fazolo</div>
									<div class="boxit-esp">Especialista de eletrônicos</div>
									<div class="boxit-estrela">
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
									</div>
									<div class="boxit-nota">5,0</div>
								</a>
							</div>
							
							<div class="boxitcar">
								<a href="#">
									<div class="boxit-fot">
										<img src="assets/_img/excluir/4.jpg" alt="Favoritos"/>
										<div class="boxit-tipo">PREMIUM</div>
									</div>
									<div class="boxit-tit">Danilo Fazolo</div>
									<div class="boxit-esp">Especialista de eletrônicos</div>
									<div class="boxit-estrela">
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
									</div>
									<div class="boxit-nota">5,0</div>
								</a>
							</div>
							
							<div class="boxitcar">
								<a href="#">
									<div class="boxit-fot">
										<img src="assets/_img/excluir/5.jpg" alt="Favoritos"/>
										<div class="boxit-tipo">PREMIUM</div>
									</div>
									<div class="boxit-tit">Danilo Fazolo</div>
									<div class="boxit-esp">Especialista de eletrônicos</div>
									<div class="boxit-estrela">
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
									</div>
									<div class="boxit-nota">5,0</div>
								</a>
							</div>
							
							<div class="boxitcar">
								<a href="#">
									<div class="boxit-fot">
										<img src="assets/_img/excluir/6.jpg" alt="Favoritos"/>
										<div class="boxit-tipo">PREMIUM</div>
									</div>
									<div class="boxit-tit">Danilo Fazolo</div>
									<div class="boxit-esp">Especialista de eletrônicos</div>
									<div class="boxit-estrela">
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
									</div>
									<div class="boxit-nota">5,0</div>
								</a>
							</div>
							
							<div class="boxitcar">
								<a href="#">
									<div class="boxit-fot">
										<img src="assets/_img/excluir/1.jpg" alt="Favoritos"/>
										<div class="boxit-tipo">PREMIUM</div>
									</div>
									<div class="boxit-tit">Danilo Fazolo</div>
									<div class="boxit-esp">Especialista de eletrônicos</div>
									<div class="boxit-estrela">
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
									</div>
									<div class="boxit-nota">5,0</div>
								</a>
							</div>
							
						</div>

					</div>
				</div>
				
				
			</div>
		</div>
	
	
	
	
	
	
		<div class="bloco blackc">
			<div class="blprom">
				<div class="blg">
					<div class="titfxprom">Promoções</div>
					<div class="destvprom"><a href="promocoes">Visualizar todas</a></div>
				</div>
			</div>

	
			<div class="blg">
				<div class="bl-prom-slide">
					<div class="slider responsive-eventos">


						<div class="cx-promo">
							<div class="promo-fot" style="background-image: url(assets/_img/excluir/10.jpg)">
								<a href="#">
									<div class="promo-mask"></div>
									<div class="boxit-tipo">PREMIUM</div>
									<div class="promo-name">Silverio Barreto</div>
									<div class="promo-name-serv">Automação Comercial</div>
									<div class="boxit-estrela-promo">
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
									</div>
									<div class="boxit-promo-nota">3,2</div>
									<div class="boxit-promo-love"><i class="fas fa-heart estred"></i></div>
								</a>
							</div>
							<div class="promo-desc">70% <span class="promo-desconto">DESCONTO</span></div>
							<div class="bx-dad-tit-promo">
								<div class="promo-tit-serv">Formatação de computadores</div>
								<div class="promo-desc-serv">Desconto para serviços de ilustração para livros.  </div>
							</div>
						</div>



						<div class="cx-promo">
							<div class="promo-fot" style="background-image: url(assets/_img/excluir/11.jpg)">
								<a href="#">
									<div class="promo-mask"></div>
									<div class="boxit-tipo">PREMIUM</div>
									<div class="promo-name">Silverio Barreto</div>
									<div class="promo-name-serv">Automação Comercial</div>
									<div class="boxit-estrela-promo">
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
									</div>
									<div class="boxit-promo-nota">3,2</div>
									<div class="boxit-promo-love"><i class="fas fa-heart estred"></i></div>
								</a>
							</div>
							<div class="promo-desc">70% <span class="promo-desconto">DESCONTO</span></div>
							<div class="bx-dad-tit-promo">
								<div class="promo-tit-serv">Formatação de computadores</div>
								<div class="promo-desc-serv">Desconto para serviços de ilustração para livros.  </div>
							</div>
						</div>



						<div class="cx-promo">
							<div class="promo-fot" style="background-image: url(assets/_img/excluir/12.jpg)">
								<a href="#">
									<div class="promo-mask"></div>
									<div class="boxit-tipo">PREMIUM</div>
									<div class="promo-name">Silverio Barreto</div>
									<div class="promo-name-serv">Automação Comercial</div>
									<div class="boxit-estrela-promo">
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
									</div>
									<div class="boxit-promo-nota">3,2</div>
									<div class="boxit-promo-love"><i class="fas fa-heart estred"></i></div>
								</a>
							</div>
							<div class="promo-desc">70% <span class="promo-desconto">DESCONTO</span></div>
							<div class="bx-dad-tit-promo">
								<div class="promo-tit-serv">Formatação de computadores</div>
								<div class="promo-desc-serv">Desconto para serviços de ilustração para livros.  </div>
							</div>
						</div>



						<div class="cx-promo">
							<div class="promo-fot" style="background-image: url(assets/_img/excluir/10.jpg)">
								<a href="#">
									<div class="promo-mask"></div>
									<div class="boxit-tipo">PREMIUM</div>
									<div class="promo-name">Silverio Barreto</div>
									<div class="promo-name-serv">Automação Comercial</div>
									<div class="boxit-estrela-promo">
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star estyellow"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
									</div>
									<div class="boxit-promo-nota">3,2</div>
									<div class="boxit-promo-love"><i class="fas fa-heart estred"></i></div>
								</a>
							</div>
							<div class="promo-desc">70% <span class="promo-desconto">DESCONTO</span></div>
							<div class="bx-dad-tit-promo">
								<div class="promo-tit-serv">Formatação de computadores</div>
								<div class="promo-desc-serv">Desconto para serviços de ilustração para livros.  </div>
							</div>
						</div>



					</div>
				</div>
			</div>
		</div>
	
	
	

		<div class="blg">
			<div class="titfxdest">Mais Profissionais</div>
			<div class="subtititem">Conheça mais profissionais</div>
		</div>


	
	
	

		<div class="blg">
			<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
				<div class="boxit">
					<a href="#">
						<div class="boxit-fot">
							<img src="assets/_img/excluir/1.jpg" alt="Favoritos"/>
							<div class="boxit-tipo">PREMIUM</div>
						</div>
						<div class="boxit-tit">Danilo Fazolo</div>
						<div class="boxit-esp">Especialista de eletrônicos</div>
						<div class="boxit-estrela">
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
						</div>
						<div class="boxit-nota">5,0</div>
					</a>	
				</div>
			</div>


			<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
				<div class="boxit">
					<a href="#">
						<div class="boxit-fot"><img src="assets/_img/excluir/2.jpg" alt="Favoritos"/></div>
						<div class="boxit-tit">Jefferson Santos </div>
						<div class="boxit-esp">Técnico em manutenção de celulares</div>
						<div class="boxit-estrela">
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</div>
						<div class="boxit-nota">3,2</div>
					</a>
				</div>
			</div>


			<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
				<div class="boxit">
					<a href="#">
						<div class="boxit-fot"><img src="assets/_img/excluir/3.jpg" alt="Favoritos"/></div>
						<div class="boxit-tit">Magiclik Compras Coletivas</div>
						<div class="boxit-esp">Técnico PABX </div>
						<div class="boxit-estrela">
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</div>
						<div class="boxit-nota">3,2</div>
					</a>
				</div>
			</div>


			<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
				<div class="boxit">
					<a href="#">
						<div class="boxit-fot">
							<img src="assets/_img/excluir/4.jpg" alt="Favoritos"/>
							<div class="boxit-tipo">PREMIUM</div>
						</div>
						<div class="boxit-tit">Vitor Torres</div>
						<div class="boxit-esp">Técnico de informática</div>
						<div class="boxit-estrela">
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</div>
						<div class="boxit-nota">3,2</div>
					</a>
				</div>
			</div>


			<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
				<div class="boxit">
					<a href="#">
						<div class="boxit-fot"><img src="assets/_img/excluir/5.jpg" alt="Favoritos"/></div>
						<div class="boxit-tit">Shekinah Info&Cel Assistência Técnica</div>
						<div class="boxit-esp">Técnico em manutenção de celulares</div>
						<div class="boxit-estrela">
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star"></i>
						</div>
						<div class="boxit-nota">4,3</div>
					</a>
				</div>
			</div>


			<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
				<div class="boxit">
					<a href="#">
						<div class="boxit-fot"><img src="assets/_img/excluir/6.jpg" alt="Favoritos"/></div>
						<div class="boxit-tit">Magiclik Compras Coletivas</div>
						<div class="boxit-esp">Técnico PABX </div>
						<div class="boxit-estrela">
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</div>
						<div class="boxit-nota">2,5</div>
					</a>
				</div>
			</div>




			<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
				<div class="boxit">
					<a href="#">
						<div class="boxit-fot">
							<img src="assets/_img/excluir/1.jpg" alt="Favoritos"/>
							<div class="boxit-tipo">PREMIUM</div>
						</div>
						<div class="boxit-tit">Danilo Fazolo</div>
						<div class="boxit-esp">Especialista de eletrônicos</div>
						<div class="boxit-estrela">
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
						</div>
						<div class="boxit-nota">5,0</div>
					</a>	
				</div>
			</div>


			<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
				<div class="boxit">
					<a href="#">
						<div class="boxit-fot"><img src="assets/_img/excluir/2.jpg" alt="Favoritos"/></div>
						<div class="boxit-tit">Jefferson Santos </div>
						<div class="boxit-esp">Técnico em manutenção de celulares</div>
						<div class="boxit-estrela">
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</div>
						<div class="boxit-nota">3,2</div>
					</a>
				</div>
			</div>


			<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
				<div class="boxit">
					<a href="#">
						<div class="boxit-fot"><img src="assets/_img/excluir/3.jpg" alt="Favoritos"/></div>
						<div class="boxit-tit">Magiclik Compras Coletivas</div>
						<div class="boxit-esp">Técnico PABX </div>
						<div class="boxit-estrela">
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</div>
						<div class="boxit-nota">3,2</div>
					</a>
				</div>
			</div>


			<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
				<div class="boxit">
					<a href="#">
						<div class="boxit-fot">
							<img src="assets/_img/excluir/4.jpg" alt="Favoritos"/>
							<div class="boxit-tipo">PREMIUM</div>
						</div>
						<div class="boxit-tit">Vitor Torres</div>
						<div class="boxit-esp">Técnico de informática</div>
						<div class="boxit-estrela">
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</div>
						<div class="boxit-nota">3,2</div>
					</a>
				</div>
			</div>


			<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
				<div class="boxit">
					<a href="#">
						<div class="boxit-fot"><img src="assets/_img/excluir/5.jpg" alt="Favoritos"/></div>
						<div class="boxit-tit">Shekinah Info&Cel Assistência Técnica</div>
						<div class="boxit-esp">Técnico em manutenção de celulares</div>
						<div class="boxit-estrela">
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star"></i>
						</div>
						<div class="boxit-nota">4,3</div>
					</a>
				</div>
			</div>


			<div class="grdGRL-2 grdDSK-3 grdTBLp-4 grdSMPr-6 grdMOPr-12">
				<div class="boxit">
					<a href="#">
						<div class="boxit-fot"><img src="assets/_img/excluir/6.jpg" alt="Favoritos"/></div>
						<div class="boxit-tit">Magiclik Compras Coletivas</div>
						<div class="boxit-esp">Técnico PABX </div>
						<div class="boxit-estrela">
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star estyellow"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</div>
						<div class="boxit-nota">2,5</div>
					</a>
				</div>
			</div>
		</div>


	
	
	<div class="vazio"></div>
</div>