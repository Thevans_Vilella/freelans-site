<?php
session_start();
if(isset($_SESSION["login_ses"]) and ($_SESSION["status_ses"] == "1")) {
	
require_once("painel/conexao.php");
require_once("painel/classes.php");

$item			=	(int)$_POST['item'];
$clitem			=	(int)$_POST['clitem'];
$atendimento	=	(int)$_POST['atendimento'];
$desc			=	anti_injection_Desc($_POST['descricao']);
$url			=	anti_injection_Desc($_POST['dir']);
$data			=	date("Y-m-d");
$hora			=	date("H:i:s");

$media			=	$atendimento;
    
//===========================================================================================================================================================

	if ( $item == $clitem  )  { 

	echo "<script type='text/javascript'>alert('Por Favor, você não pode votar em você mesmo! Escolha outro prestador para votar', '$url');</script>";
	include "painel/destruidor.php";
	exit();

} 
	
//===========================================================================================================================================================

//===========================================================================================================================================================

	if ( $item == "" or $clitem == "" or $atendimento == "" or $desc == "" or $url == "" )  { 

	echo "<script type='text/javascript'>alert('Por Favor, Preencha todos os Campos!', '$url');</script>";
	include "painel/destruidor.php";
	exit();

} 
	
//===========================================================================================================================================================

$sqlverif	=	"select * from avaliacoes_usuario WHERE perf_codigo = '$clitem' AND perf_cod_ref = '$item'";
$resverif	=	mysqli_query($cn, $sqlverif);
$cntverif	=	mysqli_num_rows($resverif);
$linverif	=	mysqli_fetch_array($resverif);
	
	if($cntverif == 0){ // Se não tem voto deste usuário faz abaixo
		
		$sqlavauser	=	"insert into avaliacoes_usuario 
		
		(perf_codigo, perf_cod_ref, ava_user_atendimento, ava_user_media_estrelas, ava_user_data_created, ava_user_hora_created, ava_user_descricao)

		VALUES 
		('$clitem', '$item', '$atendimento', '$media', '$data', '$hora', '$desc')";

		mysqli_query($cn, $sqlavauser);
		
		
	} else { // Se já existe voto deste usuário faz abaixo
		
		$sqlalt		=	"UPDATE avaliacoes_usuario SET
		
		perf_codigo				= 	'$clitem',
		perf_cod_ref			= 	'$item',
		ava_user_atendimento	= 	'$atendimento',
		ava_user_media_estrelas	= 	'$media',
		ava_user_data_modifed	= 	'$data',
		ava_user_hora_modifed	= 	'$hora',
		ava_user_descricao		= 	'$desc'

		WHERE 
		perf_codigo = '$clitem' 
		AND 
		perf_cod_ref = '$item'";

		mysqli_query($cn, $sqlalt);
		
	} // Termina IF
	
//===========================================================================================================================================================
// Aqui verificamos quantos votos o prestador já teve.

$presatend		= 	0;	
$medgeral		= 	0;	
	
$sqlqtde		=	"select * from avaliacoes_usuario WHERE perf_cod_ref = '$item'";
$resqtde		=	mysqli_query($cn, $sqlqtde);
$cntqtde		=	mysqli_num_rows($resqtde);
while($linvqtde	=	mysqli_fetch_array($resqtde)) {
	
	// Aqui encontramos os valores no banco de dados e somamos
	$presatend	=	$presatend + $linvqtde['ava_user_atendimento'];
	$medgeral	=	$medgeral + $linvqtde['ava_user_media_estrelas'];
	
}

$qtdvotos	=	$cntqtde;
$medgeral	=	$medgeral  / $qtdvotos;
$presatend	=	$presatend / $qtdvotos;

$medgeral	=	(int)$medgeral;
$presatend	=	(int)$presatend;

//===========================================================================================================================================================

$sqlconf	=	"select * from avaliacoes WHERE perf_codigo = '$item'";
$resconf	=	mysqli_query($cn, $sqlconf);
$cntconf	=	mysqli_num_rows($resconf);
$linconf	=	mysqli_fetch_array($resconf);
	
//===========================================================================================================================================================
	
	if($cntconf == 0){ // Verifica se usuário já recebeu voto de pelo menos um usuário
		
		$sqlger	=	"insert into avaliacoes 
		(perf_codigo, ava_atendimento, ava_media_estrelas, ava_qtde_votos)

		VALUES 
		('$item', '$presatend', '$medgeral', '$qtdvotos')";

		mysqli_query($cn, $sqlger);
		
//===========================================================================================================================================================
	
	} else { // Se já teve votos atualizada a tabela de avaliações abaixo

//===========================================================================================================================================================

		$sqlvot		=	"UPDATE avaliacoes SET
		
		ava_atendimento		= 	'$presatend',
		ava_media_estrelas	= 	'$medgeral',
		ava_qtde_votos		= 	'$qtdvotos'

		WHERE perf_codigo 	=   '$item'";

		mysqli_query($cn, $sqlvot);

//===========================================================================================================================================================

} // Fecha IF de verificação se já recebeu votos aqui
		
//===========================================================================================================================================================
// Se a nota dada no voto for menor ou igual a 2
// Manda email avisando o administrador do sistema.
	
	if( $media <=2 ){

//===============================================================================================================
// Aqui envia email para administrador do sistema avisando a nota baixa
//===============================================================================================================


$sql11 = "SELECT * FROM metas_google";
$res11 = mysqli_query($cn, $sql11);
$lin11 = mysqli_fetch_array($res11);

$key	 = $lin11['tag_alt'];
$urlproj = $lin11['tag_url'];



$sql10 = "SELECT * 
			FROM contato
	  INNER JOIN estado 		
			  ON estado.est_codigo = contato.est_codigo
	  INNER JOIN logradouro 	
			  ON logradouro.log_codigo = contato.log_codigo";
$res10 = mysqli_query($cn, $sql10);
$lin10 = mysqli_fetch_array($res10);	

$fone  = substr($lin10['cont_telefone'], 0, 14);
$fone0 = substr($lin10['cont_telefone'], 1, 2);
$fone1 = substr($lin10['cont_telefone'], 0, 4);
$fone2 = substr($lin10['cont_telefone'], 5, 9);



$contnome  = $lin10['cont_nome'];   
$namestop  = "Freelans";  
$empfone   = $lin10['cont_telefone'];
$empemail  = $lin10['cont_email'];
$empcep    = $lin10['cont_cep'];
$emploca   = $lin10['cont_endereco'].", ".$lin10['cont_numero'];
$emplocal  = $lin10['log_sigla']." ".$lin10['cont_endereco'].", ".$lin10['cont_numero'];
$empbairro = $lin10['cont_bairro'];
$empcidade = $lin10['cont_cidade'];
$empestado = $lin10['est_nome'];
		
$maisdest  = $lin10['cont_email_formulario'];
//==================================================================================================================
		
	$sqluser	=	"select * from perfil where perf_codigo = $item";
	$resuser	=	mysqli_query($cn, $sqluser);
	$linuser	=	mysqli_fetch_array($resuser);	

	$pernamemail	=	$linuser['perf_nome'];
		
//===============================================================================================================
// Passando os dados obtidos pelo formulário para as variáveis abaixo
$nomeremetente     = $namestop;
$emailremetente    = $user_mail;
$emaildestinatario = $maisdest;
$titulo            = 'Aviso de voto baixo pelo site Freelans';
//===============================================================================================================

//===============================================================================================================
require_once("phpmailer/PHPMailerAutoload.php");
// Inicia a classe PHPMailer
//===============================================================================================================
$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               	// Enable verbose debug output

$mail->isSMTP();                                      	// Define que a mensagem será SMTP
//$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Especifique os servidores SMTP principal e backup
$mail->Host = $serv_sis;  								// Especifique os servidores SMTP principal e backup
$mail->SMTPAuth = true;                               	// Usa autenticação SMTP? (opcional)
$mail->Username = $user_sis;     						// Usuário do servidor SMTP - Mesmo email da linha 45
$mail->Password = $pass_sis;            				// Senha do servidor SMTP - Se não tiver de Serviço de SMTP Contrato colocar a senha do email que está sendo utilizada para envio.
$mail->SMTPSecure = $user_crip;                         // Ativar a criptografia TLS, `também ssl` aceita
$mail->Port = $user_porta;                              // Porta TCP para conectar-se

$mail->From = $emailremetente;					                      // Email do Remetente - Colocar email do servidor Locaweb (Site)
$mail->FromName = $nomeremetente;									  // Nome do Remetente - Colocar Seu Nome ou Nome de sua Empresa
$mail->addAddress($emaildestinatario, $pernamemail);         			  // Adicionar um destinatário - Nome é opcional
$mail->addReplyTo($user_mail_res, $nomeremetente);	  				  // Responder Para - Aqui define para quem vao a respósta do email
//$mail->addReplyTo('andre@galantidesign.com.br', 'André Galanti');	  // Responder Para
//$mail->AddCC('email@comcopia.com.br', 'Nome da Pessoa'); 			  // Com Cópia
//$mail->AddBCC($user_mail_res, $nomeremetente);     					  // Com Cópia Oculta



// Define os dados Técnicos da Mensagem
//===============================================================================================================
$mail->IsHTML(true); 								    // Define que o e-mail será¡ enviado como HTML
$mail->CharSet = 'utf-8'; 						        // Charset da mensagem (opcional)

//===============================================================================================================
// Aqui é a Mensagem do Contato que vai no Email - Ou seja o HTML da Mensagem
//===============================================================================================================
$txt = "

        <table width='550' border='0' style='background-color:#FFF;border:25px solid #E7E2DC' align='center' cellpadding='0' cellspacing='0'>
          <tr>
            <td colspan='3' valign='top' bgcolor='#F9F8F7'>
                <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align='center'><a href='$urlproj' target='_blank'><img src='$urlproj/assets/_img/logo.png' border='0' alt='$contnome'/></a></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                </table>
            </td>
          </tr>
          <tr>
            <td height='3' colspan='3' bgcolor='#005FA6'></td>
          </tr>
          <tr>
            <td height='40' colspan='3' bgcolor='#FFFFFF'>&nbsp;</td>
          </tr>
          <tr>
            <td width='5%' bgcolor='#FFFFFF'>&nbsp;</td>
            <td width='100%' bgcolor='#FFFFFF'>
                <table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>
                  <tr>
                    <td style='color:#000; font-size: 13px; font-family:Arial, Helvetica, sans-serif;'>
                        Olá, o prestador <span style='color:#FF0000; font-size: 14px;'>$pernamemail</span>, recebeu uma nota baixa em um voto.<br/><br/>
                        Acesse o stie e confira os votos recebidos por este prestador!!!<br/><br/>
                        Para acessar o site e ver os votos clique no link abaixo.<br/><br/><br/>
                        
						<table border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td>
								<a href='$urlproj/$url' style='text-decoration:none; color: #FFFFFF; display:block; font-size:15px; background:#005FA6; padding: 10px 15px 10px 15px;'>
								Acessar Site</a>
								</td>
                            </tr>
                        </table>
						
                    </td>
                  </tr>
                </table>
            </td>
            <td width='5%' bgcolor='#FFFFFF'>&nbsp;</td>
          </tr>
          <tr>
            <td height='40' colspan='3' bgcolor='#FFFFFF'>&nbsp;</td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#FFCA03' height='3'></td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#8BC53E' height='3'></td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#005FA6'>&nbsp;</td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#005FA6' align='center' style='color:#F9F9F9; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>
                Telefone: <a style='color:#F9F9F9; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$empfone</a><br/>
                Email: <a style='color:#F9F9F9; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$empemail</a><br/>
                $emplocal - $empbairro | CEP: <a style='color:#F9F9F9; font-size: 13px; font-style:normal; font-weight:normal; font-family:Tahoma, Geneva, sans-serif'>$empcep</a><br/>
                $empcidade - $empestado
            </td>
          </tr>
          <tr>
            <td colspan='3' bgcolor='#005FA6'>&nbsp;</td>
          </tr>
        </table>

";
//===============================================================================================================
// Define a mensagem (Texto e Assunto)
//===============================================================================================================
$mail->Subject = $titulo;				    // Assunto da mensagem
$mail->Body    = $txt;					    // Texto da mensagem se for HTML
$mail->AltBody = $txt;						// Texto da mensagem se Não for HTML
//$mail->Body = "Este Ã© o corpo da mensagem de teste, em <b>HTML</b>!  :)";
//$mail->AltBody = "Este Ã© o corpo da mensagem de teste, em Texto Plano! \r\n :)";


if(!$mail->send()) {
    echo 'NÃ£o foi possível enviar o e-mail!!!';
    echo 'Informações do erro: ' . $mail->ErrorInfo;
	
}
		

}
//===========================================================================================================================================================

echo "<script>window.location.href='$url';</script>";
include "painel/destruidor.php";

?>

<?php } else { include "alerta.php"; }// Termina IF de Login Aqui ============= ?>