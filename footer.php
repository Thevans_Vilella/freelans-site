<footer>
    <div class="container-fluid d-flex flex-wrap">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <a href="">
                <img src="assets/_img/icons/freelans-logo-footer.png" alt="Freelans">
            </a>
			
            <p class="menu-item-footer">©2019 Freelans. Todos os direitos reservados Freelans. 
                <!--<br><br> Termo de Uso &nbsp; | &nbsp; Política de Privacidade-->
            </p>
			
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <h1 class="footer-title">sobre</h1>
            <ul>
                <li class="menu-item-footer"><a href="quem-somos">Quem Somos</a></li>
                <li class="menu-item-footer"><a href="contato">Fale Conosco</a></li>
                <li class="menu-item-footer"><a href="blog/">Blog</a></li>
                <li class="menu-item-footer"><a href="register-account">Seja nosso parceiro</a></li>
                <li class="menu-item-footer"><a href="faq">Perguntas Frequentes</a></li>
            </ul>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <h1 class="footer-title">contratar</h1>
            <ul>
                <li class="menu-item-footer"><a href="como-contratar">Como contratar</a></li>
                <li class="menu-item-footer"><a href="profissionais">Encontre um profissional</a></li>
            </ul>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <h1 class="footer-title">trabalhar</h1>
            <ul>
                <li class="menu-item-footer"><a href="register-account">Quero ser um Prestador</a></li>
                <li class="menu-item-footer"><a href="register-account">Formalize-se</a></li>
            </ul>
        </div>
        <div class="d-flex flex-wrap col-lg-6 col-md-6 col-sm-6 col-12">
            <h1 class="footer-title">siga-nos</h1>
			<?php
			$sqlmid			=	"select * from midias order by mid_ordem";
			$resmid			=	mysqli_query($cn, $sqlmid);
			while($linmid	=	mysqli_fetch_array($resmid))  {
			?>
            <a target="_blank" class="social-media-container" href="<?php echo $linmid['mid_link']; ?>">
				<img src="painel/<?php echo $linmid['mid_foto']; ?>" alt="<?php echo $linmid['mid_nome']; ?>"/>
			</a>
			<?php } ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <a id="search-footer" href="profissionais"><img src="assets/_img/icons/search-icon.png" alt=""><span>Buscar por um profissional</span></a>
        </div>
    </div>
</footer>
