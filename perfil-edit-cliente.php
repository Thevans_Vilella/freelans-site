<?php  if(isset($_SESSION["login_ses"]) and ($_SESSION["status_ses"] == "1") and ($_SESSION["tipo_ses"] == "1")) {  ?>
<?php 

	$user	=	$_SESSION["login_ses"];

	$sqluser	=	"select * from perfil where perf_login = '$user'";
	$resuser	=	mysqli_query($cn, $sqluser);
	$linuser	=	mysqli_fetch_array($resuser);	

	$perfcod	=	$linuser['perf_codigo'];
?>


<div class="boxfull">
	<div class="titfxtop">Meu perfil: <span class="titfxtop-sub"><?php echo $linuser['perf_nome']; ?></span></div>

	
		<div class="boxfull cinza">
			<div class="blg perf-bloco">

				<div class="grdGRL-4 grdDSK-7 grdTBLp-12">
				<div class="bloco-perfil">
					<div class="perfil-cx-foto">
						<div class="perfil-foto-grande"  style="background-image: url(painel/<?php echo $linuser['perf_fotog']; ?>);"></div>
						<div class="perfil-foto-mask"></div>
						<div class="perfil-foto-mini"><img src="painel/<?php echo $linuser['perf_fotop']; ?>" alt="<?php echo $linuser['perf_nome']; ?>"/></div>
							<div class="icons-avaliacoes">
								<div class="perf-nome"><?php echo $linuser['perf_nome']; ?></div>
							</div>						
					</div>
				</div>
				</div>
				
				
				

				<div class="grdGRL-4 grdDSK-6 grdTBLp-12">
				<div class="bloco-perfil">
						<div class="bl-atua-bloco">
							<div class="bl-atua-tit">DADOS CADASTRAIS</div>
							<div class="bl-atua-linha"></div>

							<form name="form" class="cadastro" id="cadastro1" method="post" enctype="multipart/form-data"  action="perfil-edit-cliente-altera.php">
								<ul>
									<li>
										<div class="dad">
											<label>Foto do perfil: 460 x 330 pixel jpg.</label>
												<div class="dad-fot">
													<input type="file" id="upload" name="arquivo" onChange="this.form.submit()">
													<label class ="new-button" for="upload">Buscar Foto</label>
												</div>
										</div>
									</li>
									<li>
										<div class="dad">
											<label>Nome:</label>
											<input type="text" name="username" id="username" class="validate[required]" value="<?php echo $linuser['perf_nome']; ?>"/>
										</div>
									</li>
									<li>
										<div class="dad">
											<label>Email:</label>
											<input type="email" name="email" id="email" class="validate[required]" value="<?php echo $linuser['perf_email']; ?>"/>
										</div>
									</li>
									<li>
										<div class="dad">
											<label>Telefone:</label>
											<input type="tel" name="fone" id="fone" onkeyup="maskIt(this,event,'(##) ####-#####')" maxlength="14" class="validate[required]" value="<?php echo $linuser['perf_telefone']; ?>"/>
										</div>
									</li>
									<li>
										<div class="dad">
											<label>Celular:</label>
											<input type="tel" name="celular" id="celular" onkeyup="maskIt(this,event,'(##) ####-#####')" maxlength="15" class="validate[required]" value="<?php echo $linuser['perf_celular']; ?>"/>
										</div>
									</li>
								</ul>

								<input type="submit" class="regdados" value="Alterar Dados" />
								<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
							</form>
						</div>
				</div>
				</div>
				
				

				<div class="grdGRL-4 grdDSK-6 grdTBLp-12">
				<div class="bloco-perfil">
						<div class="bl-atua-bloco">
							<div class="bl-atua-tit">MUDAR PARA PERFIL PRESTADOR</div>
							<div class="bl-atua-linha"></div>
							<div class="bl-atua-tit-alert">Ao mudar para prerfil prestador, é necessário fazer login novamente no sistema!</div>
							<div class="bl-atua-linha"></div>

							<form class="cadastro" method="post" action="perfil-edit-cliente-plano.php">
								<ul>
									<li>
										<div class="dad">
											<label>Selecione a cidade para se cadastrar:</label>
											<select name="city" class="select" id="city">
												<option value="">Selecione uma Cidade</option>
												<option value="">----------------------------</option>
												<?php
												$sql	=	"select * from cidades
															INNER JOIN estado	  	on		estado.est_codigo		=	cidades.est_codigo
															order by est_nome, cid_nome";
												$res	=	mysqli_query($cn, $sql);
												while($lin	=	mysqli_fetch_array($res))  {
												?>
												<option value="<?php echo $lin['cid_codigo']; ?>"><?php echo $lin['cid_nome']; ?>/ <?php echo $lin['est_sigla']; ?></option>
												<?php } ?>
											</select>
										</div>
									</li>
									<li>
										<div class="dad">
											<label>Selecione a Categoria:</label>
											<select name="category" class="select" id="category">
												<option value="">Selecione uma Categoria</option>
												<option value="">----------------------------</option>
												<?php
												$sql01	=	"select * from categorias order by cat_ordem";
												$res01	=	mysqli_query($cn, $sql01);
												while($lin01	=	mysqli_fetch_array($res01))  {
												?>
												<option value="<?php echo $lin01['cat_codigo']; ?>"><?php echo $lin01['cat_nome']; ?></option>
												<?php } ?>
											</select>
										</div>
									</li>
									<li>
										<div class="dad">
											<label>Selecione o plano:</label>
											<select name="plans" class="select" id="plans">
												<option value="">Selecione um Plano</option>
												<option value="">----------------------------</option>
												<?php
												$sql02	=	"select * from planos order by pla_ordem";
												$res02	=	mysqli_query($cn, $sql02);
												while($lin02	=	mysqli_fetch_array($res02))  {
												?>
												<option value="<?php echo $lin02['pla_codigo']; ?>">
													<?php echo $lin02['pla_nome']; ?> - R$<?php echo CorrigirDinheiro($lin02['pla_valor']); ?>/<?php echo $lin02['pla_titulo']; ?>
												</option>
												<?php } ?>
											</select>
										</div>
									</li>
								</ul>

								<input type="submit" class="regdados" value="Alterar Plano" />
								<input type="hidden" name="user" size="5" value="<?php echo $perfcod; ?>" />
							</form>
						</div>
					
				</div>
				</div>
				
				
				
			</div>
			<div class="vazio"></div>	
		</div>

</div>
<?php } else { require_once("alerta.php");}// Termina IF de Login Aqui ============= ?>